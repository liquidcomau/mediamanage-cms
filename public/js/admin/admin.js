$(document).on('click', '.nav-primary a', function (e) {
	var $this = $(e.target), $active;      
	$this.is('a') || ($this = $this.closest('a'));
	if( $('.nav-vertical').length ){
		return;
	}
	  
	$active = $this.parent().siblings( ".active" );
	$active && $active.find('> a').toggleClass('active') && $active.toggleClass('active').find('> ul:visible').slideUp(200);
	  
	($this.hasClass('active') && $this.next().slideUp(200)) || $this.next().slideDown(200);
	$this.toggleClass('active').parent().toggleClass('active');
	  
	$this.next().is('ul') && e.preventDefault();
});

$(document).on('click', '[data-toggle^="class"]', function(e){
	e && e.preventDefault();
	var $this = $(e.target), $class , $target, $tmp, $classes, $targets;
	!$this.data('toggle') && ($this = $this.closest('[data-toggle^="class"]'));
	$class = $this.data()['toggle'];
	$target = $this.data('target') || $this.attr('href');
	$class && ($tmp = $class.split(':')[1]) && ($classes = $tmp.split(','));
	$target && ($targets = $target.split(','));
	$targets && $targets.length && $.each($targets, function( index, value ) {
		($targets[index] !='#') && $($targets[index]).toggleClass($classes[index]);
	});
	$this.toggleClass('active');
});