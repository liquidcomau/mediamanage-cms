(function(mmng, $) {
	var notify = function() {
		
		var mmNotification = {
			
			getTemplate: function() {
				var template = ['<div class="mmNotification" style="display:none">'];
				template.push('<header class="mmnSuccess"><span class="mmNotifyDismiss pull-right"><i class="fa fa-times"></i></span><span class="mmNotify-headerMessage">Success</span></header>');
				template.push('<div class="mmNotification-body"></div></div>');
				
				return $(template.join(''));
			},
			
			newNotification: function(note) {
				var id = new Date().getTime();
				
				var markup = this.getTemplate();
				markup.find('.mmNotification-body').html(note.body);
				markup.attr('id', id);
				
				if (note.response) {
					
					var header = markup.find('header'); 
					header.removeClass('mmnSuccess').addClass('mmn' + note.response.charAt(0).toUpperCase() + note.response.slice(1));
				
					if (!note.title) {
						
						var titleArea = markup.find('.mmNotify-headerMessage');
						
						switch (note.response) {
							case 'success':
								titleArea.text('Success');
								break;
							case 'fail': 
								titleArea.text('Error');
								break;
							default:
								titleArea.text('Success');
								break;	
						}
						
					}
					
				}
				
				return {
					id: id,
					markup: markup,
					length: note.length
				};
			}
				
		};
		
		var queueTemplate = $('<section id="mmNotifyQueue"></section>');
		
		var queue = {
	
			maxLength: 3,
			defaultLength: 2000,
			queue: [],
			
			appendTo: $(document.body),
			
			_init: function(args) {
				
				for (var i in args) {
					if (this.hasOwnProperty(i)) this[i] = args[i];
				}
				
				queueTemplate.appendTo(this.appendTo);
				
				return this;
				
			},
			
			add: function(config) {
				
				if (!config.body) return;
				if (!config.length) config.length = this.defaultLength;
				
				var note = mmNotification.newNotification(config);
				
				this.queue.push(note);
				
				if (this.queue.length > this.maxLength) this.dismiss(this.queue[0]);
				
				this.registerDismissal(note);
				this.draw();
			},
			
			registerDismissal: function(notification) {
				var that = this;
				
				window.setTimeout(function(){
					if (document.getElementById(notification.id)) that.dismiss(notification);
				}, notification.length);
				
				$(document).on('click', '#' + notification.id, function() {
					that.dismiss(notification);
				});
			},
			
			dismiss: function(notification) {
				$('#' + notification.id).fadeOut();
				
				var index = this.queue.indexOf(notification);
				this.queue.splice(index, 1);
			},
			
			draw: function() {
				for (var i = 0; i < this.queue.length; i++) {
					var note = $(this.queue[i].markup).appendTo('#mmNotifyQueue');
					note.fadeIn(300);
				}
			}, 
			
		};
		
		return {
			init: function(args) {
				return queue._init(args);
			}
		};
		
	};
	
	mmng.notify = notify().init({appendTo: $('#mmNotifyArea')});

})(mmng, jQuery);