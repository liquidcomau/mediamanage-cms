window.ParsleyValidator
  	.addValidator('urlpath', function (value) {
  		var pattern = /^([#a-zA-Z0-9]|[a-zA-Z0-9\-_\/])+$/i;
  		var matches = value.match(pattern);
  		
  		return (matches && value.trim() !== '') ? true : false;
  	}, 32)
  	.addMessage('en', 'urlpath', 'Not a valid URL path');