'use strict';

(function() {
	
	var mmng = window.mmng = {};

	mmng.util = {

		ucFirst: function(string) 
		{
			return string.charAt(0).toUpperCase() + string.slice(1);
		},
		
		toTitleCase: function(str)
		{
		    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		},
		
		tag: function(t) 
		{
			return {
				open: '<' + t + '>',
				close: '</' + t + '>'
			};
		},
		
		// finds an object in an array of objects by key: value.
		// doesn't test for strict equality.
		
		find: function(arr, key, val) 
		{
			var len = arr.length,
				obj;
			for (var i = 0; i < len; i++) {
				obj = arr[i];
				if (obj[key] == val) return obj;
			}
		},
		
		getSearchParams: function(string) 
		{
		    var string = string ? string.replace(/^.+\?/, '') : window.location.search.slice(1);
		    var parts = string.split('&');

		    if (!string) return {}; // return empty object if no search string
		    
		    var params = {};
		    
		    parts.forEach(function(part) {
			var split = part.split('=');
			params[split[0]] = split[1];
		    });
		    
		    return params;
		},
		
		updateQueryStringParams: function(key, value, uri)
		{
		    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
		    if (uri.match(re)) {
			return uri.replace(re, "$1" + key + "=" + value + "$2");
		    } else {
			var separator = uri.indexOf("?") !== -1 ? "&" : "?",
				hash = "";
			
			if (uri.indexOf("#") !== -1) {
			    hash = uri.replace(/.*#/, "#");
			    uri = uri.replace(/#.*/, "");
			}
			return uri + separator + key + "=" + value + hash;
		    }
		},
		
		buildSearchString: function(params) 
		{
		    var str = [];
		    for (var k in params) {
			str.push([k + '=' + params[k]]);
		    }
		    
		    return '?' + str.join('&');
		},
		
		/*
		* Helps map a string to a reference in a base object.
		* 
		* E.g. "data" would map to <base>.data
		*		"data.image" would map to <base>.data.image
		*		
		* If any of the objects are undefined function will return False.
		* 
		* Returns a "mock pointer" object with get/set methods that reference the base object.
		* 
		* @param {string} name
		* @param {object} base. The base object reference.
		*/
	   stringToObjReference: function(name, base) {
		   base = base || {};

		   var parts = name.split(".");

		   // this reduce (and method) is now ugly. now has a side effect of generating 'parent' var. consider rewriting.
		   var parent = base;
		   var ref = parts.reduce(function(reference, part, i) {
			   if (reference === false) {
				   return false;
			   }
			   var newRef = reference[part];

			   if (typeof newRef === "undefined") {
				   return false;
			   } else {
				   parent = reference;
				   return newRef;
			   }
		   }, base);

		   if (ref === false) {
			   return ref;
		   }

		   // bind variables to return object.
		   return (function(parentObject, propertyKey) {
			   return {
				   get: function() {
					   return parentObject[propertyKey];
				   },
				   set: function(value) {
					   parentObject[propertyKey] = value;
				   }
			   };
		   })(parent, parts.reverse()[0]);
	   }
	};

	// place to store project editable config information

	mmng.conf = {
		tinymce: {}	
	};
	
	// holds information such as db ID for restful services,
	// user role for permissions, etc

	mmng._activeUser = null;

	mmng.getActiveUser = function() {
		return this._activeUser;
	};

	mmng.setActiveUser = function(conf) {
		this._activeUser = conf;
		return this;
	};
	
	// stores time difference in hours i.e. 3, -2, etc
	
	mmng.serverTime = null;
	
	mmng.prePageInit = function() {};
	
	mmng._currentPage = {
		init: function() {},
		model: {}
	};
	
	mmng.postPageInit = function() {};

	mmng.data = {};
	
	mmng.getCurrentPage = function() {
		return this._currentPage;
	};

	mmng.setCurrentPage = function(pg) {
		this._currentPage = pg;
		
		var event = new CustomEvent('pageDidSet');
		document.dispatchEvent(event);
		
		return this;
	};

	// automatically configures obj keys

	mmng.confIgnoredProps = ['util', 'constructors'];
	
	mmng.configure = function(conf) {
		var setName;

		for (var k in conf) {

			if (k.slice(0,3) === 'get' ||
				k.slice(0,3) === 'set' ||
				(typeof this.confIgnoredProps !== 'undefined' && this.confIgnoredProps.indexOf(k) !== -1)) continue;

			if (typeof this[setName = 'set' + mmng.util.ucFirst(k)] !== 'undefined') {
				this[setName](conf[k]);
			} else if (typeof this[k] !== 'undefined' && k.charAt(0) !== '_') {
				this[k] = conf[k];
			}
		}
	};

	mmng.preInit = function() {};
	mmng.postInit = function() {};
	
	mmng.initMmng = function() {
		this.preInit();
		this.initPage();
		this.postInit();
	};
	
	mmng.initPage = function() {
		this.prePageInit();
		this._currentPage.init();
		this.postPageInit();
	};
	
	mmng.destroyCurrentPage = function(includeHooks) {
		this._currentPage = {
			init: function() {}
		};
		
		if (includeHooks !== false) {
			this.prePageInit = function() {};
			this.postPageInit = function() {};
		}
	};

})();
