(function($) {
	mmng.Table = function(selector) {
		this.init(selector);
	};
	
	var tbls = {};
	mmng.getCurrentPage().tables = {};
	
	var tableP = mmng.Table.prototype;
	
	tableP.init = function(selector) {
		$(selector + ' td.bool').each(function() {
			var val = $(this).html().trim();
			if (parseInt(val) === 1) $(this).html('True');
			if (parseInt(val) === 0) $(this).html('False');
		});
		
		tbls[selector] = mmng.getCurrentPage().tables[selector] = this.datatables.init(selector);
		this.initHandlers(selector);
	};
	
	tableP.initHandlers = function(selector) {
		var that = this;
		
		// Datatables
		
		$(selector).find('.theadFilters input').keyup(function() {
			tbls[selector].fnFilter(this.value, $(selector).find('.theadFilters input').index(this) + 1);
		});
		
		$(selector).on("click", '.deleteItem', function() {
			
			var deleteBtn = $(this),
			row = $(this).parents('tr');
			
			mmng.confirm({
				title: 'Delete',
				body: 'Do you wish to delete this item?'
			}, function() {
				var parent = $(".tab-pane").length ? row.parents(".tab-pane") : row.parents(".panel-full");
				var selector = "#" + parent.attr("id") + " .datatable";

				var req = $.ajax({
					type: "delete",
					url: deleteBtn.attr('data-endpoint'),
					dataType: 'json'
				});
				
				req.done(function(data) {
					mmng.notify.add({body: 'Item successfully deleted', response: 'success', length: 4500});
					tbls[selector].fnDeleteRow(row.get(0));
				});
				
				req.fail(function(err) {
					mmng.notify.add({body: 'Could not delete', response: 'fail', length: 4500});
					console.log(err);
				});
			});
		});
		
		/*
		// Bulk Actions
		
		$('#bulkApplyBtn').click(function() {
			
			var btn = $(this);
			
			var action = $('.bulkActions select').val();
			
			var list = document.querySelectorAll('tbody input[type="checkbox"]');
			
			var resourceList = [];
			for (var i = 0; i < list.length; i++) {
				if (list[i].checked) {
					resourceList.push(list[i].getAttribute('data-id'));
				}
			}
			
			if (action == '0' || !resourceList.length) return;
			
			switch (action) {
				case 'delete':
					mmInt.confirm({
						title: 'Confirm',
						body: 'Are you sure you want to delete these resources? Deleting a resource will also permanently delete all child items e.g. deleting a conversation will also remove all related comments and associated data.'
					}, function() {
						btn.append(' <i class="fa fa-rotate-right"></i>').attr('disabled', 'disabled');
						that.bulk.deleteResources(resourceList);
					});
					
					break;
				case 'bulkTag':
					that.bulk.tags.revealModal(resourceList);
					break;
				case 'export':
					break;
			}
		});
		*/
		
		$(document).on('mouseover', '.tooltipCell', function() {
			$(this).tooltip();
		});
		
		$(document).on('change', '.tblFooter select', function() {
			localStorage.iDisplayLength = $(this).val();
		});
		
	};
	
	tableP.datatables = {
		init: function(selector) {
			
			// Datatable sorting / rendering custom functions (eg for timestamp columns)
			
			function timestampRender(source, type, val) {
				if (type === 'display' || type === 'filter') {
					
					if (source && source != 0) {
						var date = new Date(parseInt(source));
		
						var timeArr = [date.getHours(), ( date.getMinutes() < 10 ? '0' : '') + date.getMinutes()];
						var dateArr = [date.getDate(), date.getMonth() + 1, date.getFullYear()];
						
						return [timeArr.join(':'), dateArr.join('/')].join(' ');
						
					} else {
						return '&mdash;';
					}
					
				} else if (type === 'sort') {
					return source;
				}
				
				return '';
			}
			
			var timestampCache = Array.prototype.slice.call(document.querySelectorAll('tbody tr td.createdDateCell')).map(function(td) {
				return td.attributes['data-timestamp'].value * 1000;
			});
			
			// Custom range filtering
			
			if ($('.dateInbetween').length) {
				
				$.fn.dataTableExt.afnFiltering.push(
					function( oSettings, aData, iDataIndex ) {
		
						var minParts = document.querySelector('.createdDateStart').value.split('/');
						var maxParts = document.querySelector('.createdDateEnd').value.split('/');
		
						if (minParts.length === 3) var minStamp = (new Date(parseInt(minParts[2]), parseInt(minParts[1]) - 1, parseInt(minParts[0]))).getTime();
						if (maxParts.length === 3) var maxStamp = (new Date(parseInt(maxParts[2]), parseInt(maxParts[1]) - 1, parseInt(maxParts[0]))).getTime();
						
						var thisTime = timestampCache[iDataIndex];
						
						if ( minStamp && thisTime <= minStamp) {
							return false;
						}
						
						if ( maxStamp && thisTime >= maxStamp) {
							return false;
						}
		
						return true;
						
					}
				);
			
			}
			
			if (typeof localStorage.iDisplayLength === "undefined") {
				localStorage.iDisplayLength = 25;
			}
			
			// Datatable instantiation
			
			var dtConf = {
				"sPaginationType": "full_numbers",
				"sDom": 't<"row tblFooter padder"<"col-md-3"><"col-md-5 midCol"li><"col-md-4"p>>',
				"aoColumnDefs": [{
							"bSortable": false,
							"aTargets": ["nosort"],
						}, {
							"aTargets": ['timestampCol'],
							'asSorting': ['desc', 'asc'],
							"mRender": timestampRender,
						}
					],
				"aaSorting": [],
				"fnDrawCallback": function() {
					$("[data-toggle=popover]").popover();
				},
				"bAutoWidth": false,
				"iDisplayLength": localStorage.iDisplayLength
			};
			
			var th, ind, elList;
			
			if ($('.createdDateCol').length) {
				th = document.querySelector('.createdDateCol');
				Array.prototype.push.apply(elList = [], document.querySelector('.tHeadings').children);
				
				dtConf.aaSorting.push([elList.indexOf(th), 'desc']);
			}
			
			if ($('.lastModifiedCol').length) {
				th = document.querySelector('.lastModifiedCol');
				Array.prototype.push.apply(elList = [], document.querySelector('.tHeadings').children);
				
				dtConf.aaSorting.push([elList.indexOf(th), 'desc']);
			}
			
			var returnVal = $(selector).dataTable(dtConf);
			/*
			var bulkActions = [];
			
				bulkActions.push('<div class="bulkActions">');
				bulkActions.push('<select class="form-control inline">');
				bulkActions.push('<option value="0">Bulk action</option>');
				bulkActions.push('<option value="delete">Delete selected</option>');
				
				if ($('.commentCell').length) bulkActions.push('<option value="bulkTag">Bulk Tag</option>');
		
				bulkActions.push('</select>');
				bulkActions.push('<button class="btn btn-sm btn-default" id="bulkApplyBtn">Apply</button>');
				bulkActions.push('</div>');
			
			$('.tblFooter > div:first-child').html(bulkActions.join(''));
			*/
			return returnVal;
		}
	};
	
	var event = new CustomEvent('mmng:tablesReady', {
		details: {}  // can add further details here such as type of form etc
	});

	document.dispatchEvent(event);
})(jQuery);
