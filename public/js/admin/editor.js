(function() {
	
	"use strict";
	
	var selected = "";
	
	$( '#dl-menu' ).dlmenu({
		animationClasses : {
			classin : 'dl-animate-in-2',
			classout : 'dl-animate-out-2'
		}
	});
	
	$( '#dl-menu' ).dlmenu('openMenu');
	
	var crumbs = ["Shortcodes"];
	$('a[data-editor-bc]').click(function() {
		crumbs.push($(this).attr("data-editor-bc"));
		refreshCrumbs();
	});
	
	$('li.dl-back').click(function() {
		crumbs.pop();
		refreshCrumbs();
	});
	
	function refreshCrumbs()
	{
		$(".editor-shortcode-breadcrumbs").text(crumbs.join(" > "));
	}
	
	$("a[data-identifier]").click(function() {
		selected = $(this).attr("data-identifier");
		$(".editor-shortcode-preview").text(selected);
	});
	
	if (window.top.tinymce) {
		var editor = window.top.tinymce.activeEditor;
		editor.windowManager.windows[0].settings.callbacks.submit = function() {

			editor.insertContent(selected);
			editor.windowManager.windows[0].close();

		};
	}
})();