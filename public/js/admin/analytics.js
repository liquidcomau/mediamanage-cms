(function(aData) {
	
	if (!aData) return;
	
	var params = {};
	
	if (window.location.search) {
		var qString = window.location.search.slice(1);
		
		qString.split('&').forEach(function(part) {
			params[part.split('=')[0]] = part.split('=')[1];
		});
	}
	
	params.range = params.range || 'month';
	
	$('.range-' + params.range).removeClass('btn-default').addClass('btn-info').addClass('active');
	
	
	// Tables
	// -----------------------------------------
	
	var pgTbl = $('.pageVisits'),
		kwTbl = $('.keywordVisits');
	
	aData.pages.forEach(function(page) {
		pgTbl.find('tbody').append(['<tr>',
		                           '<td><a href="' + page.path + '" target="_blank">' + page.path + '</a></td>',
		                           '<td style="text-align:right">' + page.views + '</td>',
		                           '</tr>'].join(''));
	});
	
	aData.keywords.forEach(function(kw) {
		kwTbl.find('tbody').append(['<tr>',
		                           '<td>' + kw.keyword + '</td>',
		                           '<td style="text-align:right">' + kw.visits + '</td>',
		                           '</tr>'].join(''));
	});
	
	
	// LineGraph
	// ----------------------------------------
	
	var startDate = moment(new Date()).subtract(params.range + 's', 1).unix() * 1000;

	$('#visitChartArea').highcharts({
		chart: {
			type: 'line',
		},
		credits: {
			enabled: false
		},
		title: {
            text: 'Traffic Sources',
            style: {display: 'none'}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: false
				},
				showInLegend: false
			}
		},
		series: [{
			type: 'line',
			data: aData.visits.split(',').map(function(val) {
				return parseInt(val);
			}),
			pointInterval: 24 * 3600 * 1000, // one day
			pointStart: startDate
		}],
		tooltip: {
			formatter: function() {
  			    var xDate = moment(this.x).format('MMM Do');
        	    return xDate + '<br/><span style=\'color: blue;\'>Visits: </span><strong>'+ Highcharts.numberFormat(this.y, 0, null, '</strong>');
		    }
		},
		xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
            	day: '%b %e'
            }
        },
        yAxis: {
           title: {
              text: 'Visits'
           },
           //formatter: function() {
             //  return Highcharts.dateFormat('%a %d %b', this.value);
           //}
        },
	});
	
	
	// PieChart
	// ----------------------------------------
	
	if ($('#srcChartArea').length) {
		$('#srcChartArea').highcharts({
			chart: {
				type: 'pie',
				margin: [20, 220, 20, 0],
				spacingTop: 0
			},
			credits: {
				enabled: false
			},
			title: {
	            text: 'Traffic Sources',
			},
			plotOptions: {
				pie: {
					dataLabels: {
						enabled: false
					},
					showInLegend: true
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'top',
				x: 0,
				y: 120
			},
			series: [{
				type: 'pie',
				data: JSON.parse(aData.sources)
			}],
			tooltip: {
				formatter: function() {
	        	    return this.point.name + '<br/><span style=\'color: blue;\'>Visits: </span><strong>'+ Highcharts.numberFormat(this.y, 0, null, '</strong>');
			    }
			},
		});
	}
	
	
})(mmng.getCurrentPage().model.data);