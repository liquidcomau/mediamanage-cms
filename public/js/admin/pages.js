'use strict';
(function() {
	
	var page = mmng.getCurrentPage();
	
	if ($.pjax.defaults) { // ie < 10
		$.pjax.defaults.timeout = 3500;
	}
	/**
	 * Node types and associated information
	 *  
	 * @returns array
	 */
	var treeObjects = page.model.treeObjects;

	function fullText(node) 
	{
		var text = node.text;
		if (typeof node.data.published !== "undefined" && !Number(node.data.published)) {
			text += ' &nbsp;<small><span style="font-style:italic;">(Unpublished)</span></small>';
		}
		
		return text;
	}

	/**
	 * Converts an object with a structure like from the api to a node readable by jsTree
	 */
	function objToNode(node) {
		
		node.data.resource = {
			type: node.data.type.replace('_', '-'),
			formUrl: '/admin/pages/edit/' + node.data.type.replace('_', '-') + '/' + node.id
		};

		delete node.data.type;
		
		node.parent = node.parent === "0" ? "#" : node.parent; //jstree requires '#' be the root parent, we use '0' in the db
		
		switch (node.data.resource.type) {
			default:
				node.data.resource.endpoint = '/ajax/api/' + node.data.resource.type + 's';
				node['icon'] = treeObjects[node.data.resource.type].icon;
		}
		
		node.text = fullText(node);
		
		return node;
	}
	
	page.model.tree = page.model.tree.map(function(pg) {
		return objToNode(pg);
	});
	
	var tree = $('.treeMenuBody').jstree({
		core: {
			check_callback: true,
			data: page.model.tree,
			multiple: false
		},
		plugins: [ 'dnd', 'wholerow', 'contextmenu', 'search'],
		contextmenu: {
        	items: customMenu,
        	select_node: false,
        	show_at_node: false
        },
        search: {
        	show_only_matches: true
        }
	});
	var jstree = $('.treeMenuBody').jstree(); // make reference to instance available

	// system select flag true selects node without pjax load
	var system_select = false;
	tree.on('select_node.jstree', function(e, obj) {
		if (!system_select) {
			if (typeof mmng.getCurrentPage().form.lastSavedSnapshot !== 'undefined' && 
				mmng.getCurrentPage().form.lastSavedSnapshot !== mmng.getCurrentPage().form.snapshot())
			{
				var confirm = window.confirm('There are unsaved changes! Leaving this page without saving will remove all changes since last save. Continue?');
			}
			
			if (typeof confirm === 'undefined' || confirm === true) { 
				$.pjax({url: obj.node.data.resource.formUrl, container: '#formContainer'});
				page.model.selected = mmng.util.find(page.model.tree, 'id', obj.node.id);
			} else {
				system_select = true;
				
				jstree.deselect_all();
				jstree.select_node(page.model.selected.id);
				
				system_select = false;
			}
		}
	});
	
	// Using request_move flag to detect if a move is currently being requested.
	// Only sends a new xhr if request_move is equal to false.
	
	var request_move = false;
	tree.on('move_node.jstree', function (e, obj) {
		if (!request_move) {
		
			var id = obj.node.id;
			var oldPos = obj.old_position;
			var oldPar = obj.old_parent;
			
			var pl = request_move = {
				id: obj.node.id,
				parentId: (obj.parent === '#' ? 0 : obj.parent),
				oldParentId: (obj.old_parent === '#' ? 0 : obj.old_parent),
				position: obj.position,
				oldPosition: obj.old_position
			};
			
			var req = $.ajax({
				url: '/ajax/api/tree/' + obj.node.id,
				type: 'put',
				data: JSON.stringify(pl),
				dataType: 'json',
				contentType: 'json'
			});
			
			req.done(function(data) {
				mmng.notify.add({body: 'Update successful.', response: 'success', length: 4500});
				request_move = false;
			});
			
			req.fail(function(err) {
				mmng.notify.add({body: 'Could not update position.', response: 'fail', length: 4500});

				// jstree seems to count the node in its old position when moving it...
				// when moving forward have to add an extra count for the ghost position
				// if in the same parent where it used to be...
				
				if (request_move.position < request_move.oldPosition && request_move.parent === request_move.oldParent) oldPos += 1;
				jstree.move_node(id, oldPar, oldPos);
				
				request_move = false;
			});
		
		}
	});
	
	function fullUrl(node)
	{
		// remove root "#" from parent tree
		var parents = node.parents.filter(function(parent) {
			return parent !== "#";
		});

		// map actual objects to parent ids
		parents = parents.map(function(id) {
			return page.model.tree.reduce(function(carry, obj) {
				return carry.id === id ? carry : obj;
			}, page.model.tree[0]);
		});

		var urlParts = parents.reduce(function(urlParts, obj) {
			if (typeof obj.data.url_part !== "undefined") {
				urlParts.push(obj.data.url_part);
			}

			return urlParts;
		}, [node.data.url_part]);

		var url = urlParts.reverse().join("/");

		if (url[0] !== "/") {
			url = "/" + url;
		}
		
		return url;
	}
	
	/**
	 * Retuns a customised context menu for the node selected
	 * 
	 * @param jsTree_item node 
	 */
	function customMenu(node) {
	    // The default set of all items
		var createdSubmenu = createSubmenu(node);
	    var items = {
			view: {
				label: "View",
				icon: "/images/admin/icons/application_go.png",
				action: function() {
					var url = fullUrl(node);
					window.open(url,'_blank');
				}
			},
			preview: {
				label: "Preview",
				icon: "/images/admin/icons/eye.png",
				action: function() {
					var url = fullUrl(node);
					url = url + "?cms_preview=true";
					
					window.open(url,'_blank');
				}
			},
	    	createItem: {
				separator_before: true,
	            label: "Create",
	            icon: "/images/admin/icons/add.png",
	            action: function () {
	            	//console.log(node);
	            },
	            submenu: createdSubmenu            
	        },
	        deleteItem: { // The "delete" menu item
	            label: "Delete",
	            icon : "/images/admin/icons/cross.png",
	            action: function (e) {
	            	var isSelected = false;
					var label = e.reference.text();
	            	mmng.confirm({
	    				title: 'Confirm',
	    				body: 'Are you sure you want to delete "' + label + '"?'
	    			}, function() {
	    				isSelected = jstree.is_selected(node.id);
	    				
	    				$.ajax({
	    					type: "DELETE",
	    					url: treeObjects[node.data.resource.type].endpoint + "/" + node.id,
	    					dataType: "json",
	    					contentType: "json"
	    				})
	    				.done(function(data) {
	    					$.pjax({url: '/admin/pages', container: '#formContainer'});

	    					if (isSelected) {
		    					if (node.parent === '#') {
		    						jstree.deselect_all();
		    					} else {
		    						jstree.select_node(node.parent);
		    					}
	    					}

	    					jstree.delete_node(node.id);
	    					mmng.notify.add({body: 'Deleted successfully.', response: 'success', length: 4500});
	    				})
	    				.fail(function(err) {
	    					mmng.notify.add({body: 'Error deleting.', response: 'fail', length: 4500});			
	    				});
	    			});
	            }
	        }
	    };
	 
	    // filter out menu options where applicable
	    if (node.children.length > 0) {
	        delete items.deleteItem;
	    }
	    
	    var type = treeObjects[node.data.resource.type];

		if (typeof type.delete !== 'undefined' && type.delete.indexOf(mmng.getActiveUser().role) === -1) {
	    	delete items.deleteItem;
	    }
		
		if (!type.view) {
			delete items.view;
		}
		
		if (!type.preview) {
			delete items.preview;
		}
	    
	    return items;
	}
	
	/**
	 * Retuns a context submenu for the create action
	 * 
	 * @param jsTree_item node 
	 */
	function createSubmenu(node) {
		var submenu = {};
		for (var key in treeObjects) {
			// @todo: add logic to omit items where necessary
			if  (typeof treeObjects[key].create === 'undefined' || treeObjects[key].create.indexOf(mmng.getActiveUser().role) !== -1) {
				var keyText = mmng.util.ucFirst(getKeyText(key).replace("-", " "));
				submenu[key] = {
					label : keyText, 
					icon : treeObjects[key]['icon'],
					action: createEvent(key, node.id) 
				};
			}
		}
		return submenu;
	}
	
	/**
	 * Returns a string to represent a singular instance of the object
	 * 
	 * @returns string
	 */
	function getKeyText(keyText) {	
		if (keyText == "galleries") {
			keyText = "gallery"; 
		}
		return keyText;
	}
	
	/**
	 * Creates an object via the API and adds node to jsTree
	 * 
	 * @param string key
	 * @param array treeObjects
	 * @param int parentId
	 * @returns function
	 */
	function createEvent(key, parentId) {
		parentId = parentId === '#' ? 0 : parentId;
		
		// gather variables for post
		var keyText = mmng.util.ucFirst(key.replace("-", " "));
		var icon = treeObjects[key]['icon'];
		var typeId = treeObjects[key]['typeId']
		var newHeading = "New "+keyText;
		var postData = {"heading": newHeading, "parentId": parentId};

		if (key.split('-').reverse()[0] === 'page')  postData.url = newHeading.replace(/[ *|_]/g, '-').toLowerCase();
		
		return function(event) { 

			mmng.progressIndicator.init(['']);
			
			$.ajax({
				type: "POST",
				url: treeObjects[key].endpoint,
				dataType: "json",
				contentType: "json",
				data : JSON.stringify(postData)
			})
			.done(function(data) {
				var newNode = objToNode({id: data.id, parent: parentId, text: data.heading,  data: {position: data.position, type: key}}); 
				jstree.create_node((parentId === 0 ? '#' : parentId), newNode, newNode.data.position);
				
				jstree.deselect_all();
				jstree.select_node(data.id);

				mmng.notify.add({body: 'Successfully created ' + keyText + '.', response: 'success', length: 4500});
			})
			.fail(function(err) {
				mmng.notify.add({body: 'Error creating ' + keyText + '.', response: 'fail', length: 4500});
				if (parentId == 0) {
					$('#tlContextMenu').hide();
				}
			})
			.always(function() {
				mmng.progressIndicator.destroy();
			});
		}
	}
	
	var scriptsDidLoad = false,
	    formDidSet = false;
	
	// tree search
	var to = false;
	$('.pageFilterInput').keyup(function () {
	    if (to) { clearTimeout(to); }
	    to = setTimeout(function () {
	        var v = $('.pageFilterInput').val();
	        $('.treeMenuBody').jstree(true).search(v);
	    }, 250);
	});
	
	$('#formContainer').on('pjax:beforeSend', function() {
		if (!$('#formContainer .progressOverlay').length) {
			$('.progressOverlay').clone().appendTo('#formContainer').show();
		}
	});
	
	$('#formContainer').on('pjax:beforeReplace', function() {
		// do a bunch of resets
		
		scriptsDidLoad = formDidSet = false;
		
		// unset event handlers so they don't double-up
		$('#formContainer').off('click', '.saveBtns button');
		$('#formContainer').off('click');
		
		// dynamic content script
		$('#formContainer').off('change');
		
		// remove scripts relevant to other forms
		$('[data-resource-form-script]').remove();
		tinymce.remove();
	});
	
	
	// Event needs to be set on initialize and every pjax load as .resourceForm gets replaced
	function addNodeSaveEvent()
	{
		document.querySelector('.resourceForm').addEventListener('mmng:resourceDidSave', function(e) {
			var res = e.detail.resource;
			var node = page.model.tree.reduce(function(carry, obj) {
				return carry.id === res.id ? carry : obj;
			}, page.model.tree[0]);
			
			node.text = res.internalLabel || res.heading;
			node.data.published = res.published;
			
			jstree.rename_node(res.id, fullText(node));
		});
	}
	
	$('#formContainer').on('pjax:success', function(data, status, xhr, options) {
		addNodeSaveEvent();
	});
	
	if ($(".resourceForm").length) {
		addNodeSaveEvent();
	}
	
	$('#formContainer').on('pjax:end', function() {
		$('#formContainer .progressOverlay').remove();
	});
	
	
	// Event listeners to check that the form itself has loaded, as well as any
	// dependent scripts that are being loaded async before initializing.
	
	document.addEventListener('mmng:formDidSet', function() {
		formDidSet = true;
		if (scriptsDidLoad) {
		    mmng.getCurrentPage().form.init();
		}
	});
	
	document.addEventListener('mmng:formScriptsDidLoad', function() {
		scriptsDidLoad = true;
		if (formDidSet) {
		    mmng.getCurrentPage().form.init();
		}
	});
	
	$('.newPageBtnGrp').on('click', 'a', function() {
		createEvent($(this).attr('data-object-key'), '#')();
	});

	$(document).ready(function() {
		
		function firstSelect()
		{
			system_select = true;
			jstree.select_node(/\d+/.exec(id));
			system_select = false;
		}
		
		// Load appropriate content based on url
		var pattern = new RegExp(/^\/admin\/pages\/edit\//);
		if (pattern.test(window.location.pathname)) {
			var trimmedPath = window.location.pathname.replace(pattern, '');
			var parts = trimmedPath.split('/');
			
			var type = parts[0],
				id = parts[1];
			
			if (id && mmng.util.find(page.model.tree, 'id', /\d+/.exec(id)).data.resource.type === type) {
				
				// jstree select event handles active class and opening the tree, and pjax loading of form
				// using private var to check if jstree is ready before setting event checking for ready if not

				if (jstree._data.core.ready === true) {
					firstSelect();
				} else {
					$('.treeMenuBody').on('ready.jstree', function() {
						firstSelect();
					});
				}
	 		}
		}
		
		// create new menu
		var obj, objs = [];
		for (var key in page.model.treeObjects) {
			obj = page.model.treeObjects[key];
			obj.key = key;
			if (typeof obj.alias === 'undefined') {
				obj.alias = mmng.util.toTitleCase(key.replace('-', ' ')); 
			}
			objs.push(obj);
		}

		$('.newPageBtnGrp ul').html(objs.reduce(function(out, obj) {
			return out += '<li><a href="#" data-object-key="' + obj.key + '">' + obj.alias + '</a>';
		}, ''));

		// then strip based on permissions
		$('.newPageBtnGrp li a').each(function() {
			var permissions = treeObjects[$(this).attr('data-object-key')].create;
			
			if (typeof permissions !== 'undefined' && permissions.indexOf(mmng.getActiveUser().role) === -1) {
				$(this).remove();
			}
		});
		
		if ($("form").data("init-on-ready") === 1) {
			formDidSet = true;
			scriptsDidLoad = true;
			
			mmng.getCurrentPage().form.init();
		}
		
	});

})();
