(function() {
	"use strict";
	
	var _form = {};
	Object.defineProperty(mmng.getCurrentPage(), 'form', {
		enumerable: true,
		configurable: false,
		get: function() { return _form; },
		set: function(form) {
			_form = form;
			var event = new CustomEvent('mmng:formDidSet', {
				details: {}  // can add further details here such as type of form etc
			});
			
			document.dispatchEvent(event);
		}
	});
	
	var ResourceForm = mmng.ResourceForm = function(selector, conf) {
		
		this.scriptsLoaded = 0;
		
		if (arguments.length === 2 && typeof selector === 'string') {
			this._selector = selector;
		} else if (typeof selector === 'object') {
			conf = selector;
			this._selector = null;
		}
		
		this._resource = {
			type: null,
			alias: null,
			objectTypeId: null,
			endpoint: null,
			
			setEndpoint: function(ep) {
				this.endpoint = ep.charAt(ep.length - 1) === '/' ? ep : ep + '/';
			}
		};
		
		if ($('.resourceUrl').length) {
			this.autoUrl.init($('.resourceUrl'), $('.resourceHeading'));
		}
		
		this._init();
		this._initFormEventHandlers();
		
		mmng.configure.call(this, conf);
		
		var els = $(document.querySelector(this._selector).getElementsByTagName('*')).filter(':input');
		this.parsleyEls = [];
		for (var i = 0; i < els.length; i++) {
			var elMarkup = els[i].outerHTML;
			if (/data-parsley-/.test(elMarkup)){
				this.parsleyEls.push(new Parsley($(els[i])));
			};
		}
	};
	
	var rfp = ResourceForm.prototype = {};
	
	rfp.plugins = {};
	
	rfp.getSelector = function() 
	{
		return this._selector;
	};
	
	rfp.setSelector = function(selector) 
	{
		this._selector = selector;
		return this;
	}
	
	rfp.getResource = function() 
	{
		return this._resource;
	};
	
	rfp.setResource = function(conf) 
	{
		mmng.configure.call(this._resource, conf);
	};
	
	rfp.save = function() 
	{
		this.preSaveProcess();
		if (typeof tinymce !== 'undefined') tinymce.triggerSave();
		this.sendRequest(this.objectifyFormData($('form')), this.overwriteData);
	};

	rfp.overwriteData = {};
	
	// executes before the entire save process
	rfp.preSaveProcess = function() {
		mmng.progressIndicator.init(['Saving resource']);
	};
	
	// executes right before the xhr is sent
	rfp.modifySaveData = function(data) { return data; };
	
	// executes after ajax response returns, and success/error functions have been called
	rfp.postSaveProcess = function() {
		mmng.progressIndicator.destroy();
	};
	
	rfp.sendRequest = function(data, overwriteData) 
	{
		var that = this;
		// disabled url in-case-of
		if ($(".resourceUrl").attr('disabled') === 'disabled') data.url = $('.resourceUrl').val();
		data = this.modifySaveData(data);

		var ajaxConf = {
			url: this._resource.endpoint + data.id, 
			type: data.id ? 'put' : 'post',
			data: data,
			dataType: 'json',
			contentType: 'json'
		};
		
		for (var k in overwriteData) {
			if (typeof ajaxConf.data[k] !== 'undefined') ajaxConf.data[k] = overwriteData[k];
		}
		
		ajaxConf.data = JSON.stringify(ajaxConf.data);
		var req = $.ajax(ajaxConf);
		
		req.done(function(data) {
			
			// change URL to reflect new ID
			
			if (ajaxConf.type === 'post') {
				var newPath = window.location.pathname + "/" + data.id + window.location.search;
				if (typeof window.history.pushState === 'undefined') { //check for window.history.pushState?
					return window.location = newPath; // Refreshes page if cannot alter URL to add id
				} else {
					window.history.pushState(history.state, 'resourceIdentified', newPath);
				}
			}

			$('[name="id"]').val(data.id); // can be removed after 'binding' is implemented
			
			if (data.published == 1 && $('[data-overwrite-draft="1"]').length) {
				$('[data-overwrite-draft="1"]').remove();
				$('[data-overwrite-publish="1"]')
					.removeAttr('data-overwrite-publish')
					.text('Update');
			}
			
			that.saveSuccess(data);
		});
		
		req.fail(function(err) {
			var body = 'Could not save';
			if (typeof err.responseJSON !== "undefined" && typeof err.responseJSON.userMessage !== "undefined" && err.responseJSON.userMessage) {
				body += ":<br><em>" + err.responseJSON.userMessage + "</em>";
			}
			mmng.notify.add({body: body, response: 'fail', length: 4500});
		});
		
		req.always(function() {
			that.postSaveProcess();
			that.overwriteData = {};
		});
	};
	
	rfp.saveSuccess = function(data) {
		this.lastSavedSnapshot = this.snapshot();
		
		var event = new CustomEvent(
			'mmng:resourceDidSave', 
			{
				detail: {
					time: new Date(),
					resource: data
				},
				bubbles: true,
				cancelable: true
			}
		);
		document.querySelector(this._selector).dispatchEvent(event);
		
		mmng.notify.add({body: 'Saved successfully', response: 'success', length: 4500});
	};
	
	/**
	 * Converts a form to an object.
	 * Expects a jq form object or serialized array.
	 * 
	 * Only works 3 levels or less deep currently, eg:
	 * 
	 * form[object][key], or
	 * array[type][]
	 * 
	 * For more levels add more capturing groups in the regex pattern
	 */
	rfp.objectifyFormData = function(data) {
		data = Array.isArray(data) ? data : data.serializeArray();

		var formObj = {};
		
		$.each(data, function() {
			//var name = this.name.match(/(^[a-zA-Z0-9_-]*(?=\[)|\[[a-zA-Z0-9_-]*\])/g); //splits string by square brackets eg forums[events] becomes [forums, [events]]
			
			var pattern = /^([a-zA-Z0-9_-]+)(\[[a-zA-Z0-9_-]*\])?(\[[a-zA-Z0-9_-]*\])?/g;
			var name = pattern.exec(this.name);
			
			name = name.filter(function(name, i) {
				return !(i === 0 || typeof name === "undefined"); // remove first index (input string) and undefined values
			});

			name = name.map(function(part, i) {
				return i === 0 ? part : part.replace(/(\[|\])/g ,""); // and unwrap from square brackets
			});

			var val = this.value;
			
			// Sets value on appropriate object/subobject - if nonexistent creates an object with properties from the array above
			
			function setProperty(nameParts) {
				
				// first make sure object and subobjects exist in the root object
				
				var key = nameParts.reduce(function(parentObj, part, i) {
					
					// if we're the last key, this is what we want to set the value as, just return the parentObject
					
					if (i === nameParts.length - 1) return parentObj;
					
					// otherwise the key is undefined and we need to set it up
					
					if (typeof parentObj[part] === "undefined") {
						// if the object is undefined, set to either empty array or object depending on the next key
						if (nameParts[i + 1] === "") {
							parentObj[part] = []; // if the last key is an empty string "" then we have an array
						} else {
							parentObj[part] = {};
						}
					}

					return parentObj[part];
				
				}, formObj);
				
				if (Array.isArray(key)) {
					key.push(val);
				} else {
					key[nameParts[nameParts.length - 1]] = val;
				}
			};
			
			setProperty(name);
		});

		return formObj;
	};
	
	rfp.bind = function(data) {
		
		var bindPrep = {
			
			published: function(val) {
				val = parseInt(val);
				return val ? 'PUBLISHED' : 'UNPUBLISHED';
			},
				
			createdDate: function(val) {
				if (val == '0000-00-00 00:00:00') return 'UNPUBLISHED'; 
				return modDate(val);
			},
			
			lastModified: function(val) {
				return modDate(val);
			},
			
			publishedLink: function(val) {
				console.log('publishedLlink: ', val);
				if (val === false) {
					return '<em>Cannot view unpublished resource</em>';
				} else {
					return '&nbsp;<a href="' + val + '" class="btn btn-info btn-sm btn-rounded" target="_blank">Published Page</a>';
				}
			}
				
		};
		
		function modDate(val) {
			var rawDate = val.split(' ').shift();
			var rawTime = val.split(' ').pop();
			
			var date = rawDate.split('-').reverse().join('/');
			var time = rawTime.split(':');
			time.pop();
			
			return time.join(':') + ' ' + date;
		}
		
		$('[data-mmng-bind]').each(function(index, el) {
		
			var elData = el.getAttribute('data-mmng-bind').split('.');
			
			function findProp(elData, obj) {
				
				if (typeof obj !== 'object') return;
				
				elData = elData.slice(0);
				var prop = elData.shift();
				
				if (!obj.hasOwnProperty(prop)) return;

				if (elData.length) {
					return findProp(elData, obj[prop]);
				} else {
					return obj[prop];
				}
			}
			
			var dataResult = findProp(elData, data);
			
			if (dataResult || el.getAttribute('data-mmng-bind') === 'publishedLink') {
				
				var val = '';
				
				var bindPrepResult = findProp(elData, bindPrep);
				
				if (bindPrepResult) {
					val = bindPrepResult(dataResult);
				} else {
					val = dataResult;
				}
				
				if (el.hasOwnProperty('value')) {
					el.value = val;
				} else {
					el.innerHTML = val;
				}
					
			}
		});
	};
	
	rfp.lastSavedSnapshot = null;
	
	rfp.snapshot = function(selector) 
	{
		if (!selector) selector = this._selector;
		
		var form = $(selector);
		
		var disabled = form.find(':input:disabled').removeAttr('disabled');
		var snapshot = JSON.stringify(form.serializeArray());
		
		disabled.attr('disabled', 'disabled');
		return snapshot;
	}
	
	rfp._init = function() {
		if (typeof tinymce !== 'undefined') {
			this.tinymceInit.apply(this, (this.tinymceConf ? [this.tinymceConf.selector, this.tinymceConf.plugins, this.tinymceConf.config] : null)); // add user options here
		}
	};
	
	rfp.init = function() {
		this.lastSavedSnapshot = this.snapshot();
	};
	
	rfp.tinymceConf = null;
	
	rfp._initFormEventHandlers = function() {
		var that = this;
		
		$('#formContainer').on('click', '.saveBtns button', function() {
			if (!that.validate()) {
				alert("Not valid");
				return;
			}
			
			if ($(this).attr('data-overwrite-publish') == 1) {
				rfp.overwriteData.published = 1;
			} else if ($(this).attr('data-overwrite-draft') == 1) { 
				rfp.overwriteData.published = 0;
			}
			
			that.save();
		});
		
		$('#formContainer').on('click', '.editResourceStatus', function() {
			var el = $(this);
			if (el.hasClass('active')) {
				el.removeClass('active');
				$('.resourcePublishArea').hide();
			} else {
				el.addClass('active');
				$('.resourcePublishArea').show();
			}
		});
		
		$('#formContainer').on('change', '.resourcePublishArea select', function() {
			var selected = $(this).find('option:selected').val();
			$('[name="published"]').val($(this).val());
			//that.bind({ published: selected});
		});
		
		this.moxman._init();
	};
	
	rfp.moxman = {
		_init: function() {
			this._initEventHandlers();
		},
		
		_initEventHandlers: function() {
			var that = this;
			
			$('.moxmanBrowse').click(function() {
				var forEl = $(this).attr('data-for');
				
				moxman.browse({
					multiple: false,
					relative_urls : true,
					document_base_url: '/uploaded/',
					oninsert: function(data) {
						$(forEl).val(data.files[0].url);
						that.refreshThumb(data.focusedFile.path);
					},
					title: 'File Manager'
				});
			});
			
			$('#featureImgInput').on('keyup', function() {
				var val = $(this).val();
				
				clearTimeout(that.refreshTimer);
				that.refreshTimer = setTimeout(function() {
					that.refreshThumb('/uploaded/' + val);
				}, 600);
			});
		},
		
		_allowedFormats: ('jpg,jpeg,png,gif,html,htm,txt,docx,doc,zip,pdf'.split(',')),
		
		refreshThumb: function(path) {
			var rootPath = '/uploaded/';
			$('.featureThumbInsert').empty();
			
			var isFile = this._allowedFormats.reduce(function(prev, curr) {
				return prev === true ? prev : (function() {
					return (path.slice(-(curr.length + 1)) === '.' + curr) ? true : false;
				})();
			}, false);
			
			if (isFile) {
				if (path === rootPath) {
					return $('.featureThumbInsert').empty();
				}
				
				var newImg = new Image;
				newImg.src = path;
				
				newImg.onload = function() {
					$('.featureThumbInsert').html('<img src="' + path + '" alt="" style="width:100%" />');
				};
				
				newImg.onerror = function() {
					$('.featureThumbInsert').html('<strong><small>Cannot find image:</strong> ' + path.slice(rootPath.length) + '</small>');
				};
			} else {
				$('.featureThumbInsert').html('<strong><small>Slideshow from:</strong> ' + path + '</small>');
			}
		},
		
		refreshTimer: null,
	};
	
	rfp.tinymceInit = function(selector, plugins, config) {
		plugins = plugins || [['moxiemanager', '/js/moxiemanager/plugin.min.js']];
		plugins.forEach(function(pi) {
			tinymce.PluginManager.load.apply(tinymce.PluginManager, pi);
		});
		
		var templates = mmng.conf.tinymce.templates || [];
		var style_formats = mmng.conf.tinymce.style_formats || [
					    {title: 'Float Image Left', selector: 'img', classes: 'img-left'},
					    {title: 'Float Image Right', selector: 'img', classes: 'img-right'},
					    {title: 'Lead Text', block: 'p', classes: 'lead'},
					    {title: 'Launch Modal', inline: 'a', classes: 'launch-modal'},
					    {title: 'H1', block: 'h1'},
					    {title: 'H2', block: 'h2'},
					    {title: 'H3', block: 'h3'},
					    {title: 'H4', block: 'h4'},
					    {title: 'H5', block: 'h5'},
					    {title: 'H6', block: 'h6'},
					    {title: 'Paragraph', block: 'p'},
					    {title: 'Blockquote', block: 'blockquote'}
				    ];
		
		var content_css;
		if (typeof mmng.conf.tinymce.content_css !== "undefined") {
		    mmng.conf.tinymce.content_css.unshift("/css/admin/editor.css");
		    content_css = mmng.conf.tinymce.content_css.join(", ");
		} else {
		    content_css = "/css/admin/editor.css";
		}

		var shortcodes_plugin = false;
		if (typeof mmng.conf.tinymce.custom_plugins !== "undefined" && mmng.conf.tinymce.custom_plugins.indexOf("shortcodes") !== -1) {
			shortcodes_plugin = true;
		}
		
		config = config || {
			
			selector: (selector ? selector : '#contentEditor'),
			menubar: false,
			height:320,
			content_css: content_css,
			plugins: [
			    'moxiemanager code link image textcolor paste wordcount media preview charmap hr anchor fullscreen nonbreaking template table charmap'
			],
			relative_urls: false,
			image_advtab: true,
			toolbar: 'bold italic | charmap | pastetext | styleselect | forecolor | bullist numlist | alignleft aligncenter alignright | insertfile link unlink video image googlemap hr anchor | code | removeformat | table' + (templates.length ? " template" : "") + (shortcodes_plugin ? " shortcodes" : ""),
			style_formats: style_formats,
			templates: templates,
			moxiemanager_title: 'File Manager',
			toolbar_items_size: 'small',
			theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
			paste_auto_cleanup_on_paste : true,
//			valid_children: "+div[div]",
			extended_valid_elements: "a[rel|rev|charset|hreflang|tabindex|accesskey|type|name|href|target|title|class|onfocus|onblur|onclick]",
			
			setup: function(editor) {
				
				editor.addButton('video', {
					text: false,
					icon: 'media',
					tooltip: 'Video',
					onclick: function() {
						 editor.windowManager.open({
							title: 'Embed Video',
							url: '/partials/mce/responsiveEl.html',
							width: 500,
							height: 240,
							buttons: [
							          {text: 'Cancel', onclick: 'close'},
							          {text: 'Insert', onclick: 'submit', classes: 'widget btn primary first abs-layout-item'}
							          ],
						});
					}
				});
				
				editor.addButton('googlemap', {
					icon: 'mapIcon',
					tooltip: 'Google Map',
					onclick: function() {
						 editor.windowManager.open({
							title: 'Google Map',
							url: '/partials/mce/responsiveEl.html',
							width: 500,
							height: 240,
							buttons: [
							          {text: 'Cancel', onclick: 'close'},
							          {text: 'Insert', onclick: 'submit', classes: 'widget btn primary first abs-layout-item'}
							          ],
						});
					}
				});
				
				if (shortcodes_plugin) {
					editor.addButton("shortcodes", {
						text: false,
						icon: 'shortcodeIcon',
						tooltip: "Shortcodes",
						onclick: function() {
							editor.windowManager.open({
								title: "Shortcodes",
								url: "/admin/editor/shortcode-interface",
								width: 500,
								height: 400,
								buttons: [
									{text: "Cancel", onclick: "close"},
									{text: "Insert", onclick: "submit", classes: 'widget btn primary first abs-layout-item'}
								]
							});
						}
					});
				}
			}
		};

		tinymce.init(config);
	};
	
	rfp.autoUrl = {
		
		disabled: false,
		
		init: function(urlEl, headingEl, thisArg) {
			var inputGrp  = ['<div class="input-group">', $('.resourceUrl').get(0).outerHTML];
			inputGrp.push('<span class="input-group-btn"><button class="btn btn-default editResourceUrl" type="button">Edit</button></span>');
			inputGrp.push('</div>');
			
			$('.resourceUrl').replaceWith(inputGrp.join(''));

			if (this.sameCheck() || $('.resourceUrl').val().trim() === '') {
				this.disabled = true;
				$('.resourceUrl').attr('disabled', 'disabled');
				
				if ($('.resourceUrl').val().trim() === '') {
					$('.resourceUrl').val(this.generateUrl($('.resourceHeading').val()));
				}
			} else {
				this.disabled = false;
			}
			
			this.initHandlers();
		},
		
		initHandlers: function() {
			var that = this;
			
			$('#formContainer').on('keyup', '.resourceHeading', function() {
				if (that.disabled) {
					$('.resourceUrl').val(that.generateUrl($(this).val()));
				}
			});
			
			$('#formContainer').on('click', '.editResourceUrl', function() {
				that.disabled = false;
				$('.resourceUrl').removeAttr('disabled');
			});
			
		},
		
		sameCheck: function() {
			var linkRep = $('.resourceHeading').val().toLowerCase().replace(/ /g, '-');
			if (linkRep === $('.resourceUrl').val()) return true;
			
			return false;
		},
		
		generateUrl: function(val) {
			var regex = /[a-zA-Z0-9_ ]/;
			
			val = val.split('').filter(function(thisChar) {
				if (thisChar.search(regex) !== -1) return true;
			}).join('');
			
			return val.toLowerCase().trim().replace(/ +/g, '-').replace(/(^\-+(?=[a-zA-Z0-9_ ])|\-+$)/, '');
		}
			
	};

	rfp.statusTemplateArea = function(published) {
		var text = parseInt(published) ? 'PUBLISHED' : 'UNPUBLISHED';
		
		var template = $(['<div class="form-group"><label>Status: &nbsp;</label>',
		              '<span class="resourceStatus"><span data-mmng-bind="published">' + text + '</span> | <a class="editResourceStatus" href="#">Edit</a></span>',
		              '<div class="resourcePublishArea" style="display: none">',
		              '<select><option value="0">Unpublished</option><option value="1" selected="selected">Published</option></select>',
		              '</div></div></div>'].join(''));
		
		template.find('option[value="' + published + '"]').attr('selected', 'selected');
		return template;
	};
	
	rfp.parsleyEls = [];
	rfp.validate = function() {
		//console.log(this.parsleyEls);
		
		// NEED TO UNSET PARSLEY ELS WHEN THE DAMN FORM IS RESET
		
		var validate = true;
		this.parsleyEls.forEach(function(parsEl) {
			if (parsEl.validate() !== true) {
				validate = false;
			}
		}, this);
		return validate;
	};
	
	
	// base component
	
	function Component(conf)
	{
		this._conf = conf;
		
		this.init();
	}
	
	Component.prototype.init = function() {
		this.initHandlers();
	};
	
	Component.prototype.initHandlers = function() {
		
	};
	
	
	rfp.initComponents = function(useComponents) {
		var _this = this;
		
		if (typeof _this._activeComponents === "undefined") {
			_this._activeComponents = [];
		}
		
		useComponents.forEach(function(c) {
			
			var Constructor = _this.components[c.componentType];

			// create temporary obj we can apply component constructor to
			var temp = Object.create(Constructor.prototype);
			
			c.initArgs = c.initArgs || [];
			Constructor.apply(temp, c.initArgs)
			
			_this._activeComponents.push({
				saveToKey: c.saveToKey,
				instance: temp
			});
		});
	};
	
	rfp.saveComponentStates = function(obj) {
		if (typeof obj !== "object") {
			obj = {};
		}
		
		if (typeof this._activeComponents === "undefined") {
			return obj;
		}
		
		this._activeComponents.forEach(function(c) {
//			obj[c.saveToKey] = c.instance.saveState();
			mmng.util.stringToObjReference(c.saveToKey, obj).set(c.instance.saveState());
		});
		
		return obj;
	};
	
	
	// form components
	
	var components = rfp.components = {
//		MediaCollection: MediaCollectionComponent
	};
	
})();
