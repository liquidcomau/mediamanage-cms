"use strict";

(function() {
	
	var mmngModel = mmng.getCurrentPage().model;
	
	mmngModel.contentAreas = [];
	
	var formConf = {
		selector: '.resourceForm'
	};
	
	formConf.resource = {
		type: $(formConf.selector).attr('data-resource-type'),
		alias: $(formConf.selector).attr('data-resource-alias'),
		endpoint: $(formConf.selector).attr('data-resource-endpoint')
	};

	function SlideshowForm(conf) {
		var that = this;
		
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			
			if (typeof mmngModel.slides.data === 'undefined') mmngModel.slides.data = [];
			if (typeof mmngModel.slides.settings === 'undefined') mmngModel.slides.settings = [];
			
			mmngModel.slides.data =	mmngModel.slides.data.map(function(slide) {
				return new that.Slide(slide);
			});
			
			that.drawSlides();
			$('.slidesContainer').sortable({
				update: function() {
					that.updateSlidesFromForm();
				}
			});
			
			initEventHandlers();
			this.lastSavedSnapshot = this.snapshot(this._selector);
		};
		
		this.save = function() {
			mmng.progressIndicator.init(['Saving']);
			
			var data = mmngModel.slides;
			this.sendRequest(data);
		};
	}
	
	var sp = SlideshowForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	// overwriting snapshot functionality to include tinymce save
	sp.snapshot = function(selector) 
	{
		return JSON.stringify(mmngModel.slides);
	}

	sp.Slide = function(conf) {
		conf = conf ? conf : {};
		this.image = conf.image || null;
		this.link = conf.link || null;
		this.newWindow = conf.newWindow || 0;
	};
	
	sp._slideTemplate = (['<div class="slide row">',
	                     '<div class="col-sm-8">',
	                     '<div class="form-group"><label>Image</label><div class="input-group"><input type="text" class="form-control" name="slideImage" />',
	                     '<span class="input-group-btn"><button class="btn btn-default slideBrowseBtn" type="button">Browse</button></span>',
	                     '</div></div>',
	                     '<div class="form-group"><label>Link</label><div class="input-group"><input type="text" class="form-control" name="slideLink" />',
	                     '<span class="input-group-addon"><label><input type="checkbox" class="slideNewWindow">&nbsp; Open in new window</label></span></div></div>',
	                     '<button class="btn btn-xs btn-danger removeSlideBtn">Remove</button>',
	                     '</div>',
	                     '<div class="col-sm-4 slideImgThumb"></div>',
	                     '</div>'
	                     ].join(''));
	
	sp.drawSlides = function() {
		var that = this;

		$('.slidesContainer').empty();
		
		var mu = mmngModel.slides.data.forEach(function(slide, ind) {
			var st = $(that._slideTemplate);
			st.find('[name="slideLink"]').val(slide.link).attr('data-ind', ind);
			if (slide.newWindow) st.find('.slideNewWindow').prop('checked', true);
			st.find('.slideNewWindow').attr('data-ind', ind);
			
			st.find('[name="slideImage"]').val(slide.image).attr('data-ind', ind);

			if (slide.image) st.find('.slideImgThumb').html('<label>Preview</label><img src="/uploaded/' + slide.image.trim() + '" style="width:100%" />')
			
			st.find('.removeSlideBtn').attr('data-ind', ind);
			st.find('.slideBrowseBtn').attr('data-ind', ind);
			
			$('.slidesContainer').append(st);
		});
	};
	
	sp.updateSlideFromForm = function(ind) {
		var slide = mmngModel.slides.data[ind];
		
		slide.image = $('[name="slideImage"][data-ind="' + ind + '"]').val();
		slide.link = $('[name="slideLink"][data-ind="' + ind + '"]').val();
		slide.newWindow = $('.slideNewWindow[data-ind="' + ind + '"]').prop('checked') ? 1 : 0;
	};
	
	sp.updateSlidesFromForm = function() {
		var that = this;
		
		mmngModel.slides.data = [];
		$('.slidesContainer .slide').each(function(ind, el) {
			mmngModel.slides.data.push(new that.Slide({
				image: $(el).find('[name="slideImage"]').val(),
				link: $(el).find('[name="slideLink"]').val(),
				newWindow: $(el).find('.slideNewWindow').prop('checked') ? 1 : 0
			}));
		});
		
		this.drawSlides();
	};
	
	function initEventHandlers() {
		var form = mmng.getCurrentPage().form;
		
		$(document).on('click', '.addSlideBtn', function() {
			mmngModel.slides.data.push(new form.Slide());
			form.drawSlides();
		});
		
		$(document).on('keyup', '.slide input', function() {
			form.updateSlideFromForm($(this).attr('data-ind'));
		});
		
		$(document).on('click', '.removeSlideBtn', function() {
			mmngModel.slides.data.splice($(this).attr('data-ind'), 1);
			form.drawSlides();
		});
		
		$(document).on('click', '.slideBrowseBtn', function() {
			var el = $('[name="slideImage"][data-ind="' + $(this).attr('data-ind') + '"]');
			moxman.browse({
				multiple: false,
				relative_urls : true,
				document_base_url: '/uploaded/',
				oninsert: function(data) {
					el.val(data.files[0].url);
					form.updateSlideFromForm(el.attr('data-ind'));
					form.drawSlides();
				},
				title: 'File Manager'
			});
		});
		
		$(document).on('click', '[name="slideImage"]', function() {
			$('.slideBrowseBtn[data-ind="' + $(this).attr('data-ind') + '"]').trigger('click');
		});
		
		$(document).on('click', '.slideNewWindow', function() {
			form.updateSlideFromForm($(this).attr('data-ind'));
		});
	}
	
	var form = mmng.getCurrentPage().form = new SlideshowForm(formConf);
	
})();