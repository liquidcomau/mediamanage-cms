"use strict";

(function() {
	var mmngModel = mmng.getCurrentPage().model;
	mmngModel.contentAreas = [];
	if (typeof mmngModel.form === 'undefined') mmngModel.form = {};
	
	var formConf = {
		selector: '.resourceForm'
	};
	
	formConf.resource = {
		type: $(formConf.selector).attr('data-resource-type'),
		alias: $(formConf.selector).attr('data-resource-alias'),
		endpoint: $(formConf.selector).attr('data-resource-endpoint')
	};

	function ListingForm(conf) {
		var that = this;
		
		mmng.ResourceForm.call(this, conf);
		
		var useComponents = [
			{
				saveToKey: "gallery.items",
				initArgs: [{
					data: JSON.parse($('[name="gallery[items]"]').val() || "[]"),
					container: ".imagesArea",
					template: $('[name="gallery[itemTemplate]"]').val()
				}],
				componentType: "MediaCollection"
			}
		];
		
		this.init = function() {
			this.initComponents(useComponents);
			
			this.makeWysiwygs();
			this.saveTextareaContent();
			
			this.lastSavedSnapshot = this.snapshot(this._selector);
		};
		
		this.save = function() {
		    mmng.progressIndicator.init(['Saving']);

		    var data = JSON.parse(this.snapshot());
		    this.sendRequest(data);
		};
	}
	
	var lp = ListingForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	lp.snapshot = function(selector)
	{
	    if (!selector) {
			selector = this._selector;
		}

	    var form = $(selector);
	    var disabled = form.find(':input:disabled').removeAttr('disabled');

		this.saveTextareaContent();
	    
		var formData = this.objectifyFormData(form.serializeArray());
		
		this.saveComponentStates(formData);

		var snapshot = JSON.stringify(formData);
		
	    disabled.attr('disabled', 'disabled');
	    return snapshot;
	}
	
	lp.makeWysiwygs = function()
	{
		var that = this;
		
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			that.tinymceInit('#' + el.attr('id'));
		});
	};
	
	lp.saveTextareaContent = function()
	{
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			
			if (tinymce.get(el.attr('id'))) tinymce.get(el.attr('id')).save();
			mmngModel.contentAreas[i] = el.val();
		});
	};
	
	var form = mmng.getCurrentPage().form = new ListingForm(formConf);
	
})();