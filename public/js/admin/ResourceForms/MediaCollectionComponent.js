mmng.ResourceForm.prototype.components.MediaCollection = (function() {

	"use strict";
   
	var stringToModelReference = mmng.util.stringToObjReference;
   
	// media collection
	function MediaCollectionComponent(conf)
	{
		this.configure(conf);
		this.init();
	}
	
	MediaCollectionComponent.prototype.init = function() {
		var that = this;
		
		this._model = (this._conf.data || []).map(function(img) {
			if (img instanceof MediaCollectionItem) {
				return img;
			} else {
				return new MediaCollectionItem(img);
			}
		});
		
		this._container = this._conf.container || this._container;
		
		this.drawItems();

		$(this._container).sortable({
			update: function() {
				that.updateItemsFromForm(true);
			},
			axis: "y",
			placeholder: "sortable-placeholder"
		});
		
		// init default handlers
		if (this._conf.init_default_events !== false) {
			this._initHandlers();
		}
		
		// initialize user-provided event handler fn
		if (typeof this._conf.event_handlers === "function") {
			this._conf.event_handlers();
		}
	};
	
	MediaCollectionComponent.prototype.configure = function(conf) {
		// store original data
		this._conf = conf;
		
		//this._model = conf.model;
		this._container = conf.container;
		this._template = '<li class="mediaCollectionItem">' + (conf.template || this._defaultTemplate) + '</li>';
	};
	
	MediaCollectionComponent.prototype.saveState = function() {
		this.updateItemsFromForm();
		var data = this._model.reduce(function(list, item) {
			list.push(item.saveState());
			return list;
		}, []);

		return data;
	};
	
	MediaCollectionComponent.prototype._initHandlers = function() {
		var that = this;
		
		$("#formContainer").on("click", ".tab-item-viewType", function() {
			var tab = $(this); //.parent(".tab-item-viewType");
			tab.parents(".mediaCollectionItem").find("input[data-item-property='viewType']").val(tab.data("view-type"));
			
			that.updateImageFromForm($(this).attr('data-ind'));
		});

		$("#formContainer").on('click', '.addImageBtn', function() {
			that.updateItemsFromForm();
			that._model.push(that.item.create("image"));
		    that.drawItems();
	    });

	    $("#formContainer").on('keyup', '.mediaCollectionItem input', function() {
		    that.updateImageFromForm($(this).attr('data-ind'));
	    });

	    $("#formContainer").on('click', '.removeImageBtn', function() {
		    that.updateItemsFromForm();
			that._model.splice($(this).attr('data-ind'), 1);
		    that.drawItems();
	    });

	    $("#formContainer").on('click', '.imageBrowseBtn', function() {
			var ind = $(this).attr('data-ind'),
				relatedTo = $(this).attr("data-for-property");
		    var el = $('[data-item-property="' + relatedTo + '"][data-ind="' + $(this).attr('data-ind') + '"]');
		    moxman.browse({
			    multiple: false,
			    relative_urls : true,
			    document_base_url: '/uploaded/',
			    oninsert: function(data) {
				    el.val(data.files[0].url);
					that.updateItemsFromForm();
			    },
			    title: 'File Manager',
				remember_last_path: true
		    });
	    });
		
	    $("#formContainer").on('click', '.imgMain', function() {
		    that.updateItemsFromForm();
	    });
	};
	
	MediaCollectionComponent.prototype._defaultTemplate =[
						'<div class="row">',
						'<div class="col-sm-8">',
						'<div class="form-group"><label>Image</label><div class="input-group"><input type="text" class="form-control" name="imageSrc" />',
						'<span class="input-group-btn"><button class="btn btn-default imageBrowseBtn" type="button">Browse</button></span>',
						'</div></div>',
						'<div class="form-group"><label><input type="radio" class="imgMain" name="imgMain">&nbsp; Main Image</label></div>',
						'<div class="form-group"><button class="btn btn-xs btn-danger removeImageBtn">Remove</button></div>',
						'</div>',
						'<div class="col-sm-4 imgThumb"></div>',
						'</div>'
				    ].join('');
	
	MediaCollectionComponent.prototype.drawItems = function() {
	    var that = this;

	    $(this._container).empty();
		this._model.forEach(function(item, ind) {
			var view = $(that._template).clone();
			view.attr("data-ind", ind);

			// assign href to item tabs
			view.find(".tab-item-viewType a").each(function() {
				var href = $(this).attr("href");
				$(this).attr("href", '[data-ind="' + ind + '"] ' + href);
			});

			$(that._container).append(item.drawToView(view));
	    });
	};
	
	MediaCollectionComponent.prototype.updateImageFromForm = function(ind) {
	    var image = this._model[ind];

		$('[data-ind="' + ind + '"][data-item-property]').each(function() {
			var input =  $(this),
				modelReference = stringToModelReference(input.attr("data-item-property"), image.model),
				value;
				
			if (modelReference === false) return; // not a part of form	
				
			switch (input.attr("type")) {
				case "checkbox":
				case "radio":
					value = input.prop("checked");
					break;
				default:
					value = input.val();
			}
			
			modelReference.set(value);
		});
	};
	
	MediaCollectionComponent.prototype.updateItemsFromForm = function() {
	    var collection = this,
			conf = {},
			ind;

	    this._model = [];
	    $(this._container).children().each(function(i, itemTemplate) {
			var viewType = $(itemTemplate).find('[data-item-property="viewType"]').val();
			
			var item = collection.item.create(viewType);
			item.populateModelFromView($(itemTemplate));

			collection._model.push(item);
	    });
		
		collection.drawItems();
	};
	
	
	function MediaCollectionItem(viewType, conf)
	{
		if (typeof viewType === "object") {
			conf = viewType;
			viewType = conf.viewType;
		}
		
		// set default if viewType still not present
		viewType = viewType || "image";

		if (!conf) {
			conf = {};
		}

		// ensure viewType is also set in conf
		conf.viewType = viewType;
		
		// make copy of viewType model template from base models
		this._model = $.extend(true, {}, this._modelTemplate);
		this.mapItemConfigToModel(conf);
	}
	
	MediaCollectionItem.prototype.setModel = function(model) {
		this._model = model;
	};
	
	MediaCollectionItem.prototype._modelTemplate = {
		itemId: null,
		caption: null,
		image: {source: null},
		youtube: {source: null, displaySource: null},
		position: null,
		viewType: null,
		main: null
	};
	
	MediaCollectionItem.prototype.setPropertyByKey = function (key, value) {
		var modelRef = stringToModelReference(key, this._model);

		if (modelRef === false) {
			return;
		}

		modelRef.set(value);
	};
	
	/*
	 * @param {jQuery} view. Jquery object holding item element.
	 */
	MediaCollectionItem.prototype.populateModelFromView = function(view) {
		var _this = this;
		
		view.find("[data-item-property]").each(function() {
			var input =  $(this),
				key = $(this).data("item-property");

			switch (input.attr("type")) {
				case "checkbox":
				case "radio":
					_this.setPropertyByKey(key, input.prop("checked"));
					break;
				default:
					_this.setPropertyByKey(key, input.val());
			}
		});
	};
	
	MediaCollectionItem.prototype.drawToView = function(view) {
		var _this = this,
			ind = view.data("ind");
		
		view.find("[data-item-property]").each(function() {
			var input =  $(this),
				modelReference = stringToModelReference($(this).attr("data-item-property"), _this._model);

			// bail out if no place in model
			if (modelReference === false) {
				return;
			}

			switch (input.attr("type")) {
				case "checkbox":
				case "radio":
					input.prop("checked", modelReference.get());
					break;
				default:
					input.val(modelReference.get());
			}

			input.attr("data-ind", ind);
		});

		if (view.find(".imgThumb").length) {
			view.find(".imgThumb").each(function() {
				var thumb = $(this),
					relatedTo = thumb.attr("data-thumb-for"),
					modelReference = stringToModelReference(relatedTo, _this._model);

					if (modelReference && modelReference.get()) {
						thumb.html('<label>Preview</label><img src="/uploaded/' + modelReference.get() + '" style="width:100%" />');
					}
			});
		}

		view.find('.removeImageBtn').attr('data-ind', ind);
		view.find('.imageBrowseBtn').attr('data-ind', ind);

		var viewType = view.find('[data-item-property="viewType"]').val();
		view.find('.tab-item-viewType[data-view-type="' + viewType + '"] a').tab("show");
		
		view.find('.tab-pane').removeClass("active");
		view.find('.tab-pane[data-view-type="' + viewType + '"]').addClass("active");

		return view;
	};
	
//	function MediaCollectionItemImage(conf)
//	{
//		MediaCollectionItem.call(this, "image", conf);
//	}
	
	//MediaCollectionItemImage.prototype = Object.create(MediaCollectionItem.prototype);
	
	MediaCollectionItem.prototype.mapItemConfigToModel = function(conf) {
		if (typeof conf.viewType !== "undefined") {
			switch (conf.viewType) {
				case "youtube":
					this._model.youtube.source = conf.source || null;
					this._model.youtube.displaySource = conf.displaySource || null;
					break;
				case "image":
				default:
					this._model.image.source = conf.source || null;
			}
			delete conf.source;
		}
		
		for (var k in conf) {
			if (this._model.hasOwnProperty(k)) {
				this._model[k] = conf[k];
			}
		}
	};
	
	MediaCollectionItem.prototype.saveState = function() {
		var _this = this;
		
		var data = {
			itemId: _this._model.itemId,
			caption: _this._model.caption,
			viewType: _this._model.viewType,
			main: _this._model.main
		};
		
		switch (data.viewType) {
			case "youtube":
				data.source = _this._model.youtube.source;
				data.displaySource = _this._model.youtube.displaySource;
				break;
			default:
				data.source = _this._model.image.source;
		}

		return data;
	};
	
	MediaCollectionComponent.prototype.item = {
		create: function(viewType, conf) {
			//var viewTypeMethod = viewType.slice(0,1).toUpperCase() + viewType.slice(1);
			return new MediaCollectionItem(viewType, conf);
		},
		
		createWithModel: function(viewType, model) {
			var viewTypeMethod = viewType.slice(0,1).toUpperCase() + viewType.slice(1);
			var instance = new MediaCollectionItem(viewType);
			
			instance.setModel(model);
			
			return instance;
		}
	};
	
	MediaCollectionComponent.prototype.Item = MediaCollectionItem;
	
	return MediaCollectionComponent;
	
})();
