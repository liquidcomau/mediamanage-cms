(function() {
	
	var formConf = {
		resource: {
			type: 'fragment',
			alias: 'fragment',
			endpoint: '/ajax/api/fragments'
		},
		selector: '.resourceForm'
	};

	//var form = new mmng.getConstructor('ResourceForm')(formConf);
	function FragmentForm(conf) {
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			$('.formTypeHeading').text('Fragment');
		}
	}
	
	var ffProto = FragmentForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	mmng.getCurrentPage().form = new FragmentForm(formConf);
	
})();