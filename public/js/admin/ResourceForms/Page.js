// Handles both Static and Freeform Pages

"use strict";

(function() {
	
	var model = mmng.getCurrentPage().model;
	if (typeof model.form === 'undefined') model.form = {};
	
	var formConf = {
		selector: '.resourceForm'
	};
	
	formConf.resource = {
		type: $(formConf.selector).attr('data-resource-type'),
		alias: $(formConf.selector).attr('data-resource-alias'),
		endpoint: $(formConf.selector).attr('data-resource-endpoint')
	};

	function PageForm(conf) {
		var _this = this;
		
		this.init = function() {
			$('.formTypeHeading').text(mmng.util.toTitleCase(conf.resource.alias));
			
			if ($('input:regex(class,datepicker-)').length) {
				var datepicker = $('input:regex(class,datepicker-)').datepicker({
					format: "dd/mm/yyyy",
					separator: "/"
				});
				
				$('input:regex(class,datepicker-)').keydown(function(e) {
					if (e.which === 9) {
						$(this).datepicker('hide');
					}
				});
				
				$('.formContainerBody').scroll(function() {
					datepicker.datepicker('place');
					datepicker.each(function() {
						if ($(this).position().top < 0) {
							$(this).datepicker('hide');
						} else if ($(this).is(':focus')) {
							$(this).datepicker('show');
						}
					});
				});
			}
			
			// if the variable content form is detected
			// set up templates, models and content areas
			if ($('.variableContent').length) {
			    // ensure empty content/templates by default
			    model.form.content = [];
			    model.form.templates = {};
			    
			    model.form.content = $('[name="contentModel"]').val() ?  JSON.parse($('[name="contentModel"]').val()) : model.form.content;
			    model.form.templates = JSON.parse($('[name="templatesModel"]').val());
			    model.form.currentTemplate = $('[name="template"]').val();

			    //testing
			    /*
			    model.form.content = {"dca-content": [{style: 'container-sm', columns: ['Vestibulum ipsum est, luctus ut nulla et, pretium consectetur orci.', 'Proin leo magna, lobortis a auctor id, consectetur ut enim. Nunc massa urna, iaculis non est a, consectetur tristique orci.']},
				    {style: 'container', columns: ['Cras rhoncus porta metus id tristique. Donec hendrerit velit ut erat rhoncus sollicitudin. Vivamus non orci eros. Duis lacinia varius purus sed vulputate. Ut vitae ipsum est.']},
				    {style: 'container-sm', columns: ['Etiam eleifend sem non metus aliquet accumsan. Donec eget est a elit consequat laoreet.', 'Etiam vel odio pulvinar, rhoncus dui vitae, sodales leo. Mauris consequat, diam vitae elementum volutpat, quam odio feugia', 'Vivamus facilisis sollicitudin enim ut varius. Curabitur venenatis, metus sit amet tempus tincidunt, ligula lectus rhoncus eros, id rutrum dolor ipsum at lorem. Curabitur sit amet risus nibh. Facilisis sollicitudin enim ut varius.']}	
			    ], "ca-pageFooter": 'Vestibulum ipsum est, luctus ut nulla et, pretium consectetur orci. Proin leo magna, lobortis a auctor id, consectetur ut enim. Nunc massa urna, iaculis non est a, consectetur tristique orci.'
			    };
			    */
			   
			    // set up dynamic content
			    for (var k in model.form.content) {
				if (Array.isArray(model.form.content[k])) {
				    
				    // if the model is an array, it's a dynamic content area
				    
				    model.form.content[k] = model.form.content[k].map(function(rowContent) {
					var row = {columns: []};
					if (typeof rowContent.style !== 'undefined') {
					    row.style = rowContent.style;
					}
					rowContent.columns.forEach(function(col) {
					    tempId++
					    contentAreas[tempId] = col;
					    row.columns.push(tempId);
					});
					return row;
				    });
				} else {
				    
				    // if the model is a string, it's a single wysiwyg
				    
				    $('#' + k).val(model.form.content[k]);
				}
			    }

			    // and ensure all dynamic content areas have relevant models
			    this.fillEmptyDynamicContentModels();

			    this.drawDynamicContent();
			}
			
			this.makeWysiwygs();
			this.saveTextareaContent();
			
			this.lastSavedSnapshot = this.snapshot(this._selector);
		};
		
		this.save = function() {
			this.saveTextareaContent();
			
			var data = this.objectifyFormData(JSON.parse(this.snapshot(this._selector)));

			mmng.progressIndicator.init(['Saving resource']);
			this.sendRequest(data);
		};
		
		mmng.ResourceForm.call(this, conf);
	}
	
	var pProto = PageForm.prototype = Object.create(mmng.ResourceForm.prototype);

	var displayAs = 'col',
	    tempId = 0,
	    contentAreas = {};
	  
	/**
	 * Generates placeholder content for new or empty
	 * Dynamic content areas.
	 * 
	 * @returns void.
	 */
	pProto.fillEmptyDynamicContentModels = function() {
	    $('.dynamicContentAreaContainer').each(function() {
		var id = $(this).attr('id');
		if (typeof model.form.content[id] === 'undefined') {
		     tempId++
		     contentAreas[tempId] = "";
		     model.form.content[id] = [{columns: [tempId]}];
		}
	     });
	};

	pProto.drawDynamicContent = function(id) {
	    var _this = this;

	    var areas = id ? $('#' + id) : $('.dynamicContentAreaContainer');
	    areas.each(function() {
		var areaId = $(this).attr('id');
		
		var areaTemplates = model.form.templates[model.form.currentTemplate].areas;
		var styles = mmng.util.find(areaTemplates, 'name', areas.attr('id').replace(/^d?ca\-/, '')).containerStyles;
		
		if (styles) {
		    styles = styles.reduce(function(out, style) {
			out += '<option value="' + style.name + '">' + style.alias + '</option>';
			return out;
		    }, '');
		} else {
		    styles = '<option value="">Default</option>';
		}
		
		$('#' + areaId + ' .panel-body').empty();
		
		var out = model.form.content[areaId].forEach(function(row, i) {
		    var rowOut = $(['<div class="row dynamicContentRow clearfix" data-row="' + i + '">',
				'<header class="dynamicContentRowHeader clearfix">',
					'<div class="pull-right">',
						'Row style: ',
						'<select class="rowStyleSelect">',
						styles,
						'</select> &nbsp;',
						'<button class="btn btn-xs addColBtn" type="button">Add Column</button>',
						'<button class="btn btn-xs removeRowBtn" type="button">Remove Row</button>',
					'</div>',
					'<i class="fa fa-bars"></i>',
				'</header>',
				'<div class="dynamicContentCols clearfix">' + _this.markupRowWithContent(row, i) + '</div>',
			    '</div>'].join(''));

		    if (typeof row.style !== 'undefined') {
			rowOut.find('.rowStyleSelect option[value="' + row.style + '"]').prop('selected', 'selected');
		    } else {
			rowOut.find('.rowStyleSelect option:first').prop('selected', 'selected');
		    }
		    $('#' + areaId + ' .panel-body').append(rowOut);
		}, $(''));
	    });
	    
	    _this.registerSortable();
	};
	
	pProto.markupRowWithContent = function(row, rowNum) {
	    var width = displayAs === 'col' ? String(12 / row.columns.length) : "12";
	    
	    return row.columns.reduce(function(out, col, i) {
		var colOut = ['<div class="dynamicContentArea" data-parent-row="' + rowNum + '" data-col="' + i + '" data-content-id="' + col + '">',
			    '<div class="dynamicContentAreaTitle font-bold text-danger"><small>Column ' + (i + 1) + '</small></div>',
			    '<div class="dynamicContentAreaContent">' + contentAreas[col] + '</div>',
			    '<div class="dynamicContentAreaBtns btn-group">',
				    '<button class="btn btn-xs btn-default editDynamicColBtn" type="button"><i class="fa fa-pencil"></i> Edit</button>',
				    '<button class="btn btn-xs btn-default removeDynamicColBtn" type="button"><i class="fa fa-times"></i></button>',
			    '</div>',
		    '</div>'].join('');
	
		return out + '<div class="col-sm-' + width + '">' + colOut + '</div>';
	    }, '');
	};

	pProto.registerSortable = function(id) {
	    // Have to do this because of dynamic/future content messing with jqSortable
	    
	    var _this = this;
	    
	    var areas = id ? $('#' + id) : $('.dynamicContentAreaContainer');
	    
	    areas.each(function() {
		var areaId = $(this).attr('id');
		
		$('#' + areaId + ' .panel-body')
		    .sortable({update: function() {
			_this.reorderContent();
		    }});
	    
		$('#' + areaId + " .dynamicContentCols").sortable({
			connectWith: '#' + areaId + " .dynamicContentCols",
			update: function() {
			    _this.reorderContent();
			}
		});
	    });
	};
	
	pProto.reorderContent = function(id) {
	    var _this = this;

	    var areas = id ? $('#' + id) : $('.dynamicContentAreaContainer');

	    areas.each(function() {
		var areaId = $(this).attr('id');

		var newContent = [];
		$(this).find('.dynamicContentRow').each(function(row, ind) {
			var newRowContent = [];

			$(this).find('.dynamicContentArea').each(function() {
				newRowContent.push($(this).attr('data-content-id'));
			});
			newContent.push({style: $(this).find('.rowStyleSelect option:selected').val(), columns: newRowContent});
		});

		model.form.content[areaId] = newContent;
		console.log(newContent);
		$(this).find('.panel-body').html(_this.drawDynamicContent(areaId));
	    
	    });
	    
	    _this.registerSortable();
	};
	
	pProto.mapContentFromModel = function() {
	    var allContent = {};
	    for (var k in model.form.content) {
		var content = model.form.content[k];
		if (typeof content === 'object') {
		    allContent[k] = content.map(function(row) {
			var columns = row.columns.map(function(id) {
			   return contentAreas[id]; 
			});
			
			return {
			    style: row.style,
			    columns: columns
			};
		    });
		} else {
		    allContent[k] = content;
		}
	    }
	    return allContent;
	};

	pProto.generateTextarea = function(conf) 
	{
		var label, 
		    editor,
		    content;
		
		if (typeof conf.alias !== 'undefined') {
			label = '<label class="optional">' + conf.alias + '</label>';
		} else {
			label = null;
		}
		
		content = typeof conf.content !== 'undefined' ? conf.content : ''; 
		
		var editor = '<textarea name="content[' + conf.name + ']" id="ca-' + conf.name + '" class="form-control wysiwyg" rows="24" cols="80">' + content + '</textarea>';
		
		return ['<div class="form-group">', label, editor, '</div>'].join('');
	};
	
	pProto.generateDynamicArea = function(conf)
	{
	    return ['<div class="form-group">',
		    '<section id="dca-' + conf.name + '" class="dynamicContentAreaContainer panel panel-info">',
		    '<header class="panel-heading">',
		    '<div class="dynamicContentBtns pull-right">',
		    '<button class="btn btn-xs btn-default addRowBtn" type="button"><i class="fa fa-plus"></i> &nbsp;Add Row</button>',
		    '</div><span class="font-bold">' + conf.alias + '</span></header>',
		    '<div class="panel-body ui-sortable"></div></section></div>'].join('');
	}
	
	pProto.makeWysiwygs = function()
	{
		var that = this;

		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			that.tinymceInit('#' + el.attr('id'));
		});
	};
	
	pProto.saveTextareaContent = function()
	{
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			
			if (tinymce.get(el.attr('id'))) tinymce.get(el.attr('id')).save();
			if (typeof model.form.content === "undefined") {
			    model.form.content = {}; // just in case
			}
			model.form.content[el.attr('id')] = el.val();
		});
	};
	
	// overwriting snapshot functionality to include tinymce save and datepicker
	pProto.snapshot = function(selector) 
	{
		if (!selector) selector = this._selector;
		
		var form = $(selector);
		
		var disabled = form.find(':input:disabled').removeAttr('disabled');

		this.saveTextareaContent();
		
		var data = form.serializeArray();
		if ($('.variableContent').length) {
		    // remove "content[]" fields before adding it back in from page model
		    data = data.filter(function(field) {
			if (Array.isArray(field)) return; // don't know where these empty arrays are coming from...
			return !/^content\[.*\]$/.test(field.name);
		    });

		    var mappedContent = this.mapContentFromModel();
		    for (var k in mappedContent) {
			data.push({
			    name: 'content[' + k.split('-').pop() + ']', // keys take the form of ca-content or dca-content ("content area" and "dynamic content area")
			    value: mappedContent[k]
			});
		    }

		    data = data.filter(function(field) {
		       return !(field.name, /(?:contentModel|templatesModel)/.test(field.name));
		    });
		}
		
		var snapshot = JSON.stringify(data);
		
		disabled.attr('disabled', 'disabled');
		return snapshot;
	}

	var form = mmng.getCurrentPage().form = new PageForm(formConf);
	
	// events
	
	$('select#template').change(function(e)
	{
	    mmng.confirm({
		title: "Switch template",
		body: "Switching template will delete content. Are you sure you wish to proceed?"
	    }, function() {
//		var opt = $(this).find('option[value="' + e.target.value + '"]'),
		var selected = $('[name="template"]').find('option:selected').val(),
		    template = model.form.templates[selected];
		
		
		// first remove dynamic areas
		$('section:regex(id,dca-.*)').parents('.form-group').remove();
		$('textarea:regex(id,ca-.*)').parents('.form-group').remove();
		
		// then generate, populate, and draw new areas.
		var out = template.areas.reduce(function(out, area) {
		    out += area.type === 'dynamic' ? form.generateDynamicArea(area) : form.generateTextarea(area);
		    return out;
		}, '');
		
		$('.variableContent').append(out);

		model.form.content = [];
		form.fillEmptyDynamicContentModels();
		
		form.drawDynamicContent();
		form.makeWysiwygs();
	    });
	});
	
	$('#formContainer').on('click', '.displayDynamicContentAs', function() {
	   $('.displayDynamicContentAs').removeClass('active');
	   
	   $(this).addClass('active');
	   displayAs = $(this).attr('data-display-type');
	   
	   form.drawDynamicContent();
	});
	
	$('#formContainer').on('click', '.removeRowBtn', function() {
	    var _this = this,
		parentArea = $(this).parents('.dynamicContentAreaContainer').attr('id');
	    mmng.confirm({title: 'Delete',
			  body: 'Are you sure you wish to delete this row?'}, 
			  function() {
				var row = $(_this).parents('.dynamicContentRow');
				var rowContent = model.form.content[parentArea].splice(row.attr('data-row'), 1);

				rowContent.forEach(function(id) {
				    delete contentAreas[id];
				});

				form.drawDynamicContent();
			  });
	});
	
	$('#formContainer').on('click', '.removeDynamicColBtn', function() {
	    var _this = this,
		parentArea = $(this).parents('.dynamicContentAreaContainer').attr('id');
		mmng.confirm({title: 'Delete',
			  body: 'Are you sure you wish to delete this content area?'}, 
			  function() {
				var col = $(_this).parents('.dynamicContentArea');
				model.form.content[parentArea][col.attr('data-parent-row')].columns.splice(col.attr('data-col'), 1);
				delete contentAreas[col.attr('data-content-id')];
				form.drawDynamicContent();
	    });
	});
	
	$('#formContainer').on('click', '.addColBtn', function() {
	   var row = $(this).parents('.dynamicContentRow'),
		parentArea = $(this).parents('.dynamicContentAreaContainer').attr('id');
	   
	   tempId++;
	   contentAreas[tempId] = "";
	   
	   model.form.content[parentArea][row.attr('data-row')].columns.push(tempId);
	   form.drawDynamicContent();
	});
	
	$('#formContainer').on('click', '.addRowBtn', function() {
	    var parentArea = $(this).parents('.dynamicContentAreaContainer').attr('id');
	    
	    tempId++;
	    contentAreas[tempId] = ""; 
	    
	    model.form.content[parentArea].push({columns: [tempId]});
	    form.drawDynamicContent();
	});
	
	$('#formContainer').on('change', '.rowStyleSelect', function() {
	    var parentArea = $(this).parents('.dynamicContentAreaContainer').attr('id'),
		rowInd = $(this).parents('.dynamicContentRow').attr('data-row');
	    
	    var selected = $(this).find('option:selected').val();
	    
	    model.form.content[parentArea][rowInd].style = selected;
	    form.drawDynamicContent();
	});

	$('#formContainer').on('click', '.editDynamicColBtn', function() {
	    var col = $(this).parents('.dynamicContentArea'),
		id = col.attr('data-content-id'),
		editorId = 'wysiwyg-' + id,
		parentArea = $(this).parents('.dynamicContentAreaContainer').attr('id');;
	    
	    
	    var out = $('<div class="wysiwygOverlay"></div>');
	    
	    var header = $('<header class="wysiwygOverlayHeader"><div class="pull-right"><button class="btn btn-primary wysiwygSaveBtn" type="button">Done</button><button class="btn btn-default wysiwygCancelBtn" type="button">Cancel</button></div></header>');
	    header.find('.btn')
		    .attr('data-editor-instance', editorId)
		    .attr('data-for-content-id', id)
		    .attr('data-parent-area', parentArea);
	    header.append('<h1>Editing Content</h1><div class="wysiwygOverlayStrap">Column ' + (Number(col.attr('data-col')) + 1) + ', Row ' + (Number(col.attr('data-parent-row')) + 1) + '</div>').appendTo(out);

	    out.append('<textarea id="'+ editorId + '" class="form-control"></textarea>');
	    out.find('#' + editorId).val(contentAreas[id]);

	    $('#formContainer').append(out);
	    console.log(editorId);
	    form.tinymceInit("#" + editorId);
	});
	
	$('#formContainer').on('click', '.wysiwygCancelBtn', function() {
	    var id = $(this).attr('data-for-content-id'),
		wysiwygId = $(this).attr('data-editor-instance');
		
	    tinymce.get(wysiwygId).destroy();
	    $('.wysiwygOverlay').remove();
	});
	
	$('#formContainer').on('click', '.wysiwygSaveBtn', function() {
	    var id = $(this).attr('data-for-content-id'),
		wysiwygId = $(this).attr('data-editor-instance'),
		parentArea = $(this).attr('data-parent-area');
		
	    tinymce.get(wysiwygId).save();
	    
	    contentAreas[id] = $('#' + wysiwygId).val();
	    form.drawDynamicContent(parentArea);
	    
	    tinymce.get(wysiwygId).destroy();
	    $('.wysiwygOverlay').remove();
	});
})();
