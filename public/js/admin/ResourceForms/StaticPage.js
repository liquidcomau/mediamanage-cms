(function() {
	
	var formConf = {
		resource: {
			type: 'static-page',
			alias: 'static page',
			endpoint: '/ajax/api/static-pages'
		},
		selector: '.resourceForm'
	};

	function StaticPageForm(conf) {
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			$('.formTypeHeading').text('Static Page');
		};
	}
	
	var spProto = StaticPageForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	mmng.getCurrentPage().form = new StaticPageForm(formConf);

})();