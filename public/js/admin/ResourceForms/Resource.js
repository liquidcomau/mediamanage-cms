"use strict";

(function() {
	
	var mmngModel = mmng.getCurrentPage().model;
	
	mmngModel.contentAreas = [];
	
	var formConf = {
		selector: '.resourceForm'
	};
	
	formConf.resource = {
		type: $(formConf.selector).attr('data-resource-type'),
		alias: $(formConf.selector).attr('data-resource-alias'),
		endpoint: $(formConf.selector).attr('data-resource-endpoint')
	};

	function ResourceForm(conf) {
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			$('.formTypeHeading').text(mmng.util.toTitleCase(conf.resource.alias));
			
			this.makeWysiwygs();
			this.saveTextareaContent();
			this.lastSavedSnapshot = this.snapshot(this._selector);

			if ($(".datetimepicker").length) {
				$(".datetimepicker").each(function() {
					var id = $(this).attr("id");
					$("#" + id).datetimepicker({
						language: "en"
					});
				});
			}

			if ($(".multiselect-sortable").length) {
				$(".multiselect-sortable").each(function() {
					var id = $(this).attr("id"),
						el = $("#" + id);

					var order = el.attr("data-initial-order");
					if (order) {
						order = JSON.parse(order);
						order.forEach(function(id) {
							el.find('option[value="' + id + '"]').appendTo(el);
						});
					}
					
					el.multiselect({
					});
				});
			}
		};
		
		this.save = function() {
			this.saveTextareaContent();
			
			var data = this.objectifyFormData($(this._selector).serializeArray());

			$(this._selector).find(":input").each(function() {
		
				var name = $(this).attr("name"),
					key;
				
				
				// match fields with names such as newsRecommendationIds[]
				if (typeof name !== "undefined" && name.match(/\[\]$/)) {
					key = name.replace(/\[\]$/, "");
					if (typeof data[key] === "undefined") {
						data[key] = [];
					}
				}
				
			});
			
			// fix for empty datetimes
			$(".datetimepicker input").each(function() {
				
				var name = $(this).attr("name");
				
				if (typeof data[name] !== "undefined") {
			   
				var half = data[name].slice(-2).toLowerCase() === "am" ? "a" : "A"; // "a" = AM, "A" = PM
			    var stamp = moment(data[name], "DD-MM-YYYY hh:mm " + half);
			    
			    if (stamp.isValid()) {
					data[name] = stamp.format("YYYY-MM-DD HH:mm:ss");
			    } else {
					delete data[name];
			    }
			}
				
				
			});
			
			mmng.progressIndicator.init(['Saving']);
			this.sendRequest(data);
		};
	}
	
	var rProto = ResourceForm.prototype = Object.create(mmng.ResourceForm.prototype);

	rProto.generateTextarea = function(conf) 
	{
		var label, 
			editor,
			content;
		
		if (typeof conf.alias !== 'undefined') {
			label = '<label class="optional">' + conf.alias + '</label>';
		} else {
			label = null;
		}
		
		content = typeof conf.content !== 'undefined' ? conf.content : ''; 
		
		var editor = '<textarea name="content[' + conf.name + ']" id="content-' + conf.name + '" class="form-control wysiwyg" rows="24" cols="80">' + content + '</textarea>';
		
		return ['<div class="form-group">', label, editor, '</div>'].join('');
	};
	
	rProto.makeWysiwygs = function()
	{
		var that = this;
		
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			that.tinymceInit('#' + el.attr('id'));
		});
	};
	
	rProto.saveTextareaContent = function()
	{
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			
			if (tinymce.get(el.attr('id'))) tinymce.get(el.attr('id')).save();
			mmngModel.contentAreas[i] = el.val();
		});
	};
	
	// overwriting snapshot functionality to include tinymce save
	rProto.snapshot = function(selector) 
	{
		if (!selector) selector = this._selector;
		
		var form = $(selector);
		
		var disabled = form.find(':input:disabled').removeAttr('disabled');

		this.saveTextareaContent();
		
		var snapshot = JSON.stringify(form.serializeArray());
		
		disabled.attr('disabled', 'disabled');
		return snapshot;
	}

	var form = mmng.getCurrentPage().form = new ResourceForm(formConf);
})();
