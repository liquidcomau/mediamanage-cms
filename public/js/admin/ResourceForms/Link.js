(function() {
	
	var formConf = {
		resource: {
			type: 'link',
			alias: 'link',
			endpoint: '/ajax/api/links'
		},
		selector: '.resourceForm'
	};

	function linkLabelsToContainers() {
		
		$('input.link-type').parents('.form-group').addClass('link-types').find('br').remove();
		
		var label, type;
		
		$('input.link-type').each(function(ind, el) {
			
			type = $(el).attr('value');
			
			$(el).addClass('link-type-' + type);
			
			label = $(el).parent('label');
			label.replaceWith([
			        '<section class="link-type-section link-type-' + type + '-section panel panel-default">',
			        '<div class="panel-heading font-bold link-type-header">' + label.html() + '</div>',
			        '<div class="panel-body"></div>',
			        '</section>'
			        ].join(''));
			
			
			// internal link select box
			
			// need to find where the up-to-date model is stored by jstree
			
			var pages = findPageNodes(mmng.getCurrentPage().model.tree).reduce(function(markup, page) {
				return markup + '<option value="' + page.value + '">' + page.text + '</option>';
			}, '<option value="">--- Please select Page ---</option>');
			
			$('.link-type-internal-section .panel-body').html([
			                                                   '<select class="pageId">' + pages + '</select>',
			                                                   ].join(''));
			
			
			// external link text field
			
			$('.link-type-external-section .panel-body').html([
			                                                   '<div class="input-group"><span class="input-group-addon">http://</span>',
			                                                   '<input type="text" class="form-control externalRef">',
			                                                   '</div>'
			                                                   ].join(''));
			
			if ($('[name="linkType"]:checked').val() === 'internal') {
				$('.pageId').val($('[name="reference"]').val());
			} else {
				$('.externalRef').val($('[name="reference"]').val());
			}
			
			$("select.pageId").select2();
		});
		
		toggleActiveLinkType();
	};
	
	function toggleActiveLinkType() {
		$('.link-type-section').removeClass('panel-info').addClass('panel-default').find(':input').attr('disabled', 'disabled');
		
		$('.link-type-section').has('input[type="radio"]:checked').removeClass('panel-default').addClass('panel-info').find(':input').removeAttr('disabled');
		
		setLinkReference();
	}
	
	function LinkForm(conf) {
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			$('.formTypeHeading').text('Link');

			// initialize containers
			linkLabelsToContainers();
		}
		
		this.save = function() {

			var referenceObj,
				linkType,
				discard = ['linkType',
			               'externalRef',
			               'pageId', // readding this later for consistency
			               'reference'
			               ];
			
			var data = $(this._selector).serializeArray().filter(function(obj) {
				if (obj.name === 'linkType') linkType = obj.value;
				if (obj.name === 'reference') referenceObj = obj;

				return discard.indexOf(obj.name) === -1;
			});
			
			data.push(linkType === 'internal' ? {name: 'pageId', value: referenceObj.value} : {name: 'href', value: referenceObj.value});
			
			mmng.progressIndicator.init(['Saving']);
			this.sendRequest(this.objectifyFormData(data));
		};
	}
	
	// get the page nodes from the model
	
	function findPageNodes(model) {
		var pages = model.filter(function(node) {
			return (node.data.resource.type === 'freeform-page' || node.data.resource.type === 'static-page');
		});
		
		return pages.map(function(page) {
			return {
				value: page.id,
				text: page.text
			};
		});
	}
	
	function setLinkReference() {
		var el = $('.link-type-section').has('input[type="radio"]:checked');
		
		if (el.hasClass('link-type-internal-section')) {
			$('[name="reference"]').val($('select.pageId').find('option:selected').val());
		} else if (el.hasClass('link-type-external-section')) {
			$('[name="reference"]').val($('.externalRef').val());
		}
	}
	
	var lfProto = LinkForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	var pageForm = mmng.getCurrentPage().form = new LinkForm(formConf);

	
	// events
	
	$(pageForm._selector).on('change', '.link-type', function() {
		toggleActiveLinkType();
	});
	
	$(pageForm._selector).on('click', '.link-type-header', function() {
		$(this).find('input[type="radio"]').prop('checked', true).trigger('change');
	});
	
	$(pageForm._selector).on('change', setLinkReference);
	
	$(pageForm._selector).on('keyup', '.externalRef', setLinkReference);
})();