(function() {
	
	var formConf = {
		resource: {
			type: 'fragment',
			alias: 'fragment',
			endpoint: '/ajax/api/menus'
		},
		selector: '.resourceForm'
	};

	//var form = new mmng.getConstructor('ResourceForm')(formConf);
	function MenuForm(conf) {
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			$('.formTypeHeading').text('Menu');
		}
	}
	
	var mfProto = MenuForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	mmng.getCurrentPage().form = new MenuForm(formConf);
	
})();