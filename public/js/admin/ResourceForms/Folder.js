(function() {
	
	var formConf = {
		resource: {
			type: 'fragment',
			alias: 'fragment',
			endpoint: '/ajax/api/tree/folders'
		},
		selector: '.resourceForm'
	};

	//var form = new mmng.getConstructor('ResourceForm')(formConf);
	function FolderForm(conf) {
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			$('.formTypeHeading').text('Folder');
		}
	}
	
	var ffProto = FolderForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	mmng.getCurrentPage().form = new FolderForm(formConf);
	
})();