"use strict";

(function() {
	var model = mmng.getCurrentPage().model;
	if (typeof model.form === 'undefined') model.form = {};
	model.contentAreas = [];
	
	var formConf = {
		selector: '.resourceForm'
	};
	
	formConf.resource = {
		type: $(formConf.selector).attr('data-resource-type'),
		alias: $(formConf.selector).attr('data-resource-alias'),
		endpoint: $(formConf.selector).attr('data-resource-endpoint')
	};

	function ListingForm(conf) {
		var that = this;
		
		mmng.ResourceForm.call(this, conf);
		
		this.init = function() {
			this.makeWysiwygs();
			this.saveTextareaContent();
			
			model.form.images = JSON.parse($('[name="images"]').val() || "[]").map(function(img) {
			    return new that.Image(img);
			});
			
			that.drawImages();
			$('.imagesArea').sortable({
			    update: function() {
					that.updateImagesFromForm();
			    }
			});
			
			$(".datetimepicker").each(function() {
				var id = $(this).attr("id");
				$("#" + id).datetimepicker({
					language: "en"
				});
			});
			
			initEventHandlers();
			this.lastSavedSnapshot = this.snapshot(this._selector);
		};
		
		this.save = function() {
		    mmng.progressIndicator.init(['Saving']);

			this.saveTextareaContent();

		    var data = this.objectifyFormData(JSON.parse(this.snapshot()));
		    
		    if (typeof data.publishedDate !== "undefined") {
			var half = data.publishedDate.slice(-2).toLowerCase() === "am" ? "a" : "A"; // "a" = AM, "A" = PM
			var stamp = moment(data.publishedDate, "DD-MM-YYYY hh:mm " + half);

			if (stamp.isValid()) {
			    data.publishedDate = stamp.format("YYYY-MM-DD HH:mm:ss");
			} else {
			    delete data.publishedDate;
			}
		    }
		    
		    this.sendRequest(data);
		};
	}
	
	var lp = ListingForm.prototype = Object.create(mmng.ResourceForm.prototype);
	
	lp.Image = function(conf) {
	    conf = conf ? conf : {};
		
		for (var k in conf) {
			this[k] = conf[k];
		}
	};
	
	lp._imageTemplate = $('[name="imageTemplate"]').length 
				? $('[name="imageTemplate"]').val() 
				: (['<li class="imgItem"><div class="row">',
				    '<div class="col-sm-8">',
				    '<div class="form-group"><label>Image</label><div class="input-group"><input type="text" class="form-control" name="imageSrc" />',
				    '<span class="input-group-btn"><button class="btn btn-default imageBrowseBtn" type="button">Browse</button></span>',
				    '</div></div>',
				    '<div class="form-group"><label><input type="radio" class="imgMain" name="imgMain">&nbsp; Main Image</label></div>',
				    '<div class="form-group"><button class="btn btn-xs btn-danger removeImageBtn">Remove</button></div>',
				    '</div>',
				    '<div class="col-sm-4 imgThumb"></div>',
				    '</div></li>'
				    ].join(''));
	
	lp.drawImages = function() {
	    var that = this;

	    $('.imagesArea').empty();
		console.log(JSON.stringify(model.form.images))
	    var mu = model.form.images.forEach(function(img, ind) {
			var st = $(that._imageTemplate).clone();
			st.attr("data-ind", ind);
			st.find("[name]").each(function() {
				var input =  $(this),
					name = input.attr("name");
				
				if (typeof img[name] !== "undefined") {
					switch (input.attr("type")) {
						case "checkbox":
						case "radio":
							input.prop("checked", img[name]);
							break;
						default:
							input.val(img[name]);
					}
					
					if (name === "src") {
						st.find('.imgThumb').html('<label>Preview</label><img src="/uploaded/' + img.src.trim() + '" style="width:100%" />');
					}
				}
				
				input.attr("data-ind", ind);
			});
			
			st.find('.removeImageBtn').attr('data-ind', ind);
			st.find('.imageBrowseBtn').attr('data-ind', ind);

			$('.imagesArea').append(st);
	    });
	};
	
	lp.updateImageFromForm = function(ind) {
	    var image = model.form.images[ind];

		$('[data-ind="' + ind + '"]').each(function() {
			var input =  $(this),
				name = input.attr("name"),
				value;
				
			switch (input.attr("type")) {
				case "checkbox":
				case "radio":
					value = input.prop("checked");
					break;
				default:
					value = input.val();
			}
			
			image[name] = value;
		});
	};
	
	lp.updateImagesFromForm = function() {
	    var that = this,
			conf = {},
			ind;

	    model.form.images = [];
	    $('.imagesArea .imgItem').each(function(i, el) {
			conf = {};
			
			ind = $(this).attr("data-ind");
			$('[data-ind="' + ind + '"]').each(function() {
				var input =  $(this),
					name = input.attr("name"),
					value;

				switch (input.attr("type")) {
					case "checkbox":
					case "radio":
						value = input.prop("checked");
						break;
					default:
						value = input.val();
				}
				conf[name] = value;
			});
			model.form.images.push(new that.Image(conf));
	    });
		this.drawImages();
	};
	
	lp.makeWysiwygs = function()
	{
		var that = this;
		
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			that.tinymceInit('#' + el.attr('id'));
		});
	};
	
	lp.saveTextareaContent = function()
	{
		$('.wysiwyg').each(function(i, el) {
			el = $(el);
			
			if (tinymce.get(el.attr('id'))) tinymce.get(el.attr('id')).save();
			model.contentAreas[i] = el.val();
		});
	};
	
	// overwriting snapshot functionality to include tinymce save and datepicker

	lp.snapshot = function(selector) 
	{
		this.saveTextareaContent();
		this.updateImagesFromForm();
		
	    if (!selector) selector = this._selector;

	    var form = $(selector);

	    var disabled = form.find(':input:disabled').removeAttr('disabled');

	    var formData = form.serializeArray();
	    formData.push({name: "images", value: model.form.images});

	    var snapshot = JSON.stringify(formData);

	    disabled.attr('disabled', 'disabled');
	    return snapshot;
	}

    function initEventHandlers() {
	    var form = mmng.getCurrentPage().form;

	    $(document).on('click', '.addImageBtn', function() {
		    model.form.images.push(new form.Image());
		    form.drawImages();
	    });

	    $(document).on('keyup', '.image input', function() {
		    form.updateSlideFromForm($(this).attr('data-ind'));
	    });

	    $(document).on('click', '.removeImageBtn', function() {
		    model.form.images.splice($(this).attr('data-ind'), 1);
		    form.drawImages();
	    });

	    $(document).on('click', '.imageBrowseBtn', function() {
		    var el = $('[name="src"][data-ind="' + $(this).attr('data-ind') + '"]');
		    moxman.browse({
			    multiple: false,
			    relative_urls : true,
			    document_base_url: '/uploaded/',
			    oninsert: function(data) {
				    el.val(data.files[0].url);
				    form.updateImageFromForm(el.attr('data-ind'));
				    form.drawImages();
			    },
			    title: 'File Manager'
		    });
	    });

	    $(document).on('click', '[name="imgItem"]', function() {
		    $('.imageBrowseBtn[data-ind="' + $(this).attr('data-ind') + '"]').trigger('click');
	    });

	    $(document).on('click', '.imgMain', function() {
		    form.updateImagesFromForm();
	    });
	}
	
	var form = mmng.getCurrentPage().form = new ListingForm(formConf);
	
})();
