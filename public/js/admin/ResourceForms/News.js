(function() {
	
	var formConf = {
		resource: {
			type: 'post',
			alias: 'news',
			endpoint: '/ajax/api/posts'
		}
	};

	//var form = new mmng.getConstructor('ResourceForm')(formConf);
	function NewsForm(conf) {
		//this.tinymceInit('#content');
		mmng.constructors.ResourceForm.call(this, conf);
		
		this.preInit = function() {
			alert("hello")
		};
	}
	
	var newsFP = NewsForm.prototype = Object.create(mmng.constructors.ResourceForm.prototype);
	
	mmngConf.currentPage = {
		form: (new NewsForm(formConf))
	};
	
})();