/**
 * "Refreshes" the default Bootstrap modal to an empty state.
 */
$.fn.clearModal = function() {
	this.find('.modal-title').empty();
	this.find('.modal-body').empty();
	this.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal">Close</button>');

	return this;
}

/**
 * Populates the default Bootstrap modal.
 * 
 * @param object obj: Allow for options title, body and footer.
 */
$.fn.populateModal = function(obj) {
	if (typeof obj !== 'undefined') {
		if (obj.hasOwnProperty('title')) {
			this.find('.modal-title').text(obj.title);
		}
		
		if (obj.hasOwnProperty('body')) {
			this.find('.modal-body').html(obj.body);
		}
		
		if (obj.hasOwnProperty('footer')) {
			this.find('.modal-footer').html(obj.footer);
		}
	}
	
	return this;
};

(function() {
	
	//allowing regex in selectors
	// http://james.padolsey.com/javascript/regex-selector-for-jquery/
	jQuery.expr[':'].regex = function(elem, index, match) {
	    var matchParams = match[3].split(','),
	        validLabels = /^(data|css):/,
	        attr = {
	            method: matchParams[0].match(validLabels) ? 
	                        matchParams[0].split(':')[0] : 'attr',
	            property: matchParams.shift().replace(validLabels,'')
	        },
	        regexFlags = 'ig',
	        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
	    return regex.test(jQuery(elem)[attr.method](attr.property));
	}
	
	mmng.confirm = function(modalContent, callback) {
		$('#myModal').clearModal();
		
		modalContent.footer = '<button class="btn btn-primary modalConfirm">Okay <i class="fa"></i></button><button class="btn btn-default" data-dismiss="modal">Close</button>'
		
		$('#myModal').populateModal(modalContent);
		
		$('#myModal').off('click', '.modalConfirm');
		$('#myModal').on('click', '.modalConfirm', function() {
			callback();
			$('#myModal').modal('hide');
		});
		
		$('#myModal').modal();
	};

	mmng.progressIndicator = {
			
		init: function(messages, speed) {
			$('.progressOverlay').show();
			
			$('.progressMsg').text(messages[0]);
			
			this._timer = window.setInterval(function() {
				messages.push(messages.shift());
				$('.progressMsg').text(messages[0]);
			}, speed || 2000);
		},

		destroy: function() {
			clearTimeout(this._timer);
			$('.progressMsg').text("");
			$('.progressOverlay').hide();
		},
		
		_timer: null,
			
	};
	
	mmng.PaginationCtrl = function(conf) {
	    if (typeof conf === 'object') {
		this.currentPage = conf.currentPage || this.currentPage;
		this.itemsPerPage = conf.itemsPerPage || this.itemsPerPage;
		this.totalItemsCount = conf.totalItemsCount || null;
	    } else {
		this.totalItemsCount = conf;
	    }
	    
	    this._calcChange();
	};
	
	mmng.PaginationCtrl.prototype.currentPage = 1;
	mmng.PaginationCtrl.prototype.itemsPerPage = 10;
	
	mmng.PaginationCtrl.prototype.setCurrentPage = function(pg) {
	    this.currentPage = Number(pg);
	    this._calcChange();
	    return this;
	};
	
	mmng.PaginationCtrl.prototype.setItemsPerPage = function(ct) {
	    this.itemsPerPage = ct;
	    this._calcChange();
	    return this;
	};
	
	mmng.PaginationCtrl.prototype.setTotalItemsCount = function(ct) {
	    this.totalItemsCount = ct;
	    this._calcChange();
	    return this;
	};
	
	mmng.PaginationCtrl.prototype._calcTotalPages = function() {
	    this.totalPages = Math.ceil(this.totalItemsCount / this.itemsPerPage);
	    return this;
	};
	
	mmng.PaginationCtrl.prototype._calcPrevPage = function() {
	    this.prevPage = this.currentPage - 1 > 0 ? this.currentPage - 1 : false;
	    return this;
	};
	
	mmng.PaginationCtrl.prototype._calcNextPage = function() {
	    this.nextPage = this.currentPage + 1 <= this.totalPages ? this.currentPage + 1 : false;
	    return this;
	};
	
	mmng.PaginationCtrl.prototype._calcChange = function() {
	    this._calcTotalPages()
		._calcPrevPage()
		._calcNextPage();
	    return this;
	};
	
	$('.fileManagerBtn').click(function() {
		moxman.browse({
			multiple: false,
			relative_urls : true,
			document_base_url: '/uploaded/',
			insert: false,
			title: 'File Manager'
		});
	});
	
})();