<?php
class Admin_FreeformPagesController extends Admin_Controller_Action {
	
	public function indexAction() {
		
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		$this->view->headScript()->appendFile('/js/admin/tables.js');

		$freeformPageManager = new Admin_Model_FreeformPageManager();
		$this->view->freeformPages = $freeformPageManager->getFreeformPages();
	}
	
	public function editAction() {
		
		$this->view->headLink()->appendStylesheet('/theme/js/select2/select2.css');
		$this->view->headLink()->appendStylesheet('/theme/js/select2/theme.css');
		$this->view->headLink()->appendStylesheet('/theme/js/datepicker/datepicker.css');
		$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');
		$this->view->headScript()->appendFile('/js/admin/editContent.js');
		$this->view->headScript()->appendFile('/theme/js/select2/select2.min.js');
		$this->view->headScript()->appendFile('/theme/js/datepicker/bootstrap-datepicker.js');
		
		$form = new Admin_Form_FreeformPage();
		
		$id = $this->_request->getParam('id');
		if ($id && $id !== 'new') {

			$api = new Admin_Api();
			
			$formData = Admin_Model_FreeformPage::getFreeformPage($id, null, true)->toArray();
			$form->populate($formData);
		}
		
		$this->view->form = $form;		
	}	
}