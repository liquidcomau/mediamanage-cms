<?php
class Admin_EditorController extends Admin_Controller_Action
{
	public function preDispatch()
	{
		parent::preDispatch();
		$this->_helper->layout()->setLayout("admin/editor");
		
		$this->view->headScript()->appendFile('/js/admin/jquery.dlmenu/modernizr.custom.js');
		$this->view->headScript()->appendFile('/js/admin/jquery.dlmenu/jquery.dlmenu.js');
		$this->view->headScript()->appendFile('/js/admin/editor.js');
    	$this->view->headLink()->appendStylesheet('/js/admin/jquery.dlmenu/css/component.css');
	}
	
	public function shortcodeInterfaceAction()
	{
		$this->view->shortcodeItems = [];
	}
}
