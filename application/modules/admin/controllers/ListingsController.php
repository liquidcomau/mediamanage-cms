<?php
class Admin_ListingsController extends Admin_Controller_Action
{
	// Index action
	public function indexAction()
    {
    	$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
    	$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
    		
    	$this->view->headScript()->appendFile('/js/admin/tables.js');
    	$this->view->headScript()->appendFile('/js/admin/listings.js');
    	
		$class = Admin_Factory::getClass("Model_Listing");
		$this->view->listings = $class::getAll(["published" => "all"]);
    }
    
    public function editAction()
    {
    	$this->view->form = $form = Admin_Factory::create("Form_Listing");
    	
    	foreach ($form->getAttrib('scripts') as $script) {
    		$this->view->headScript()->appendFile($script);
    	}
    	
    	if ($this->_request->getParam('id')) {
    		$form->setResourceEndpoint('listings');
    		$form->setResourceId($this->_request->getParam('id'));
    		
			$form->populateResource();
    	}
    }
}
