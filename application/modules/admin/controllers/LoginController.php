<?php
class Admin_LoginController extends Admin_Controller_Action
{
	/**
	 * Initialize object.
	 *
	 * @return void
	 */
	public function init() {
		$this->_logger = Admin_Util::getLogger();    
	}
	
	/**
	* Called before an action is dispatched by Zend_Controller_Dispatcher.
	*
	* @return void
	*/
	
	public function preDispatch()
	{
		$this->view->headLink()->appendStylesheet('/css/admin/style.css');
		
		$this->view->headScript()->appendFile('/theme/js/jquery.min.js');
		$this->view->headScript()->appendFile('/theme/js/bootstrap.js');
		$this->view->headScript()->appendFile('/theme/js/parsley/parsley.min.js');
		$this->view->headScript()->appendFile('/theme/js/parsley/parsley.extend.js');
	}
	
    public function indexAction()
    {
    	$this->view->headTitle("CMS - Login");
		$this->view->layout()->setLayout('admin/login');
    	
    	$request = $this->getRequest();
        $this->view->loginForm = new Admin_Form_Login();
		
		if ($_COOKIE["remember_me"]) {
			$this->view->loginForm->getElement("username")->setValue($_COOKIE["remember_me"]);
			$this->view->loginForm->getElement("remember")->setValue(1);
		}

        $this->view->message = "";
        if($request->isPost())
        {
            if($this->view->loginForm->isValid($request->getPost()))
            {
                $authAdapter = $this->getAuthAdapter();

                // get the username and password from the form
                $username = $this->view->loginForm->getValue('username');
                $password = $this->view->loginForm->getValue('password');

                // pass to the adapter the submitted username and password
                $authAdapter->setIdentity($username)
                            ->setCredential($password);

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);
                
                // is the user a valid one?
                if($result->isValid())
                {
					$remember = $this->view->loginForm->getValue('remember');
					if ($remember == true) {
						$year = time() + 31536000;
						setcookie('remember_me', $username, $year, '/');
					}
					elseif ($remember == false) {
						if(isset($_COOKIE['remember_me'])) {
							$past = time() - 100;
							setcookie('remember_me', 'gone', $past, '/');
						}
					}
					
                	// save member data to session (excluding password)
                    $userInfo = $authAdapter->getResultRowObject(null, 'password');

                    // the default storage is a session with namespace Zend_Auth
                    $authStorage = $auth->getStorage();
                    $authStorage->write($userInfo);
                    $auth = Zend_Auth::getInstance();
                    
                    // moxie manager login
		        	$_SESSION['isLoggedIn'] = true;
		        	$_SESSION['moxiemanager.filesystem.rootpath'] = realpath(APPLICATION_PATH . "/../public/uploaded"); // Set a root path for this use
                    
                    // log success
        			$name = $auth->getIdentity()->firstName;
                    $this->_logger->info($name . " has logged in");
                    
    				// redirect
    				$session = new Zend_Session_Namespace('login-check');
    				if ($auth->getIdentity()->role == Admin_Auth_Acl::ROLE_MEMBER) {
                    	$this->_redirect("/");
    				} else if ($session->redirect != null) {
                    	$this->_redirect($session->redirect);
                    } else {
                    	$this->_redirect('/admin/index');
                    }
                }
                else
                {
                	$this->_logger->warn("Failed login detected with username = " . $username . " & password = " . Admin_Util::obscurePassword($password));
                    $this->view->message = '<div class="alert alert-warning alert-block">
                    	<button data-dismiss="alert" class="close" type="button">&times;</button>
                    	<p>Failed Login Attempt</p>
                  		</div>';
                }
            }
        }
    }
    
    public function logoutAction()
    {
    	// moxie manager logout
    	$_SESSION['isLoggedIn'] = false;
    	
        // clear everything
        $auth = Zend_Auth::getInstance();
        $name = $auth->getIdentity()->firstName;
        Zend_Auth::getInstance()->clearIdentity();
        $this->_logger->info($name . " has logged out");
        $this->_redirect('/admin/login/index');
    }
    
    /**
     * Gets the adapter for authentication against a database table
     *
     * @return object
     */
    protected function getAuthAdapter()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('user')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password')
                    ->setCredentialTreatment('MD5(?)');
                    
        return $authAdapter;
    }
}
?>