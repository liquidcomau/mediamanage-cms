<?php
class Admin_NewsController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		
		$this->view->posts = Admin_Model_News::getAll(array('published_status' => 'all', "hidden" => "all", "order" => "createdDate DESC"));
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');

		$this->view->headScript()->appendFile("/js/admin/moment.js");
		
		$this->view->headLink()->appendStylesheet("/js/admin/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");
		$this->view->headScript()->appendFile("/js/admin/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");
		
		$this->view->headScript()->appendFile('/js/admin/parsley.js');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js');
		
		if (Zend_Registry::get("config")->cms->modules->news->recommendations) {
			$this->view->headLink()->appendStylesheet("/js/admin/jquery-ui-1.11.0/jquery-ui.min.css");
			$this->view->headLink()->appendStylesheet("/js/admin/multiselect/css/ui.multiselect.css");
			$this->view->headScript()->appendFile('/js/admin/jquery-ui-1.11.0/jquery-ui.min.js');
			$this->view->headScript()->appendFile('/js/admin/multiselect/js/ui.multiselect.js');
		}
		
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');
		
		$this->view->form = $form = Admin_Factory::create("Form_News");
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}

		if ($this->_request->getParam('id')) {
			$form->setResourceEndpoint('news');
			$form->setResourceId($this->_request->getParam('id'));
			$form->populateResource();
		}
	}
	
}
