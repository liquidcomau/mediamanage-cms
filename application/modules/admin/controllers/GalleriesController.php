<?php
class Admin_GalleriesController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		
		$this->view->galleries = Admin_Model_Gallery::getAll(["published" => "all"]);
		
		$class = Admin_Factory::getClass("Model_Gallery");
		$this->view->hasCategories = $class::hasCategories();
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');
		$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
		
		$this->view->headScript()->appendFile("/js/admin/moment.js");
		
		$this->view->headLink()->appendStylesheet("/js/admin/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");
		$this->view->headScript()->appendFile("/js/admin/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");
		
		$this->view->headScript()->appendFile('/js/admin/parsley.js');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js');
		
		$this->view->headScript()->appendFile('/js/admin/jqSortable.js');
		
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');
		
		$this->view->form = $form = Admin_Factory::create("Form_Gallery");
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}

		if ($this->_request->getParam('id')) {
			$form->setResourceId($this->_request->getParam('id'));
			$form->populateResource();
			
			$images = $form->getDefaults();
		} 		

		$this->view->images = $images ? json_encode($images) : '[]';
	}
	
}
