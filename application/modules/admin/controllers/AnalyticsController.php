<?php

class Admin_AnalyticsController extends Admin_Controller_Action
{	
    public function indexAction()
    {
    	$this->view->headTitle("CMS - Analytics");
    	
    	$this->view->headScript()->appendFile('/js/admin/moment.js');
    	$this->view->headScript()->appendFile('/js/admin/highcharts.js');
    	$this->view->headScript()->appendFile('/js/admin/analytics.js');
    	
    	$config = Zend_Registry::get(Admin_Constants::CONFIG)->settings;
    	$this->view->analytics = $config->analytics;
    	
    	$range = $this->_request->getParam('range');
		if (!$range || !in_array($range, array('week', 'month', 'year'))) $range = 'month';    	
    	
    	$this->view->gaResults = ($this->view->analytics) ? $this->view->analytics()->gaResults($range) : false;
    }
}