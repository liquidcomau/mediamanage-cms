<?php
class Admin_RedirectsController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		
		$redirect_class = Admin_Factory::getClass("Model_Redirect");
		$this->view->redirects = $redirect_class::getAll(array('published_status' => 'all'));
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');
		
		$this->view->form = $form = Admin_Factory::create("Form_Redirect");
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}

		
		
		if ($this->_request->getParam('id')) {
			$form->setResourceEndpoint('redirects');
			$form->setResourceId($this->_request->getParam('id'));
			$form->populateResource();
		}
	}
}
