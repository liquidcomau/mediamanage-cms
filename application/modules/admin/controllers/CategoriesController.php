<?php
class Admin_CategoriesController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		$this->view->headScript()->appendFile('/js/admin/posts.js');
		
		$categoryIndex = array_column(Admin_Model_Category::getAll(null, Admin_Constants::TYPE_ARRAY), null , "id");

		// closure to populate children of the category index... using the category index
		$populateChildren = function ($category) use (&$populateChildren, &$categoryIndex) {
			if (isset($category["childCategoryIds"]) && count($category["childCategoryIds"])) {
				$category = $categoryIndex[$category["id"]];
				
				$category["children"] = array_map(function ($id) use (&$categoryIndex, &$populateChildren) {
					$child = $categoryIndex[$id];
					if (isset($child["childCategoryIds"])) {
						$child["children"] = $populateChildren($child);
					}
					return $child;
				}, $category["childCategoryIds"]);
			}
			return $category;
		};
		
		// use above function to fill references to children
		$categories = array_map(function ($category) use ($populateChildren) {
			return $populateChildren($category);
		}, $categoryIndex);
		
		// send the root-level references to the front
		$this->view->categories = array_filter($categories, function($category) {
			return  (!isset($category["parentCategoryId	"]));
		});
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');

		$this->view->headScript()->appendFile("/js/admin/moment.js");
		
		$this->view->headLink()->appendStylesheet("/js/admin/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");
		$this->view->headScript()->appendFile("/js/admin/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");
		
		$this->view->headScript()->appendFile('/js/admin/parsley.js');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js');
		
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');
		
		$this->view->form = $form = Admin_Factory::create("Form_News");
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}

		if ($this->_request->getParam('id')) {
			$form->setResourceEndpoint('news');
			$form->setResourceId($this->_request->getParam('id'));
			$form->populateResource();
		}
	}
	
}
