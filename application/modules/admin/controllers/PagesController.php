<?php
class Admin_PagesController extends Admin_Controller_Action
{
	public function preDispatch()
	{
		// $this->_request->getActionName() differs from $this->getParam('action').... it all makes sense now
	
		$ie = preg_match('/(?i)msie [1-9]/',$_SERVER['HTTP_USER_AGENT']);
		
		if ($this->_request->getActionName() === 'edit' && ($ie || !$this->_request->getHeader('X-PJAX'))) {
			$this->forward('index');
		}

		parent::preDispatch();
	}
	public function indexAction()
	{
		$this->view->headLink()->appendStylesheet('/js/admin/jstree/dist/themes/default/style.min.css');
		$this->view->headLink()->appendStylesheet('/theme/js/select2/select2.css');
		$this->view->headLink()->appendStylesheet('/theme/js/datepicker/datepicker.css');
		
		$this->view->headScript()->appendFile('/js/admin/jstree/dist/jstree.js', 'text/javascript');
		$this->view->headScript()->appendFile('/js/admin/pjax.js', 'text/javascript');
		$this->view->headScript()->appendFile('/js/admin/parsley.js', 'text/javascript');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js', 'text/javascript');
		
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js', 'text/javascript');
		$this->view->headScript()->appendFile('/js/admin/bootstrap-datepicker.js', 'text/javascript');

		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js', 'text/javascript');
		$this->view->headScript()->appendFile('/js/admin/pages.js', 'text/javascript');
		
		$nodes = json_decode(Admin_Api::get('tree')->getBody(), true);

		// remove the 'root' node from the tree, treat parent=0 as top level.
		$this->view->tree = array_values(array_filter($nodes, function($node) {
			return $node['id'] != 0;
		}));
		
		$this->view->treeObjects = $this->readTreeObjects();
		
		if ($this->_request->getParam("action") === "edit" && $this->_request->getParam("type"))
		{
			$this->view->form = $form = $this->buildForm($this->_request->getParam("type"), $this->_request->getParam('id'));
			
			foreach ($form->getAttrib('scripts') as $script) {
				$this->view->headScript()->appendFile($script);
			}
		}
	}
	
	public function editAction()
	{
		try {
			$this->_helper->layout()->disableLayout();
			$type = $this->_request->getParam('type');
	
			$this->view->form = $this->buildForm($type, $this->_request->getParam('id'));
		} catch (Exception $e) {
			$this->getResponse()->setHttpResponseCode(500);
			echo $e->getMessage();
		}
	}
	
	private function readTreeObjects()
	{
		$appFile = APPLICATION_PATH . '/config/treeObjects.json'; 
		$mmFile = MEDIAMANAGE_ENV . '/application/config/treeObjects.json';
		
		$file = file_exists($appFile) ? $appFile : $mmFile;
		$file = file_get_contents($file);

		if (!$file) throw new Exception("Tree objects missing");

		return $file;
	}
	
	protected function buildForm($type, $id = null)
	{
		$name = 'Form_' . array_reduce(explode('-', $type), function($name, $part) {
			return $name . ucfirst($part);
		}, '');

		$form = Admin_Factory::create($name);

		switch ($type) {
			case 'freeform-page':
				$form->setResourceEndpoint('freeform-pages');
				break;
			case 'static-page': 
				$form->setResourceEndpoint('static-pages');
				break;
			case 'folder':
				$form->setResourceEndpoint('tree/folders');
				break;
		}
		if ($id) {
			$form->setResourceId($id);
			$form->populateResource();
		} elseif ($form instanceof Admin_Form_MasterAbstract) {
			$parentId = $this->_request->getParam('parentId') ? $this->_request->getParam('parentId') : 0;
			$form->getMeta()->parentId->setValue($this->_request->getParam('parentId'));
		}
		
		return $form;
	}
}
