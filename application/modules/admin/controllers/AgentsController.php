<?php
class Admin_AgentsController extends Admin_Controller_Action
{
	// Index action
	public function indexAction()
    {
    	$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
    	$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
    		
    	$this->view->headScript()->appendFile('/js/admin/tables.js');
    		
//    	$this->view->agents = json_decode(Admin_Api::get('agents')->getBody(), true);
//    	$this->view->agencies = json_decode(Admin_Api::get('agencies')->getBody(), true);
		
		// replace above with direct database calls
		
		$agentClass = Admin_Factory::getClass("Model_Agent");
		$agencyClass = Admin_Factory::getClass("Model_Agency");
		
		$this->view->agents = $agentClass::getAll();
		$this->view->agencies = $agencyClass::getAll();
    }
    
    public function editAgentAction()
    {
		$this->view->form = $form = Admin_Factory::create("Form_Agent");
		if (!$form) throw new Exception("Nothing to edit");
    	
    	foreach ($form->getAttrib('scripts') as $script) {
    		$this->view->headScript()->appendFile($script);
    	}
    	
    	if ($this->_request->getParam('id')) {
    		$form->setResourceId($this->_request->getParam('id'));
    		$form->populateResource();
    	}
    }

    public function editAgencyAction()
    {
		$this->view->form = $form = Admin_Factory::create("Form_Agency");
		if (!$form) throw new Exception("Nothing to edit");
    	
    	foreach ($form->getAttrib('scripts') as $script) {
    		$this->view->headScript()->appendFile($script);
    	}
    	
    	if ($this->_request->getParam('id')) {
    		$form->setResourceId($this->_request->getParam('id'));
    		$form->populateResource();
    	}
    }
}
