<?php
class Admin_StaticPagesController extends Admin_Controller_Action {
	
	public function indexAction() {
		
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		$this->view->headScript()->appendFile('/js/admin/tables.js');

		$staticPageManager = new Admin_Model_StaticPageManager();
		$this->view->staticPages = $staticPageManager->getStaticPages();
	}
	
	public function editAction() {
		
		$this->view->headLink()->appendStylesheet('/theme/js/select2/select2.css');
		$this->view->headLink()->appendStylesheet('/theme/js/select2/theme.css');

		$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');
		$this->view->headScript()->appendFile('/js/admin/editContent.js');
		$this->view->headScript()->appendFile('/theme/js/select2/select2.min.js');
		
		$form = new Admin_Form_StaticPage();
		
		$id = $this->_request->getParam('id');
		
		if ($id && $id !== 'new') {
			$formData = Admin_Model_StaticPage::getStaticPage($id, null, true)->toArray();
			$form->populate($formData);
		}

		$this->view->form = $form;
	}	
}