<?php
class Admin_UsersController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		$this->view->headScript()->appendFile('/js/admin/posts.js');
		
		$this->view->users = $users = Admin_Model_User::getAll();
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');
		$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/parsley.js');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js');
		
		$this->view->headScript()->appendFile('/js/admin/jqSortable.js');
		
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');
		
//		$this->view->form = $form = new Admin_Form_User();
		$this->view->form = $form = Admin_Factory::create("Form_User");
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}

		if ($this->_request->getParam('id')) {
			$form->setResourceEndpoint('users');
			$form->setResourceId($this->_request->getParam('id'));
			$form->populateResource();
		}
	}
	
}