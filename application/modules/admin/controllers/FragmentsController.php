<?php
class Admin_FragmentsController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		$this->view->headScript()->appendFile('/js/admin/posts.js');
		
		$class = Admin_Factory::getClass("Model_Fragment");
		$this->view->fragments = $class::getAll();
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');

		$this->view->headScript()->appendFile('/js/admin/parsley.js');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js');
		
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');
		
		$this->view->form = $form = Admin_Factory::create("Form_Fragment");
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}

		if ($this->_request->getParam('id')) {
			$form->setResourceId($this->_request->getParam('id'));
			$form->populateResource();
		}
	}
	
}
