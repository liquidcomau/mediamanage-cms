<?php
class Admin_CollectionsController extends Admin_Controller_Action {
	
	public function indexAction() {
		$this->view->headLink()->appendStylesheet('/theme/js/datatables/datatables.css');
		$this->view->headScript()->appendFile('/theme/js/datatables/jquery.dataTables.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/tables.js');
		
		$this->view->collections = Admin_Model_Collection::getAll();

	}
	
	public function editAction() {
		$this->view->headLink()->prependStylesheet('/js/admin/jquery-ui-1.11.0/jquery-ui.min.css');
		$this->view->headLink()->prependStylesheet('/js/admin/multiselect/css/ui.multiselect.css');
		
		$this->view->headScript()->appendFile('/theme/js/tinymce/tinymce.min.js');
		$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
		
		$this->view->headScript()->appendFile('/js/admin/parsley.js');
		$this->view->headScript()->appendFile('/js/admin/manageParsley.js');
		
		$this->view->headScript()->appendFile('/js/admin/jquery-ui-1.11.0/jquery-ui.min.js');
		$this->view->headScript()->appendFile('/js/admin/multiselect/js/ui.multiselect.js');
		
		$this->view->headScript()->appendFile('/js/admin/ResourceForm.js');

		$id = $this->_request->getParam("id");
		if ($id) {
			$class = Admin_Factory::getClass("Model_Collection");
			$data = $class::get($id)->toArray();
			
			$form = Admin_Factory::create("Form_Collection", [$data["entityName"]]);
			$form->setResourceId($id);
			$form->populate($data);
		} elseif ($this->_request->getParam("entity")) {
			$form = Admin_Factory::create("Form_Collection", [$this->_request->getParam("entity")]);
		}
		
		$this->view->form = $form;
		
		foreach ($form->getAttrib('scripts') as $script) {
			$this->view->headScript()->appendFile($script);
		}
	}
	
}
