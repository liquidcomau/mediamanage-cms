<?php
class Api_TreeController extends Admin_Rest_Controller
{  
    /**
     * Returns all nodes in tree structure
     * 
     * Usage: GET http://<hostname>/api/pages
     */ 
    public function indexAction()
    {
    	try {
    		$response = Admin_Model_Tree::get()->getPages();
    	} catch (Exception $e) {
    		$response = array('error' => $e->getMessage());
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->_helper->json($response);
    }
    
    /**
     * Returns a single node with matching id
     * 
     * Usage: GET http://<hostname>/api/pages/:id
     */  
    public function getAction()
    {
    	try {
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->_helper->json($response);
	}
    
	/**
     * Creates a new node
     * 
     * Usage: POST http://<hostname>/api/tree
     */  
    public function postAction() {
    	try {
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->_helper->json($response);
    }
    
 	/**
     * Updates an existing node
     *
     * Usage: PUT http://<hostname>/api/tree/:id
     */  
    public function putAction() {
    	try {
    		$response = Admin_Model_Tree::updatePage($this->_params);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->_helper->json($response);
    }
    
    /**
     * Deletes an existing node
     *
     * Usage: DELETE http://<hostname>/api/tree/:id
     */  
    public function deleteAction() {
		try {
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->_helper->json($response);
    }
	
}