<?php
class Api_MenuController extends Admin_Rest_Controller
{  
    /**
     * Returns all menus
     * 
     * Usage: GET http://<hostname>/api/menus
     */ 
    public function indexAction()
    {
    	if ($this->_params['type'] === 'main') return $this->getMainMenu();
    	
    	try {
    		$response = Admin_Model_Menu::getAll($this->_params, Admin_Constants::TYPE_ARRAY);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
    /**
     * Returns a single menu with matching id
     * 
     * Usage: GET http://<hostname>/api/menus/:id
     */  
    public function getAction()
    {
    	try {
    		$response = Admin_Model_Menu::get($this->_params["id"], Admin_Constants::TYPE_ARRAY);
    		if (!$response) $this->getResponse()->setHttpResponseCode(404);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->send($response);
	}
	
	public function postAction()
	{
		try {
			$instance = new Admin_Model_Menu($this->_params);
			$id = $instance->create();
			
			$response = Admin_Model_Menu::get($id)->toArray();
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}
	
	public function putAction()
	{
		try {
			$instance = new Admin_Model_Menu($this->_params);
			$instance->update();
			$response = Admin_Model_Menu::get($this->_params['id']);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}

	/**
     * Deletes an existing menu from master table
     *
     * Usage: DELETE http://<hostname>/api/menus/:id
     */  
    public function deleteAction() {
    	try {
    		$reponse = Admin_Model_Menu::deleteById($this->_params['id']);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
    public function getMainMenu()
    {
    	try {
			$class = Admin_Factory::getClass("Model_Menu");
    		$response = $class::getMainMenu();
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
}
