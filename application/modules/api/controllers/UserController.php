<?php
class Api_UserController extends Admin_Rest_Controller
{  
    /**
     * Returns all users
     * 
     * Usage: GET http://<hostname>/api/users
     */ 
    public function indexAction()
    {
    	try {
    		$response = Admin_Model_User::getAll(null, Admin_Constants::TYPE_ARRAY);
    		$response = array_map(function($user) {
				unset($user['password']);
				unset($user['token']);
    			return $user;
    		}, $response);
    	} catch (Exception $e) {
    		$response = array('error' => $e->getMessage());
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
    /**
     * Returns a single member with matching id
     * 
     * Usage: GET http://<hostname>/api/users/:userid
     */  
    public function getAction()
    {
    	try {
			$user_class = Admin_Factory::getClass("Model_User");
    		$response = $user_class::get($this->_params["id"], Admin_Constants::TYPE_ARRAY);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->_helper->json($response);
	}
    
	/**
     * Creates a new member
     * 
     * Usage: POST http://<hostname>/api/users
     */  
    public function postAction() {
    	try {
    		$this->verifyPasswordChange();
			$instance = Admin_Factory::create("Model_User", [$this->_params]);
    		$instance->setId($instance->create());
    		$response = $instance->toArray();
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
 	/**
     * Updates an existing member
     *
     * Usage: PUT http://<hostname>/api/users/:userid
     */  
    public function putAction() {
    	try {
    		$this->verifyPasswordChange();
    		
//			$instance = new Admin_Model_User($this->_params);
			$user_class = Admin_Factory::getClass("Model_User");
			$instance = new $user_class($this->_params);
			$instance->setId($this->_params['id'])->update();
			
			$response = $user_class::get($this->_params['id']);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
    }
    
    /**
     * Deletes an existing member
     *
     * Usage: DELETE http://<hostname>/api/users/:userid
     */  
    public function deleteAction() {
		try {
	    	$id = $this->_params['id'];
	    	$response = Admin_Model_User::delete($id);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->_helper->json($response);
    }
    
    public function loginAction()
    {
    	Admin_Logger::dump($this->_params());
    }
	
    public function verifyPasswordChange()
    {
    	if (isset($this->_params['password'])) unset($this->_params['password']);
    	
    	if (isset($this->_params['newPassword']) && $this->_params['newPassword']) {
    		if ($this->_params['newPassword'] !== $this->_params['verifyPassword']) {
    			throw new Exception('New password must match verify password field');
    		} else {
    			$this->_params['password'] = $this->_params['newPassword'];
    		}
    	}
    }
}