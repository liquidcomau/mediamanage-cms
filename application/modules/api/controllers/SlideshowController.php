<?php
class Api_SlideshowController extends Admin_Rest_Controller
{  
    /**
     * Returns all posts
     * 
     * Usage: GET http://<hostname>/api/posts
     */ 
    public function indexAction()
    {
    	try {
    		$response = array_map(function($model) {
    			return $model->toArray();
    		}, Admin_Model_Slideshow::getAll());
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
    /**
     * Returns a single post with matching id
     * 
     * Usage: GET http://<hostname>/api/posts/:postid
     */  
    public function getAction()
    {
    	try {
    		$response = Admin_Model_Slideshow::get($this->_params['id'])->toArray();
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->send($response);
	}
	
	public function postAction()
	{
		try {
			$instance = new Admin_Model_Slideshow($this->_params);
			
			$instance->create();
			
			$response = Admin_Model_Slideshow::get($this->_params['id']);
		} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->send($response);
	}
	
	public function putAction()
	{	
		try {
			$instance = new Admin_Model_Slideshow($this->_params);
			$instance->setId($this->_params['id'])->update();
				
			$response = Admin_Model_Slideshow::get($this->_params['id']);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}

	/**
     * Deletes an existing post
     *
     * Usage: DELETE http://<hostname>/api/posts/:postid
     */  
    public function deleteAction() 
    {
    	try {
    		$reponse = Admin_Model_Slideshow::deleteById($this->_params['id']);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
}