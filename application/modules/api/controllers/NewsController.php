<?php
class Api_NewsController extends Admin_Api_ResourceController
{  
	public function init()
	{
		parent::init();
		
		// retrieve endpoint-specific clauses from params and unset if we don't 
		// want to build model with those keys in POST/PUT methods.
		$clauses = ["category", "year", "month", "hidden"];
		$this->_clauses = array_merge($this->_clauses, array_intersect_key($this->_params, array_flip($clauses)));
		
		unset($this->_params["category"]);
		unset($this->_params["year"]);
		unset($this->_params["month"]);
	}
	
    /**
     * Returns all posts
     * 
     * Usage: GET http://<hostname>/api/posts
     */ 
    public function indexAction()
    {
    	try {
			
			// prepare published date range clause
			if (isset($this->_clauses["month"]) || isset($this->_clauses["year"])) {
				
				if (isset($this->_clauses["year"])) {
					$year = $this->_clauses["year"];
				} else {
					$year = date("Y");
				}

				if (isset($this->_clauses["month"])) {
					$monthStart = $monthEnd = $this->_clauses["month"];
						
					$monthStart = strlen($monthStart) === 2 ? $monthStart : 0 . (string) $monthStart;
					$monthEnd = strlen($monthEnd) === 2 ? $monthEnd : 0 . (string) $monthEnd;
					
					$daysEnd = cal_days_in_month(CAL_GREGORIAN, $this->_clauses["month"], $year);
				} else {
					$monthStart = "01";
					$monthEnd = "12";
					
					$daysEnd = "31";
				}
				
				$start = implode("-", [$year, $monthStart, "01"]) . " 00:00:00";
				$end = implode("-", [$year, $monthEnd, $daysEnd]) . " 23:59:59";
				
				$this->_clauses["publishedDateRange"] = [$start, $end];
				
				unset($this->_clauses["month"]);
				unset($this->_clauses["year"]);
			}

			$newsClass = Admin_Factory::getClass("Model_News");
    		$response = array_map(function($model) {
    			$data = $model->toArray();
				$data["description"] = $model->getSnippet();
				
				unset($data["content"]);
				
				return $data;
    		}, $newsClass::getAll($this->_clauses));
			
			$totalCount = $newsClass::countTotal($this->_clauses);
			$this->getResponse()->setHeader("X-Total-Count", $totalCount);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
}
