<?php
class Api_CollectionController extends Admin_Api_ResourceController
{
	public function indexAction()
	{
		try {
			$model = $this->_model;
			$response = array_map(function($instance) {
				unset($instance["entityIds"]);
				unset($instance["entities"]);
				
				return $instance;
			}, $model::getAll(null, Admin_Constants::TYPE_ARRAY));
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}
	
	public function postAction()
	{
		$model = $this->_model;
		try {
			$props = $this->_params;
			unset($props["entityName"]);
			
			$instance = new $model($this->_params["entityName"], $props);

			$id = $instance->create();
			
			$response = $model::get($id, Admin_Constants::TYPE_ARRAY);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}
	
	public function putAction()
	{
		$model = $this->_model;
		try {
			$props = $this->_params;
			unset($props["entityName"]);
			
			$instance = new $model($this->_params["entityName"], $props);
			
			$instance->update();
			$response = $model::get($this->_params['id']);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}
}