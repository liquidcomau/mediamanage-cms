<?php
class Api_FolderController extends Admin_Rest_Controller
{  
    /**
     * Returns all folders
     * 
     * Usage: GET http://<hostname>/api/folders
     */ 
    public function indexAction()
    {
    	try {
    		$response = Admin_Model_Folder::getAll($this->_params, Admin_Constants::TYPE_ARRAY);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
    
    /**
     * Returns a single folder with matching id
     * 
     * Usage: GET http://<hostname>/api/folders/:id
     */  
    public function getAction()
    {
    	try {
    		$response = Admin_Model_Folder::get($this->_params["id"], Admin_Constants::TYPE_ARRAY);
    		if (!$response) $this->getResponse()->setHttpResponseCode(404);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->send($response);
	}
	
	public function postAction()
	{
		try {
			$instance = new Admin_Model_Folder($this->_params);
			$id = $instance->create();
			
			$response = Admin_Model_Folder::get($id)->toArray();
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}
	
	public function putAction()
	{
		try {
			$instance = new Admin_Model_Folder($this->_params);
			$instance->update();
			$response = Admin_Model_Folder::get($this->_params['id']);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}

	/**
     * Deletes an existing folder from master & seo table
     *
     * Usage: DELETE http://<hostname>/api/folders/:id
     */  
    public function deleteAction() {
    	try {
    		$reponse = Admin_Model_Folder::deleteById($this->_params['id']);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
}