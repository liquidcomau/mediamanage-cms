<?php
class Rest_IndexController extends Zend_Controller_Action
{
    /**
     * Zend Rest Server.
     *
     * @var Zend_Rest_Server
     */
    protected $_server;
    
    /**
     * Service name.
     *
     * @var string
     */
    protected $_service;
    
    /**
     * Method name.
     *
     * @var string
     */
    protected $_method;

    /** 
     * Pre-dispatch routines.
     *
     * @throws Zend_Rest_Server_Exception
     * @return void
     */
    public function preDispatch()
    {
        parent::preDispatch();
    }
        
    /**
     * Index action.
     *
     * @return void
     */
    public function indexAction()
    {   
    	$logger = Zend_Registry::get('logger');
    	
    	if (!$this->_service = $this->_request->getParam('service', false)) {
            throw new Zend_Rest_Server_Exception("No Service Specified.");
        }
    
        if (!$this->_method = $this->_request->getParam('method', false)) {
            throw new Zend_Rest_Server_Exception("No Method Specified.");
        }
        
        $config = Zend_Registry::get('config');
        $autoloadernamespaces = $config->autoloadernamespaces->toArray();
        
        $serviceName = Zend_Filter::filterStatic($this->_service, 'DashToCamelCase', array(), 'Zend_Filter_Word');
        $className = (class_exists('Project_Rest_Server_'.$serviceName)) ? 'Project_Rest_Server_'.$serviceName : 'Admin_Rest_Server_'.$serviceName; // map service name uri to class name 
        
        $methodName = Zend_Filter::filterStatic($this->_method, 'DashToCamelCase', array(), 'Zend_Filter_Word');
        $methodName = strtolower(substr($methodName, 0, 1)).substr($methodName, 1); // note lcfirst() only supported in PHP >= 5.3.0
        
        if (!class_exists($className)) {
        	$logger->err("Unknown Service Class ".$className);
            throw new Zend_Rest_Server_Exception("Unknown Service '$this->_service'.");
        }
        
        $_REQUEST['method'] = $methodName; // Zend_Rest_Server uses $_REQUEST array
        
        $this->_server = new Zend_Rest_Server(); // initialize server
        $this->_server->setClass($className);
        $this->_server->handle();
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }     
}