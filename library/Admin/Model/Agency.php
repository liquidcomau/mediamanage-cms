<?php
class Admin_Model_Agency extends Admin_Model_Resource
{
	protected $_id = null;
	protected $_name = null;
	protected $_email = null;
	protected $_address = null;
	protected $_logo = null;
	protected $_website = null;

	public function getId()
	{
		return $this->_id;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function getEmail()
	{
		return $this->_email;
	}

	public function getAddress()
	{
		return $this->_address;
	}

	public function getLogo()
	{
		return $this->_logo;
	}

	public function getWebsite()
	{
		return $this->_website;
	}

	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}

	public function setName($name)
	{
		$this->_name = $name;
		return $this;
	}

	public function setEmail($email)
	{
		$this->_email = $email;
		return $this;
	}

	public function setAddress($address)
	{
		$this->_address = $address;
		return $this;
	}

	public function setLogo($logo)
	{
		$this->_logo = $logo;
		return $this;
	}

	public function setWebsite($website)
	{
		$this->_website = $website;
		return $this;
	}

}
