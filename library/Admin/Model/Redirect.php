<?php
class Admin_Model_Redirect extends Admin_Model_Resource
{
	protected $_objectName = 'redirect';

	protected $_id = null;
	protected $_url = null;
	protected $_destination = null;
	protected $_type = null;
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function getUrl()
	{
		return $this->_url;
	}
	
	public function getDestination()
	{
		return $this->_destination;
	}
	
	public function getType()
	{
		return $this->_type;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setUrl($url)
	{
		$this->_url = $url;
		return $this;
	}
	
	public function setDestination($destination)
	{
		$this->_destination = $destination;
		return $this;
	}
	
	public function setType($type)
	{
		$this->_type = $type;
		return $this;
	}
}
