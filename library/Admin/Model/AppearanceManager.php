<?php
class Admin_Model_AppearanceManager extends Admin_Model_AbstractManager
{
	/**
	 * Get Appearance Settings via RESTful API.
	 *
	 * @return Admin_Model_Appearance object
	 */
	public function getSettings()
	{
		$response = $this->_api->request("settings/appearance", Zend_Http_Client::GET);
		if ($response && $response->isSuccessful()) {
			$objectData = Zend_Json::decode($response->getBody());
			$object = new Admin_Model_Appearance($objectData);
			return $object;
		} 
		return $response;
	}
}
