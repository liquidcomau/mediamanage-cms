<?php
class Admin_Model_Fragment extends Admin_Model_Resource
{
	protected $_id = null;
	protected $_internalLabel = null;
	protected $_identifier = null;
	protected $_content = null;
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function getInternalLabel()
	{
		return $this->_internalLabel;
	}
	
	public function getIdentifier()
	{
		return $this->_identifier;
	}
	
	public function getContent()
	{
		return $this->_content;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setInternalLabel($label)
	{
		$this->_internalLabel = $label;
		return $this;
	}
	
	public function setIdentifier($identifier)
	{
		$this->_identifier = $identifier;
		return $this;
	}
	
	public function setContent($content)
	{
		$this->_content = $content;
		return $this;
	}
}