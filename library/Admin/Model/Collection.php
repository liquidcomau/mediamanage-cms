<?php
class Admin_Model_Collection extends Admin_Model_Resource
{
	/**
	 * Variable so we can always get the extended static
	 * variable from static functions called outside of
	 * child class.
	 *
	 * @var string
	 */
	protected static $_projectClass = null;

	protected static function getProjectClass()
	{
		if (!self::$_projectClass) {
			self::$_projectClass = Admin_Factory::getClass("Model_Collection");
		}
		return self::$_projectClass;
	}

	/**
	 * Can only create collections of entities with these names.
	 * Corresponds to objectName in object_type table.
	 * 
	 * @var array 
	 */
	protected static $_availableEntities = [
		"static_page",
		"freeform_page"
	];

	public function getAvailableEntities()
	{
		$class = self::getProjectClass();
		return $class::$_availableEntities;
	}
	
	protected $_id = null;
	protected $_heading = null;
	protected $_identifier = null;
	protected $_entityName = null;
	protected $_entityIds = [];
	protected $_entities = [];
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function getHeading()
	{
		return $this->_heading;
	}
	
	public function getIdentifier()
	{
		return $this->_identifier;
	}
	
	public function getEntityName()
	{
		return $this->_entityName;
	}
	
	public function getEntityIds()
	{
		return $this->_entityIds;
	}
	
	public function getEntities()
	{
		if (!$this->_entityIds || !count($this->_entityIds)) {
			return null; // return null or empty array?
		}
		
		if (!$this->_entities || !count($this->_entities)) {
			$this->_entities = static::loadEntities($this->_entityName, $this->_entityIds);
		}
		
		return $this->_entities;
	}
	
	public function getEntitiesFilteredForDisplay()
	{
		return $this->filterEntitiesForDisplay($this->_entities);
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setHeading($heading)
	{
		$this->_heading = $heading;
		return $this;
	}
	
	public function setIdentifier($identifier)
	{
		$this->_identifier = $identifier;
		return $this;
	}
	
	public function setEntityName()
	{
		throw new Exception("Collection entity name must be set in constructor");
	}
	
	public function setEntityIds($ids)
	{
		$ids = (array) $ids;
		$this->_entityIds = $ids;
		
		return $this;
	}
	
	public function setEntities()
	{
		throw new Exception("Cannot manually set entities");
	}
	
	/**
	 * Describes the "modelName", "populate" methods and "displayFilter" methods of each available
	 * entity type. Would've been a property but can't have anonymous functions as array values
	 * in class properties.
	 * 
	 * The returned array should contain the keys:
	 * 
	 * "modelName"	key should simply be last part ie freeform_page = "FreeformPage", 
	 *				not "Admin_Model_FreeformPage".
	 * "load"		an anonymous function that takes argument (array) $ids to return
	 *				an array of entities that corresponds to those Ids. If no Ids are supplied,
	 *				return a generic set of entities.
	 * "filter"		an anonymous function that takes on argument $entity which returns true or false
	 *				depending on whether or not to show or hide the given entity 
	 *				(ie if published property = true)
	 * 
	 * @param string $entityName i.e. "freeform_page"
	 * @return array
	 */
	public static function describeEntity($entityName = null)
	{
		if (!in_array($entityName, static::$_availableEntities)) return;
		
		switch ($entityName) {
			case "static_page":
				$info = [
					"modelName" => "StaticPage",
					"load" => function($ids) {
						$class = Admin_Factory::getClass("Model_StaticPage"); 
						
						if ($ids && count($ids)) {
							return call_user_func_array([$class, "getAll"], [["in" => $ids]]);
						} else {
							return call_user_func([$class, "getAll"]);
						}
					},
					"displayFilter" => function ($instance) {
						if (is_object($instance)) $instance = $instance->toArray();
						return $instance["published"];
					}
				];
				break;
			default:
				$info = [];
		}
		
		return $info;
	}
	
	public function __construct($entityName, $properties) {
		if (!in_array($entityName, static::$_availableEntities)) {
			throw new Exception("Cannot create collection of type " . $entityName);
		}
		$this->_entityName = $entityName;
		
		parent::__construct($properties);
	}
	
	public static function getEntityModelName($entityName)
	{
		$class = self::getProjectClass();
		$info = $class::describeEntity($entityName);
		if (isset($info["modelName"])) {
			return "Model_" . $info["modelName"];
		} else {
			return "Model_" . Zend_Filter::filterStatic($entityName, "Word_UnderscoreToCamelCase");
		}
	}
	
	public static function getEntityAlias($entityName)
	{
		$info = static::describeEntity($entityName);
		if (isset($info["alias"])) {
			return $info["alias"];
		} else {
			return ucwords(str_replace("_", " ", $entityName));
		}
	}
	
	public static function loadEntities($entityName, $ids = null)
	{
		$class = self::getProjectClass();
		$info = $class::describeEntity($entityName);

		if (isset($info["load"])) {
			$populateMethod = $info["load"];
		} else {
			$populateMethod = function ($ids) use ($entityName, $class) {
				$entityClass = Admin_Factory::getClass($class::getEntityModelName($entityName));
				if (method_exists($entityClass, "getAll")) {
					if ($ids && count($ids)) {
						$entities = call_user_func_array([$entityClass, "getAll"], [["in" => $ids]]);
						// return in correct sort order
						return array_map(function($id) use ($entities) {

							// find correct entity if entities are object classes using a fold
							return array_reduce($entities, function ($carry, $entity) use ($id) {
								if (is_object($entity)) {
									return $entity->getId() == $id ? $entity : $carry;
								} else {
									return $entity["id"] == $id ? $entity : $carry;
								}
							}, array_shift($entities));
						}, $ids);
					} else {
						return call_user_func([$entityClass, "getAll"]);
					}
				} else {
					throw new Exception("Cannot populate entities - no get all method.");
				}
			};
		}
		return $populateMethod($ids);
	}
	
	public function filterEntitiesForDisplay($entities)
	{
		$info = static::describeEntity($this->_entityName);
		if (isset($info["displayFilter"])) {
			$filterMethod = $info["displayFilter"];
		} else {
			$filterMethod = function ($instance) {
				if (is_object($instance)) {
					$instance = $instance->toArray();
				}
				
				// default takes account of published property,
				// otherwise defaults to display = true
				
				if (isset($instance["published"])) {
					return $instance["published"];
				} else {
					return true;
				}
			};
		}
		
		return array_filter($entities, function ($entity) use ($filterMethod) {
			return $filterMethod($entity);
		});
	}
	
	public static function getByIdentifier($identifier)
	{
		return self::getMapper()->getByIdentifier($identifier);
	}
	
	public function toArray($filterDisplayed = true)
	{
		$modelArray = parent::toArray();
		if (count($modelArray["entities"])) {
			
			$entities = $filterDisplayed ? $this->filterEntitiesForDisplay($modelArray["entities"]) : $entities;
			
			$modelArray["entities"] = [];
			foreach ($entities as $entity) {
				if (is_object($entity)) {
					$entity = $entity->toArray();
				}
				$modelArray["entities"][$entity["id"]] = $entity;
			}
		}
		return $modelArray;
	}
}
