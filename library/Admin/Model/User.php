<?php
class Admin_Model_User extends Admin_Model_Resource
{
	protected $_id = null;
	protected $_userName = null;
	protected $_firstName = null;
	protected $_lastName = null;
	protected $_email = null;
	protected $_createdDate = null;
	protected $_lastModified = null;
	protected $_password = null;
	protected $_role = null;
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function getFirstName()
	{
		return $this->_firstName;
	}
	
	public function getLastName()
	{
		return $this->_lastName;
	}
	
	public function getUserName()
	{
		return $this->_userName;
	}
	
	public function getEmail()
	{
		return $this->_email;
	}
	
	public function getCreatedDate($ts)
	{
		return $ts === true ? strtotime($this->_createdDate) : $this->_createdDate;
	}
	
	public function getLastModified($ts)
	{
		return $ts === true ? strtotime($this->_lastModified) : $this->_lastModified;
	}
	
	public function getPassword()
	{
		return $this->_password;
	}
	
	public function getRole()
	{
		return $this->_role;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setFirstName($firstName)
	{
		$this->_firstName = $firstName;
		return $this;
	}
	
	public function setLastName($lastName)
	{
		$this->_lastName = $lastName;
		return $this;
	}
	
	public function setUserName($name)
	{
		$this->_userName = $name;
		return $this;
	}
	
	public function setEmail($email)
	{
		$this->_email = $email;
		return $this;
	}
	
	public function setCreatedDate($createdDate)
	{
		$this->_createdDate = $createdDate;
		return $this;
	}
	
	public function setLastModified($lastModified)
	{
		$this->_lastModified = $lastModified;
		return $this;
	}
	
	public function setPassword($password)
	{
		$this->_password = $password;
		return $this;
	}
	
	public function setRole($role)
	{
		$this->_role = $role;
		return $this;
	}
}