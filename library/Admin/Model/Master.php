<?php
abstract class Admin_Model_Master extends Admin_Model_Resource
{
	protected $_id = null;
	protected $_parentId = null;
	protected $_objectTypeId = null;
	protected $_order = null;
	protected $_heading = null;
	protected $_subheading = null;
	protected $_linkText = null;
	protected $_url = null;
	protected $_internalLabel;
	protected $_published = null;
	protected $_hidden = null;
	protected $_createdDate = null;
	protected $_lastModified = null;

	public function getId()
	{
		return $this->_id;
	}
	
	public function getObjectTypeId()
	{
		return $this->_objectTypeId;
	}
	
	public function getParentId()
	{
		return $this->_parentId;
	}
	
	public function getOrder()
	{
		return $this->_order;
	}
	
	public function getHeading()
	{
		return $this->_heading;
	}
	
	public function getSubheading()
	{
		return $this->_subheading;
	}
	
	public function getLinkText()
	{
		return $this->_linkText;
	}
	
	public function getUrl($fullUrl = false)
	{
		return $fullUrl ? self::fetchUrl($this->_id) : $this->_url;
	}
	
	public function getInternalLabel()
	{
		return $this->_internalLabel;
	}
	
	public function getPublished()
	{
		return $this->_published;
	}
	
	public function getHidden()
	{
		return $this->_hidden;
	}
	
	public function getCreatedDate($timestamp = false)
	{
		if ($timestamp) {
			return strtotime($this->_createdDate);	
		} else {
			return $this->_createdDate;
		}
	}
	
	public function getLastModifiedDate($timestamp = false)
	{
		if ($timestamp) {
			return strtotime($this->_lastModified);
		} else {
			return $this->_lastModified;
		}
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setParentId($parentId)
	{
		$this->_parentId = $parentId;
	}
	
	public function setObjectTypeId($id)
	{
		$this->_objectTypeId = $id;
	}
	
	public function setPosition($position)
	{
		$this->_position = $position;
	}
	
	public function setHeading($heading)
	{
		$this->_heading = $heading;
		return $this;
	}
	
	public function setSubheading($subheading)
	{
		$this->_subheading = $subheading;
		return $this;
	}

	public function setLinkText($linkText)
	{
		$this->_linkText = $linkText;
		return $this;
	}

	public function setPublished($published)
	{
		$this->_published = $published;
	}
	
	public function setHidden($hidden)
	{
		$this->_hidden = $hidden;
		return $this;
	}
	
	public function setUrl($url)
	{
		$this->_url = $url;
	}
	
	public function setInternalLabel($label)
	{
		$this->_internalLabel = $label;
		return $this;
	}
	
	public function setCreatedDate($createdDate)
	{
		$this->_createdDate = $createdDate;
	}
	
	public function setLastModified($lastModified)
	{
		$this->_lastModified = $lastModified;
	}
	
	public function __construct($data) 
	{
		parent::__construct($data);
		
		if ($this->_objectName) $this->loadObjectTypeId();
	}
	
	/**
	 * Returns only the properties of this class.
	 * 
	 * @param string $nullVals
	 * @return multitype:
	 */
	public function getMasterProperties($nullVals = false)
	{
		$props = $this->getProperties($nullVals);
		$names = preg_replace('/^_/', '', array_keys(get_class_vars(__CLASS__)));

		return array_intersect_key($props, array_flip($names));
	}
	
	/**
	 * Returns only the properties of the child class.
	 * 
	 * @param string $nullVals
	 * @return mixed|multitype:
	 */
	public function getOwnProperties($nullVals = false)
	{
		$chClass = new ReflectionClass(get_called_class());
		$names = array_reduce($chClass->getProperties(), function($props, $prop) {
			if ($prop->class === get_called_class()) $props[] = preg_replace('/^_/', '', $prop->name);
			return $props;
		}, array());

		return array_intersect_key($this->getProperties($nullVals), array_flip($names));
	}
	
	/**
	 * Returns a singleton instance of current object.
	 *
	 * @return object
	 */
	public static function getInstance()
	{
		return new self();
	}
	
	/**
	 * Returns an array of child objects
	 * 
	 * @param std_class $node
	 * @param Admin_DbTable $mainDb
	 */
	private function menuChildren($node, $mainDb) {
		$nodeArray = array();
		$count = 0;
		$nodeArray['id'] = $node->id;
		$nodeArray['heading'] = $node->heading;
		$nodeArray['url'] = $node->url;
		//$nodes = $mainDb->fetchActiveNavByParentID($node->id);
		$nodes = $mainDb->fetchPublishedNavByParentID($node->id);
		if ($nodes) {
			foreach ($nodes as $node) {
				$childArray[] = self::menuChildren($node, $mainDb);
				$count++;
			}
			if ($count > 0) {
				$nodeArray['children'] = $childArray;
			}
		}
		return $nodeArray;
	}
	
	/**
	 * Returns an array of master objects that exist in the main menu
	 * 
	 * @return array
	 */
	public static function getMainMenuLayout() {
		$nodesArray = array();
		$mainDb = new Admin_DbTable_Master();
		$nodes = $mainDb->fetchPublishedNavByParentID(0);
		foreach($nodes as $node) {
			$nodesArray[] = self::menuChildren($node, $mainDb);
		}
		$result = $nodesArray;
	
		return $result;
	}

	
	/**
	 * Returns an array of child objects
	 *
	 * @param std_class $node
	 * @param Admin_DbTable $mainDb
	 */
	private function adminChildren($node, $mainDb) {
		$nodeArray = array();
		$count = 0;
		$nodeArray['id'] = $node->id;
		$nodeArray['module'] = $node->module;
		$nodeArray['url'] = $node->url;
		$nodeArray['accessLevel'] = $node->accessLevel;
		$nodeArray['icon'] = $node->icon;
		$nodes = $mainDb->fetchByParentID($node->id);
		if ($nodes) {
			foreach ($nodes as $node) {
				$childArray[] = self::adminChildren($node, $mainDb);
				$count++;
			}
			if ($count > 0) {
				$nodeArray['children'] = $childArray;
			}
		}
		return $nodeArray;
	}
	
	/**
	 * Returns an array of layout_admin objects for use with mm_v3 menu
	 *
	 * @return array
	 */
	public static function getAdminMenuLayout() {
		$nodesArray = array();
		$mainDb = new Admin_DbTable_LayoutAdminMenu();
		$nodes = $mainDb->fetchByParentID(0);
		foreach($nodes as $node) {
			$nodesArray[] = self::adminChildren($node, $mainDb);
			//$nodesArray[] = self::menuChildren($node->id);
		}
		return $nodesArray;
	}
	
	/**
	 * Returns an array of master objects that exist in the footer menu
	 *
	 * @return array
	 */
	public static function getFooterMenuLayout() {
		/* @todo: error handling */
		$mainDb = new Admin_DbTable_LayoutFooterMenu();
		$menuItems = $mainDb->getAll();
		$result = (count($menuItems)) ? $menuItems->toArray() : array();
		return $result;
	}
	
	/**
	 * Returns an array of master objects that exist in the primary section
	 *
	 * @return array
	 */
	public static function getPrimaryLayout() {
		/* @todo: error handling */
		die("lol");
		$mainDb = new Admin_DbTable_LayoutPrimary();
		$items = $mainDb->getAll();
		$result = (count($items)) ? $items->toArray() : array();
		return $result;
	}
	
	/**
	 * Returns an array of master objects that exist in the sidebar
	 *
	 * @return array
	 */
	public static function getSidebarLayout() {
		/* @todo: error handling */
		$mainDb = new Admin_DbTable_LayoutSidebar();
		$items = $mainDb->getAll();
		$result = (count($items)) ? $items->toArray() : array();
		return $result;
	}

	/**
	 * Returns an array of supporting tree data based on object type
	 * 
	 * @param string $type
	 * @return array
	 */
	public function treeMap($type) {
		switch ($type) {
			case 'event' :
				$apiType = 'events';
				$icon = '/images/admin/icons/calendar_view_day.png';
				break;
			case 'forum' :
				$apiType = 'forums';
				$icon = '/images/admin/icons/group.png';
				break;
			case 'gallery' :
				$apiType = 'galleries';
				$icon = '/images/admin/icons/photos.png';
				break;
			case 'news' :
				$apiType = 'news';
				$icon = '/images/admin/icons/newspaper.png';
				break;
			case 'faq' :
				$apiType = 'faqs';
				$icon = '/images/admin/icons/help.png';
				break;
			case 'page_static' :
				$apiType = 'static-pages';
				$icon = '/images/admin/icons/page_key.png';
				break;
			case 'survey' :
				$apiType = 'surveys';
				$icon = '/images/admin/icons/chart_bar.png';
				break;
			case 'poll' :
				$apiType = 'polls';
				$icon = '/images/admin/icons/chart_pie.png';
				break;
			case 'page_freeform':
				$apiType = 'freeform-pages';
				$icon = '/images/admin/icons/page.png';
				break;
			case 'dynamic_page':
				$apiType = 'dynamic-pages';
				$icon = '/images/admin/icons/page.png';
				break;
			default :
				$apiType = $type;
				$icon = '/images/admin/icons/page.png';
		}
		return array("icon" => $icon, "type" => $apiType);
	}
	
	/**
	 * Returns a json array of objects for tree menu (jsTree)
	 * 
	 * @param int $parentID
	 */
    public static function getTreeMenu($parentID) {
    	$masterTable = new Admin_DbTable_Master();
    	$nodesArray = $childArray = array();
    	$nodes = $masterTable->getAll();
    	$masterArray = array();
    	$nodeArray = array();
    	foreach($nodes as $node) {
    		// filter out non-essential objects
    		if (!in_array($node->objectName, array('cta', 'footer', 'firstClass', 'comment', 'news')) && !$node->hidden) {
	    		$parentId = ($node->parentId == 0) ? '#' : $node->parentId;
	    		$extraData = self::treeMap($node->objectName);
	    		$nodesArray[] = array(
	    			"id" => $node->id,
	    		    "data" => array("type" => $extraData['type'], "position" => $node->position),
	    		    "parent" => $parentId,
	    		    "text" => $node->heading,
	    		    "icon" => $extraData['icon']
	    		);
    		}
    	}
    	return $nodesArray;
    }
    
    /**
     * Updates the parentId and orderNum of a master record
     * 
     * @param array $nodes
     */
    public static function updatePosition($nodes) {
    	$masterTable = new Admin_DbTable_Master();

    	foreach ($nodes as $node) {

    		$items = array();
	    	array_push($items, $node);
	    	
    		// fetch all nodes matching parentId
    		$siblings = $masterTable->fetchSiblings($node['parentId']);
    		
    		$count = 0;
    		if ($siblings) {
				foreach ($siblings->toArray() as $sibling) {
					if (($sibling['id'] != $node['id'])) {
						if ($count == $node['position']) $count++;
						$itemData = array(
							"id" => $sibling['id'],
							"parentId" => $sibling['parentId'],
							"position" => $count
						);
						array_push($items, $itemData);
						$count++;
					}
				}
			}
			
    		foreach ($items as $item) {
    			$masterTable->update(array('parentId' => $item['parentId'], "position" => $item['position']), 'id = "' . $item['id'] . '"');
			}
    	}
    	return $items;
    }

    /**
     * Returns a node's url path built from page manager hierarchy
     * 
     * @param integer $nodeId
     * @param Admin_DbTable_Master|boolean $masterTable
     */
    public static function fetchUrl($nodeId, $masterTable = false) {
    	if (!$masterTable) {
    		$masterTable = new Admin_DbTable_Master();
    	}
    	$fullUrl = '';
    	$node = $masterTable->fetchByID($nodeId);
    	if (isset($node)) {
    		if ((strpos($node->url, "http://") !== false) || (strpos($node->url, "/") === 0)) {
    			return $node->url;
    		}
    		if ($node->parentId) {
    			$fullUrl = self::fetchUrl($node->parentId, $masterTable);
    		}
    		if (($node->url != "/") && ($node->url != "")) {
    			return $fullUrl = $fullUrl . "/" . $node->url;
    		} else {
    			return $fullUrl;
    		}
    	}
    }
    
    /**
     * Returns an array of crumbs
     * 
     * @param integer $nodeId
     * @return array
     */
    public function crumbArray($nodeId) {
    	$crumbArray = array();
    	$masterTable = Admin_Factory::create("DbTable_Master");
    	$node = $masterTable->fetchByID($nodeId);
    	$crumbArray[] = $node->toArray();
    	$parentId = $node->parentId;
    	while ($parentId != 0) {
    		$node = $masterTable->fetchByID($parentId);
    		if (($node->url != '/') && ($node->url != '')) {
    			$crumbArray[] = $node->toArray();
    		}
    		$parentId = $node->parentId;
    	}
    	return array_reverse($crumbArray);
    }
    
    /**
     * Returns an list of breadcrumbs to assist navigation
     * 
     * @param integer $nodeId
     */
    public static function breadcrumbs($nodeId) {
    	$breadcrumbs = '';
    	$crumbArray = self::crumbArray($nodeId);
    	$crumbLength = count($crumbArray);
    	if ($crumbLength > 1) {
    		$count = 1;
    		$breadcrumbs = '<ol class="breadcrumb">';
    		$breadcrumbs .= '<li><a href="/">Home</a></li>';
    		foreach ($crumbArray as $crumb) {
    			if ($count != $crumbLength) $breadcrumbs .= '<li><a href="' . self::fetchUrl($crumb['id']) . '">' . $crumb['heading'] . '</a></li>';
    			else  $breadcrumbs .= '<li class="active">' . $crumb['heading'] . '</li>';
    			$count++;
    		}
    		$breadcrumbs .= '</ol>';
    	}
    	return $breadcrumbs;
    }
    
    public function getChildNodes()
    {
    	return self::getMapper()->getMasterChildNodes($this);
    }
	
	public static function getByUrl($url)
	{
		$id = self::getMapper()->getIdFromUrl($url);
		if (!$id) return null;
		
		return self::get($id);
	}
	
	public static function countPublished()
	{
		$instance = new static();
		return self::getMapper()->countPublished($instance->_objectName);
	}

	/**
	 * Need to think of a better way to do this - users should not have any concepts of "nodes".
	 * Need to be able to overwrite and map object_type fields to models on a project-by-project basis
	 * so we can make a "getRootParent" method with returns the parent model directly.
	 * 
	 * Another config file?
	 * 
	 * @return Zend_Db_Table_Row
	 */
	public function getRootParentNode()
	{
		return self::getMapper()->getRootParentNode($this);
	}
	
	/**
	 * Returns string for single image, or array of strings for multiple images
	 * when feature image property is either file path or directory path
	 * 
	 * @param string $prepend. Path prepend ie "/uploaded/"
	 * @param string $method. The getter method which returns the feature image
	 * @param array $methodArgs. Arguments you'd usually pass the feature image method
	 * @throws Exception
	 */
	public function feature($prepend = "/uploaded/", $method = "getFeatureImg", $methodArgs = [])
	{
		if (!$method || !method_exists($this, $method)) {
			throw new Exception("Cannot find featured method");
		}
		
		$path = $prepend . call_user_method_array($method, $this, $methodArgs);
		
		// bail out if nothing specified
		if ($path === $prepend) {
			return;
		}
		
		// case insensitive match of files ending with image paths
		$pattern = "/.+\.(?:jpg|jpeg|gif|png|svg)$/i";
		
		if (preg_match($pattern, $path)) {
			// image
			return $path;
		} else {
			// directory
			
			// do something below im not quite sure what
			$dir = ROOT_PATH . '/public' . $path;
			if (is_dir($dir)) {
				return array_map(function($fullPath) {
					return str_replace(ROOT_PATH . '/public', '', $fullPath);
				}, glob($dir . '/{*.jpg,*.jpeg,*.png,*.gif}', GLOB_BRACE));
			} else {
				return false;
			}
		}
	}
}
