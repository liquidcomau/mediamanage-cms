<?php
class Admin_Model_Appearance extends Admin_Model_Master
{
	protected $_id = null;
	protected $_primaryColour = null;
	protected $_secondaryColour = null;
	protected $_highlightColour = null;
	protected $_logo = null;
	protected $_headerImg = null;
	protected $_favicon = null;
	protected $_masterAvatar = null;

	public function getId()
	{
		return $this->_id;
	}
	
	public function getPrimaryColour()
	{
		return $this->_primaryColour;
	}
	
	public function getSecondaryColour()
	{
		return $this->_secondaryColour;
	}
	
	public function getHighlightColour()
	{
		return $this->_highlightColour;
	}
	
	public function getLogo()
	{
		return $this->_logo;
	}
	
	public function getHeaderImg()
	{
		return $this->_headerImg;
	}
	
	public function getFavicon()
	{
		return $this->_favicon;
	}	
	
	public function getMasterAvatar()
	{
		return $this->_masterAvatar;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
	}
	
	public function setPrimaryColour($primaryColour)
	{
		$this->_primaryColour = $primaryColour;
	}
	
	public function setSecondaryColour($secondaryColour)
	{
		$this->_secondaryColour = $secondaryColour;
	}
	
	public function setHighlightColour($highlightColour)
	{
		$this->_highlightColour = $highlightColour;
	}
	
	public function setLogo($logo)
	{
		$this->_logo = $logo;
	}
	
	public function setHeaderImg($headerImg)
	{
		$this->_headerImg = $headerImg;
	}
	
	public function setFavicon($favicon)
	{
		$this->_favicon = $favicon;
	}
	
	public function setMasterAvatar($ma)
	{
		$this->_masterAvatar = $ma;
	}
	
	public function updateSettings($format)
	{
		$appearanceTable = new Admin_DbTable_Appearance();
		$update = $appearanceTable->update($this->getDbFields(), 'id = 1');
		$result = self::getSettings($format);
		if ($result) {
			self::compileLess();
		}
		return $result;
	}

	public static function getSettings($format)
	{
		$appearanceTable = new Admin_DbTable_Appearance();
		$result = $appearanceTable->fetchById(1);
		return ($format === Admin_Constants::TYPE_ARRAY) ? $result->toArray() : $result;
	}
	
	
	private function getDbFields() {
		return array(
			'primaryColour' => $this->_primaryColour,
			'secondaryColour' => $this->_secondaryColour,
			'highlightColour' => $this->_highlightColour,
			'logo' => $this->_logo,
			'headerImg' => $this->_headerImg,
			'favicon' => $this->_favicon,
			'masterAvatar' => $this->_masterAvatar
		);
	}
	
	protected function compileLess() {
		try{
			$logger = Zend_Registry::get('logger');
			$logger->info("Compiling custom less");
				
			// build custom less file
			$customVars = array();
			$customLess = realpath(APPLICATION_PATH . "/../public/less/custom.less");
			if ($this->getPrimaryColour() != "") $customVars[] = "@colour_base: " . $this->getPrimaryColour() . ";";
			if ($this->getSecondaryColour() != "") $customVars[] = "@colour_contrast: " . $this->getSecondaryColour() . ";";
			if ($this->getHighlightColour() != "") $customVars[] = "@colour_highlight:	" . $this->getHighlightColour() . ";";
			file_put_contents($customLess, implode(" ", $customVars));
	
			// compile less
			require_once realpath(MEDIAMANAGE_ENV . "/library/Admin/Resources/less.php/Less.php");
			$parser = new Less_Parser(array('compress'=>true));
			$lessFile = realpath(APPLICATION_PATH . "/../public/less/style.less");
			$parser->parseFile($lessFile);
			$css = $parser->getCss();
			$cssFile  = realpath(APPLICATION_PATH . "/../public/css/style.css");
			file_put_contents($cssFile, $css);
			
			$logger->info("Compile complete");
			return true;
		} catch (Exception $e){
			$logger->err($e->getMessage());
			return false;
		}
	}
}