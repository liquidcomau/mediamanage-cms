<?php
abstract class Admin_Model_Resource extends Admin_Model_Abstract
{
	private static $_objectTypeIndex = null;
	
	public function __construct($data)
	{
		$data = is_array($data) ? $data : array('id' => $data);
		parent::__construct($data);
	}
	
	public static function getMapper()
	{
		$name = preg_replace('/(?:Admin_|Project_)Model_/', 'Model_Mapper_', get_called_class());
		return Admin_Factory::create($name);
	}
	
	public static function getAll($clauses, $format)
	{
		$list = self::getMapper()->getAll($clauses);
		return $format !== Admin_Constants::TYPE_ARRAY
												? $list
												: array_map(function($res) {
													return $res->toArray();
												}, $list);	 
	}
	
	public static function countTotal($clauses)
	{
		unset($clauses["page"]);
		unset($clauses["limit"]);
		unset($clauses["offset"]);
		
		return count(static::getAll($clauses));
	}
	
	public static function get($id, $format)
	{
		$instance = self::getMapper()->get($id);
		if (!$instance) return null;
		
		return $format === Admin_Constants::TYPE_ARRAY ? $instance->toArray() : $instance;
	}

	public static function getGetCache()
	{
		if (property_exists(get_called_class(), "_getCache")) {
			return static::$_getCache;
		} else {
			return null;
		}
	}

	public static function getFromCache(array $cache, $key, $val)
	{
		return $cache[$key][$val];
	}

	public function create()
	{
		return self::getMapper()->create($this);
	}
	
	public function update()
	{
		return self::getMapper()->update($this);
	}
	
	public static function delete($id)
	{
		return self::getMapper()->deleteById($id);
	}
	
	public static function deleteById($id)
	{
		return self::getMapper()->deleteById($id);
	}
	
	public function loadObjectTypeId()
	{
		if (self::$_objectTypeIndex === null) self::$_objectTypeIndex = Admin_Model_Mapper_ResourceAbstract::getObjectTypes();

		$this->_objectTypeId = array_flip(self::$_objectTypeIndex)[$this->_objectName];
		return $this;
	}
}
