<?php
class Admin_Model_Category extends Admin_Model_Resource
{
	protected $_objectName = "category";

	protected $_id = null;
	protected $_name = null;
	protected $_label = null;
	protected $_parentCategoryId = null;
	protected $_childCategoryIds = null;

	public function getId()
	{
		return $this->_id;
	}

	public function getName()
	{
		return $this->_name;
	}
	
	public function getLabel($fallbackOnName = true)
	{
		if ($fallbackOnName && !$this->_label) {
			$label = ucwords(str_replace("_", " ", Zend_Filter::filterStatic($this->_name, "Word_CamelCaseToUnderscore")));
		} else {
			$label = $this->_label;
		}
		
		return $label;
	}

	public function getParentCategoryId()
	{
		return $this->_parentCategoryId;
	}
	
	public function getChildCategoryIds()
	{
		return $this->_childCategoryIds;
	}

	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}

	public function setName($name)
	{
		$this->_name = $name;
		return $this;
	}

	public function setLabel($label)
	{
		$this->_label = $label;
		return $this;
	}
	
	public function setParentCategoryId($parentCategoryId)
	{
		$this->_parentCategoryId = $parentCategoryId;
		return $this;
	}
	
	public function setChildCategoryIds($ids)
	{
		$this->_childCategoryIds = $ids;
		return $this;
	}
}
