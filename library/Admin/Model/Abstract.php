<?php
abstract class Admin_Model_Abstract
{
	/**
	 * Constructor.
	 *
	 * @param array $data Object properties.
	 * @param Admin_Dependency_Manager $dependencyManager Object containing sets of data not included in database tables.
	 * @return void
	 * @throws Zend_Exception
	 */
	public function __construct($data = array(), Admin_Dependency_Manager $dependencyManager = null)
	{
		try {
			$config = Zend_Registry::get("config");
		} catch(Zend_Exception $e) {
			throw new Zend_Exception('Configuration file not found.');
		}
	
		$this->setValues($data);
		if ($dependencyManager) {
			$dependencyManager->populateModel($this);
		}
		
		$this->init();
	}
	
	/**
	 * Area to add custom insantiation code in each model.
	 */
	protected function init() 
	{
	}
	
    /**
     * Sets the current object's properties based on an associative array of data.
     * 
     * @param array $data Object properties.
     * @return void
     */
    public function setValues($data)
    {
    	foreach ($data as $key => $val) { // loop array data
    		if (method_exists($this, 'set' . ucfirst($key))) {
                $this->{'set'.ucfirst($key)}($val);
            } else if (property_exists($this, '_' . lcfirst($key))) {
                $this->{'_' . lcfirst($key)} = $val;
            }
        }
    }
    
    /**
     * Serialize object data into an array
     * 
     * return array
     */
    public function toArray() {
    	$vars = get_object_vars($this);
    	$array = array();
    	foreach ($vars as $key => $value) {
    		//if ($value !== NULL && $key != '_password') { // we don't want to return password field, is this the best place to omit it??
    		if ($value !== NULL) {
    			if ($value instanceof Admin_Model_Abstract) {
    				$array[lcfirst(ltrim($key, '_'))] = $value->toArray();
    			} else {
    				$array[lcfirst(ltrim($key, '_'))] = $value;
    			}
    		}
    	}
    	return $array;
    }
    
    /**
	 * Returns an associative array of field names in current object
	 *
     * return array
     */
    public function getVars($includeParent = true) {
    	$varArray = array();
    	$vars = get_object_vars($this);
    	foreach($vars as $key => $var) {
    		$varArray[str_replace("_", "", $key)] = $var;
    	}
    	return $varArray;
    }
    
    
    /**
     * Same as toArray(), can include null vals by argument.
     * 
     * @param string $nullVals
     * @return multitype:|boolean
     */
    public function getProperties($nullVals = false)
    {
    	$vars = get_object_vars($this);

		// remove only initial underscore in property name    	
    	$names = array_map(function($n) {
    		return preg_replace('/^_/', '', $n);
    	}, array_keys($vars));
    	
    	return $nullVals ? array_combine($names, $vars) : array_filter(array_combine($names, $vars), function($p) {
    		return $p !== NULL;
    	});
    }
}