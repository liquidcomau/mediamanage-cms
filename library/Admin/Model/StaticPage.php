<?php
class Admin_Model_StaticPage extends Admin_Model_Master
{
	protected $_objectName = 'static_page';
	
	protected $_content = null;
	protected $_seo = null;

	public function getContent()
	{
		return $this->_content;
	}
	
	public function getSeo()
	{
		return $this->_seo;
	}
	
	public function setContent($content)
	{
		$this->_content = $content;
		return $this;
	}

	public function setSeo($seo)
	{
		$this->_seo = $seo instanceof Admin_Model_Seo ? $seo : new Admin_Model_Seo($seo);
		if (isset($this->_id) && !$this->_seo->getId()) $this->_seo->setId($this->_id);
	
		return $this;
	}
}