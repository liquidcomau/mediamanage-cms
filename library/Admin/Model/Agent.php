<?php
class Admin_Model_Agent extends Admin_Model_Resource
{
	protected $_id = null;
	protected $_name = null;
	protected $_email = null;
	protected $_phone = null;
	protected $_profilePicture = null;
	protected $_agencyId = null;

	protected $_agency = null;

	public function getId()
	{
		return $this->_id;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function getEmail()
	{
		return $this->_email;
	}

	public function getPhone()
	{
		return $this->_phone;
	}

	public function getProfilePicture()
	{
		return $this->_profilePicture;
	}

	public function getAgencyId()
	{
		return $this->_agencyId;
	}

	public function getAgency()
	{
		return $this->_agency;
	}
	/*
	public function getAgency()
	{
		if (!$this->_agency) {
			$class = Admin_Factory::create("Model_Agency");
			$this->_agency = $class::get($this->_agencyId);
		}
		
		return $this->_agency;
	}
	*/
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}

	public function setName($name)
	{
		$this->_name = $name;
		return $this;
	}

	public function setEmail($email)
	{
		$this->_email = $email;
		return $this;
	}

	public function setPhone($phone)
	{
		$this->_phone = $phone;
		return $this;
	}

	public function setProfilePicture($pic)
	{
		$this->_profilePicture = $pic;
		return $this;
	}

	public function setAgencyId($id)
	{
		$this->_agencyId = $id;
		return $this;
	}

	public function setAgency($agency)
	{
		if (is_array($agency)) {
			$agency = new Project_Model_Agency($agency);
		}
		
		$class = Admin_Factory::getClass("Model_Agency");
		if (!($agency instanceof $class)) {
			throw new Exception("Agency property must either be array or instance of Project Model Agency");
		}

		$this->_agency = $agency;
		return $this;
	}

}
