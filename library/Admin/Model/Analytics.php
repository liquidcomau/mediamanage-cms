<?php
class Admin_Model_Analytics extends Admin_Model_Abstract {
	
	protected $_username;
	protected $_password;
	protected $_viewId;
	protected $_trackingId;
	
	public function getUsername() {
		return $this->_username;
	}
	
	public function getPassword() {
		return $this->_password;
	}
	
	public function getViewId() {
		return $this->_viewId;
	}
	
	public function getTrackingId() {
		return $this->_trackingId;
	}
	
	public function setUsername($u) {
		$this->_username = $u;
		return $this;
	}
	
	public function setPassword($p) {
		$this->_password = $p;
		return $this;
	}
	
	public function setViewId($id) {
		$this->_viewId = $id;
		return $this;
	}
	
	public function setTrackingId($id) {
		$this->_trackingId = $id;
		return $this;
	}
	
	public static function getAnalytics($format) {
		$settings = new self((new Admin_DbTable_Analytics())->getAnalytics());
		return $format === Admin_Constants::TYPE_ARRAY ? $settings->toArray() : $settings;
	}
	
	public function update() {
		$data = $this->toArray();
		return (new Admin_DbTable_Analytics())->updateAnalytics($data);
	}
}