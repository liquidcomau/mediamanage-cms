<?php
class Admin_Model_Seo extends Admin_Model_Resource
{
	protected $_id = null;
	
	protected $_title = null;
	protected $_metaDescription = null;
	protected $_robots = null;
	protected $_goalTracking = null;
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function getTitle()
	{
		return $this->_title;
	}
	
	public function getMetaDescription()
	{
		return $this->_metaDescription;
	}
	
	public function getRobots()
	{
		return $this->_robots;
	}
	
	public function getGoalTracking()
	{
		return $this->_goalTracking;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setTitle($title)
	{
		$this->_title = $title;
	}
	
	public function setMetaDescription($description)
	{
		$this->_metaDescription = $description;
	}
	
	public function setRobots($robots)
	{
		$this->_robots = $robots;
	}
	
	public function setGoalTracking($goalTracking)
	{
		$this->_goalTracking = $goalTracking;
	}
}