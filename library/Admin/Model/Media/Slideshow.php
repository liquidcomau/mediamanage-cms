<?php
class Admin_Model_Media_Slideshow extends Admin_Model_Media_Gallery
{
	protected $_itemClass = "Model_Media_Item_Slideshow";
	
	public function getMediaCollectionType()
	{
		return "slideshow";
	}
}
