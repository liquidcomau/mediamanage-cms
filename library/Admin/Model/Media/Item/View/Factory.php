<?php
class Admin_Model_Media_Item_View_Factory
{
	public function create($type, $data)
	{
		// if provided data as first param $type, extract
		// view type and store data in $data variable
		if (is_array($type)) {
			// copy data if container in first param
			$data = $type;
			
			$type = $data["viewType"];
			unset($data["viewType"]);
		}

		if (!is_array($data)) {
			throw new Exception("Must provide Media Item View factory data array");
		}
		switch ($type) {
			case "image":
				$view = Admin_Factory::create("Model_Media_Item_View_Image", [$data]);
				break;
			case "youtube":
				$view = Admin_Factory::create("Model_Media_Item_View_Youtube", [$data]);
				break;
			default:
				throw new Exception("Media Item View factory cannot recognize view type");
		}

		return $view;
	}
}
