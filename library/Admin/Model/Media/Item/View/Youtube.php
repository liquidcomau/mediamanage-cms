<?php 
class Admin_Model_Media_Item_View_Youtube extends Admin_Model_Media_Item_View_Abstract
{
	const VIEW_TYPE = "youtube";

	protected $_displaySource = null;
	protected $_thumbnailSource = null;

	public function getDisplaySource()
	{
		return $this->_displaySource;
	}

	public function getThumbnailSource()
	{
		return $this->_thumbnailSource;
	}

	public function setDisplaySource($src)
	{
		$this->_displaySource = $src;
		return $this;
	}

	public function setThumbnailSource($src)
	{
		$this->_thumbnailSource = $src;
		return $this;
	}

	public function display()
	{
		return $this->_displaySource;
	}

	/**
	 * Returns thumbnailSource if set, else displaySource if set,
	 * else returns link to auto-generated youtube thumbnail
	 * 
	 * @param Int $number
	 * @return String
	 */
	public function thumbnail($number = 0)
	{
		if ($this->_thumbnailSource) {
			return $this->_thumbnailSource;
		}
		
		if ($this->_displaySource) {
			return $this->_displaySource;
		}
		
		// youtube generates 4 thumbs numbered [0-3]
		// revert to first if number specified is too high
		if ($number > 3) {
			$number = 0;
		}
		
		$code = $this->extractCode();
		return "http://img.youtube.com/vi/" . $code . "/" . $number . ".jpg";
	}

	public function getViewType()
	{
		return self::VIEW_TYPE;
	}

	public function toArray()
	{
		$array = parent::toArray();

		$array["thumbnail"] = $this->thumbnail();
		$array["display"] = $this->display();
		$array["viewType"] = $this->getViewType();

		return $array;
	}
	
	/**
	 * Extracts code from $this->_source youtube url
	 * 
	 * @return String
	 */
	public function extractCode()
	{
		$params = [];
		parse_str(parse_url($this->_source, PHP_URL_QUERY), $params);
		
		return $params["v"];
	}
}
