<?php 
class Admin_Model_Media_Item_View_Image extends Admin_Model_Media_Item_View_Abstract
{
	const VIEW_TYPE = "image";

	public function display()
	{
		return $this->_source;
	}

	public function thumbnail()
	{
		return $this->_source;
	}

	public function getViewType()
	{
		return self::VIEW_TYPE;
	}

	public function toArray()
	{
		$array = parent::toArray();

		$array["thumbnail"] = $this->_source;
		$array["display"] = $this->_source;
		$array["viewType"] = $this->getViewType();

		return $array;
	}
	/*
	public function getDisplaySource()
	{
		return $this->_source;
	}
	
	public function setDisplaySource($src)
	{
		$this->_source = $src;
		return $this;
	}
	
	public function getThumbnailSource()
	{
		return $this->_source;
	}
	
	public function setThumbnailSource($src)
	{
		$this->_source = $src;
		return $this;
	}
	 */
}
