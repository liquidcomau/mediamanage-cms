<?php
abstract class Admin_Model_Media_Item_View_Abstract extends Admin_Model_Abstract implements Admin_Model_Media_Item_View_Interface
{
	protected $_source = null;

	public function getItemId()
	{
		return $this->_itemId;
	}

	public function getSource()
	{
		return $this->_source;
	}
	
	public function setItemId($id)
	{
		$this->_itemId = $id;
		return $this;
	}

	public function setSource($source)
	{
		$this->_source = $source;
		return $this;
	}
}
