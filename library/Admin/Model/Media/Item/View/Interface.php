<?php
interface Admin_Model_Media_Item_View_Interface
{
	public function getViewType();
	public function getSource();
	public function display();
	public function thumbnail();
	public function toArray();
}
