<?php
class Admin_Model_Media_Item_Slideshow extends Admin_Model_Media_Item
{
	protected $_newTarget = null;
	protected $_link = null;

	public function getNewTarget()
	{
		return $this->_newTarget;
	}

	public function getLink()
	{
		return $this->_link;
	}

	public function setNewTarget($nt)
	{
		$this->_newTarget = $nt;
		return $this;
	}

	public function setLink($link)
	{
		$this->_link = $link;
		return $this;
	}
}
