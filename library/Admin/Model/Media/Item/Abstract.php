<?php
abstract class Admin_Model_Media_Item_Abstract extends Admin_Model_Resource implements Admin_Model_Media_Item_Interface
{
	private $_view;

	protected $_itemId = null;
	protected $_mediaCollectionId = null;
	protected $_position = null;
	protected $_caption = null;
	protected $_main = null;

	public function getItemId()
	{
		return $this->_itemId;
	}

	public function getMediaCollectionId()
	{
		return $this->_mediaCollectionId;
	}

	public function getPosition()
	{
		return $this->_position;
	}
	
	public function getCaption()
	{
		return $this->_caption;
	}

	public function getMain()
	{
		return $this->_main;
	}

	public function setItemId($id)
	{
		$this->_itemId = $id;
	}

	public function setMediaCollectionId($id)
	{
		$this->_mediaCollectionId = $id;
		return $this;
	}

	public function setPosition($i)
	{
		$this->_position = $i;
		return $this;
	}
	
	public function setCaption($caption)
	{
		$this->_caption = $caption;
		return $this;
	}

	public function setMain($bool)
	{
		$this->_main = $bool;
		return $this;
	}

	public function getView()
	{
		return $this->_view;
	}

	public function setView($view)
	{
		if (!($view instanceof Admin_Model_Media_Item_View_Interface || is_array($view))) {
			throw new Exception("Cannot add illegal VIEW to media gallery");
		}

		if (is_array($view)) {
			$viewFactory = Admin_Factory::getClass("Model_Media_Item_View_Factory");
			$view = $viewFactory::create($view);
		}

		$view->setItemId($this->_id);
		$this->_view = $view;
	}

	public function getSource()
	{
		return $this->_view->getSource();
	}

	public function setSource($src)
	{
		$this->_view->setSource($src);
		return $this;
	}
	
	public function getDisplaySource()
	{
		if (method_exists($this->_view, "getDisplaySource")) {
			return $this->_view->getDisplaySource();
		} else {
			throw new Exception("Cannot get display source on view type");
		}
	}

	public function setDisplaySource($src)
	{
		if (method_exists($this->_view, "setDisplaySource")) {
			$this->_view->setDisplaySource($src);
			return $this;
		} else {
			throw new Exception("Cannot set display source on view type");
		}
	}

	public function getViewType()
	{
		return $this->_view->getViewType();
	}

	public function display()
	{
		return $this->_view->display();
	}

	public function thumbnail()
	{
		return $this->_view->thumbnail();
	}

	public function __construct($data)
	{
		// make sure view component is set first
		if (isset($data["view"])) {
			$this->setView($data["view"]);
		} else {
			if (!isset($data["viewType"])) {
				throw new Exception("Must provide view type to media item constructor");
			}
			
			$viewFactory = Admin_Factory::getClass("Model_Media_Item_View_Factory");
			$this->setView($viewFactory::create([
				"viewType" => $data["viewType"],
				"itemId" => $data["itemId"]
			]));
		}

		unset($data["view"], $data["viewType"]);
		
		// stripping null values - why aren't we doing this by default?
		$data = array_filter($data, function($item) {
			return $item;
		});

		parent::__construct($data);
	}
	
	public function toArray()
	{
		$array = parent::toArray();
		$viewArray = $this->_view->toArray();
		
		return array_merge($array, $viewArray);
	}
	
	public static function getAllByMediaCollectionId($id)
	{
		return static::getMapper()->getAllByMediaCollectionId($id);
	}
}
