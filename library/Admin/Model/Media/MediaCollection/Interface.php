<?php
interface Admin_Model_Media_MediaCollection_Interface
{
	public function getMediaCollectionId();
	public function getMediaCollectionType();
}