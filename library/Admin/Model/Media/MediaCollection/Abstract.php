<?php
abstract class Admin_Model_Media_MediaCollection_Abstract extends Admin_Model_Resource implements SeekableIterator, Countable, Admin_Model_Media_MediaCollection_Interface
{
	private $position = 0;

	protected $_itemClass = "Model_Media_Item";

	protected $_mediaCollectionId = null;
	protected $_relationshipId = null;
	protected $_items = [];
	
	// getters/setters
	
	public function getMediaCollectionId()
	{
		return $this->_mediaCollectionId;
	}

	public function getRelationshipId()
	{
		return $this->_relationshipId;
	}

	public function getItems()
	{
		return $this->_items;
	}

	public function mainItem()
	{
		if (!count($this->getItems())) {
			return;	
		}

		$main = array_reduce($this->getItems(), function($carry, $item) {
			if ($carry->getMain()) {
				return $carry;
			}

			return $item;
		}, $this->getItems()[0]);

		return $main->getMain() ? $main : $this->getItems()[0];
	}
	
	public function setMediaCollectionId($id)
	{
		$this->_mediaCollectionId = $id;

		if (count($this)) {
			foreach ($this as $item) {
				$item->setMediaCollectionId($this->_mediaCollectionId);
			}
		}

		return $this;
	}

	public function setRelationshipId($relationshipId)
	{
		$this->_relationshipId = $relationshipId;
		return $this;
	}
	
	public function setItems($items)
	{
		$this->_items = [];
		$this->addItems($items);
		return $this;
	}

	
	// Countable
	
	public function count()
	{
		return count($this->_items);
	}

	
	// SeekableIterator
	
	public function seek($i)
	{
		if (!isset($this->_items[$i])) {
			throw new OutOfBoundsException("Nonexistant media collection item position: " . $i);
		}

		$this->position = $i;	
	}

	public function rewind()
	{
		$this->position = 0;
	}

	public function current() {
		return $this->_items[$this->position];
	}

	public function key()
	{
		return $this->position;
	}

	public function next()
	{
		$this->position++;
	}

	public function valid()
	{
		return isset($this->_items[$this->position]);
	}
	
	
	// Class methods

	public function addItem($item)
	{
		$resolvedClass = Admin_Factory::getClass($this->_itemClass);
		if (!($item instanceof $resolvedClass || is_array($item))) {
			throw new Exception("collection expects data constructor of type Array or " . $resolvedClass);
		}

		if (is_array($item)) {
			$itemClass = Admin_Factory::getClass($this->_itemClass);
			$item = new $itemClass($item);
		}

		$item->setMediaCollectionId($this->_id);
		$this->_items[] = $item;
	}

	public function addItems(Array $items)
	{
		foreach ($items as $item) {
			$this->addItem($item);
		}
	}

	public function toArray()
	{
		$array = parent::toArray();

		$array["items"] = [];		
		foreach ($this as $item) {
			$array["items"][] = $item->toArray();
		}
		
		unset($array["itemClass"], $array["position"]);
		
		return $array;
	}
	
	public function getItemAtPosition($i)
	{
		return $this->_items[$i];
	}
	
	public function removeItemAtPosition($i)
	{
		if (array_splice($this->_items, $i, 1)) {
			return true;
		} else {
			return false;
		}
	}

	public static function getByRelationshipId($id)
	{
		return static::getMapper()->getByRelationshipId($id);
	}
}
