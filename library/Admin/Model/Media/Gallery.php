<?php
class Admin_Model_Media_Gallery extends Admin_Model_Media_MediaCollection_Abstract
{
	protected $_internalLabel = null;
	protected $_identifier = null;

	public function getInternalLabel()
	{
		return $this->_internalLabel;
	}
	
	public function getIdentifier()
	{
		return $this->_identifier;
	}
	
	public function setInternalLabel($label)
	{
		$this->_internalLabel = $label;
		return $this;
	}
	
	public function setIdentifier($identifier)
	{
		$this->_identifier = $identifier;
		return $this;
	}
	
	public function getMediaCollectionType()
	{
		return "gallery";
	}
}
