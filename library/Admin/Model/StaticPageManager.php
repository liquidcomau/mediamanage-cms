<?php
class Admin_Model_StaticPageManager extends Admin_Model_AbstractManager
{
	/**
	 * Get Static Pages via RESTful API.
	 *
	 * @return Admin_Model_Seo object array
	 */
	public function getStaticPages()
	{
		//$response = $this->_api->request("static-pages?filter=all&objectType=page_static", Zend_Http_Client::GET);
		$response = $this->_api->request("static-pages?filter=all&objectTypeId=5", Zend_Http_Client::GET);
		if ($response && $response->isSuccessful()) {
			$jsonData = Zend_Json::decode($response->getBody());
			$pageArray = array();
			foreach ($jsonData as $data) {
				$pageArray[] = new Admin_Model_StaticPage($data);
			}
			return $pageArray;
		} 
		return $response;
	}
	
	/**
	 * Get Static Pages via RESTful API.
	 *
	 * @return Admin_Model_Seo object
	 */
	public function getStaticPage($pageId)
	{
		$response = $this->_api->request("static-pages/" . $pageId, Zend_Http_Client::GET);
		if ($response && $response->isSuccessful()) {
			$jsonData = Zend_Json::decode($response->getBody());
			$seo = new Admin_Model_Seo($jsonData);
			return $seo;
		}
		return $response;
	}
}
