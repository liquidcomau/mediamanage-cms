<?php
class Admin_Model_Listing extends Admin_Model_Master
{
	protected $_objectName = 'listing';
	protected static $_hasCategories = false;
	
	protected $_id = null;
	protected $_headline = null;
	protected $_description = null;
	protected $_content = null;
	protected $_featureImg = null;
	protected $_publishedDate = null;
	protected $_featured = null;
	protected $_price;
	protected $_priceView;
	
	protected $_gallery = null;
	
	protected $_agentIds;
	protected $_agents;
	
	protected $_seo = null;
	
	public function getHeadline()
	{
		return $this->_headline;
	}
	
	public function getDescription()
	{
		return $this->_description;
	}
	
	public function getContent()
	{
		return $this->_content;
	}
	
	public function getFeatureImg()
	{
		return $this->_featureImg;
	}
	
	public function getPublishedDate($asTimestamp = false)
	{
		return $asTimestamp ? strtotime($this->_publishedDate) : $this->_publishedDate;
	}
        
	public function getFeatured()
	{
		return $this->_featured;
	}
	
	public function getPrice()
	{
		return $this->_price;
	}
	
	public function getPriceView()
	{
		return $this->_priceView;
	}
	
	public function getCategoryIds()
	{
		return $this->_categoryIds;
	}
	
	public function getAgentIds()
	{
		return $this->_agentIds;
	}
	
	public function getAgents()
	{
		$agentClass = Admin_Factory::getClass("Model_Agent");
		if (!$this->_agents) {
			if (!$this->_agentIds) {
				$this->_agents = [];
			} else {
				$this->_agents = $agentClass::getAll(["in" => $this->_agentIds]) ?: [];
			}
		}
		
		return $this->_agents;
	}
	
	public function getGallery()
	{
		return $this->_gallery;
	}
	
	public function getSeo()
	{
		return $this->_seo;
	}
	
	public function setHeadline($headline)
	{
		$this->_headline = $headline;
		return $this;
	}
	
	public function setDescription($description)
	{
		$this->_description = $description;
		return $this;
	}
	
	public function setContent($content)
	{
		$this->_content = $content;
		return $this;
	}
	
	public function setFeatureImg($img)
	{
		$this->_featureImg = $img;
		return $this;
	}
	
	public function setPublishedDate($date)
	{
		$this->_publishedDate = $date;
		return $this;
	}
        
	public function setFeatured($featured)
	{
		$this->_featured = $featured;
		return $this;
	}
	
	public function setPrice($price)
	{
		$this->_price = $price;
		return $this;
	}
	
	public function setPriceView($view)
	{
		$this->_priceView = $view;
		return $this;
	}
	
	public function setAgentIds($ids) 
	{
		$this->_agentIds = $ids;
		return $this;
	}
	
	public function setGallery($gallery)
	{
		if (is_array($gallery)) {
			$gallery = Admin_Factory::create("Model_Media_Gallery", [$gallery]);
		}

		if (!($gallery instanceof Admin_Model_Media_MediaCollection_Interface)) {
			throw new Exception("Gallery property must be instance of Admin_Model_Media_MediaCollection_Interface");
		}

		$this->_gallery = $gallery;
		$this->_mainItem = $this->_gallery->mainItem();
		
		return $this;
	}
	
	public function setSeo($seo) 
	{
		$this->_seo = $seo instanceof Admin_Model_Seo ? $seo : new Admin_Model_Seo($seo);
		if (isset($this->_id) && !$this->_seo->getId()) $this->_seo->setId($this->_id);
		
		return $this;
	}
	
	public function getSnippet($length = 140)
	{
		return $this->_description ? $this->_description : strip_tags(substr($this->_content, 0, $length));
	}
	
	public static function getByUrl($url)
	{
		return self::getMapper()->getByUrl($url);
	}
	
	public function toArray()
	{
		$array = parent::toArray();
		
		if ($this->_gallery && count($this->_gallery)) {
			$array["mainItem"] = $this->_gallery->mainItem()->toArray();
		}

		return $array;
	}
}
