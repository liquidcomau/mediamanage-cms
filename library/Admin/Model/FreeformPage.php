<?php
class Admin_Model_FreeformPage extends Admin_Model_Master
{
	protected $_objectName = 'freeform_page';

	protected $_id = null;
	protected $_template = null;
	protected $_description = null;
	protected $_content = null;
	protected $_seo = null;

	public function getDescription()
	{
		return $this->_description;
	}
	
	public function setDescription($description)
	{
		$this->_description = $description;
		return $this;
	}
	
	public function getTemplate()
	{
		return $this->_template;
	}
	
	public function setTemplate($template)
	{
		$this->_template = $template;
		return $this;
	}
	
	public function getContent()
	{
		return $this->_content;
	}
	
	public function setContent($content)
	{
		$this->_content = $content;
		return $this;
	}
	
	public function getSeo()
	{
		return $this->_seo;
	}

	public function setSeo($seo)
	{
		$this->_seo = $seo instanceof Admin_Model_Seo ? $seo : new Admin_Model_Seo($seo);
		if (isset($this->_id) && !$this->_seo->getId()) $this->_seo->setId($this->_id);
	
		return $this;
	}
}
