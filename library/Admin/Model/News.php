<?php
class Admin_Model_News extends Admin_Model_Master
{
	protected $_objectName = 'news';
	
	protected $_id = null;
	protected $_description = null;
	protected $_content = null;
	protected $_featureImg = null;
	protected $_publishedDate = null;
	protected $_featured = null;
	
	protected $_categoryIds = null;
	protected $_categories = null;
	
	protected $_newsRecommendationIds = null;
	protected $_newsRecommendations = null;
	
	protected $_seo = null;
	
	public function getDescription()
	{
		return $this->_description;
	}
	
	public function getContent()
	{
		return $this->_content;
	}
	
	public function getFeatureImg()
	{
		return $this->_featureImg;
	}
	
	public function getPublishedDate($asTimestamp = false)
	{
		return $asTimestamp ? strtotime($this->_publishedDate) : $this->_publishedDate;
	}
        
        public function getFeatured()
	{
		return $this->_featured;
	}
	
	public function getCategoryIds()
	{
		return $this->_categoryIds;
	}
	
	public function getCategories()
	{
		return $this->_categories;
	}
	
	public function getNewsRecommendationIds()
	{
		return $this->_newsRecommendationIds;
	}
	
	public function getNewsRecommendations()
	{
		if (!$this->_newsRecommendationIds) {
			return [];
		}
		
		if (!$this->_newsRecommendations) {
			$recommendations = static::getAll(["in" => ["column" => "n.id", "values" => $this->_newsRecommendationIds]]);

			$map = array_combine(array_map(function($r) { return $r->getId(); }, $recommendations), $recommendations);

			// ensure they are in order according to our property
			$recommendations = array_map(function($id) use ($map) {
				return $map[$id];
			}, $this->_newsRecommendationIds);
			
			// unpublished will result in empty array values, filter out
			$this->_newsRecommendations = array_filter($recommendations, function($news) {
				return $news;
			});
		}
		
		return $this->_newsRecommendations;
	}

	public function getSeo()
	{
		return $this->_seo;
	}
	
	public function setDescription($description)
	{
		$this->_description = $description;
		return $this;
	}
	
	public function setContent($content)
	{
		$this->_content = $content;
		return $this;
	}
	
	public function setFeatureImg($img)
	{
		$this->_featureImg = $img;
		return $this;
	}
	
	public function setPublishedDate($date)
	{
		$this->_publishedDate = $date;
		return $this;
	}
        
	public function setFeatured($featured)
	{
		$this->_featured = $featured;
		return $this;
	}
	
	public function setCategoryIds($ids, $andPopulate = true)
	{
		$this->_categoryIds = $ids;
		
		if ($andPopulate) {
			$this->populateCategories();
		}
		
		return $this;
	}
	
	public function setNewsRecommendationIds($ids)
	{
		$this->_newsRecommendationIds = $ids;
	}
	
	public function setSeo($seo) 
	{
		$this->_seo = $seo instanceof Admin_Model_Seo ? $seo : new Admin_Model_Seo($seo);
		if (isset($this->_id) && !$this->_seo->getId()) $this->_seo->setId($this->_id);
		
		return $this;
	}
	
	public function getSnippet($length = 140)
	{
		return $this->_description ? $this->_description : strip_tags(substr($this->_content, 0, $length));
	}
	
	public static function getByUrl($url)
	{
		return self::getMapper()->getByUrl($url);
	}
	
	public static function getFeaturedNews()
	{
		return self::getMapper()->getFeaturedNews();
	}
	
	public static function getTotalPostsByCategory($category = null, $published = true)
	{
		return self::getMapper()->getTotalPostsByCategory($category, $published);
	}
	
	protected function populateCategories()
	{
		if (!count($this->_categoryIds)) return;
		
		$class = Admin_Factory::getClass("Model_Category");
		$this->_categories = $class::getAll(["in" => $this->_categoryIds]);
		
		return true;
	}
	
	public function toArray()
	{
		if (isset ($this->_categories)  && count($this->_categories)) {
			$this->_categories = array_map(function($category) {
				return is_object($category) ? $category->toArray() : $category;
			}, $this->_categories);
		}
		
		return parent::toArray();
	}

	public static function getPrevious($date, $limit = 1, $published = true)
	{
		if ($date instanceof Admin_Model_News) {
			$date = $date->getPublishedDate();
		}	

		$clauses["limit"] = $limit;	
		$clauses["order"] = "publishedDate DESC";
		$clauses["publishedDateLessThan"] = $date;

		return self::getMapper()->getAll($clauses)[0];
	}

	public static function getNext($date, $limit = 1, $published = true)
	{
		if ($date instanceof Admin_Model_News) {
			$date = $date->getPublishedDate();
		}	

		$clauses["limit"] = $limit;	
		$clauses["order"] = "publishedDate ASC";
		$clauses["publishedDateGreaterThan"] = $date;

		return self::getMapper()->getAll($clauses)[0];
	}
}
