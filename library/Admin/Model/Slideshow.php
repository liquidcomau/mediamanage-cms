<?php
class Admin_Model_Slideshow extends Admin_Model_Resource
{
	protected $_id = null;
	protected $_name = null;
	protected $_settings = null;
	protected $_data = null;
	
	public function getId() 
	{
		return $this->_id;
	}
	
	public function getName()
	{
		return $this->_name;
	}
	
	public function getSettings()
	{
		return $this->_settings;
	}
	
	public function getData()
	{
		return $this->_data;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
		return $this;
	}
	
	public function setName($name)
	{
		$this->_name = $name;
		return $this;
	}
	
	public function setSettings($settings)
	{
		$this->_settings = is_array($settings) ? $settings : json_decode($settings, true);
		return $this;
	}
	
	public function setData($data)
	{
		$this->_data = $data !== null && is_array($data) ? $data : json_decode($data, true);
		return $this;
	}
	
	public static function getSlideshowByName($name, $format = null)
	{
		return self::getMapper()->getSlideshowByName($name, $format);
	}
}