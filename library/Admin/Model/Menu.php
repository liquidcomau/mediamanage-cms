<?php
class Admin_Model_Menu extends Admin_Model_Master
{
	protected $_objectName = 'menu';
	
	public static function getMainMenu()
	{
		return self::getMapper()->getMainMenu();
	}
	
	public static function getAdminMenu()
	{
		return parent::getAdminMenuLayout();
	}
}