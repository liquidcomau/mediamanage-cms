<?php
class Admin_Model_Link extends Admin_Model_Master
{
	protected $_objectName = 'link';
	
	protected $_href;
	protected $_pageId;
	protected $_newWindow;
	protected $_linkType; // ('internal'|'external')
	
	public function getHref() 
	{
		return $this->_href;
	}
	
	public function getPageId()
	{
		return $this->_pageId;
	}
	
	public function getNewWindow()
	{
		return $this->_newWindow;
	}
	
	public function getLinkType()
	{
		return $this->_linkType;
	}
	
	public function setHref($href)
	{
		if ($href) {
			$this->_href= $href;
			$this->_pageId = null;
		
			$this->_linkType = 'external';
		}
		
		return $this;
	}
	
	public function setPageId($id)
	{
		if ($id) {
			$this->_pageId = $id;
			$this->_href = null;
		
			$this->_linkType = 'internal';
		}
					
		return $this;
	}
	
	public function setNewWindow($nw)
	{
		$this->_newWindow = $nw;
		return $this;
	}
	
	public function setLinkType()
	{
		throw new Exception('Cannot explicitly set link type. Link type is inferred from external href or internal reference.');
	}
	
	/**
	 * Provides either the external link
	 * or the fully resolved internal link 
	 * 
	 * @return string
	 */
	public function getLink()
	{
		return 'http://' . 'lol';
	}
}