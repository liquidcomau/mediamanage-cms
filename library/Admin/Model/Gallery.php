<?php
class Admin_Model_Gallery extends Admin_Model_Master
{
	protected $_objectName = "gallery";
	protected static $_hasCategories = false;
	
	protected $_description = null;
	protected $_content = null;
	protected $_identifier = null;
	protected $_images = null;
	protected $_publishedDate = null;
	protected $_categoryIds = null;
	protected $_categories = null;
	
	public function getDescription()
	{
		return $this->_description;
	}
	
	public function getContent()
	{
		return $this->_content;;
	}
	
	public function getIdentifier()
	{
		return $this->_identifier;
	}
	
	public function getImages()
	{
		return $this->_images;
	}
	
	public function getPublishedDate($asTimestamp = false)
	{
		return $asTimestamp ? strtotime($this->_publishedDate) : $this->_publishedDate;
	}
	
	public function getCategoryIds()
	{
		if (!static::$_hasCategories) {
			throw new Exception("Gallery model does not accept categories");
		}
		
		return $this->_categoryIds;
	}
	
	public function getCategories()
	{
		if (!static::$_hasCategories) {
			throw new Exception("Gallery model does not accept categories");
		}
		
		return $this->_categories;
	}
	
	public function setDescription($description)
	{
		$this->_description = $description;
		return $this;
	}
	
	public function setContent($content)
	{
		$this->_content = $content;
		return $this;
	}

	public function setIdentifier($identifier)
	{
		$this->_identifier = $identifier;
		return $this;
	}
	
	public function setImages($images)
	{
		$this->_images = $images;
		return $this;
	}
	
	public function setPublishedDate($date)
	{
		$this->_publishedDate = $date;
		return $this;
	}
	
	public function setCategoryIds($ids, $andPopulate = true)
	{
		if (!static::$_hasCategories) {
			throw new Exception("Gallery model does not accept categories");
		}
		
		$this->_categoryIds = $ids;
		
		if ($andPopulate) {
			$this->populateCategories();
		}
		
		return $this;
	}
	
	public static function getGalleryByIdentifier($identifier, $format = null)
	{
		return self::getMapper()->getGalleryByIdentifier($identifier, $format);
	}

	public function getMainImg()
	{
		if (!$this->_images) {
			return null;
		}

		$mainImg = null;
		foreach ($this->_images as $image) {
			if ($image["main"]) {
				$mainImg = $image;
				break;
			}
		}

		return $mainImg ?: $this->_images[0];
	}
	
	protected function populateCategories()
	{
		if (!count($this->_categoryIds)) return;
		
		$class = Admin_Factory::getClass("Model_Category");
		$this->_categories = $class::getAll(["in" => $this->_categoryIds]);
		
		return true;
	}
	
	public static function hasCategories()
	{
		return static::$_hasCategories;
	}
	
	public static function categories()
	{
		if (!static::$_hasCategories) {
			throw new Exception("Gallery model does not accept categories");
		}
		
		return static::getMapper()->categories();
	}
	
	public function toArray()
	{
		if (isset ($this->_categories)  && count($this->_categories)) {
			$this->_categories = array_map(function($category) {
				return is_object($category) ? $category->toArray() : $category;
			}, $this->_categories);
		}
		
		$gallery = parent::toArray();
		
		$gallery["mainImg"] = $this->getMainImg();
		return $gallery;
	}
	
	public static function getTotalsByCategory($category = null, $published = true)
	{
		if (!static::$_hasCategories) {
			throw new Exception("Gallery model does not accept categories");
		}
		
		return self::getMapper()->getTotalsByCategory($category, $published);
	}
}
