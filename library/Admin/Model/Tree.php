<?php
class Admin_Model_Tree extends Admin_Model_Resource
{
	protected $_pages = null;
	
	public function getPages()
	{
		return $this->_pages;
	}
	
	public function setPages($pgs)
	{
		$this->_pages = $pgs;
		return $this;
	}
	
	public static function getAll()
	{
		return parent::get();
	}
	
	// leaving this method empty/open in case any other changes
	// can be made from tree menu in future... proxying to
	// updatePagePosition() for now	
	public static function updatePage($pg)
	{
		if (isset($pg['oldPosition'])) return self::updatePagePosition($pg);
	}
	
	/**
	 * Returns the number of page (master) rows updated
	 * 
	 * @param unknown $pg
	 * @return unknown
	 */
	public static function updatePagePosition($pg)
	{
		return self::getMapper()->updatePagePosition($pg);
	}
}