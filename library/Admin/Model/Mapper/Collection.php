<?php
class Admin_Model_Mapper_Collection extends Admin_Model_Mapper_ResourceAbstract
{
	/**
	 * Lists the join table names for each model's entityName (objectName)
	 * 
	 * @var array
	 */
	protected static $_joinTables = [
		"static_page" => "collection_static_page",
		"freeform_page" => "collection_freeform_page"
	];
	
	/**
	 * Does not include entityIds by default.
	 * 
	 * @param type $clauses
	 * @return null
	 */
	public function getAll($clauses, $includeEntities = false)
	{
		$tbl = Admin_Factory::create('DbTable_Collection');
		$recs = $tbl->fetch($clauses)->toArray();

		if (!count($recs)) return null;
		
		return array_map(function($data) use ($includeEntities) {
			if ($includeEntities === true) {
				$data["entityIds"] = $this->getEntityIds($data["objectName"], $data["id"]);
			}
			return new $this->_model($data["objectName"], $data);
		}, $recs);
	}
	
	/**
	 * Includes entityIds
	 * 
	 * @param type $id
	 * @return null|\_model
	 */
	public function get($id)
	{
		$tbl = Admin_Factory::create('DbTable_Collection');
		
		$record = $tbl->fetchById($id);
		if (!$record) return null;
		
		$record = $record->toArray();
		$record["entityIds"] = $this->getEntityIds($record["objectName"], $record["id"]);
		
		return new $this->_model($record["objectName"], $record);
	}
	
	public function getByIdentifier($identifier)
	{
		$tbl = Admin_Factory::create('DbTable_Collection');
		
		$record = $tbl->fetchByIdentifier($identifier);
		if (!$record) return null;
		
		$record = $record->toArray();
		$record["entityIds"] = $this->getEntityIds($record["objectName"], $record["id"]);
		
		return new $this->_model($record["objectName"], $record);
	}
	
	public function getEntityIds($entityName, $id)
	{
		$joinTblName = static::$_joinTables[$entityName];
		$joinTbl = new Admin_DbTable($joinTblName);

		$select = $joinTbl->select();
		$select->from(["j" => $joinTblName], 
						["GROUP_CONCAT(j.entityId ORDER BY j.position) as entityIds"])
				->where("j.collectionId = ?", $id);

		$entityIdsRec = $joinTbl->fetchRow($select)->toArray();
		return explode(',', $entityIdsRec["entityIds"]);
	}
	
	public function create($model)
	{	
		$tbl = Admin_Factory::create('DbTable_Collection');
		
		$props = $model->toArray();
		$props["object_type"] = array_flip(static::getObjectTypes())[$props["entityName"]];

		$collectionId = $props['id'] = $tbl->create($this->filterCollectionKeys($props));
		
		if (isset($props['entityIds'])) {
			$this->updateEntityReferences($props["entityName"], $collectionId, $props["entityIds"]);
		}
		
		return $props['id'];
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		$props["object_type"] = array_flip(static::getObjectTypes())[$props["entityName"]];
		
		$tbl = Admin_Factory::create('DbTable_Collection');
		
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($this->filterCollectionKeys($props), $where);
		
		if (isset($props['entityIds'])) {
			$this->updateEntityReferences($props["entityName"], $props["id"], $props["entityIds"]);
		}
		
		return true;
	}
	
	public function deleteById($id)
	{
		return Admin_Factory::create('DbTable_Collection')->deleteById($id);
	}
	
	public function filterCollectionKeys($properties)
	{
		$keys = array(
				'id',
				'heading',
				'identifier',
				'object_type'
		);
		return array_intersect_key($properties, array_flip($keys));
	}
	
	protected function updateEntityReferences($entityName, $collectionId, array $entityIds)
	{
		$joinTbl = new Zend_Db_Table(static::$_joinTables[$entityName]);

		$select = $joinTbl->select()->where("collectionId = ?", $collectionId);

		if ($collectionId) {
			$existing = $this->getEntityIds($entityName, $collectionId);
		}
		if (!$existing) $existing = [];
		$toDelete = array_diff($existing, $entityIds);
		
		if (count($toDelete)) {
			$where = [];
			$where[] = $joinTbl->getAdapter()->quoteInto("entityId IN(?)", $toDelete);
			$where[] = $joinTbl->getAdapter()->quoteInto("collectionId = ?", $collectionId);
			
			$joinTbl->delete($where);
		}
		
		$toUpdate = array_intersect($entityIds, $existing);
		for ($i = 0; $i < count($entityIds); $i++) {
			
			$entityId = $entityIds[$i];
			
			if (in_array($entityId, $toUpdate)) {
				$where = [];
				$where[] = $joinTbl->getAdapter()->quoteInto("entityId = ?", $entityId);
				$where[] = $joinTbl->getAdapter()->quoteInto("collectionId = ?", $collectionId);
				
				$joinTbl->update(["position" => $i], $where);
			} else {
				$joinTbl->insert(["entityId" => $entityId, "collectionId" => $collectionId, "position" => $i]);
			}
		}

		return true;
	}
}
