<?php
class Admin_Model_Mapper_Agent extends Admin_Model_Mapper_ResourceAbstract
{
public function getAll($clauses)
	{
		$tbl = Admin_Factory::create('DbTable_Agent');
		
		$recs = $tbl->fetch($clauses)->toArray();

		if (!count($recs)) return null;
		
		return array_map(function($data) {
			$data['agency'] = $this->getAgencySubModel($data);
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = Admin_Factory::create('DbTable_Agent');
		
		$record = $tbl->fetchById($id);
		if (!$record) return null;
		
		$data = $record->toArray();
		$data['agency'] = $this->getAgencySubModel($data);
		
		return new $this->_model($data);
	}
	
	public function create($model)
	{	
		$tbl = Admin_Factory::create('DbTable_Agent');
		$props = $model->toArray();
		
		return $tbl->create($this->prepareAgentKeys($props));
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		
		$tbl = Admin_Factory::create('DbTable_Agent');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($this->prepareAgentKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		return Admin_Factory::create('DbTable_Agent')->deleteById($id);
	}
	
	public function prepareAgentKeys($props)
	{
		$keys = ['id', 'name', 'email', 'phone', 'profilePicture', 'agencyId'];
		return array_intersect_key($props, array_flip($keys));
	}

	public function getAgencySubModel($props) 
	{
		$agencyMapper = Admin_Factory::create("Model_Mapper_Agency");

		$agencyId = $props['agencyId'];
		$agencyProps = array_diff_key($props, $this->prepareAgentKeys($props));
		$agencyProps['id'] = $agencyId;
		
		$agencyKeys = $agencyMapper->prepareAgencyKeys($agencyProps);
		$agencyKeys['name'] = $agencyKeys['agencyName'];
		$agencyKeys['email'] = $agencyKeys['agencyEmail'];
		
		return Admin_Factory::create("Model_Agency", [$agencyKeys]);
	}
}
