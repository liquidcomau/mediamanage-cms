<?php
class Admin_Model_Mapper_Fragment extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses)
	{
		$tbl = new Admin_DbTable_Fragment();
		$records = $tbl->fetch($clauses)->toArray();
		
		if (!count($records)) return null;
		
		return array_map(function($data) use ($records) {
			return new $this->_model($data);
		}, $records);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Fragment();
		
		$record = $tbl->fetchById($id);
		if (!$record) return null;
		
		$instance = new $this->_model($record->toArray());

		return $instance;
	}
	
	public function create($model)
	{	
		$tbl = new Admin_DbTable_Fragment();
		$props = $model->toArray();

		return $tbl->create($this->filterFragmentKeys($props));
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		
		$tbl = new Admin_DbTable_Fragment();
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($this->filterFragmentKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		$tbl = new Admin_DbTable_Fragment();
		return $tbl->deleteById($id);
	}
	
	public function filterFragmentKeys($props)
	{
		$keys = [
			"id",
			"internalLabel",
			"identifier",
			"content"
		];
		
		return array_intersect_key($props, array_flip($keys));
	}
}