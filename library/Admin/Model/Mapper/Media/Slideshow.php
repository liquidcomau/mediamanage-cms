<?php
class Admin_Model_Mapper_Media_Slideshow extends Admin_Model_Mapper_Media_Gallery
{
	protected function constructDbTable()
	{
		return Admin_Factory::create("DbTable_MediaCollection", ["slideshow"]);
	}
	
	protected function mediaCollectionItemClass()
	{
		return Admin_Factory::getClass("Model_Media_Item_Slideshow");
	}
}