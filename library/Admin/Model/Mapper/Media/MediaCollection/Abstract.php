<?php
abstract class Admin_Model_Mapper_Media_MediaCollection_Abstract extends Admin_Model_Mapper_ResourceAbstract
{
	public static $_typeIds = null;
	
	public static function getTypeIdFromType($typeName)
	{
		if (!self::$_typeIds) {
			$tbl = new Zend_Db_Table("media_collection_type");
			$recs = $tbl->fetchAll();
			
			$index = [];
			foreach ($recs as $rec) {
				$index[$rec->mediaCollectionType] = $rec->typeId;
			}
			
			self::$_typeIds = $index;
		}
		
		return self::$_typeIds[$typeName];
	}
}