<?php
class Admin_Model_Mapper_Media_Item extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses)
	{
		$tbl = Admin_Factory::create("DbTable_MediaItem");
		$recs = $tbl->fetch($clauses)->toArray();
		
		if (!count($recs)) return null;
		
		return array_map(function($data) use ($format) {
			return new $this->_model($data);
		}, $recs);
	}
	
	public function getAllByMediaCollectionId($mediaCollectionId)
	{
		return $this->getAll(["mediaCollectionId" => $mediaCollectionId]);
	}
	
	public function get($itemId)
	{
		$tbl = Admin_Factory::create("DbTable_MediaItem");
		$rec = $tbl->fetchById($itemId);
		
		if (!$rec) {
			return;
		} else {
			$rec = $rec->toArray();
		}
		return new $this->_model($rec);
	}
	
	public function create($model)
	{
		$iTbl = Admin_Factory::create("DbTable_MediaItem");
		$vTbl = Admin_Factory::create("DbTable_MediaItemView");

		$props = $model->toArray();
		$itemId = $iTbl->insert($this->filterItemKeys($props));
		
		$props["itemId"] = $itemId;
		$model->setItemId($itemId);
		$model->getView()->setItemId($itemId);
		
		$vTbl->insert($this->prepareItemViewKeys($model->getView()));

		return $itemId;
	}
	
	public function update($model)
	{
		$iTbl = Admin_Factory::create("DbTable_MediaItem");
		$vTbl = Admin_Factory::create("DbTable_MediaItemView");
		
		$props = $model->toArray();
		
		$where = $iTbl->getAdapter()->quoteInto('itemId = ?', $model->getItemId());
		
		$iTbl->update($this->filterItemKeys($props), $where);
		$vTbl->update($this->prepareItemViewKeys($model->getView()), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		$tbl = Admin_Factory::create("DbTable_MediaItem");
		$where = $tbl->getAdapter()->quoteInto('itemId = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function filterItemKeys($properties)
	{
		$keys = array(
			'itemId',
			'mediaCollectionId',
			'position',
			'caption',
			'main'
		);
	
		$filledProps = array_intersect_key($properties, array_flip($keys));
		return $filledProps;
	}
	
	public function prepareItemViewKeys($viewModel)
	{
		$props = [];
		
		$props["itemId"] = $viewModel->getItemId();
		$props["source"] = $viewModel->getSource();
		$props["viewType"] = $viewModel->getViewType();
		
		if ($props["viewType"] === "youtube") {
			$props["thumbnailSource"] = $viewModel->getThumbnailSource();
			$props["displaySource"] = $viewModel->getDisplaySource();
		}
		
		return $this->filterItemViewKeys($props);
	}
	
	public function filterItemViewKeys($properties)
	{
		$keys = array(
			'itemId',
			'source',
			'displaySource',
			'thumbnailSource',
			'viewType'
		);
	
		$filledProps = array_intersect_key($properties, array_flip($keys));
		return $filledProps;
	}
}
