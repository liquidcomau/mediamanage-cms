<?php
class Admin_Model_Mapper_Media_Gallery extends Admin_Model_Mapper_Media_MediaCollection_Abstract
{
	public function getAll($clauses, $format)
	{
		// Should not be able to get all as MediaCollection components are components of larger models?
		throw new Exception("Not allowed to get all media collections as instances are components of larger models (slideshow, page, etc)");
		
		/*
		$tbl = $this->constructDbTable();
		
		$recs = $tbl->fetch($clauses);

		if (!count($recs)) {
			return null;
		} else {
			$recs = $recs->toArray();
		}

		$itemClass = $this->mediaCollectionItemClass();
		return array_map(function($data) use ($itemClass) {
			// should prob do this with just one query instead of in a loop?
			$instance = new $this->_model($data);
			$items = $itemClass::getAllByMediaCollectionId($instance->getMediaCollectionId());
			
			$instance->setItems($items);
			return $instance;
		}, $recs);
		 * 
		 */
	}
	
	public function get($id)
	{
		$tbl = $this->constructDbTable();
		$rec = $tbl->fetchById($id);

		if (!$rec) {
			return;
		} else {
			$instance = new $this->_model($rec->toArray());
		}

		$itemClass = $this->mediaCollectionItemClass();
		$items = $itemClass::getAllByMediaCollectionId($instance->getMediaCollectionId());
		if ($items) {
			$instance->setItems($items);
		}
		
		return $instance;
	}
	
	public function getByRelationshipId($id)
	{
		$tbl = $this->constructDbTable();
		$rec = $tbl->fetchByRelationshipId($id);

		if (!$rec) {
			return;
		} else {
			$instance = new $this->_model($rec->toArray());
		}
		$itemClass = $this->mediaCollectionItemClass();
		$items = $itemClass::getAllByMediaCollectionId($instance->getMediaCollectionId());
		if ($items) {
			$instance->setItems($items);
		}
		return $instance;
	}

	public function create($model)
	{	
		$tbl = $this->constructDbTable();
		$props = $model->toArray();
		$values = $this->filterMediaCollectionKeys($props);
		
		$values["typeId"] = self::getTypeIdFromType($model->getMediaCollectionType());
		
		$id = $tbl->create($values);
		$model->setMediaCollectionId($id);
		
		if (isset($props["items"])) {
			$this->updateItems($id, $model->getItems());
		}
		
		return $id;
	}
	
	public function update($model)
	{
		$tbl = $this->constructDbTable();
		
		$where = $tbl->getAdapter()->quoteInto('mediaCollectionId = ?', $model->getMediaCollectionId());
		
		$props = $model->toArray();
		$values = $this->filterMediaCollectionKeys($props);
		
		$values["typeId"] = self::getTypeIdFromType($model->getMediaCollectionType());
		
		$tbl->update($values, $where);
		
		if (isset($props["items"])) {
			$this->updateItems($props["mediaCollectionId"], $model->getItems());
		}
		return true;
	}
	
	public function deleteById($id)
	{
		$tbl = new Zend_Db_Table('master');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function filterMediaCollectionKeys($properties)
	{
		$keys = array(
			'mediaCollectionId',
			'internalLabel',
			'identifier',
			'relationshipId'
		);
	
		// type ID added in create/update methods
		
		return array_intersect_key($properties, array_flip($keys));
	}
	
	// handles saving/updating/deleting a collection's media items
	// delegates each crud operation to item's model class
	protected function updateItems($mediaCollectionId, $items)
	{
		$newItemIds = array_map(function($item) {
			if (!is_array($item)) {
				$item = $item->toArray();
			}
			
			return $item["itemId"];
		}, $items);
		
		// create index with ID as key
		$itemIndex = array_combine($newItemIds, $items);
		
		$itemTbl = Admin_Factory::create("DbTable_MediaItem");

		$existingRecs = $itemTbl->fetch(["mediaCollectionId" => $mediaCollectionId])->toArray();
		$existingIds = array_map(function($row) {
			return $row["itemId"];
		}, $existingRecs);

		if (!$existingIds) $existingIds = [];
		$toDelete = array_diff($existingIds, $newItemIds);

		foreach ($toDelete as $id) {
			$itemTbl->deleteById($id);
		}

		for ($i = 0; $i < count($items); $i++) {
			$item = $items[$i];
			$item->setPosition($i);
			
			if ($item->getItemId()) {
				// update if already has an id
				$item->update();
			} else {
				// else insert
				$item->setMediaCollectionId($mediaCollectionId);
				$item->create();
			}
		}

		return true;
	}
	
	protected function constructDbTable()
	{
		return Admin_Factory::create("DbTable_MediaCollection", ["gallery"]);
	}
	
	protected function mediaCollectionItemClass()
	{
		return Admin_Factory::getClass("Model_Media_Item");
	}
}
