<?php
class Admin_Model_Mapper_Redirect extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses)
	{
		$tbl = new Zend_Db_Table("redirect");
		$select = $tbl->select()
						->order("url ASC");
		
		$recs = $tbl->fetchAll($select)->toArray();
		
		if (!count($recs)) {
			return null;
		}
		
		return array_map(function($data) {
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Zend_Db_Table("redirect");
		$select = $tbl->select()->where("id = ?", $id);
		
		$record = $tbl->fetchRow($select);
		
		if (!$record) return null;
		
		return new $this->_model($record->toArray());
	}
	
	public function create($model)
	{	
		$tbl = new Zend_Db_Table("redirect");
		
		$props = $model->toArray();
		$props['id'] = $tbl->insert($this->filterRedirectKeys($props));
		
		return $props['id'];
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		
		$tbl = new Zend_Db_Table("redirect");
		
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($this->filterRedirectKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		if (!$id) {
			throw new Exception("Must provide ID to deleteById method");
		}
		
		$tbl = new Zend_Db_Table("redirect");
		$where = $tbl->getAdapter()->quoteInto("id = ?", $id);
		
		return $tbl->delete($where);
	}
	
	public function filterRedirectKeys($properties)
	{
		$keys = [
				'id',
				'url',
				'destination',
				'type'
		];
		
		return array_intersect_key($properties, array_flip($keys));
	}
}