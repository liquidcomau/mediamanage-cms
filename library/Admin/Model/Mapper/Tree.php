<?php
class Admin_Model_Mapper_Tree extends Admin_Model_Mapper_ResourceAbstract
{
	public function get()
	{
		$model = new $this->_model;
		
		$pgs = (new Admin_DbTable_Master())->fetchNodes(array('order' => 'position ASC'))->toArray();
		$nodes = array_map(function($node) {
			$n = array(
				'id' => $node['id'],
				'data' => array(
					'position' => $node['position'],
					'type' => $node['objectName'],
					"published" => $node["published"]
				),
				'parent' => (isset($node['parentId']) ? $node['parentId'] : '#')
			);
			
			if ($node['internalLabel']) {
				$n["text"] = $node['internalLabel'];
			} elseif ($node['linkText']) {
				$n["text"] = $node['linkText'];
			} else {
				$n["text"] = $node['heading'];
			}
			
			if ($node["url"]) {
				$n["data"]["url_part"] = $node["url"];
			}
			
			return $n;
		}, $pgs);

		$model->setPages($nodes);
		return $model;
	}
	
	public function update()
	{
		
	}
	
	public function updatePagePosition($pg)
	{
		$tbl = new Admin_DbTable_Master();
		$where = $tbl->getAdapter()->quoteInto('id = ?', $pg['id']);
		
		// start transaction
		if ($pg['parentId'] === $pg['oldParentId']) {
			// same parent - alter children in same parent

			$direction = $pg['position'] - $pg['oldPosition'];
			if ($direction > 0) {
				$res = $this->decrementChildPositions($pg['parentId'], $pg['oldPosition'], $pg['position']);				
			} else {
				$res = $this->incrementChildPositions($pg['parentId'], $pg['position'], $pg['oldPosition']);
			}
		} else {
			// parent changed - alter children in both old and new parent
			
			$res = $this->decrementChildPositions($pg['oldParentId'], $pg['oldPosition']);
			$res += $this->incrementChildPositions($pg['parentId'], $pg['position']);
		}
		
		$res += $tbl->update(array(
				'parentId' => $pg['parentId'],
				'position' => $pg['position']
		), $where);
		
		// end transaction
		
		return $res;
	}
	
	/**
	 * LOl this actually worked. Faith in Zend gateway = woo.
	 * Used to shift a bunch of rows underneath a parentId up or down in bulk.
	 * 
	 * Returns number of rows updated.
	 * 
	 * @param string $operation. 'increment' or 'decrement'
	 * @param int $parentId
	 * @param int $from. inclusive.
	 * @param int $to. inclusive.
	 */
	public function updateChildPositions($operation, $parentId, $from, $to)
	{
		// should this logic be moved to the database gateway rather than the mapper?
		
		$tbl = new Admin_DbTable_Master();
		
		$where = array(
			'position >= ?' => $from
		);
		
		if ($to) $where['position <= ?'] = $to;
		$where['parentId = ?'] = $parentId; 
		
		/*
		if ($parentId) {
			$where['parentId = ?'] = $parentId;
		} else {
			$where['parentId ?'] = new Zend_Db_Expr('IS NULL');
		}
		*/
		
		if ($operation === 'increment') {
			$symbol = '+';
		} elseif ($operation === 'decrement') {
			$symbol = '-';
		}

		return $tbl->update(array('position' =>  new Zend_Db_Expr('position ' . $symbol . ' 1')), $where);
	}
	
	public function incrementChildPositions($parentId, $from, $to)
	{
		return $this->updateChildPositions('increment', $parentId, $from, $to);
	}
	
	public function decrementChildPositions($parentId, $from, $to)
	{
		return $this->updateChildPositions('decrement', $parentId, $from, $to);
	}
}