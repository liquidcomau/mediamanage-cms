<?php
abstract class Admin_Model_Mapper_ResourceAbstract
{
	public function __construct()
	{
		$class = str_replace('_Mapper_', '_', get_class($this));
		
		// Remove Admin/Project suffix to prepare for factory
		$class = preg_replace('/^(?:Admin_|Project_)/', '', $class);

		$this->_model = Admin_Factory::getClass($class);
	}
	
	public static function getObjectTypes()
	{
		$tbl = new Zend_Db_Table('object_type');
		$records = $tbl->fetchAll()->toArray();
		
		return array_column($records, 'objectName', 'id');
	}
	
	/**
	 * Returns an array key difference with keys belong to a
	 * master table from a supplied array.
	 * 
	 * @param array $properties: array('id' => 2, 'parentId' => 1, 'heading' => 'Lorem Ipsum Dolor'... )
	 * @return assoc array:
	 */	
	public function filterMasterKeys($properties)
	{
		$keys = array(
				'id',
				'parentId',
				'objectTypeId',
				'heading',
				'subheading',
				'linkText',
				'url',
				'internalLabel',
				'published',
				'hidden',
				'createdDate',
				'lastModified',
				'position'
		);
		return array_intersect_key($properties, array_flip($keys));
	}
	
	/**
	 * Same as filterMasterKeys, but for 'page' table
	 * 
	 * @param array $properties
	 * @return assoc array:
	 */
	public function filterPageKeys($properties)
	{
		$keys = array(
				'id',
				'url',
				'description',
				'content',
				'featureImg',
				'linkSubheading'
		);
		return array_intersect_key($properties, array_flip($keys));
	}
	
	public function getIdFromUrl($url)
	{
		$rec = (new Admin_DbTable_Master())->fetchByUrl($url);
		return $rec ? $rec->id : null;
	}
	
	public function getMasterChildNodes($model)
	{
		if (!($model instanceof Admin_Model_Master)) throw new Exception("Resource does not have a master parent");
		return Admin_Factory::create("DbTable_Master")->fetchByParentId($model->getId());
	}
	
	public function getRootParentNode($model)
	{
		if (!($model instanceof Admin_Model_Master)) throw new Exception("Resource does not have a master parent");
		
		$tbl = new Admin_DbTable_Master();
		$getRootParentNode = function ($node) use ($tbl, &$getRootParentNode) {
			if ($node instanceof Admin_Model_Master) {
				$id = $node->getId();
				$parentId = $node->getParentId();
			} else {
				$id = $node->id;
				$parentId = $node->parentId;
			}
			
			if ($parentId == 0) {
				return $tbl->fetchById($id);
			} else {
				return $getRootParentNode($tbl->fetchById($parentId));
			}
		};
		
		return $getRootParentNode($model);
	}
	
	public function countPublished($type, $params = null)
	{
		return (new Admin_DbTable_Master())->countPublished($type, $params);
	}
}
