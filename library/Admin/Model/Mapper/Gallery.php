<?php
class Admin_Model_Mapper_Gallery extends Admin_Model_Mapper_ResourceAbstract
{
	protected $_categoryJoinTable = "gallery_category";
	protected $_categoryModel = "Model_Category";
	
	public function getAll($clauses, $format)
	{
		$tbl = Admin_Factory::create("DbTable_Gallery");
		$recs = $tbl->fetch($clauses)->toArray();
		
		return array_map(function($data) use ($format, $seoData) {
			if (isset($data['images'])) $data['images'] = json_decode($data['images'], true);
			if (isset($data["categoryIds"])) {
				$data["categoryIds"] = explode(",", $data["categoryIds"]);
			}
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = Admin_Factory::create("DbTable_Gallery");

		$rec = $tbl->fetchById($id);
		
		if (!$rec) {
			return;
		} else {
			$rec = $rec->toArray();
		}
		
		if (isset($rec['images'])) {
			$rec['images'] = json_decode($rec['images'], true);
		}
		
		if (isset($rec["categoryIds"])) {
			$rec["categoryIds"] = explode(",", $rec["categoryIds"]);
		}

		return new $this->_model($rec);
	}
	
	public function create($model)
	{	
		$mTbl = Admin_Factory::create('DbTable_Master');
		$pTbl = Admin_Factory::create('DbTable', ['page']);
		$tbl = Admin_Factory::create('DbTable_Gallery');
		
		$props = $model->toArray();
		
		if (!$props["createdDate"]) {
			$props["createdDate"] = date("Y-m-d H:i:s");
		}

		$id = $props['id'] = $mTbl->create($this->filterMasterKeys($props));
		$pTbl->create($this->filterPageKeys($props));
		
		if (isset($props['images'])) $props['images'] = json_encode($props['images']);
		$tbl->create($this->filterGalleryKeys($props));
		
		if (isset($props["categoryIds"])) {
			$this->updateCategoryReferences($props["id"], $props["categoryIds"]);
		}

		return $id;
	}
	
	public function update($model)
	{
		$mTbl = Admin_Factory::create('DbTable_Master');
		$pTbl = Admin_Factory::create('DbTable', ['page']);
		$tbl = Admin_Factory::create('DbTable_Gallery');
		
		$props = $model->toArray();
		
		$where = $tbl->getAdapter()->quoteInto('id = ?', $props["id"]);
		
		$mTbl->update($this->filterMasterKeys($props), $where);
		$pTbl->update($this->filterPageKeys($props), $where);
		
		if (isset($props["categoryIds"])) {
			$this->updateCategoryReferences($props["id"], $props["categoryIds"]);
		}
		
		if (isset($props['images'])) $props['images'] = json_encode($props['images']);
		
		$tbl->update($this->filterGalleryKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		$tbl = new Zend_Db_Table('master');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function filterGalleryKeys($properties)
	{
		$keys = array(
				'id',
				'images',
				'identifier',
				'publishedDate'
		);
		return array_intersect_key($properties, array_flip($keys));
	}
	
	public function getGalleryByIdentifier($identifier, $format = null)
	{
		$tbl = new Admin_DbTable_Gallery();
		$where = $tbl->getAdapter()->quoteInto('identifier = ?', $identifier);
		
		$rec = $tbl->fetchByIdentifier($identifier);
		
		if (!$rec) return;
		$rec = $rec->toArray();
		
		if (isset($rec['images'])) $rec['images'] = json_decode($rec['images'], true);

		$gallery = Admin_Factory::create('Model_Gallery', [$rec]);

		return $format !== Admin_Constants::TYPE_ARRAY 
								? $gallery
								: $gallery->toArray();
	}
	
	public function categories()
	{
		$catModel = Admin_Factory::getClass($this->_categoryModel);
		return $catModel::getAll();
	}
	
	protected function updateCategoryReferences($galleryId, array $categoryIds)
	{
		$joinTbl = new Zend_Db_Table($this->_categoryJoinTable);

		$select = $joinTbl->select()->where("galleryId = ?", $galleryId);

		$existing = $joinTbl->fetchAll($select)->toArray();
		$existing = array_map(function($row) {
			return $row["categoryId"];
		}, $existing);

		if (!$existing) $existing = [];
		$toDelete = array_diff($existing, $categoryIds);

		if (count($toDelete)) {
			$where = [];
			$where[] = $joinTbl->getAdapter()->quoteInto("categoryId IN(?)", $toDelete);
			$where[] = $joinTbl->getAdapter()->quoteInto("galleryId = ?", $galleryId);
			
			$joinTbl->delete($where);
		}

		$toInsert = array_diff($categoryIds, $existing);
		foreach ($toInsert as $categoryId) {
			$joinTbl->insert(["galleryId" => $galleryId, "categoryId" => $categoryId]);
		}

		return true;
	}
	
	public function getTotalsByCategory($category = null, $available = true)
	{
		$tbl = Admin_Factory::create("DbTable_Gallery");
		return $tbl->fetchTotalsByCategory($category, $available);
	}
}
