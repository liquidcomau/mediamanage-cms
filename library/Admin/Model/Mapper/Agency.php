<?php
class Admin_Model_Mapper_Agency extends Admin_Model_Mapper_ResourceAbstract
{
public function getAll($clauses)
	{
		$tbl = Admin_Factory::create('DbTable_Agency');
		
		$recs = $tbl->fetch($clauses)->toArray();

		if (!count($recs)) return null;
		
		return array_map(function($data) {
			$data['name'] = $data['agencyName'];
			$data['email'] = $data['agencyEmail'];

			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = Admin_Factory::create('DbTable_Agency');
		
		$record = $tbl->fetchById($id);
		if (!$record) return null;
		
		$data = $record->toArray();
		$data['name'] = $data['agencyName'];
		$data['email'] = $data['agencyEmail'];
		
		return new $this->_model($data);
	}
	
	public function create($model)
	{	
		$tbl = Admin_Factory::create('DbTable_Agency');
		$props = $model->toArray();
		
		return $tbl->create($this->prepareAgencyKeys($props));
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		
		$tbl = Admin_Factory::create('DbTable_Agency');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($this->prepareAgencyKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		return Admin_Factory::create('DbTable_Agency')->deleteById($id);
	}
	
	public function prepareAgencyKeys($properties)
	{
		if (!isset($properties['agencyName'])) $properties['agencyName'] = $properties['name'];
		if (!isset($properties['agencyEmail'])) $properties['agencyEmail'] = $properties['email'];

		unset($properties['name']);
		unset($properties['email']);

		$keys = ['id', 'agencyName', 'agencyEmail', 'address', 'logo', 'website'];

		return array_intersect_key($properties, array_flip($keys));
	}
}
