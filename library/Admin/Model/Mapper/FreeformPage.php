<?php
class Admin_Model_Mapper_FreeformPage extends Admin_Model_Mapper_ResourceAbstract
{
public function getAll($clauses)
	{
		$tbl = Admin_Factory::create('DbTable_FreeformPage');
		
		$recs = $tbl->fetch($clauses)->toArray();

		if (!count($recs)) return null;
		
		$ids = array_map(function($row) {
			return $row['id'];
		}, $recs);		

		$seoTbl = new Admin_DbTable_Seo();
		$seoData = $seoTbl->fetch(array('in' => $ids))->toArray();
		$seoData = array_column($seoData, null, 'id');
		
		return array_map(function($data) use ($seoData) {
			
			$data['content'] = json_decode($data['content'], true);
			
			$instance = new $this->_model($data);
			if (array_key_exists($instance->getId(), $seoData)) $instance->setSeo($seoData[$instance->getId()]);
			return $instance;
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = Admin_Factory::create('DbTable_FreeformPage');
		
		$record = $tbl->fetchById($id);
		if (!$record) return null;
		
		$record = $record->toArray();
		$record['content'] = json_decode($record['content'], true);

		$instance = new $this->_model($record);

		$seo = Admin_Model_Seo::get($instance->getId());
		if ($seo) {
			$instance->setSeo($seo);
		}
		
		return $instance;
	}
	
	public function create($model)
	{	
		$mTbl = Admin_Factory::create('DbTable_Master');
		$pTbl =  Admin_Factory::create('DbTable', ['page']);
		$fTbl = Admin_Factory::create('DbTable_FreeformPage');

		$props = $model->toArray();
		
		$props['id'] = $mTbl->create($this->filterMasterKeys($props));
		
		if (isset($props['content'])) $props['content'] = json_encode($props['content']);
		
		$pTbl->create($this->filterPageKeys($props));
		$fTbl->create($this->filterFreeformKeys($props));
		
		if (!$model->getSeo()) {
			$seo = new Admin_Model_Seo(array('id' => $props['id']));
			$model->setSeo($seo);	
		} else {
			$seo = $model->getSeo()->setId($props['id']);
		}
		
		$seo->create();
		
		return $props['id'];
	}
	
	public function update($model)
	{
		$model->getSeo()->update();
		
		$props = $model->toArray();
		
		$mTbl = Admin_Factory::create('DbTable_Master');
		$pTbl =  Admin_Factory::create('DbTable', ['page']);
		$fTbl = Admin_Factory::create('DbTable_FreeformPage');
		
		$where = $mTbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$mTbl->update($this->filterMasterKeys($props), $where);
		
		if (isset($props['content'])) $props['content'] = json_encode($props['content']);
		
		$pTbl->update($this->filterPageKeys($props), $where);
		$fTbl->update($this->filterFreeformKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		return Admin_Factory::create('DbTable_Master')->deleteById($id);
	}
	
	public function filterFreeformKeys($properties)
	{
		$keys = array(
				'id',
				'template'
		);
		return array_intersect_key($properties, array_flip($keys));
	}
}
