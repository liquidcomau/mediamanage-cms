<?php
class Admin_Model_Mapper_Category extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses)
	{
		$tbl = new Admin_DbTable_Category();
		
		$recs = $tbl->fetch($clauses)->toArray();
		
		if (!count($recs)) return null;
		
		return array_map(function($data) {
			
			if (isset($data["childCategoryIds"])) $data["childCategoryIds"] = explode(",", $data["childCategoryIds"]);
			
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Category();
		$record = $tbl->fetchById($id);
		
		if (!$record) return null;
		$data = $record->toArray();
		
		if (isset($data["childCategoryIds"])) {
			$data["childCategoryIds"] = explode(",", $data["childCategoryIds"]);
		}
		return new $this->_model($data);
	}
	
	public function create($model)
	{	
		$tbl = new Admin_DbTable_Category();

		$props = $model->toArray();
		return $tbl->insert($this->filterCategoryKeys($props));
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		
		$tbl = new Admin_DbTable_Category();
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($this->filterCategoryKeys($props), $where);
		
		return true;
	}
	
	public function filterCategoryKeys($props)
	{
		$keys = [
			"id",
			"name",
			"label",
			"parentCategoryId"
		];

		return array_intersect_key($props, array_flip($keys));
	}

	public function deleteById($id)
	{
		return (new Admin_DbTable_Category())->deleteById($id);
	}
}
