<?php
class Admin_Model_Mapper_Seo extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($format)
	{
		$tbl = new Admin_DbTable_Post();
		
		return array_map(function($data) use ($format) {
			return new $this->_model($data);
		}, $tbl->fetchAll()->toArray());
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Seo();
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		$rec = $tbl->fetchById($id);
		return count($rec) ? new $this->_model($rec->toArray()) : null;
	}
	
	public function create($model)
	{	
		$tbl = new Zend_Db_Table('seo');
		return $tbl->insert($model->getProperties());
	}
	
	public function update($model)
	{
		$tbl = new Zend_Db_Table('seo');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$tbl->update($model->getProperties(), $where);
		
		return true;
	}
	
	public function delete($model)
	{
		$tbl = new Zend_Db_Table('seo');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		return $tbl->delete($where);
	}
}