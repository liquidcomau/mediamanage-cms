<?php
class Admin_Model_Mapper_News extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses, $format)
	{
		$tbl = new Admin_DbTable_News();
		
		$recs = $tbl->fetch($clauses)->toArray();

		if (!count($recs)) return null;
		
		$ids = array_map(function($row) {
			return $row['id'];
		}, $recs);		

		$seoTbl = new Admin_DbTable_Seo();
		$seoData = $seoTbl->fetch(array('in' => $ids))->toArray();
		$seoData = array_column($seoData, null, 'id');
		
		return array_map(function($data) use ($format, $seoData) {
			if (isset($data["categoryIds"])) {
				$data["categoryIds"] = explode(",", $data["categoryIds"]);
			}
			
			if (isset($data["newsRecommendationIds"])) {
				$data["newsRecommendationIds"] = explode(",", $data["newsRecommendationIds"]);
			}
			
			$instance = new $this->_model($data);
			if (array_key_exists($instance->getId(), $seoData)) $instance->setSeo($seoData[$instance->getId()]);
			return $instance;
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_News();
		$rec = $tbl->fetchById($id);

		if (!$rec) return null;
		$rec = $rec->toArray();
		if (isset($rec["categoryIds"])) {
			$rec["categoryIds"] = explode(",", $rec["categoryIds"]); 
		}
		
		
		if (isset($rec["newsRecommendationIds"])) {
			$rec["newsRecommendationIds"] = explode(",", $rec["newsRecommendationIds"]);
		}

		$instance = new $this->_model($rec);

		$seo = Admin_Model_Seo::get($instance->getId());
		if ($seo) {
			$instance->setSeo($seo);
		} else {
			$instance->setSeo(["id" => $id]);	
		}
		
		return $instance;
	}
	
	public function create($model)
	{	
		$mTbl = new Admin_DbTable_Master();
		
		$props = $model->toArray();

		if ($props["published"] && !$props["publishedDate"]) {
			$props["publishedDate"] = date("Y-m-d H:i:s");
		}
		
		if (!$props["createdDate"]) {
			$props["createdDate"] = date("Y-m-d H:i:s");
		}

		$id = $props['id'] = $mTbl->insert($this->filterMasterKeys($props));
		$model->setId($id);
		
		$tbl = new Admin_DbTable_News();
		$tbl->create($this->filterNewsKeys($props));
		
		if (isset($props["categoryIds"])) {
			$this->updateCategoryReferences($id, $props["categoryIds"]);
		}
		
		if (isset($props["newsRecommendationIds"])) {
			$this->updateNewsRecommendationReferences($id, $props["newsRecommendationIds"]);
		}
		
		if (!$model->getSeo()) {
			$seo = new Admin_Model_Seo(array('id' => $props['id']));
			$model->setSeo($seo);	
		} else {
			$seo = $model->getSeo()->setId($props['id']);
		}
		$seo->create();

		unset($props['seo']);
		
		return $id;
	}
	
	public function update($model)
	{
		$model->getSeo()->update();
		
		$mTbl = new Admin_DBTable_Master;
		$tbl = new Admin_DbTable_News();
		
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$props = $model->toArray();

		if ($props["published"] && !$props["publishedDate"]) {
			$props["publishedDate"] = date("Y-m-d H:i:s");
		}
		
		$mTbl->update($this->filterMasterKeys($props), $where);
		$tbl->update($this->filterNewsKeys($props), $where);
		
		if (isset($props["categoryIds"])) {
			$this->updateCategoryReferences($props["id"], $props["categoryIds"]);
		}
		
		if (isset($props["newsRecommendationIds"])) {
			$this->updateNewsRecommendationReferences($model->getId(), $props["newsRecommendationIds"]);
		}
		
		return true;
	}
	
	public function deleteById($id)
	{
		$tbl = new Zend_Db_Table('master');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function filterNewsKeys($properties)
	{
		$keys = array(
			'id',
			'description',
			'content',
			'featureImg',
			'publishedDate',
			'featured'
		);
	
		$filledProps = array_intersect_key($properties, array_flip($keys));
	
		return $filledProps;
	}
	
	/**
	 * Finds resource by Url part.
	 * Resource must be published.
	 * 
	 * @param string $url
	 */
	public function getByUrl($url)
	{
		$res = $this->getAll(array('url' => $url, 'published' => 1, 'hidden' => "all"));
		return count($res) ? $res[0] : null;
	}
	
	public function getFeaturedNews()
	{
		return $this->getAll(array('featured' => 1, 'published' => 1, 'order' => 'publishedDate DESC'));
	}
	
	public function getTotalPostsByCategory($category = null, $available = true)
	{
		$tbl = Admin_Factory::create("DbTable_News");
		return $tbl->fetchTotalPostsByCategory($category, $available);
	}
	
	protected function updateCategoryReferences($newsId, array $categoryIds)
	{
		$joinTbl = new Zend_Db_Table("news_category");

		$select = $joinTbl->select()->where("newsId = ?", $newsId);

		$existing = $joinTbl->fetchAll($select)->toArray();
		$existing = array_map(function($row) {
			return $row["categoryId"];
		}, $existing);

		if (!$existing) $existing = [];
		$toDelete = array_diff($existing, $categoryIds);

		if (count($toDelete)) {
			$where = [];
			$where[] = $joinTbl->getAdapter()->quoteInto("categoryId IN(?)", $toDelete);
			$where[] = $joinTbl->getAdapter()->quoteInto("newsId = ?", $newsId);
			
			$joinTbl->delete($where);
		}

		$toInsert = array_diff($categoryIds, $existing);
		foreach ($toInsert as $categoryId) {
			$joinTbl->insert(["newsId" => $newsId, "categoryId" => $categoryId]);
		}

		return true;
	}
	
	protected function updateNewsRecommendationReferences($parentId, array $recommendationIds)
	{
		$joinTbl = new Zend_Db_Table("news_recommendations");

		$select = $joinTbl->select()
							->where("parentId = ?", $parentId)
							->order("position ASC");

		if ($parentId) {
			$existing = $joinTbl->fetchAll($select)->toArray();
		}
		
		if (!$existing) {
			$existing = [];
		}

		$existing = array_map(function($row) {
			return $row["childId"];
		}, $existing);
		
		$toDelete = array_diff($existing, $recommendationIds);
		
		if (count($toDelete)) {
			$where = [];
			$where[] = $joinTbl->getAdapter()->quoteInto("childId IN(?)", $toDelete);
			$where[] = $joinTbl->getAdapter()->quoteInto("parentId = ?", $parentId);
			
			$joinTbl->delete($where);
		}
		
		$toUpdate = array_intersect($recommendationIds, $existing);

		for ($i = 0; $i < count($recommendationIds); $i++) {
			
			$recommendationId = $recommendationIds[$i];
			
			if (in_array($recommendationId, $toUpdate)) {
				$where = [];
				$where[] = $joinTbl->getAdapter()->quoteInto("childId = ?", $recommendationId);
				$where[] = $joinTbl->getAdapter()->quoteInto("parentId = ?", $parentId);
				
				$joinTbl->update(["position" => $i], $where);
			} else {
				$joinTbl->insert(["childId" => $recommendationId, "parentId" => $parentId, "position" => $i]);
			}
		}

		return true;
	}
}
