<?php
class Admin_Model_Mapper_Folder extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses)
	{
		$tbl = new Admin_DbTable_Master();
		
		$recs = $tbl->fetchFolders($clauses)->toArray();
		
		if (!count($recs)) return null;
		
		$ids = array_map(function($row) {
			return $row['id'];
		}, $recs);

		return array_map(function($data) {
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Master();
		$record = $tbl->fetchFolderById($id);
		
		if (!$record) return null;
		
		return new $this->_model($record->toArray());
	}
	
	public function create($model)
	{	
		$mTbl = new Admin_DbTable_Master();

		$props = $model->toArray();
		return $mTbl->create($this->filterMasterKeys($props));
	}
	
	public function update($model)
	{
		$mTbl = new Admin_DbTable_Master();
		$props = $model->toArray();

		$where = $mTbl->getAdapter()->quoteInto('id = ?', $model->getId());
		$mTbl->update($this->filterMasterKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		return (new Admin_DbTable_Master())->deleteById($id);
	}
}