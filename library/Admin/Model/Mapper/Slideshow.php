<?php
class Admin_Model_Mapper_Slideshow extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses, $format)
	{
		$tbl = new Admin_DbTable_Slideshow();
		$recs = $tbl->fetchAll($clauses)->toArray();
		
		return array_map(function($data) use ($format, $seoData) {
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Slideshow();
		return new $this->_model($tbl->fetchById($id)->toArray());
	}
	
	public function create($model)
	{	
		$props = $model->toArray();

		$id = $props['id'] = $mTbl->insert($props);
		$model->setId($id);
		
		return (new Admin_DbTable_Slideshow())->create($this->prepareSlideshowProps($props));
	}
	
	public function update($model)
	{
		$tbl = new Admin_DbTable_Slideshow();
		
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		$props = $model->toArray();
		
		$tbl->update($this->prepareSlideshowProps($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		throw new Exception("Cannot delete slideshows");
		
		$tbl = new Zend_Db_Table('slideshow');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function prepareSlideshowProps($props)
	{
		if (isset($props['data'])) {
			$props['data'] = json_encode($props['data']);
		}
		
		if (isset($props['settings'])) {
			$props['settings'] = json_encode($props['settings']);
		}
		
		return $props;
	}
	
	public function getSlideshowByName($name, $format = null)
	{
		$tbl = new Admin_DbTable_Slideshow();
		$where = $tbl->getAdapter()->quoteInto('name = ?', $name);
		
		$slideshow = new $this->_model($tbl->fetchRow($where)->toArray());
		return $format !== Admin_Constants::TYPE_ARRAY 
								? $slideshow
								: $slideshow->toArray();
	}
}