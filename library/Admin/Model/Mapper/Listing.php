<?php
class Admin_Model_Mapper_Listing extends Admin_Model_Mapper_ResourceAbstract
{
	public function __construct()
	{
		parent::__construct();
		$this->_tbl = Admin_Factory::create("DbTable_Listing"); // make this parameter changeable?
	}
	
	public function getAll($clauses, $format)
	{
		$tbl = $this->_tbl;
		$recs = $tbl->fetch($clauses)->toArray();
		if (!count($recs)) {
			return null;
		}

		$ids = array_map(function($row) {
			return $row['id'];
		}, $recs);		

		$seoTbl = new Admin_DbTable_Seo();
		$seoData = $seoTbl->fetch(array('in' => $ids))->toArray();
		$seoData = array_column($seoData, null, 'id');
		
		return array_map(function($data) use ($format, $seoData) {
			if (isset($data['agentIds'])) {
				$data['agentIds'] = explode(',', $data['agentIds']);
			}
			
			$instance = new $this->_model($data);
			
			if (isset($data["relationshipId"])) {
				$mcGalleryClass = Admin_Factory::getClass("Model_Media_Gallery");
				$instance->setGallery($mcGalleryClass::getByRelationshipId($data["relationshipId"]));
			}
			
			if (array_key_exists($instance->getId(), $seoData)) {
				$instance->setSeo($seoData[$instance->getId()]);
			}
			
			return $instance;
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = $this->_tbl;
		$rec = $tbl->fetchById($id);

		if (!$rec) {
			return null;
		}
		
		$rec = $rec->toArray();
		
		if (isset($rec['agentIds'])) {
			$rec['agentIds'] = explode(',', $rec['agentIds']);
		}
		
		$instance = new $this->_model($rec);
		
		if (isset($rec["relationshipId"])) {
			$mcGalleryClass = Admin_Factory::getClass("Model_Media_Gallery");
			$instance->setGallery($mcGalleryClass::getByRelationshipId($rec["relationshipId"]));
		}

		$seo = Admin_Model_Seo::get($instance->getId());
		if ($seo) {
			$instance->setSeo($seo);
		} else {
			$instance->setSeo(["id" => $id]);	
		}
		
		return $instance;
	}
	
	public function create($model)
	{	
		$mTbl = new Admin_DbTable_Master();
		
		$props = $model->toArray();

		if ($props["published"] && !$props["publishedDate"]) {
			$props["publishedDate"] = date("Y-m-d H:i:s");
		}
		
		if (!$props["createdDate"]) {
			$props["createdDate"] = date("Y-m-d H:i:s");
		}

		$id = $props['id'] = $mTbl->insert($this->filterMasterKeys($props));
		$model->setId($id);
		
		$tbl = $this->_tbl;
		$tbl->create($this->filterListingKeys($props));
		
		if (isset($props['agentIds'])) {
			$this->updateAgentReferences($props['id'], $props['agentIds']);
		}
		
		if (isset($props['gallery'])) {
			$relationshipId = $this->createMediaCollectionRelationship($props['id'], $model);
		}
		
		if (!$model->getSeo()) {
			$seo = new Admin_Model_Seo(array('id' => $props['id']));
			$model->setSeo($seo);	
		} else {
			$seo = $model->getSeo()->setId($props['id']);
		}
		$seo->create();

		unset($props['seo']);
		
		return $id;
	}
	
	public function update($model)
	{
		$model->getSeo()->update();
		
		$mTbl = new Admin_DbTable_Master;
		$tbl = $this->_tbl;
		
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$props = $model->toArray();

		if ($props["published"] && !$props["publishedDate"]) {
			$props["publishedDate"] = date("Y-m-d H:i:s");
		}
		
		$mTbl->update($this->filterMasterKeys($props), $where);
		$tbl->update($this->filterListingKeys($props), $where);
		
		if (isset($props['agentIds'])) {
			$this->updateAgentReferences($props['id'], $props['agentIds']);
		}
//		die("hey wassup");
		if (isset($props['gallery'])) {
			if (!$model->getGallery()->getRelationshipId()) {	
				$relationshipId = $this->createMediaCollectionRelationship($props["id"], $model);
			} else {
				$model->getGallery()->update();
			}
		}
		
		return true;
	}
	
	public function deleteById($id)
	{
		$tbl = new Zend_Db_Table('master');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function filterListingKeys($properties)
	{
		$keys = array(
			'id',
			'headline',
			'description',
			'content',
			'featureImg',
			'publishedDate',
			'featured',
			'price',
			'priceView'
		);
	
		$filledProps = array_intersect_key($properties, array_flip($keys));
	
		return $filledProps;
	}
	
	public function getByUrl($url)
	{
		$res = $this->getAll(array('url' => $url, 'published' => 1, 'hidden' => "all"));
		return count($res) ? $res[0] : null;
	}
	
	public function getFeaturedListings()
	{
		return $this->getAll(array('featured' => 1, 'published' => 1, 'order' => 'publishedDate DESC'));
	}
	
	protected function updateCategoryReferences($newsId, array $categoryIds)
	{
		$joinTbl = new Zend_Db_Table("news_category");

		$select = $joinTbl->select()->where("newsId = ?", $newsId);

		$existing = $joinTbl->fetchAll($select)->toArray();
		$existing = array_map(function($row) {
			return $row["categoryId"];
		}, $existing);

		if (!$existing) $existing = [];
		$toDelete = array_diff($existing, $categoryIds);

		if (count($toDelete)) {
			$where = [];
			$where[] = $joinTbl->getAdapter()->quoteInto("categoryId IN(?)", $toDelete);
			$where[] = $joinTbl->getAdapter()->quoteInto("newsId = ?", $newsId);
			
			$joinTbl->delete($where);
		}

		$toInsert = array_diff($categoryIds, $existing);
		foreach ($toInsert as $categoryId) {
			$joinTbl->insert(["newsId" => $newsId, "categoryId" => $categoryId]);
		}

		return true;
	}
	
	public function updateAgentReferences($listingId, array $agentIds)
	{
		$joinTbl = new Zend_Db_Table("listing_agent");

		$select = $joinTbl->select()->where("listingId = ?", $listingId);

		$existing = $joinTbl->fetchAll($select)->toArray();
		$existing = array_map(function($row) {
			return $row["agentId"];
		}, $existing);

		if (!$existing) $existing = [];
		$toDelete = array_diff($existing, $agentIds);

		if (count($toDelete)) {
			// am i missing checking for listingId in where clause here??
			
			$joinTbl->delete($joinTbl->getAdapter()->quoteInto("agentId IN(?)", $toDelete));
		}

		$toInsert = array_diff($agentIds, $existing);
		foreach ($toInsert as $agentId) {
			$joinTbl->insert(["listingId" => $listingId, "agentId" => $agentId]);
		}

		return true;
	}
	
	public function createMediaCollectionRelationship($listingId, $model)
	{
		$relationshipTable =  new Zend_Db_Table("media_collection_relationship");
		$r_id = $relationshipTable->insert(["listingId" => $listingId]);
		
		// and add data to model
		
		$mediaCollection = $model->getGallery();
		$mediaCollection->setRelationshipId($r_id);
		$mediaCollection->create();
		
		return $r_id;
	}
}
