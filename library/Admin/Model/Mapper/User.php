<?php
class Admin_Model_Mapper_User extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($format)
	{
		$tbl = new Zend_Db_Table('user');
		return array_map(function($data) use ($format) {
			return new $this->_model($data);
		}, $tbl->fetchAll()->toArray());
	}
	
	public function get($id)
	{
		$tbl = new Zend_Db_Table('user');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		return new $this->_model($tbl->fetchRow($where)->toArray());
	}
	
	public function create($model)
	{
		$tbl = new Zend_Db_Table('user');
		return $tbl->insert($this->prepareProperties($model->toArray()));
	}
	
	public function update($model)
	{
		$tbl = new Zend_Db_Table('user');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $model->getId());
		return $tbl->update($this->prepareProperties($model->toArray()), $where);
	}
	
	public function deleteById($id)
	{
		$tbl = new Zend_Db_Table('user');
		$where = $tbl->getAdapter()->quoteInto('id = ?', $id);
		
		return $tbl->delete($where);
	}
	
	public function prepareProperties($props)
	{
		if (isset($props['password'])) {
			$props['password'] = md5($props['password']);
		}
		
		return $props;
	}
}