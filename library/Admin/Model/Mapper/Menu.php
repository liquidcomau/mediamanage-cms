<?php
class Admin_Model_Mapper_Menu extends Admin_Model_Mapper_ResourceAbstract
{
	private $_nodeCache = null;
	
	public function getAll($clauses)
	{
		$tbl = new Admin_DbTable_Master();
		
		$recs = $tbl->fetchMenus($clauses)->toArray();
		
		if (!count($recs)) return null;
		
		$ids = array_map(function($row) {
			return $row['id'];
		}, $recs);

		return array_map(function($data) {
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Master();
		$record = $tbl->fetchMenuById($id);
		
		if (!$record) return null;
		
		return new $this->_model($record->toArray());
	}
	
	public function create($model)
	{	
		$mTbl = new Admin_DbTable_Master();

		$props = $model->toArray();
		return $mTbl->create($this->filterMasterKeys($props));
	}
	
	public function update($model)
	{
		$mTbl = new Admin_DbTable_Master();
		$props = $model->toArray();

		$where = $mTbl->getAdapter()->quoteInto('id = ?', $model->getId());
		$mTbl->update($this->filterMasterKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		return (new Admin_DbTable_Master())->deleteById($id);
	}
	
	public function getMainMenu($depth = null)
	{
		$this->_nodeCache = [];
		
		$depth = $depth ? $depth : 3;
		
		$nodesArray = array();
		$mainDb = Admin_Factory::create("DbTable_Master");
		$nodes = $mainDb->fetchPublishedNavByParentID(0);

		foreach($nodes as $node) {
			if ($node->url === '/' && $node->parentId == 0) {
				$node->url = '/';
			} else {
				$node->url = '/' . $node->url;
			}
			$nodesArray[] = $this->getMenuChildren($node, $depth - 1, $mainDb);
		}
		$result = $nodesArray;

		return $result;		
	}
	
	protected function getMenuChildren($node, $depth, $mainDb) {
		$nodeArray = array();
		
		$count = 0;
		
		// nodeArray stores a single node's properties
		
		$nodeArray['id'] = $node->id;
		$nodeArray['heading'] = $node->heading;
		$nodeArray['objectName'] = $node->objectName;

		if ($node->objectName !== 'link') {
			$nodeArray['url'] = $node->url;
		} else {
			if ($node->pageId) {
				if (isset($this->_nodeCache[$node->pageId])) {
					$nodeArray['url'] = $this->_nodeCache[$node->pageId]['url'];
				} else {
					$nodeArray['url'] = Admin_Model_Master::fetchUrl($node->pageId, $mainDb); // can we do this without returning to the model layer?
				}
			} elseif ($node->href) {
				$nodeArray['href'] = 'http://' . $node->href;
			}
			
			$nodeArray['newWindow'] = $node->newWindow;
		}

		$nodes = $mainDb->fetchPublishedNavByParentID($node->id);

		if ($nodes && $depth > 0) {
			foreach ($nodes as $child) {
				if ($child->objectName !== 'link') {
					$child->url = $node->url . '/' . $child->url; 					
					$childArray[] = $this->getMenuChildren($child, $depth - 1, $mainDb);
					$count++;
				}
			}
			if ($count > 0) {
				$nodeArray['children'] = $childArray;
			}
		}

		$this->_nodeCache[$nodeArray['id']] = $nodeArray;
		
		return $nodeArray;
	}
}
