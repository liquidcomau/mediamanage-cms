<?php
class Admin_Model_Mapper_Link extends Admin_Model_Mapper_ResourceAbstract
{
	public function getAll($clauses)
	{
		$tbl = new Admin_DbTable_Link();
		
		$recs = $tbl->fetch($clauses)->toArray();
		
		if (!count($recs)) return null;
		
		$ids = array_map(function($row) {
			return $row['id'];
		}, $recs);

		return array_map(function($data) use ($format, $seoData) {
			return new $this->_model($data);
		}, $recs);
	}
	
	public function get($id)
	{
		$tbl = new Admin_DbTable_Link();
		$record = $tbl->fetchById($id);
		
		if (!$record) return null;
		
		return new $this->_model($record->toArray());
	}
	
	public function create($model)
	{	
		$mTbl = new Admin_DbTable_Master();
		$lTbl = new Admin_DbTable_Link();

		$props = $model->toArray();
		$props['id'] = $mTbl->create($this->filterMasterKeys($props));
		$lTbl->create($this->filterLinkKeys($props));
		
		return $props['id'];
	}
	
	public function update($model)
	{
		$props = $model->toArray();
		
		$mTbl = new Admin_DbTable_Master();
		$lTbl = new Admin_DbTable_Link();
		
		$where = $lTbl->getAdapter()->quoteInto('id = ?', $model->getId());
		
		$mTbl->update($this->filterMasterKeys($props), $where);
		$lTbl->update($this->filterLinkKeys($props), $where);
		
		return true;
	}
	
	public function deleteById($id)
	{
		return (new Admin_DbTable_Master())->deleteById($id);
	}
	
	public function filterLinkKeys($properties)
	{
		if (isset($properties['href']) && isset($properties['pageId'])) {
			throw new Exception("Link must either be external or internal.");
		}
		
		$keys = array(
				'id',
				'href',
				'pageId',
				'newWindow'
		);
		
		$filledProps = array_intersect_key($properties, array_flip($keys));
		
		if (isset($filledProps['pageId'])) {
			$filledProps['href'] = new Zend_Db_Expr('NULL');
		} elseif (isset($filledProps['href'])) {
			$filledProps['pageId'] = new Zend_Db_Expr('NULL');
		}
		
		return $filledProps;
	}
}