<?php
class Admin_Plugin_DynamicRequestHandler extends Zend_Controller_Plugin_Abstract
{
    /**
     * The name of the application module to send dynamic requests to.
     *
     * @var string
     */
    //const CMS_MODULE = 'dynamic';

	/**
    * Routes a response for dynamic content.
    */
    public function routeDynamicResponse(Zend_Controller_Request_Abstract $request, $node)
    {
    	$nodeType = $node->objectName;
    	if ($nodeType == "static_page") {
    		/* redirect to first active (published + navigation) child for static pages not found */ 
			$masterTable = new Admin_DbTable_Master();
    		$firstChild = $masterTable->firstActiveChild($node->id);
    		if ($firstChild) {
    			$redirectUrl = Admin_Model_Master::fetchUrl($firstChild->id);
    			$redirector = new Zend_Controller_Action_Helper_Redirector;
    			$redirector->gotoUrl($redirectUrl)->redirectAndExit();
    		}
    	}
    	$type = Zend_Filter::filterStatic(strtolower($nodeType), 'UnderscoreToDash', array(), 'Zend_Filter_Word');

        $routes = array(
	    	array('controller' => $type, 'action' => 'index'),
	       // array('controller' => 'index', 'action' => 'page'),
	        //array('controller' => 'index', 'action' => 'index') // append default route
	    );

		$isDispatchable = false;
	    foreach ($routes as $route) {
		    $request->setControllerName($route['controller']);
		    $request->setActionName($route['action']);
		    if ($this->isDispatchable($request)) {		  
		    	$isDispatchable = true;
		        break; // stop on first matching route ($routes array order is significant)
		    }		            
		}
    }
    
    /**
    * Called after Zend_Controller_Router exits.
    *
    * Called after Zend_Controller_Front exits from the router.
    *
    * @param  Zend_Controller_Request_Abstract $request
    * @return void
    */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
		if (!$request->isDispatched()) {
			//get request path
			$requestPath = $request->getPathInfo();
			//Zend_Debug::dump($request); die;

			$host = $request->getHttpHost();
			//Get content for given node
			$node = self::getNode($requestPath);
			// Add node to the registry
			Zend_Registry::set($requestPath, $node); // save node to registry

			$cache = Project_Plugin_PageCache::doCache($node);
			Zend_Registry::set('pageCache', $cache);
			
			if ($requestPath !== '/' && substr($requestPath,0,6) != "/admin" && substr($requestPath,0,5) != "/ajax" && substr($requestPath,0,3) != "/m/" && substr($host,0,2) != "m." ) {
				if (!self::isDispatchable($request)) {
					//Ok we have a mediamanage page, let see if there are some controllers/actions that can be used
					if ($node && $node->published) {
						$this->routeDynamicResponse($request, $node);
					} else {
						
						// show preview if logged in and applicable paramater
						if ($request->getParam("cms_preview") === "true" && Zend_Auth::getInstance()->hasIdentity()) {
							return $this->routeDynamicResponse($request, $node);
						}
						
						//if failed to find content (or resource inactive) proceed to regular 404 page
						if ($node) {
							throw new Zend_Controller_Action_Exception('Resource unpublished in CMS', 404);
						} else {
							throw new Zend_Controller_Action_Exception('Resource does not exist in CMS', 404);
						}
					} 
				} else if ($node && !$node->published) {
					throw new Zend_Controller_Action_Exception('Resource Unpublished in CMS', 404);
				}
			}
		}
    }
    
    /**
    * Called after an action is dispatched by Zend_Controller_Dispatcher.
    *
    * This callback allows for proxy or filter behavior. By altering the
    * request and resetting its dispatched flag (via
    * {@link Zend_Controller_Request_Abstract::setDispatched() setDispatched(false)}),
    * a new action may be specified for dispatching.
    *
    * @param  Zend_Controller_Request_Abstract $request
    * @return void
    */
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
    	try {
			if ($request->isDispatched()) {
				$node = Zend_Registry::get($this->_request->getPathInfo()); // fetch node from the registry
				if ($node) {
					$view = Zend_Layout::getMvcInstance()->getView();
					$seoTable = new Admin_DbTable_Seo();
					$seoData = $seoTable->fetchByID($node->id);
					$title = ($seoData['title'] != "") ? $seoData['title'] : $node->heading;
					if ($seoData) {
						$view->headTitle($title, 'PREPEND');
						$view->headMeta()->setName('description', $seoData['metaDescription']);
						$view->headMeta()->setName('robots', $seoData['robots']);
						
						$view->goalTracking = $seoData["goalTracking"];
					}
				}
			}
    	} catch (Zend_Exception $e) {
    		// do nothing
    	}
    }

	/**
     * Returns TRUE if an action can be dispatched, or FALSE otherwise.
     *
     * @param  Zend_Controller_Request_Abstract $request Request to check.
     * @return boolean
     */
    private function isDispatchable(Zend_Controller_Request_Abstract $request)
    {
        $dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
        // Check controller

        if (!$dispatcher->isDispatchable($request)) {
        	return false;
        }
        
        $class = $dispatcher->loadClass($dispatcher->getControllerClass($request));
        $method = $dispatcher->formatActionName($request->getActionName());

        return @is_callable(array($class , $method));
    }
    
    public function getNode($requestPath) {
    	$masterTable = new Admin_DbTable_Master();
    	if ($requestPath == "/") {
    		$node = $masterTable->fetchNode("/", 0);
    		if ($node) {
    			return $node;
    		} else {
    			return false;
    		}
    	} else if (!strstr($requestPath, '/admin')) {
    		$requestArray = explode("/", $requestPath);
    		// filter empty values from array
	    	$requestArray = array_filter($requestArray);
	    	$count = 1;
	    	$parentID = 0;
	    	$node = '';

	    	foreach($requestArray as $crumb) {
	    		if ($crumb != '') {
	    			$node = $masterTable->fetchNode($crumb, $parentID);
	    			if ($node) {
		    			if ($count >= sizeOf($requestArray)) { 
		    				return $node;
		    			}
		    			$parentID = $node->id;
		    		} 
		    	} 
	    		$count++;
	    	}
    	}
    	return false;
    }
}