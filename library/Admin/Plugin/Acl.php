<?php
class Admin_Plugin_Acl extends Zend_Controller_Plugin_Abstract {
	
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
	//public function routeStartup(Zend_Controller_Request_Abstract $request) {
		// set up acl
		$acl = new Zend_Acl ();
		$acl->addRole ( new Zend_Acl_Role ( 'noauth' ) );
		
		// roles that do not have access to admin
		$nonAdminRoles = array('channel', 'internal');
		
		$rolesTable = new Admin_DbTable_Roles();
		$roles = $rolesTable->getRoles();
		
		foreach ($roles as $role) {
			if (!in_array($role->role, $nonAdminRoles)) {
				$acl->addRole ( new Zend_Acl_Role ( $role->role ) );
			}
		}
		
		// add the resources
		$acl->add ( new Zend_Acl_Resource ( 'error' ) );
		$acl->add ( new Zend_Acl_Resource ( 'admin:index' ) );
		$acl->add ( new Zend_Acl_Resource ( 'admin:setup' ) );
		$acl->add ( new Zend_Acl_Resource ( 'admin:login' ) );		
		//$acl->add ( new Zend_Acl_Resource ( 'admin:ajax' ) );

		$acl->allow ( null, array ('error', 'admin:login') );
		
		$modulesTable = new Admin_DbTable_Modules();
		$modules = $modulesTable->getAll();
		
		foreach ($modules as $module) {
			$resource = 'admin:' . $module->moduleName;
			$acl->add ( new Zend_Acl_Resource ( $resource ) );
			foreach ($roles as $role) {
				if (!in_array($role->role, $nonAdminRoles)) {
					$acl->allow ($role->role,  'admin:index');
					//$acl->allow ($role->role,  'admin:ajax');
					if ($module[$role->role]) {
						$acl->allow ($role->role,  $resource);
					}
				}
			}
		}
		
		$acl->allow ('webmaster',  'admin:setup');
		
		// fetch the current user
		$auth = Zend_Auth::getInstance ();
		if ($auth->hasIdentity ()) {
			$identity = $auth->getIdentity ();
			$role = strtolower ( $identity->role );
			if ($role == 'guest') $role = 'noauth';
		} else {
			$role = 'noauth';
		}
		
		$module = $request->module;
		$controller = $request->controller;
		$action = $request->action;
		
		// admin resources must namespaced to avoid duplicate controller names
		$resourceName = ($module == "default") ? $controller : $module . ":" . $controller;
		
		// check if resource exists first to avoid exception error and continue to 404 if page not found
		/* 
		if ($acl->has($resourceName)) {
			if (!$acl->isAllowed ($role, $resourceName, $action)) {
				$request->setModuleName('admin');
				if ($role == 'noauth') {
					$request->setControllerName ('login');
					$request->setActionName ('index');
				} else {
					$request->setControllerName ( 'error' );
					$request->setActionName ( 'noauth' );
				}
			} 
		} 
		*/
	}
}