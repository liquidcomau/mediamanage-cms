<?php
class Admin_Plugin_DynamicContentReplacement extends Zend_Controller_Plugin_Abstract
{
    /**
     * Non-parseable file types.
     *
     * @var array
     */
    protected $_nonParseable = array('png', 'jpg', 'jpeg', 'gif', 'swf', 'fla', 'js', 'css', 'txt', 'pdf', 'doc', 'docx');
    
    /**
     * Called after an action is dispatched by Zend_Controller_Dispatcher.
     *
     * @param Zend_Controller_Request_Abstract $request Request object.
     * @return void
     */
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {  
    	// grab url path without request params to avoid issues with site search (ie. ?search=what%20is%20a%20robots.txt)
    	$urlArray = parse_url($_SERVER['REQUEST_URI']);
    	$urlPath = $urlArray['path'];  
    	$moduleName = $request->getModuleName();
    	
    	// Ignore replace if type exists and is non parseable
    	if (($moduleName == "admin") || $moduleName == "project-admin" || strstr ( $urlPath, "/ajax/api/" ) == true || ($this->getResponse()->getHttpResponseCode() == "200") && preg_match("/\.(".implode('|', $this->_nonParseable).")/", $urlPath)) { // skip certain file types
    		return;
        }
                    	
        $this->replacePlaceholders();
    }
    
    /**
     * Replace placeholders.
     *
     * @return void
     */
    public function replacePlaceholders()
    {    	
    	$body = $this->getResponse()->getBody();

		if (preg_match_all('/\{{[^\}]*}}/', $body, $matches)) { // placeholders exist e.g. {{PLACEHOLDER_NAME}} which need to be replaced        	
			$placeholders = array_unique($matches[0]);

            foreach ($placeholders as $placeholder) { // loop each placeholder
            	
            	$view = Zend_Layout::getMvcInstance()->getView();
            	
				$placeholderName = substr($placeholder, 2, -2);// strip {{}} markers

                if (strpos($placeholderName, ':')) {
                	$parts = explode(':', $placeholderName);
                	$placeholderName = trim($parts[0]);
                	$param = $parts[1] ? trim($parts[1]) : null;
                }

                $placeholderName = str_replace(' ','',$placeholderName); // remove any spaces
                $config = Zend_Registry::get("config");

		    	$modules = $config->admin->content->modules;
		    	if ($modules) $modules = $modules->toArray();
		    	
		    	$fragment = $ctaPanel = false;

		    	// look for match in fragments/block table		    		
		    	if (in_array("fragments", $modules)) {
	                $fragmentTable = new Admin_DbTable_Fragment();
	                $fragment = $fragmentTable->fetchByTag($placeholderName);		    		
		    	} 
		    	
		    	// look for match in ctapanels table
		    	if (in_array("ctapanels", $modules)) {
	                $ctapanelTable = new Admin_DbTable_CtaPanel();
	                $ctaPanel = $ctapanelTable->fetchByTag($placeholderName);
		    	}
                
                if ($fragment) {
                	$body = str_replace($placeholder, $fragment->content, $body); // replace placeholder
                } else if ($ctaPanel) {
                	$body = str_replace($placeholder, $view->ctaPanel()->buildPanel($ctaPanel), $body); // replace placeholder 
                } else if (substr($placeholderName,0,3) == "DB_") {
                	$requestPath = $_SERVER['REQUEST_URI'];
                	$node = Zend_Registry::get($requestPath);
                	if ($node) {
                		try {
	                		$objectClass = '_DbTable_'.Zend_Filter::filterStatic(strtolower($node->type), 'UnderscoreToCamelCase', array(), 'Zend_Filter_Word');
	                		$objectClass = (class_exists("Project" . $objectClass)) ? "Project" . $objectClass : "Admin" . $objectClass;
	                		$objectTable = new $objectClass();
	                		$object = $objectTable->fetchById($node->nodeID);
	                		
	                		$field = lcfirst(Zend_Filter::filterStatic(strtolower(str_replace("DB_", "", $placeholderName)), 'UnderscoreToCamelCase', array(), 'Zend_Filter_Word'));
	                		$value = ($object->$field != "") ? $object->$field : "NaN";
	                		$body = str_replace($placeholder, $value, $body);
                		} catch (Zend_Exception $e) {
                			// likely cause doe to field not found in db
                			// replace curly braces to avoid infinite loop
                			$body = str_replace($placeholder, "[" . str_replace("DB_", "", $placeholderName) . "]", $body); // replace placeholder
                		}
                	}
                } else {
	                if (substr($placeholderName,0,5) == "video") {
	                	// this prevents my "videostrip" placeholder, commenting out lololol 
// 	            		$param = substr($placeholderName,6); // get youtube id
// 	            		$placeholderName = "video";
	            	} else if (substr($placeholderName,0,7) == "maplink") { 
	            		$placeholderName = "maplink";	
	            	} else if (substr($placeholderName,0,3) == "map") { 
	            		$param = substr($placeholderName,4); // get size
	            		$placeholderName = "map";	
	            	} else if (substr($placeholderName,0,14) == "gmapPeppercorn") { 
	            		$param = substr($placeholderName,15); // get size
	            		$placeholderName = "gmapPeppercorn";	
	            	} else if (substr($placeholderName,0,4) == "gmap") { 
	            		$param = substr($placeholderName,5); // get size
	            		$placeholderName = "gmap";	
                	} else if (substr($placeholderName,0,20) == "callToActionsSidebar") { 
	            		$param = substr($placeholderName,21); // get size
	            		$placeholderName = "callToActionsSidebar";	
	            	} else if (strstr($placeholderName, "gallery_")) { 
	            		$param = substr($placeholderName,8); // get size
	            		$placeholderName = "galleryWidget";
	            	} 
	            	
	            	$viewHelperClass = 'Project_View_Helper_Placeholders';
	            	$viewHelperMethodName = Zend_Filter::filterStatic(strtolower($placeholderName), 'UnderscoreToCamelCase', array(), 'Zend_Filter_Word');
	                $viewHelperMethod = strtolower(substr($viewHelperMethodName, 0, 1)).substr($viewHelperMethodName, 1);

	                if (method_exists($viewHelperClass, $viewHelperMethod)) {
						$view = Zend_Layout::getMvcInstance()->getView();
	                	$helperClass = new $viewHelperClass($view);
	                	$body = str_replace($placeholder, $helperClass->{$viewHelperMethod}($param), $body); // replace placeholder
	                } else {
	                	// replace curly braces to avoid infinite loop
	                	$body = str_replace($placeholder, "[" . $placeholderName . "]", $body); // replace placeholder
	                	//throw new Zend_Exception("Method $viewHelperMethod() does not exist for dynamic content placeholder: $placeholder.");
	                }
                }
            }
            $this->getResponse()->setBody($body);
        }
        if (preg_match_all('/\{{[^\}}]*}}/', $body, $matches)) { // catch for placeholders within placeholders
            $this->replacePlaceholders();
        }        
    }        
}
