<?php
/**
 * Check if there is a legacy/alternative URI if the user hits a 404. 
 * 
 * PHP Version 5 
 * 
 * @package    Project
 * @subpackage Admin_Plugin
 * @author     Jon Trumbull <jon@jtwebspace.com>
 * @license    Proprietary
 * @category   PHP 
 * @link       http://www.jtwebspace.com
 */
class Admin_Plugin_Redirect extends Zend_Controller_Plugin_Abstract
{
    /**
     * Check for a mapping during a 404.
     * 
     * During shutdown, detect a 404 and redirect the user if
     * an alternative URI is found. 
     * 
     * @see    Zend_Controller_Plugin_Abstract::dispatchLoopShutdown()
     * @return void
     */
    public function dispatchLoopShutdown()
    {
    	if ($this->getResponse()->getHttpResponseCode() === 404) {            
    		
    		$url = parse_url($this->_request->getRequestUri());
            $url = $url['path'];

			$redirectTable = new Zend_Db_Table("redirect");
            $redirects = $redirectTable->fetchAll();
            foreach ($redirects as $redirect) {
	            // Mapping hit?
	            if (trim($url, "/") === trim($redirect->url, "/")) {
	            	// cancel page caching for redirects or it will cache a 404 page
	
					$pageCache = Zend_Registry::get('pageCache');
					if ($pageCache) {
		            	$pageCache->cancel(); // this line causes an error
					}
	            	// for some reason $redirect->type comes out as type varchar. Needs to be INT to setHttpResponseCode
	            	$redirectType = $redirect->type;
	            	settype($redirectType, "integer");
	            	$this->getResponse()->setHeader("Location", $redirect->destination, true);
	                $this->getResponse()->setHttpResponseCode($redirectType);
	            }            		
            }           
            // No hit, continue on to the 404...
        }
    }
}