<?php
class Admin_DbTable extends Zend_Db_Table {

	private static $_objectTypeIndex = null;
	
	/**
	 * @param array $fields
	 */	
	public function create($fields) {
		return $this->insert($fields);
	}
	
	/**
	 * Appends common database clauses to a select object.
	 * 
	 * Returns the select object.
	 * 
	 * @param Zend_Db_Table_Select $selectObject
	 * @param array $clauses
	 * @param string $parentId. Optional. Can specify column name to search for e.g. 'p.pollId'
	 */
	public function appendDbClauses($select, $clauses) {
		foreach ($clauses as $cKey => $cVal) {
			switch ($cKey) {
				case 'limit':
					if (strpos($cVal, ',')) {
						$val = explode(',', $cVal);
					} else {
						$val  = array($cVal);
					}
					$select->limit($val[0], $val[1]);
					break;
				case 'order':
					$select->order($cVal);
					break;
				case 'in':
					if (array_key_exists('column', $cVal) && array_key_exists('values', $cVal)) {
						$col = $cVal['column'];
						$vals = $cVal['values'];
					} else {
						$col = 'id';
						$vals = $cVal;
					}
					
					$vals = is_array($vals) ? $vals : explode(',', $vals);
					
					$select->where($col . ' IN(?)', $vals);
			}
		}

		return $select;
	}
	
	public function appendMasterClauses($select, $clauses)
	{
		foreach ($clauses as $key => $val) {
			switch ($key) {
				case 'url':
					$select->where('m.url = ?', $val);
					break;
				case 'published':
				case 'published_status':
					switch ($val) {
						case 'all':
//							$select->where('published = ?', true);
//							$select->orWhere('published = ?', false);
							$select->where("published = '1' OR published = '0'");
							break;
						case false:
						case 'unpublished':
							$select->where('published = ?', false);
							break;
						default:
							$select->where('published = ?', true);
					}
					break;
				case 'hidden':
					switch ($val) {
						case 'all':
							$select->where("hidden = '1' OR hidden = '0'");
							break;
						case true:
						case 'hidden':
							$select->where('hidden = ?', true);
							break;
						default:
							$select->where('hidden = ?', false);
					}
					break;
			}
		}

		return $select;
	}

	/**
	 * Makes sure resource has to have a published field equating to true
	 * if not otherwise specified
	 * 
	 * @param unknown $select
	 * @return unknown
	 */	
	public function checkPublished($select)
	{
		$publishedClause = false;

		foreach ($select->getPart('where') as $clause) {
			if (strpos($clause, '(published = \'') !== false) {
				$publishedClause = true;
				break;
			}
		}

		if (!$publishedClause) {
			$select->where('published = ?', true);
		}
		
		return $select;
	}
	
	/**
	 * Makes sure resource has to have a published field equating to true
	 * if not otherwise specified
	 * 
	 * @param unknown $select
	 * @return unknown
	 */	
	public function checkNotHidden($select)
	{
		$hiddenClause = false;

		foreach ($select->getPart('where') as $clause) {
			if (strpos($clause, '(hidden = \'') !== false) {
				$hiddenClause = true;
				break;
			}
		}

		if (!$hiddenClause) {
			$select->where('hidden = ?', false);
		}
		
		return $select;
	}

	public static function getObjectTypeIndex()
	{
		if (!self::$_objectTypeIndex) {
			$recs = (new Zend_Db_Table('object_type'))->fetchAll()->toArray();
			self::$_objectTypeIndex = array_column($recs, 'id', 'objectName');
		}
		
		return self::$_objectTypeIndex;
	}
	
}
