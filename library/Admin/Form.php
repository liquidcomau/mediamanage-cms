<?php
class Admin_Form extends Zend_Form
{
	protected $_headScripts = array();
	
	protected $_objectTypeId = null;
	protected $_defaults;
	
	protected $_meta = null;
	
	public function __construct($options = null)
	{
		$this->preInit($options);
		parent::__construct($options);
		$this->postInit($options);
	}
	
	public function preInit($options)
	{
	}
	
	public function postInit($options)
	{
	}
	
	public function addSubForm(Zend_Form $form, $name, $order = null, $isArray = true)
	{
		$form->setIsArray(false);
		return parent::addSubForm($form, $name, $order);
	}
	
	/**
	 * Returns the first instance of a subForm with name $name
	 * in a nested group of subForms using a depth-first search.
	 *
	 * @param string $name. The given name of the subForm.
	 * @param $recurse. Whether or not to do a nested saerch.
	 * @return (boolean|Zend_Form_SubForm)
	 */
	public function getSubForm($name, $recurse = false) {

		$name = (string) $name;
	
		$subForm = null;
		foreach ($this->getSubForms() as $key => $val) {
			if ($name === $key) {
				$subForm = $val;
				break;
			} else {
				if ($recurse) {
					$subForm = $val->getSubForm($name, true);
					if ($subForm) break;
				}
			}
		}
	
		return $subForm;
	}
	
	public function applyDecorators()
	{
		$this->applyFormDecorators();
			
		$this->applyElementDecorators();
	
		$this->setDisplayGroupDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel-body'))
		));
	
		$this->header->setDecorators(
				array('ViewHelper', array('HtmlTag', array('tag' => 'header', 'class' => 'panel-heading font-bold'))
		));
	}
	
	public function applyFormDecorators()
	{
		$this->setDecorators(array(
				'Form',
				'FormElements',
				array('HtmlTag', array('tag' => 'section', 'class' => 'panel panel-default'))
		));
	}
	
	public function applyElementDecorators()
	{
		$this->setElementDecorators(array(
				'ViewHelper',
				'Errors',
				'Label', array('Description', array('tag' => 'span', 'class' => 'help-block m-b-none')),
				array('HtmlTag', array('tag' => 'div', 'class' => 'form-group'))
		));
	}
	
	public function getDefaults()
	{
		return $this->_defaults;
	}
	
	public function setDefaults($defaults) 
	{
		if (!$this->_defaults) $this->_defaults = $defaults;
		parent::setDefaults($defaults);
	}
	
	protected function browseElement($to, $name, $label)
	{
		if (!$to) {
			$to = $this;
		}
		
		$inputIdentifier = $name . "Input";
		
		$browseBtn = '<button class="btn btn-default thumbnailBrowse moxmanBrowse" type="button" data-for=".' . $inputIdentifier . '">Browse</button>';
		
		$to->$name = new Admin_Form_Element_InputGroup($name);
		$to->$name->setLabel($label)
					->setAttribs(array(
							'class' => 'form-control ' . $inputIdentifier,
							'addon' => 	array(
									'type' => 'button',
									'markup' => $browseBtn,
									'position' => 'append'
							)
					));
	}
	
	protected function addRelatedEntitySubForm($to, $getAllMethod, $getEntitiesMap, $el_name = "relatedEntityIds")
	{
		if (!$getAllMethod) {
			throw new Exception("Must provide getAllMethod to related items form");
		}
		
		if (!$getEntitiesMap) {
			throw new Exception("Must provide getEntityMap to related items form");
		}
		
		$relatedEntityF = new Admin_Form_SubForm();
		$relatedEntityF->setAttribs(array(
			'icon' => 'fa-thumbs-up',
			'alias' => 'Recommendations'
		));

		$relatedEntitySF = new Admin_Form_SubForm();
		
		$relatedEntitySF->header = new Zend_Form_Element_Note('header');
		$relatedEntitySF->header->setValue('Recommendations');
		
		$items = $getAllMethod();
		$options = $getEntitiesMap($items);
		
		$relatedEntitySF->{$el_name} = new Zend_Form_Element_Multiselect($el_name);
		$relatedEntitySF->{$el_name}->setLabel("Project Recommendations")
													->setAttribs([
														"class" => "multiselect-sortable",
														"multiple" => "multiple"
													])
													->setMultiOptions($options);
		
		$relatedEntitySF->addDisplayGroup(array($el_name), 'recommendationsGroup');
		$relatedEntitySF->applyDecorators();

		$relatedEntityF->addSubForm($relatedEntitySF, 'relatedEntitySF', null, false);
		$to->addSubForm($relatedEntityF, 'relatedEntityF', null, false);
	}
}
