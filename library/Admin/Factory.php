<?php
/**
 * Class creates objects with Project_ namespace if exists
 * or Admin_ otherwise.
 */
class Admin_Factory
{
	public static function getClass($class)
	{
		$projClass = 'Project_' . $class;
		$mmClass = 'Admin_' . $class;
		
		if (class_exists($projClass)) {
			$class = $projClass;
		} elseif (class_exists($mmClass)) {
			$class = $mmClass;
		} else {
			throw new Exception("Could not find class: ' . $class . '");
		}
		
		return $class;
	}
	
	public static function create($class, array $args = [])
	{
		$reflection = new ReflectionClass(self::getClass($class));
		return $reflection->newInstanceArgs($args);
	}
}