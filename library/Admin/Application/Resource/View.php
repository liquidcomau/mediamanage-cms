<?php
/**
 * Resource for setting view options.
 * 
 * PHP Version 5 
 */
class Admin_Application_Resource_View extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * Zend_View object.
     * 
     * @var Zend_View
     */
    protected $_view;

    /**
     * Defined by Zend_Application_Resource_Resource.
     *
     * @return Zend_View
     */
    public function init()
    {
        $view = $this->getView();
        
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
        
        return $view;
    }

    /**
     * Retrieve view object.
     *
     * @return Zend_View
     */
    public function getView()
    {
        if (null === $this->_view) {
            $options = $this->getOptions();
            $this->_view = new Zend_View($options);

            if (isset($options['doctype'])) {
                $this->_view->doctype()->setDoctype(strtoupper($options['doctype']));
            }

            if (isset($options['encoding'])) {
                $this->_view->setEncoding($options['encoding']);
            }
            
            if (isset($options['headTitle']['separator'])) {
                $this->_view->headTitle()->setSeparator($options['headTitle']['separator']);
            }
            
            if (isset($options['headTitle']['suffix'])) {
                $this->_view->headTitle($options['headTitle']['suffix']);
            }
            
            if (isset($options['headMeta']['appendHttpEquiv']) && is_array($options['headMeta']['appendHttpEquiv'])) {
                foreach ($options['headMeta']['appendHttpEquiv'] as $httpEquiv => $content) {
                    $this->_view->headMeta()->appendHttpEquiv($httpEquiv, $content);
                }
            }
            
            if (isset($options['headMeta']['appendName']) && is_array($options['headMeta']['appendName'])) {
                foreach ($options['headMeta']['appendName'] as $name => $content) {
                    $this->_view->headMeta()->appendName($name, $content);
                }
            }
        	if (isset($options['headMeta']['viewport'])) {
        		$this->_view->headMeta()->appendName('viewport',$options['headMeata']['viewport']);
            }
            
            if (isset($options['headLink']['favicon'])) {
                $this->_view->headLink()->headLink(array('rel' => 'icon', 'href' => $options['headLink']['favicon'], 'type' => 'image/x-icon'), 'PREPEND');
            }
            
            if (isset($options['helperPaths']) && is_array($options['helperPaths'])) {
                foreach ($options['helperPaths'] as $helperPrefix => $helperPath) {
                    $this->_view->addHelperPath($helperPath, $helperPrefix);
                }
            }
            
            if (isset($options['scriptPaths']) && is_array($options['scriptPaths'])) {
                foreach ($options['scriptPaths'] as $scriptPath) {
                    $this->_view->addScriptPath($scriptPath);
                }
            }
        }
        
        return $this->_view;
    }
}
