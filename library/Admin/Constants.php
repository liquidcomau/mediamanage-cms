<?php
class Admin_Constants
{        
    // Environments
    const ENV_PRODUCTION = 'production';
    const ENV_EXTERNAL = 'external';
    const ENV_STAGE = 'stage';
    const ENV_DEV = 'dev';
    
    // Registry options
    const REGISTRY_LOGGER = 'logger'; 
    
    // Config Options
    const CONFIG = 'config';
    const CONFIG_PATH = 'path';    
    const CONFIG_DATABASE = 'database';
    const CONFIG_DATABASE_PARAMS = 'params';   
    const CONFIG_ENV = 'environment';
    const CONFIG_EMAILTO = 'adminemail';
    const CONFIG_EMAILBCC = 'supportemail';
    
    // Types
    const TYPE_ARRAY = 'array';
    const TYPE_OBJECT = 'object';
}