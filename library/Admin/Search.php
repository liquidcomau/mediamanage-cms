<?php
class Admin_Search
{
    const DEFAULT_TAG = 'mediamanage';
    const DEFAULT_ENCODING = 'UTF-8';
    const HTTP_REQUEST_TIMEOUT = 10;

    protected $_index = null;
    protected $_indexPath;
    protected $_client = null;
    protected $_nonParseable = array('png', 'jpg', 'jpeg', 'gif', 'swf', 'fla', 'js', 'css', 'txt', 'pdf', 'doc', 'docx');
    protected $_logger = false;
    protected $_config;

    /**
     * Class constructor.
     *
     * @param string $indexPath Search index path.
     */
    function __construct($indexPath)
    {
        $this->_indexPath = $indexPath;
        $this->_logger = Zend_Registry::get("logger");
        $this->_config = Zend_Registry::get("config");
        Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding(self::DEFAULT_ENCODING);
        Zend_Search_Lucene_Search_Query_Wildcard::setMinPrefixLength(0); // allow zero character wildcard matches
        Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Zend_Search_Lucene_Analysis_Analyzer_Common_TextNum_CaseInsensitive());
    }

    /**
     * Update a specific URL in the index.
     *
     * @param string $url Absolute URL to update.
     * @return string $status
     */
    public function update($url)
    {
        $status = 'Failed';

        try {
            $this->getClient()->setUri($url);
            if ($response = $this->getClient()->request(Zend_Http_Client::GET)) { // fetch the URL via HTTP
                if ($response->getStatus() == 200) {
                    $status = $this->updateHTMLDocument($url, $response->getBody());
                }
            }
        } catch (Zend_Exception $e) {
            $status = $e;
        }

        return $status;
    }

    /**
     * Update all pages within a domain, by crawling each page for links
     * starting from a given base URL.
     *
     * @param string $url Absolute URL.
     * @param array $ignore DOM elements to avoid indexing.
     * @return array $status
     */
    public function updateAll($url, $ignore = array())
    {
        $config = $this->_config->search;

        $status = $parsed = array(); // initialise variables
        $client = $this->getClient();
        $urls = (array) $url; // an array of absolute URLs to index

        $host = parse_url($url, PHP_URL_SCHEME).'://'.parse_url($url, PHP_URL_HOST);

        $ignoreList = array();
        /*
        $seoTable = new Admin_DbTable_Seo();
        $masterHelper = new Admin_View_Helper_Master();
        $noIndex = $seoTable->fetchNoIndex();
        foreach ($noIndex as $item) {
                $ignoreList[] = "http://" . $_SERVER['HTTP_HOST'] . $masterHelper->fetchUrl($item->nodeID);
        }
        */

        $this->_logger->info("indexing url's...");

        while(!empty($urls)) {
                $url = array_shift($urls); // shift from start of array
                $loginUrl = (isset($config->login->url)) ? "http://" . $_SERVER['HTTP_HOST'] . $config->login->url : false;
                if (!strstr($url, "/admin/") && ($url != $loginUrl) && (!preg_match("/\.(".implode('|', $this->_nonParseable).")/", $url) && !in_array($url, $ignoreList))) {
                    $this->_logger->info($url);
	                $relativeURL = $this->getRelativeURL($url);
                    try {
                        $client->setUri($url);
						$response = $this->getClient()->request(Zend_Http_Client::GET);
                        
                        if ($response) { // fetch the URL via HTTP
                           if ($response->getStatus() == 200) {

                                $html = $response->getBody();
                                // strip header and footer
                                $dom = new DOMDocument;
                                $dom->loadHTML($html);
                                $xPath = new DOMXPath($dom);

                                foreach ($ignore as $domElement) {
                                	$domType = ($domElement[0] == "#") ? "id" : "class";
                                    $domName = substr($domElement, 1);
                                    $nodes = $xPath->query('//*[@' . $domType . '="' . $domName . '"]');
                                    if($nodes->item(0)) {
                                    	$nodes->item(0)->parentNode->removeChild($nodes->item(0));
                                    }
                                }

                                $strippedHtml = $dom->saveHTML();

                                $updateStatus = $this->updateHTMLDocument($url, $strippedHtml);
                                if (in_array($updateStatus, array('Updated', 'Added', 'Removed', 'Invalid'))) {
                                        $doc = Zend_Search_Lucene_Document_Html::loadHTML($html, false, self::DEFAULT_ENCODING);
                                    foreach ($doc->getLinks() as $href) { // crawl links from current html document
                                        if (preg_match('/^\//', $href)) { // only consider relative URLs (starting with '/')
                                            $href = parse_url($href, PHP_URL_PATH).((parse_url($href, PHP_URL_FRAGMENT)) ? '#'.parse_url($href, PHP_URL_FRAGMENT) : ''); // strip any GET parameters
                                            if ($href != "/") $href = rtrim($href, '/'); // remove trailing slashes
                                            $newURL = $host . $href;
											if (!in_array($newURL, $urls) && !in_array($href, $parsed) && ($href != $relativeURL) && !strstr($newURL, "logout")) {
                                            	$urls[] = $newURL;
                                            }
                                        }
                                    }
                                }
                                $status[$updateStatus][] = $relativeURL;
                            } else {
                                $status['HTTP_'.$response->getStatus()][] = $relativeURL;
                            }
                        } else {
                            $status['No_Response'][] = $relativeURL;
                        }
                    } catch (Zend_Exception $e) {
                    	$this->_logger->err(print_r($e, true));
                        $status['Exception'][] = $relativeURL;
                    }
                    $parsed[] = $relativeURL;
                    //if(count($parsed) >= 15) break; // useful for debugging as it will not run through the entore index
                } else {
                        $this->_logger->info("skipping: " . $url);
                }
        }

        // remove any documents from the index that are no longer being linked to
        $hits = $this->findAll();
        if($hits) { // get all documents
            foreach ($hits as $hit) {
                $document = $hit->getDocument();
                if(!in_array($document->url, $parsed)) { // remove document
                        $this->_logger->info("removing: " . $document->url);
                    if ($this->removeDocumentsByKey(array($document->key))) {
                        $status['Removed'][] = $document->url;
                    }
                }
            }
        }

        $this->_logger->info("optimising index...");
        $this->getIndex()->optimize();

        $this->_logger->info("sorting results...");
        ksort($status['Added']);
        ksort($status['Updated']);
        ksort($status['Removed']);
        ksort($status['Failed']);
        ksort($status['Invalid']);
        ksort($status['No_Response']);
        ksort($status['Exception']);
        ksort($status);
        $this->_logger->info("sorting completed...");

        return $status;
    }

    /**
     * Adds/Updates/Removes a document.
     *
     * Lucene does not support updating documents directly, so
     * to update, existing items must first be removed, then added.
     *
     * @param string $absoluteURL URL of the HTML document.
     * @param string $html HTML document.
     * @return string $status
                        * @return string $status
     */
    public function updateHTMLDocument($absoluteURL, $html)
    {
        $status = 'Failed';

        try {
            // initialise variables to keep track of status
            $flgRemoved = false;
            $flgAdded = false;

            // parse URL & HTML
            $relativeURL = $this->getRelativeURL($absoluteURL);
            $key = md5($relativeURL);
            $title = (preg_match('/<title>(.*)<\/title>/', $html, $matches)) ? trim($matches[1]) : null;
            $description = (preg_match("/<meta name=\"description\" content=\"(.*?)\"/", $html, $matches)) ? trim($matches[1]) : null;
            $keywords = (preg_match("/<meta name=\"keywords\" content=\"(.*?)\"/", $html, $matches)) ? trim($matches[1]) : null;

            $title = ($title != "") ? $title : "No page title found";
            $description = ($description != "") ? $description : "No page description found";

            // remove document if exists in index
            if ($this->removeDocumentsByKey(array($key))) {
                $flgRemoved = true;
            }

            // add document to the index
            if (!empty($title) && !empty($description)) {
                $doc = Zend_Search_Lucene_Document_Html::loadHTML($html, false, self::DEFAULT_ENCODING);
                $doc->addField(Zend_Search_Lucene_Field::Text('title', $title, self::DEFAULT_ENCODING));
                $doc->addField(Zend_Search_Lucene_Field::Text('description', $description, self::DEFAULT_ENCODING));
                $doc->addField(Zend_Search_Lucene_Field::Text('keywords', $keywords, self::DEFAULT_ENCODING));
                $doc->addField(Zend_Search_Lucene_Field::Text('url', $relativeURL, self::DEFAULT_ENCODING));
                $doc->addField(Zend_Search_Lucene_Field::Keyword('key', $key, self::DEFAULT_ENCODING));
                $doc->addField(Zend_Search_Lucene_Field::Keyword('tag', self::DEFAULT_TAG, self::DEFAULT_ENCODING));
                $this->getIndex()->addDocument($doc);
                $flgAdded = true;
            }

            // determine status based on flag values
            if ($flgRemoved && $flgAdded) {
                $status = 'Updated';
            } else if (!$flgRemoved && $flgAdded) {
                $status = 'Added';
            } else if ($flgRemoved && !$flgAdded) {
                $status = 'Removed';
            } else if (!$flgRemoved && !$flgAdded) {
                                $status = 'Invalid';
            }
        } catch (Zend_Exception $e) {
            $status = 'Exception';
        }

        return $status;
    }

    /**
     * Remove documents by key field. Key is a md5 hash of the relativeURL.
     *
     * @param array $keys An array of keys to remove.
     * @return int $removed No of documents removed.
     */
    public function removeDocumentsByKey($keys)
    {
        $index = $this->getIndex();
        $removed = 0;
        foreach ($keys as $key) {
            if ($hits = $index->find(new Zend_Search_Lucene_Search_Query_Term(new Zend_Search_Lucene_Index_Term($key, 'key')))) {
                foreach ($hits as $hit) {
                    $index->delete($hit->id);
                    $index->commit();
                    $removed++;
                }
            }
        }
        return $removed;
    }

    /**
     * Remove documents by field value.
     *
     * @param string $field Field to search by.
     * @param string $value Value to find.
     * @return array $removed
     */
    public function removeDocumentsByFieldValue($field, $value)
    {
        $index = $this->getIndex();
        $removed = array();

        if ($hits = $index->find(new Zend_Search_Lucene_Search_Query_Term(new Zend_Search_Lucene_Index_Term($value, $field)))) { // one or more documents match criteria
            foreach ($hits as $hit) {
                $index->delete($hit->id);
                $index->commit();
                $removed[] = $field.':"'.$value.'"';
            }
		}
        return $removed;
    }

    /**
     * Returns a relative URL.
     *
     * @param string $absoluteURL Absolute URL.
     * @return string $relativeURL
     */
    public function getRelativeURL($absoluteURL)
    {
        $host = parse_url($absoluteURL, PHP_URL_HOST);
        return substr($absoluteURL, strpos($absoluteURL, $host) + strlen($host));
    }

    /**
     * Returns all items.
     *
     * @return array Zend_Search_Lucene_Search_QueryHit
     */
    public function findAll()
    {
        $hits = $this->getIndex()->find(new Zend_Search_Lucene_Search_Query_Term(new Zend_Search_Lucene_Index_Term(self::DEFAULT_TAG, 'tag')));
        return $hits;
    }

    /**
     * Get HTTP Client.
     *
     * @return Zend_Http_Client
     */
    public function getClient()
    {
        if ($this->_client == null) { // client has not been instantiated
            $this->_client = new Zend_Http_Client(null, array(
                'adapter' => 'Zend_Http_Client_Adapter_Curl',
                'keepalive' => true,
                'persistent' => false,
                'storeresponse' => false,
                'maxredirects' => 5,
                'curloptions' => array(CURLOPT_SSL_VERIFYPEER => false),
                'timeout' => self::HTTP_REQUEST_TIMEOUT
            ));
            $this->_client->setCookieJar();
        }
        return $this->_client;
    }

    /**
     * Get search index.
     *
     * @return Zend_Search_Lucene_Interface
     */
    public function getIndex()
    {
        if ($this->_index == null) { // index has not been instantiated
            try {
                $this->_index = Zend_Search_Lucene::open($this->_indexPath); // first, try to open existing index
            } catch (Zend_Search_Lucene_Exception $e) {
                try {
                    $this->_index = Zend_Search_Lucene::create($this->_indexPath); // index does not exist, create a new one
                } catch (Zend_Search_Lucene_Exception $e) {
                    $this->_index = null;
                }
            }
        }
        return $this->_index;
    }

    /**
     * Get search index path.
     *
     * @return string
     */
    public function getIndexPath()
    {
        return $this->_indexPath;
    }
}
            