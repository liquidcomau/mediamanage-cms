<?php
class Admin_Cache
{
	public function __construct()
    {    
    }
	
    public function cache()
    {
        return $this;
    }

    public function __toString()
    {        
        return $this;
    }
    
    public function clear($nodeID = "all")
    {
    	$logger = Zend_Registry::get('logger');
    	$config = Zend_Registry::get("config");
    	$staticCache = (isset($config->caching->static->disable_caching) && !$config->caching->static->disable_caching) ? true : false;
    	$pageCache = ($config->caching->enabled) ? true : false;

    	// if static caching enabled then assume page caching is disabled
    	if ($staticCache) {
    		$result = self::clearStatic();
    	} else if ($pageCache) {
    		$cache = Zend_Registry::get('pageCache');
	    	if ($nodeID == "all") {
		    	// clear all pageCache records
	    		$logger->info("Deleting all page cache records");
	    		$result = $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('pageCache'));
	    	} else {
	    		// clear cache records for given id
	    		$logger->info("Deleting page cache records for nodeID " . $nodeID);
	    		$result = $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('nodeID_' . $nodeID));
	    	}    		
    	} else {
    		// no cache enabled. do nothing.
    	}

    	return $result;
    }
    
    /*
    * Recursively deletes the contents of a directory without deleting the directory itself
    *
    * @param string $dir Directory name.
    * @param int $depth Denotes relative level in file structure
    */
    private function delTree($dir, $depth = 0) {

    	$files = glob($dir . '*', GLOB_MARK);
//    	Zend_Debug::dump($files); exit;
    	foreach ($files as $file) {
    		$lastChar = (substr($file, -1) == "\\") ? "/" : substr($file, -1);
    		if ($lastChar == "/") {
    			//echo "Dir found " . $file . "\n";
    			$depth = self::delTree($file, $depth+1);    			
    			$depth--;
    		} else {
    			unlink($file);
    		}
    	}
    	if ($depth > 0) {
    		//echo "Deleting dir " . $dir . "\n";
    		//rmdir($dir); //commenting out for time being
    	}
    	return $depth;
    }
    
    /*
     * Clears contents of static cache and static tag directories
    * WARNING: Ensure correct paths are set in the config.ini to avoid recursively deleting incorrect files
    * A check is in place to ensure path contains "cache" however this could still end badly if used incorrectly
    */
    public function clearStatic() {
    	$config = Zend_Registry::get('config');
    	$staticConfig = $config->caching->static;
    	 
    	$cache_tags = $staticConfig->tags->cache_dir;
    	if (strstr($cache_tags, "cache")) {
    		self::delTree($cache_tags);
    	} else {
    		return "cache_dir must contain the string \"cache\"";
    	}
    	 
    	$public_dir = $staticConfig->public_dir;
    	if (strstr($public_dir, "cache")) {
    		self::delTree($public_dir);
    	} else {
    		return "public_dir must contain the string \"cache\"";
    	}
    	return "Static cache cleared!";
    }
	
	public static function clearDir($name)
	{
		if (!$name) {
			throw new Exception("Must provide name to clear cache directory");
		}
		
		self::delTree(APPLICATION_PATH . "/cache/" . $name);
	}
}
