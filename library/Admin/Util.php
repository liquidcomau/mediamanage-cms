<?php
/**
 * Admin Utility class.
 * 
 * Generic php functions for use with MediaManage project
 */
class Admin_Util
{
    /**
     * The key for the logger in the Registry.
     */
    const REGISTRY_LOGGER = 'logger';

    /**
     * Constants
     */
    const ROLE_MODERATOR = 'moderator';
    
    const ROLE_ADMIN = 'admin';
    
    const ROLE_WEBMASTER = 'webmaster';
    
    /**
     * Get the global logging object.
     *
     * @return Zend_Log
     */
    public static function getLogger()
    {
        $registry = Zend_Registry::getInstance();

        // Try and find the logger in the Registry
        $logger = null;

        try {
            $logger = $registry->get(self::REGISTRY_LOGGER);
        } catch (Zend_Exception $e) {
            // If none found but a host exists, create the log in /var/tmp
            // Otherwise, log to nothing so existing code doesn't break
            $logger = new Zend_Log();
            if ($_SERVER['SERVER_NAME']) {
                $writer = new Zend_Log_Writer_Stream('/var/tmp/' . $_SERVER['SERVER_NAME'] . '-' . date('o-m-d') . '.log', 'a+');
            } else {
                $writer = new Zend_Log_Writer_Stream('/var/tmp/Admin_Util.log', 'a+');
            }
            $logger->addWriter($writer);
        }

        // Return the log instance
        return $logger;
    }

    /**
     * Get the global config (if any) from the Registry.
     *
     * @return Zend_Config_Ini $config The config from the Registry if found.
     */
    public static function getGlobalConfig()
    {
    	$config = null;
        try {
            $config = Zend_Registry::get('config');
        } catch (Zend_Exception $e) {
            self::getLogger()->err(__CLASS__ . ': Error retrieving config from the Registry.');
        }
        return $config;
    }

    /**
     * Returns TRUE if an action can be dispatched, or FALSE otherwise.
     *
     * @param  Zend_Controller_Request_Abstract $request Request to check.
     * @return boolean
     */
    public function isDispatchable(Zend_Controller_Request_Abstract $request)
    {
        $dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
        if (!$dispatcher->isDispatchable($request)) { // module/controller does not exist
            return false;
        }
        return @is_callable(array($dispatcher->loadClass($dispatcher->getControllerClass($request)) , $dispatcher->formatActionName($request->getActionName())));
    }

    /**
     * Terminates any output capturing to prevent incorrectly cached items.
     *
     * For example, during an error, debugging etc. caching should be disabled
     * to prevent debug information or caching of errors.
     *
     * @return boolean
     */
    public static function cancelOutputCaching()
    {
        try {
            $pageCache = Zend_Registry::get('pageCache');
            $pageCache->cancel();

            $cache = Zend_Registry::get('staticFileCache');
            $cache->cancel();
        } catch (Zend_Exception $e) {
            // page cache not used
        }
    }
    
    /**
    * Terminates any static output capturing to prevent incorrectly cached items.
    *
    * For example, during an error, debugging etc. caching should be disabled
    * to prevent debug information or caching of errors.
    *
    * @return boolean
    */
    public static function cancelStaticCaching()
    {
    	try {
    		$cache = Zend_Registry::get('staticFileCache');
    		$cache->setOption('caching', false);
    	} catch (Zend_Exception $e) {
    		// page cache not used
    	}
    }
    
    /**
     * Returns brightness value from 0 to 255
     *
     * @param string $hex Hexidecimal value of color to find brightness for.
     * @return integer
     */
	public static function get_brightness($hex) {
		$hex = str_replace('#', '', $hex);
		$c_r = hexdec(substr($hex, 0, 2));
		$c_g = hexdec(substr($hex, 2, 2));
		$c_b = hexdec(substr($hex, 4, 2));
		return (($c_r * 299) + ($c_g * 587) + ($c_b * 114)) / 1000;
	}
	
    /**
     * Obscure the customers credit card by only showing first and last 4 characters.
     * 
     * @param string $body Body of text to be searched and replaced.
     * @return string Body of text with credit card details now obscured.
     */
    public static function obscureCreditCard($body)
    {
    	$body = preg_replace('/card_number=(\d{4})(\d+)(\d{4})/', '$1********$3', $body);
    	$body = preg_replace('/card_number=(\d{4})(\d+)(\d{4})/', 'card_number=$1********$3', $body);
        $body = preg_replace('/\[AccountNumber\] => (\d{4})(\d+)(\d{4})/', '[AccountNumber] => $1********$3', $body);
        $body = preg_replace('/AccountNumber>(\d{4})(\d+)(\d{4})/', 'AccountNumber>$1********$3', $body);
        $body = preg_replace('/\[_cc_no:protected\] => (\d{4})(\d+)(\d{4})/', '[_cc_no:protected] => $1********$3', $body);
        $body = preg_replace('/\[_cc_cvv:protected\] => (\d+)/', '[_cc_cvv:protected] => ***', $body);
        $body = preg_replace('/\[card_number\] => (\d{4})(\d+)(\d{4})/', '[card_number] => $1********$3', $body);
        $body = preg_replace('/\[card_ccv\] => (\d+)/', '[card_ccv] => ***', $body);
        $body = preg_replace('/CARD_NUMBER=(\d{4})(\d+)(\d{4})/', 'CARD_NUMBER=$1********$3', $body);
        $body = preg_replace('/CARD_CCV=(\d+)/', 'CARD_CCV=***', $body);
        $body = preg_replace('/, \'(\d{4})(\d{8})(\d{4})\'/', ', \'$1********$3\'', $body);

    	return $body;
    }
    
    
    /**
     * Obscures or removes selected values from an array. Values are obscured by default.
     * 
     * @param array $data
     * @param boolean $remove
     * @return string
     */
    public static function obscureArrayData($data, $remove = false) {
    	$obscuredData = array();
    	$config = self::getGlobalConfig();
    	foreach ($data as $key => $field) {
    		if (is_array($field)) {
    			$obscuredData[$key] = self::obscureArrayData($field, $remove);
    		} else {
    			if (in_array($key, $config->log->obscure->toArray(), true)) {
	    			if (!$remove) {
	    				$obscuredData[$key] = "*****";
	    			}
	    		} else {
	    			$obscuredData[$key] = $field;
	    		}
    		}
    	}
    	return $obscuredData;
    }
    
    
    
    /**
     * Obscures a users password excepting the first 2 characters
     *
     * @param string $password
     * @return string
     */
    public static function obscurePassword($password) {
    	$logPwd = "";
    	for ($i = 0; $i < strlen($password); $i++) {
    		if ($i < 2) {
    			$logPwd .= $password[$i];
    		} else {
    			$logPwd .= "*";
    		}
    	}
    	return $logPwd;
    }
}