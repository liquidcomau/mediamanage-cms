<?php
interface Admin_Form_MediaCollectionInterface
{
	/**
	 * Returns the markup for each individual item in the form.
	 */
	public function itemTemplate();
	
	/**
	 * Returns the name of the property in the model that holds the media
	 * component so form items can be named appropriately for submission/population.
	 */
	public function mediaCollectionComponentName();
}
