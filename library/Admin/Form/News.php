<?php
class Admin_Form_News extends Admin_Form_Resource {
	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array('/js/admin/ResourceForms/Resource.js'),
				'data-resource-alias' => 'news post',
				'data-resource-type' => 'news',
				'data-resource-endpoint' => '/ajax/api/news'			
		));
		
		// Content table
		
		$mainF = new Admin_Form_SubForm();
		$mainF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		$this->addMainForm($mainF);
		$this->addContentForm($mainF);
		$this->addCategoriesForm($mainF);

		$seo = new Admin_Form_SubForm_Seo();
		$mainF->addSubForm($seo, 'seo');
		
		$this->addSubForm($mainF, 'mainF', null, false);
		
		$mainF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$seo->applyDecorators();
		
		if (Zend_Registry::get("config")->cms->modules->news->recommendations) {
			
			$getAllMethod = function() {
				$class = Admin_Factory::getClass("Model_News");
				return $class::getAll();
			};
			
			$getEntityMap = function(array $news_posts) {
				$map = [];
				foreach ($news_posts as $n) {
					$map[$n->getId()] = $n->getHeading();
				}

				return $map;
			};
			
			$this->addRelatedEntitySubForm($this, $getAllMethod, $getEntityMap, "newsRecommendationIds");
		}
		
		$this->addPageSettingsForm($this);
	}

	protected function addMainForm($parent)
	{
		$main = new Admin_Form_SubForm_Main();
		$main->removeElement("linkText");

		$main->publishedDate = new Admin_Form_Element_InputGroup("publishedDate");
		$main->publishedDate->setLabel("Published Date")
							->setAttribs(["class" => "form-control",
											"addon" => ["markup" => '<i class="glyphicon glyphicon-calendar data-time-icon="icon-time" data-date-icon="icon-calendar"></i>'],
											"group" => ["id" => "publishedDateGroup", "class" => "input-append datetimepicker", "data-date-format" => "DD/MM/YYYY hh:mm A"]	
										]);

		$main->addDisplayGroup(["heading", "url", "publishedDate"], "mainGroup");

		$main->applyDecorators();
		$parent->addSubForm($main, 'main', null, false);
	}

	protected function addContentForm($parent) 
	{
		$mainContent = new Admin_Form_SubForm_MainContent(null, ["description"]);
		
		$browseBtn = '<button class="btn btn-default thumbnailBrowse moxmanBrowse" type="button" data-for=".thumbnailInput">Browse</button>';
		
		$mainContent->featureImg = new Admin_Form_Element_InputGroup('featureImg');
		$mainContent->featureImg->setLabel('Feature Image')
								->setAttribs(array(
										'class' => 'form-control thumbnailInput',
										'addon' => 	array(
												'type' => 'button',
												'markup' => $browseBtn,
												'position' => 'append'
										)
								));
		
		$mainContent->addDisplayGroup(array('description', 'content', 'featureImg'), 'contentGroup');
		$mainContent->applyDecorators();

		$parent->addSubForm($mainContent, 'mainContent', null, false);
	}

	protected function addCategoriesForm($parent)
	{
		$catF = new Admin_Form_SubForm();

		$catF->header = new Zend_Form_Element_Note("header");
		$catF->header->setValue("Categories");

		$catClass = Admin_Factory::getClass("Model_Category");
		$categories = $catClass::getAll();
//		$options = array_column($categories, "name", "id");
		$options = [];
		foreach ($categories as $category) {
			$options[$category->getId()] = $category->getLabel();
		}

		$catF->categoryIds = new Zend_Form_Element_Multiselect("categoryIds");
		$catF->categoryIds->setMultiOptions($options)
							->setAttribs(["class" => "form-control"]);

		$catF->addDisplayGroup(["categoryIds"], "categoriesGroup");
		$catF->applyDecorators();

		$parent->addSubForm($catF, 'catF', null, false);
	}

	protected function addPageSettingsForm($parent)
	{
		$settingsF = new Admin_Form_SubForm();
		$settingsF->setAttribs(array(
				'icon' => 'fa-cog',
				'alias' => 'Settings'				
		));
		
		$pageSettings = new Admin_Form_SubForm_PageSettings();

		$pageSettings->removeElement('internalLabel');
                
		$pageSettings->featured = new Zend_Form_Element_Checkbox('featured');
		$pageSettings->featured->setLabel('Featured')
								->setAttribs(array(
									'class' => 'form-control'
								));

		$pageSettings->addDisplayGroup(['published', 'hidden', 'featured'], 'settingsGroup');
                
		$settingsF->addSubForm($pageSettings, 'pageSettings', null, false);
		
		$settingsF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$pageSettings->applyDecorators();
		
		$parent->addSubForm($settingsF, 'settingsF', null, false);
	}

	/**
	 * Convert published date to appropriate format.
	 * @param type $defaults
	 */
	public function populate($defaults)
	{
		if (isset($defaults["publishedDate"])) {
			$defaults["publishedDate"] = date("d/m/Y h:i A", strtotime($defaults["publishedDate"]));
		}
		
		if (Zend_Registry::get("config")->cms->modules->news->recommendations) {
			$el = $this->getSubForm("relatedEntitySF", true)->getElement("newsRecommendationIds");
			$el->removeMultiOption($defaults["id"]);
			
			// set order of recommended projects
			if (isset($defaults["newsRecommendationIds"])) {
				$this->getSubForm("relatedEntitySF", true)->getElement("newsRecommendationIds")
															->setAttrib("data-initial-order", json_encode($defaults["newsRecommendationIds"]));
			}
		}
		
		parent::populate($defaults);
	}
}
