<?php
class Admin_Form_Resource extends Admin_Form
{
	protected $_resourceId;
	protected $_resourceEndpoint;
	protected $_endpointOverride;
	
	public function getFullEndpoint()
	{
		return $this->_resourceEndpoint . '/' . $this->_resourceId;
	}
	
	public function setResourceId($id) 
	{
		$this->_resourceId = $id;
		return $this;
	}
	
	public function setResourceEndpoint($endpoint)
	{
		$this->_resourceEndpoint = $endpoint;
		return $this;
	}
	
	public function setEndpointOverride($endpoint)
	{
		$this->_endpointOverride = $endpoint;
		return $this;
	}
	
	public function __construct($options)
	{
		$this->_meta = $meta = new Admin_Form_SubForm();
		$meta->setAttribs(array(
				'style' => 'display: none'
		));
		
		$meta->id = new Zend_Form_Element_Hidden('id');
		$meta->id->setAttribs(array('data-mmng-bind' => 'id'));
		
		$meta->setDecorators(array(
				'FormElements'
		));
		
		$meta->setElementDecorators(array(
				'ViewHelper'
		));
		
		parent::__construct($options);
	}
	
	public function getResource()
	{
		$res = Admin_Api::get($this->_endpointOverride ? $this->_endpointOverride : $this->getFullEndpoint());
		if (!$res->isSuccessful()) throw new Exception('Nonexistent resource');

		return json_decode($res->getBody(), true);
	}
	
	public function populateResource()
	{
		$this->populate($this->getResource());
	}
	
	public function getMeta()
	{
		return $this->_meta;
	}
	
	public function renderMeta()
	{
		echo $this->getMeta()->render();
	}
	
	public function populate($defaults)
	{
		parent::populate($defaults);
		$this->_meta->setDefaults($defaults);
	}
} 
