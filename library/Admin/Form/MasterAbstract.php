<?php
abstract class Admin_Form_MasterAbstract extends Admin_Form_Resource {

	public function __construct($options) {
		
		// we don't use 'addForm' for the meta form because we don't want it to appear
		// alongside the others as a tab or however else the other subforms are outputted automatically
		
		parent::__construct($options);
		
		$meta = $this->_meta;
		$meta->setAttribs(array(
				'style' => 'display: none'
		));

		$meta->parentId = new Zend_Form_Element_Hidden('parentId');
		$meta->parentId->setAttribs(array('data-mmng-bind' => 'parentId'));
		
		$meta->createdDate = new Zend_Form_Element_Hidden('createdDate');
		$meta->createdDate->setAttribs(array('data-mmng-bind' => 'createdDate'));
		
		$meta->lastModified = new Zend_Form_Element_Hidden('lastModified');
		$meta->lastModified->setAttribs(array('data-mmng-bind' => 'lastModified'));
		
		$meta->objectTypeId = new Zend_Form_Element_Hidden('objectTypeId');
		
		$meta->setElementDecorators(array(
				'ViewHelper'
		));
	}
}