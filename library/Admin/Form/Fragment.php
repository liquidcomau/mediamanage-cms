<?php
class Admin_Form_Fragment extends Admin_Form_Resource
{
	protected $_resourceEndpoint = "fragments";
	
	public function init()
	{
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array('/js/admin/ResourceForms/Page.js'),
				'data-resource-alias' => 'fragment',
				'data-resource-type' => 'fragment',
				'data-resource-endpoint' => '/ajax/api/fragments'
		));
		
		// Content table
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-home',
			'alias' => 'Main'
		));
		
		$this->addMainSubForm($contentF);
		
		$this->addSubForm($contentF, 'contentF', null, false);
	}
	
	protected function addMainSubForm($to)
	{
		$mainF = new Admin_Form_SubForm();

		$mainF->header = new Zend_Form_Element_Note("header");
		$mainF->header->setValue("Fragment Details");
		
		$mainF->internalLabel = new Zend_Form_Element_Text("internalLabel");
		$mainF->internalLabel->setLabel("Internal Label")
							->setAttribs(["class" => "form-control"]);
		
		$mainF->identifier = new Zend_Form_Element_Text("identifier");
		$mainF->identifier->setLabel("Identifier")
							->setAttribs(["class" => "form-control"]);
		
		$mainF->content = new Zend_Form_Element_Textarea("content");
		$mainF->content->setLabel("Content")
						->setAttribs(["class" => "form-control wysiwyg", "id" => "contentArea"]);
		
		$mainF->addDisplayGroup(["internalLabel", "identifier", "content"], "mainGroup");
		$mainF->applyDecorators();
		
		$to->addSubForm($mainF, 'mainF', null, false);
	}
}