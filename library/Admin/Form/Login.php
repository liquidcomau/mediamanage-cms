<?php
class Admin_Form_Login extends Zend_Form {

	public function init() {
		$this->setMethod('post');
		$this->setAction('/admin/login/index/');
		$this->setAttribs(array(
				'data-validate' => 'parsley',
				'class' => 'panel-body wrapper-lg'
			)
		);
		
		$this->username = new Zend_Form_Element_Text('username');
		$this->username->setLabel('Email')
			->setRequired(true)
			->setAttribs(array(
				'class' => 'form-control input-lg',
				'data-required' => true,
				'autocomplete' => 'off',
				'placeholder' => 'test@example.com'
			)
		);
		
		$this->password = new Zend_Form_Element_Password('password');
		$this->password->setLabel('Password')
			->setRequired(true)
			->setAttribs(array(
				'class' => 'form-control input-lg',
				'data-required' => true,
				'autocomplete' => 'off',
				'placeholder' => 'Password'
			)
		);
		
		$this->submit = new Zend_Form_Element_Submit('submit');
		$this->submit->setLabel('Sign In')
			->setAttribs(array(
				'class' => 'btn btn-primary btn-s-md'
			)
		);
		
		$this->remember = new Zend_Form_Element_Checkbox('remember');
		$this->remember->setLabel('Remember Me');
		
		$this->clearDecorators();

		$this->setElementDecorators(array(
			array('ViewHelper'),
			array('Errors'),
			array('Label', array('separator'=>' ', 'escape' => false, 'class'=>'control-label')),
			array(array('control-group' => 'HtmlTag'), array('tag' => 'div', 'class'=>'form-group'))
		));
		
		$this->remember->setDecorators(array(
			array('ViewHelper'),
			array('Errors'),
			array('Label', array('separator'=>' ', 'escape' => false, 'class'=>'control-label pull-right', 'style' => 'margin-left:8px;')),
			array(array('control-group' => 'HtmlTag'), array('tag' => 'div', 'class'=>'form-group m-t-sm pull-left'))
		));
		
		$this->submit->setDecorators(array(
			array('ViewHelper'),
			array('Description'),
			array(array('control-group' => 'HtmlTag'), array('tag' => 'div', 'class'=>'form-actions pull-right'))
		));
	}
	
}