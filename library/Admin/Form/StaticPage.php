<?php
class Admin_Form_StaticPage extends Admin_Form_MasterAbstract {

	public function init() {
		parent::init();
		
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array('/js/admin/jqSortable.js', '/js/admin/ResourceForms/Page.js'),
				'data-resource-alias' => 'static page',
				'data-resource-type' => 'static-page',
				'data-resource-endpoint' => '/ajax/api/static-pages'			
		));
		
		// Content tab
		
		$mainF = new Admin_Form_SubForm();
		$mainF->setAttribs(array(
			'icon' => 'fa-home',
			'alias' => 'main'
		));
		
		$main = Admin_Factory::create("Form_SubForm_Main");
		$mainF->addSubForm($main, 'main', null, false);

		$seo = new Admin_Form_SubForm_Seo();
		$mainF->addSubForm($seo, 'seo');
		
		$this->addSubForm($mainF, 'mainF', null, false);
		$mainF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$this->addContentSubForm($this);
		
		$main->applyDecorators();
		$seo->applyDecorators();
		
		$settingsF = new Admin_Form_SubForm();
		$settingsF->setAttribs(array(
				'icon' => 'fa-cog',
				'alias' => 'Settings'				
		));
		
		$this->addSettingsSubForm($settingsF);
		
		$this->addSubForm($settingsF, 'settingsF', null, false);
	}
	
	protected function addContentSubForm($to, $defaults = [])
	{
		if (!isset($defaults['url'])) return;
		
		$contentSubForm = Admin_Form_SubForm_ContentFactory::createForStaticFromUrl($defaults['url']);
		if ($contentSubForm) {	
			$contentF = new Admin_Form_SubForm();
			$contentF->setAttribs(array(
				'icon' => 'fa-file-text',
				'alias' => 'Content'
			));
			
			$contentF->addSubForm($contentSubForm, 'contentSubForm', null, false);
			$to->addSubForm($contentF, 'contentF', null, false);
			
			return $contentF;
		}
		
		return;
	}
	
	public function populate($defaults)
	{
		parent::populate($defaults);
		
		if (isset($defaults['url'])) {
			// we want content tab to be in the middle of main and settings
			
			$subForms = $this->getSubForms();
			$this->clearSubForms();
			
			$first = array_shift($subForms);
			$this->addSubForm($first, $first->getAttrib('name'), null, false);
			
			$contentF = $this->addContentSubForm($this, $defaults);
			
			foreach ($subForms as $sf) {
				$this->addSubForm($sf, $sf->getAttrib('name'), null, false);
			}
			
			if ($contentF) {
				$contentF->setDefaults($defaults);
			}
		}
	}
	
	protected function addSettingsSubForm($to)
	{
		$pageSettings = new Admin_Form_SubForm_PageSettings();
		$pageSettings->applyDecorators();

		$to->addSubForm($pageSettings, 'pageSettings', null, false);
	}
}
