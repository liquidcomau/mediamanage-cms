<?php
class Admin_Form_Listing extends Admin_Form_Resource
{
	protected $_resourceEndpoint = 'listings';
	
	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
						'/theme/js/tinymce/tinymce.min.js',
						'/js/admin/parsley.js',
						'/js/admin/manageParsley.js',
						'/js/admin/jqSortable.js',
						'/js/admin/ResourceForm.js',
						'/js/admin/ResourceForms/MediaCollectionComponent.js',
						'/js/admin/ResourceForms/Listing.js'
//						'/js/admin/ResourceForms/Resource.js'
				),
				'data-resource-alias' => 'listing',
				'data-resource-type' => 'listing',
				'data-resource-endpoint' => '/ajax/api/listings'			
		));
		
		// Content table
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		/*
		$projectF = new Admin_Form_SubForm();
		
		$projectF->header = new Zend_Form_Element_Note('header');
		$projectF->header->setValue('Project');
		
		$projectIds = array_column(Admin_Model_Project::getAll(null, Admin_Constants::TYPE_ARRAY), 'heading', 'id');
		
		$projectF->projectId = new Zend_Form_Element_Select('projectId');
		$projectF->projectId->setMultiOptions($projectIds);
		$projectF->projectId->setAttribs(['class' => 'form-control']);

		$projectF->addDisplayGroup(['projectId'], 'projectGroup');
		$projectF->applyDecorators();
		
		$contentF->addSubForm($projectF, 'projectF', null, false);
		*/
		
		$main = new Admin_Form_SubForm_Main();
		
		$main->price = new Zend_Form_Element_Text('price');
		$main->price->setLabel('Price')
					->setAttribs(['class' => 'form-control'])
					->setDescription('Price without formatting i.e. 560000');
		
		$main->priceView = new Zend_Form_Element_Text('priceView');
		$main->priceView->setLabel('Price Label')
						->setAttribs(['class' => 'form-control'])
						->setDescription('Price as displayed i.e. "From $500,000"');
		
		$main->publishedDate = new Admin_Form_Element_InputGroup("publishedDate");
		$main->publishedDate->setLabel("Published Date")
							->setAttribs(["class" => "form-control",
											"addon" => ["markup" => '<i class="glyphicon glyphicon-calendar data-time-icon="icon-time" data-date-icon="icon-calendar"></i>'],
											"group" => ["id" => "publishedDateGroup", "class" => "input-append datetimepicker", "data-date-format" => "DD/MM/YYYY hh:mm A"]	
										]);
		
		$main->addDisplayGroup(['heading', 'url', 'price', 'priceView', 'publishedDate'], 'mainGroup');
		
		$contentF->addSubForm($main, 'main', null, false);
		
		$mainContent = new Admin_Form_SubForm('mainContent');
		
		$mainContent->header = new Zend_Form_Element_Note('header');
		$mainContent->header->setValue('Listing Content');
		
		$mainContent->headline = new Zend_Form_Element_Text('headline');
		$mainContent->headline->setLabel('Headline')
								->setAttribs(['class' => 'form-control']);
		
		$mainContent->description = new Zend_Form_Element_Textarea('description');
		$mainContent->description->setLabel('Content')
								->setAttribs(['class' => 'form-control']);
		
		$mainContent->addDisplayGroup(['headline', 'description'], 'mainContentGroup');
		$mainContent->applyDecorators();
		
		$contentF->addSubForm($mainContent, 'mainContent', null, false);
		
		$seo = new Admin_Form_SubForm_Seo();
		$contentF->addSubForm($seo, 'seo');
		$seo->applyDecorators();
		
		$this->addSubForm($contentF, 'contentF', null, false);
		
		$contentF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$main->applyDecorators();
		
		$this->addGalleryForm($this);
		$this->initAgentsForm($this);
		$this->initSettingsForm($this);
	}
	
	private function addGalleryForm($parent) {
		$galleryF = new Admin_Form_SubForm();
		$galleryF->setAttribs(array(
				'icon' => 'fa-camera',
				'alias' => 'Gallery'				
		));

		$gallerySF = new Admin_Form_SubForm_MediaCollectionStandard();
		$gallerySF->applyDecorators();
		
		$galleryF->addSubForm($gallerySF, 'gallerySF', null, false);
		
		$galleryF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$this->addSubForm($galleryF, 'galleryF', null, false);
	}
	
	private function initAgentsForm($parent)
	{
		$agentsF = new Admin_Form_SubForm();
		$agentsF->setAttribs([
			"icon" => "fa-group",
			"alias" => "Agents"
		]);

		$agentsSF = new Admin_Form_SubForm();
		$agentsSF->header = new Zend_Form_Element_Note("header");
		$agentsSF->header->setValue("Assign Agents");

		$agent_class = Admin_Factory::getClass("Model_Agent");
		
		$agents = $agent_class::getAll();
		$options = [];

		foreach ($agents as $agent) {
//			$agency = $agent->getAgency()->getName();
//			if (!isset($options[$agency])) $options[$agency] = [];
//			$options[$agency][$agent->getId()] = $agent->getName();
			
			$options[$agent->getId()] = $agent->getName();
		}

		$agentsSF->agentIds = new Zend_Form_Element_Multiselect("agentIds");
		$agentsSF->agentIds->setMultiOptions($options)
					->setAttribs(["class" => "form-control"]);
		
		$agentsSF->addDisplayGroup(["agentIds"], "agentsGroup");
		$agentsSF->applyDecorators();

		$agentsF->addSubForm($agentsSF, "agentsSF", null, false);

		$parent->addSubForm($agentsF, "agentsF", null, false);
	}
	
	protected function initSettingsForm($parent)
	{
		$settingsF = new Admin_Form_SubForm();
		$settingsF->setAttribs(array(
				'icon' => 'fa-cog',
				'alias' => 'Settings'				
		));
		
		$pageSettings = new Admin_Form_SubForm_PageSettings();
		
		$pageSettings->removeElement('hidden');
		
		$pageSettings->featured = new Zend_Form_Element_Checkbox('featured');
		$pageSettings->featured->setLabel('Featured')
								->setAttribs(['class' => 'form-control']);
		
		$pageSettings->addDisplayGroup(['published', 'featured'], 'settingsGroup');
		
		$settingsF->addSubForm($pageSettings, 'pageSettings', null, false);
		
		$settingsF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$pageSettings->applyDecorators();
		
		$parent->addSubForm($settingsF, 'settingsF', null, false);
	}
}
