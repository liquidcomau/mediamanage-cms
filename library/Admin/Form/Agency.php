<?php
class Admin_Form_Agency extends Admin_Form_Resource {
	protected $_resourceEndpoint = 'agencies';
	
	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
						'/theme/js/tinymce/tinymce.min.js',
						'/js/admin/parsley.js',
						'/js/admin/manageParsley.js',
						'/js/admin/ResourceForm.js',
						'/js/admin/ResourceForms/Page.js'
				),
				'data-resource-alias' => 'agency',
				'data-resource-type' => 'agency',
				'data-resource-endpoint' => '/ajax/api/agencies'			
		));
		
		// Content table
		
		$mainF = new Admin_Form_SubForm();
		$mainF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		$agencyF = new Admin_Form_SubForm();
		
		$agencyF->header = new Zend_Form_Element_Note('header');
		$agencyF->header->setValue('Details');
		
		$agencyF->name = new Zend_Form_Element_Text('name');
		$agencyF->name->setLabel("Agency Name")
						->setAttribs(["class" => "form-control"]);

		$agencyF->email = new Zend_Form_Element_Text('email');
		$agencyF->email->setLabel("Agency Email")
						->setAttribs(["class" => "form-control"]);

		$agencyF->address = new Zend_Form_Element_Text('address');
		$agencyF->address->setLabel("Address")
						->setAttribs(["class" => "form-control"]);

		$browseBtn = '<button class="btn btn-default thumbnailBrowse moxmanBrowse" type="button" data-for=".logoInput">Browse</button>';
		
		$agencyF->logo = new Admin_Form_Element_InputGroup('logo');
		$agencyF->logo->setLabel('Agency Logo')
						->setAttribs(array(
								'class' => 'form-control logoInput',
								'addon' => 	array(
										'type' => 'button',
										'markup' => $browseBtn,
										'position' => 'append'
								)
						));

		$agencyF->website = new Zend_Form_Element_Text('website');
		$agencyF->website->setLabel("Website")
						->setAttribs(["class" => "form-control"]);

		$agencyF->addDisplayGroup(["name", "email", "address", "logo", "website"], "agencyGroup");
		$agencyF->applyDecorators();

		$mainF->addSubForm($agencyF, "agencyF", null, false);
		$this->addSubForm($mainF, "mainF", null, false);
	}
}
