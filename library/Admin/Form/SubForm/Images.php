<?php
class Admin_Form_SubForm_Images extends Admin_Form_SubForm
{
	protected $_isArray = false;

	public function init() 
	{
		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('Images');

		$this->imagesArea = new Zend_Form_Element_Note('imagesArea');
		$this->imagesArea->setValue('<ul class="imagesArea"></ul><div class="imagesAreaFooter clearfix"><div class="pull-right"><button type="button" class="btn btn-primary addImageBtn">Add Image</button></div></div>');

		$this->addDisplayGroup(['imagesArea'], 'imagesGroup');
		
		$this->images = new Zend_Form_Element_Hidden('images');
		
		$this->imageTemplate = new Zend_Form_Element_Hidden('imageTemplate');
		$this->imageTemplate->setValue('<li></li>')
							->setAttribs(['style' => 'hidden']);
	}
	
	public function setImageTemplatePartial($path, $module = null)
	{
		$this->imageTemplate->setValue(Zend_Layout::getMVCInstance()->getView()->partial($path, $module));
	}
	
	public function setDefaults($defaults)
	{
		if (isset($defaults['images']) && is_array($defaults['images'])) {
			$defaults['images'] = json_encode($defaults['images']);
		}
		
		parent::setDefaults($defaults);
	}

	public function applyFormDecorators()
	{
		$this->setDecorators(array(
				'Form',
				'FormElements',
				array('HtmlTag', array('tag' => 'section', 'class' => 'panel panel-default panel-images-form'))
		));
	}
}
