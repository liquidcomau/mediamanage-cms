<?php
class Admin_Form_SubForm_VariableContent extends Admin_Form_SubForm {
	protected $_isArray = false;
	protected $_includeEls = [];

	protected $_dispGrp = ['els' => [], 'name' => 'contentGroup'];
	protected $_contentAreas = [];

	protected $_pageTemplates = null;
	
	public function __construct($class , $options, $includeEls = [])  
	{
		$class = (string) $class;
		$this->_includeEls = $includeEls;

		$reflection = new ReflectionClass($class);
		if ($reflection->implementsInterface('Admin_Controller_Action_DynamicTemplateInterface')) {
			$this->_pageTemplates = $class::getPageTemplates();
		} else {
			throw new Exception("Template class does not implement DynamicTemplateInterface");
		}

		parent::__construct($options);
	}
	
	public function init() {
		$this->setIsArray(false);
		
		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('Content');

		if (in_array("description", $this->_includeEls)) {
			$this->description = new Zend_Form_Element_Textarea('description');
			$this->description->setLabel('Introduction')
				->setDescription('This appears on the home/listing pages')
				->setAttribs(array('class' => 'form-control', 'rows' => '4'));
			
			$this->_dispGrp['els'][] = 'description';
		}
		
		$this->addVariableContent();

		$this->contentModel = new Zend_Form_Element_Hidden('contentModel');
		
		$this->addDisplayGroup($this->_dispGrp['els'], $this->_dispGrp['name']);
		$this->applyDecorators();
	}
	
	protected function addVariableContent()
	{
		$this->addTemplatesElement('template');
		$this->setDynamicContentForm();
	}

	/**
	 * Generates content areas according to saved form settings
	 * ie chosen template for freeform pages.
	 * 
	 * Gives appropriate 'model' hidden elements appropriate filler content
	 * with correct json structure.
	 * 
	 * @param type $defaults
	 * @param type $generated
	 */
	public function setDefaults($defaults)
	{
		if (isset($defaults["template"])) {
			$this->setDynamicContentForm($defaults['template']);
		}
		
		if (isset($defaults['content'])) {
			// need to append dca- and ca- prefixes
			// for js to identify whether area is dynamic (dca)
			// or single wysiwyg (ca)
			
			$content = [];
			foreach ($defaults['content'] as $key => $val) {
				if (is_array($val)) {
					$content['dca-' . $key] = $val;
				} else {
					$content['ca-' . $key] = $val;
				}
			}
			
			$defaults['contentModel'] = json_encode($content);
			unset($defaults['content']);
		} 
		
		parent::setDefaults($defaults);
	}

	/**
	 * Generates the template selector dropdown and hidden model
	 * No dropdown is shown if only one template (hidden element is then
	 * generated for JS to usea as key for templatesModel).
	 * 
	 * @param type $elName
	 * @return \Zend_Form_Element_Select
	 */
	protected function addTemplatesElement($elName = 'template')
	{
		$elName = (string) $elName;
		
		// templatesModel acts as dictionary of guidelines
		// for js to generate content areas when template is selected from 
		// dropdown without a refresh.
		
		$this->templatesModel = new Zend_Form_Element_Hidden('templatesModel');
		$this->templatesModel->setValue(json_encode(array_column($this->_pageTemplates, null, 'name')));
		
		if (count($this->_pageTemplates) > 1) {
			$options = [];
			foreach ($this->_pageTemplates as $template) {
				$options[$template['name']] = $template['alias'];
			}

			$this->$elName = new Zend_Form_Element_Select($elName);
			$this->$elName->setLabel('Page Templates')
						->setMultiOptions($options)
						->setAttribs(['class' => 'form-control']);

			$this->_dispGrp['els'][] = $elName;
		} else {
			$this->$elName = new Zend_Form_Element_Hidden($elName);
			$this->$elName->setValue($this->_pageTemplates[0]['name']);
		}
	}
	
	/**
	 * Generates the multiple form elements.
	 * If 'type' and 'contentStyles' array keys
	 * are detected in the _pageTemplates property,
	 * generates a dynamic drag-n-drop ridiculous
	 * content editor, otherwise a single wysiwyg.
	 * 
	 * @param string $template. The chosen template - array key for the _pageTemplates array property.
	 * @return boolean
	 */
	protected function setDynamicContentForm($template = null)
	{
		$templates = array_column($this->_pageTemplates, null, 'name');
		$areas = $template ? $templates[$template]['areas'] : array_shift($templates)['areas'];	

		$names = [];

		// first unset existing properties
		while (count($this->_contentAreas)) {
			$oldArea = array_shift($this->_contentAreas);
			unset($this->$oldArea);
			
			if ($this->getDisplayGroup($this->_dispGrp["name"])) {
				$this->getDisplayGroup($this->_dispGrp["name"])->removeElement($oldArea);
			}
		}

		foreach ($areas as $area) {
			if (isset($area['type']) && $area['type'] === 'dynamic') {
				
				// generates dynamic content area if type=dynamic
				
				$this->$area['name'] = new Admin_Form_Element_DynamicContent($area['name']);
				$this->$area['name']
						->setValue($area['alias'])
						->setAttribs(array(
								'class' => 'form-control wysiwyg',
								'id' => 'dca-' . $area['name']
						));
			} else {
				
				// else generates simply wysiwyg (textarea + class)
				
				$this->$area['name'] = new Zend_Form_Element_Textarea($area['name']);
				$this->$area['name']
						->setLabel($area['alias'])
						->setAttribs(array(
								'class' => 'form-control wysiwyg',
								'id' => 'ca-' . $area['name']
						));
			}
			
			$this->$area['name']->setBelongsTo('content');
			
			$this->_contentAreas[] = $area['name'];
			$this->_dispGrp['els'][] = $area["name"];
		}

		// removing and readding elements so content areas remain
		// in order and unseparated when regenerated. (setDefaults)
		
		$this->addDisplayGroup($this->_dispGrp['els'], $this->_dispGrp['name']);
		$this->applyDecorators();

		return true;
	}

	public function applyDecorators()
	{
		parent::applyDecorators();
		$this->setDisplayGroupDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel-body variableContent'))
		));
	}
}
