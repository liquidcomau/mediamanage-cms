<?php
class Admin_Form_SubForm_Main extends Admin_Form_SubForm {
	protected $_isArray = false;
	
	public function init() {

		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('Main');
		
		$this->heading = new Zend_Form_Element('heading');
		$this->heading->setLabel('Heading')
						->setAttribs(array('class' => 'form-control resourceHeading',
											'data-parsley-required' => 'true'))
						->setRequired();
		
		$this->subheading = new Zend_Form_Element("subheading");
		$this->subheading->setLabel("Subheading")
						->setAttribs(["class" => "form-control"]);
		
		$this->url = new Zend_Form_Element('url');
		$this->url->setLabel('URL')
					->setAttribs(array('class' => 'form-control resourceUrl',
										'data-parsley-urlpath' => 'urlpath', 'data-parsley-trigger' => 'change'))
					->setRequired();

		$this->linkText = new Zend_Form_Element_Text("linkText");
		$this->linkText->setLabel("Link Text")
						->setAttribs(["class" => "form-control"]);
		
		$this->addDisplayGroup(array('heading', 'subheading', 'url', 'linkText'), 'mainGroup');
	}
}
