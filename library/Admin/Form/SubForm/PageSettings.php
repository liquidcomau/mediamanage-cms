<?php
class Admin_Form_SubForm_PageSettings extends Admin_Form_SubForm {
	protected $_isArray = false;
	
	public function init() {

		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('Settings');
		
// 		$this->note  = new Zend_Form_ELement_Note('note');
// 		$this->note->setValue('This is where settings will be');
		
		$this->internalLabel = new Zend_Form_Element_Text('internalLabel');
		$this->internalLabel->setLabel('Internal Label')
							->setAttribs(['class' => 'form-control']);

		$this->published = new Zend_Form_Element_Select('published');
		$this->published->setLabel('Status')
						->setAttribs(array(
							'options' => array('0' => 'Unpublished', '1' => 'Published'),
							'class' => 'form-control'						
						));
						
		$this->hidden = new Zend_Form_Element_Checkbox('hidden');
		$this->hidden->setLabel('Hidden')
						->setDescription('By ticking this field, the page will not be displayed on the site (e.g. in navigation or listing pages). Note: Page will appear still in search and on Google if it is published.')
						->setAttribs(array('class' => 'form-control'));
		
		$this->addDisplayGroup(array('internalLabel', 'published', 'hidden'), 'settingsGroup');
	
	}
	
	public function applyFormDecorators()
	{
		$this->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'section', 'class' => 'panel panel-default form-horizontal'))
		));
	}
	
	public function applyElementDecorators()
	{
		$this->setElementDecorators(array(
				'ViewHelper',
				'Errors',
				array(array('elWrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-sm-9')),
				array('Label', array('class' => 'col-sm-3 control-label')), 
				array('Description', array('tag' => 'div', 'class' => 'help-block m-b-none col-sm-offset-3 col-sm-9')),
				array(array('wrapperWrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
		));
	}
}
