<?php
class Admin_Form_SubForm_ContentFactory
{
	/**
	 * Creates for dynamic page types ie FreeformPages.
	 * Appropriate for pages with a guaranteed editable area i.e. not static pages.
	 * 
	 * @param type $ctrlName
	 * @return type
	 */
	public static function createForType($ctrlName, $includeEls = [])
	{
		$className = $ctrlName . 'Controller';

		$file = Zend_Controller_Front::getInstance()->getControllerDirectory('default') . '/' . $className . '.php';
		if (!file_exists($file)) return self::createSingle();

		require_once($file);

		$isTemplate = (new ReflectionClass($className))->implementsInterface('Admin_Controller_Action_DynamicTemplateInterface');

		return $isTemplate ? self::createDynamicWithClass($className, $includeEls) : self::createSingle($includeEls);
	}
	
	/**
	 * Creates templates for static pages.
	 * Gets the controller class name from the $url
	 * i.e. url "about-us" would look for AboutUsController.php
	 * 
	 * @param type $url
	 * @return boolean
	 */
	public static function createForStaticFromUrl($url, $includeEls = []) 
	{
		if ($url === "/") {
			$className = "IndexController";
		} else {
			$className = Zend_Filter::filterStatic($url, 'Word_DashToCamelCase') . 'Controller';
		}

		$file = Zend_Controller_Front::getInstance()->getControllerDirectory('default') . '/' . $className . '.php';
		if (!file_exists($file)) return false;

		require_once($file);

		$reflection = new ReflectionClass($className);
		
		$isTemplate = (new ReflectionClass($className))->implementsInterface('Admin_Controller_Action_DynamicTemplateInterface');

		return $isTemplate ? self::createDynamicWithClass($className, $includeEls) : false;
	}

	private static function createDynamicWithClass($class, $includeEls = [])
	{
		$class = (string) $class;
		return new Admin_Form_SubForm_VariableContent((string) $class, null, $includeEls);
	}

	private static function createSingle($includeEls = [])
	{
		return new Admin_Form_SubForm_MainContent(null, $includeEls);
	}
}
