<?php
class Admin_Form_SubForm_MainContent extends Admin_Form_SubForm {
	protected $_isArray = false;
	protected $_includeEls = [];
	
	public function __construct($options = null, $includeEls = [])
	{
		$this->_includeEls = $includeEls;
		parent::__construct($options);
	}
	
	public function init() {
		$this->setIsArray(false);
		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('Content');

		if (in_array("description", $this->_includeEls)) {
			$this->description = new Zend_Form_Element_Textarea('description');
			$this->description->setLabel('Introduction')
							->setAttribs(array('class' => 'form-control', 'rows' => '4'));
		}
		
		$this->content = new Zend_Form_Element_Textarea('content');
		$this->content->setLabel('Content')
					->setAttribs(array('class' => 'form-control wysiwyg', 'id' => 'wysiwyg'));
		
		$this->addDisplayGroup(array('description', 'content'), 'contentGroup');
	}
}