<?php
class Admin_Form_SubForm_Seo extends Admin_Form_SubForm {
	public function init() {
		$this->setElementsBelongTo('seo');
		
		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('SEO');
		
		$this->title = new Zend_Form_Element_Text('title');
		$this->title->setLabel('Title')
					->setAttribs(array('class' => 'form-control'));
		
		$this->metaDescription = new Zend_Form_Element_Textarea('metaDescription');
		$this->metaDescription->setLabel('Meta Description')
					->setAttribs(array('class' => 'form-control', 'rows' => '4'));
		
		$this->robots = new Zend_Form_Element_Select('robots');
		$this->robots->setLabel('Robots')
					->setAttribs(array('class' => 'form-control'))
					->setMultiOptions(array(
							'index, follow' => 'index, follow',
							'index, nofollow' => 'index, nofollow',
							'noindex, follow' => 'noindex, follow',
							'noindex, nofollow' => 'noindex, nofollow'
		));

		$this->addDisplayGroup(array('title', 'metaDescription', 'robots'), 'seoGroup');
	}
}