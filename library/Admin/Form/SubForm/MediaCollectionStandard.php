<?php
class Admin_Form_SubForm_MediaCollectionStandard extends Admin_Form_SubForm_MediaCollectionAbstract
{
	public $mediaCollectionComponentName = "gallery";
	
	public function init() 
	{
		$this->setItemTemplatePartial("partials/form/mediaCollectionItem.phtml", "admin");
		
		$this->header = new Zend_Form_Element_Note('header');
		$this->header->setValue('Images');

		$this->imagesArea = new Zend_Form_Element_Note('imagesArea');
		$this->imagesArea->setValue('<ul class="imagesArea"></ul><div class="imagesAreaFooter clearfix"><div class="pull-right"><button type="button" class="btn btn-primary addImageBtn">Add Image</button></div></div>');

		$this->addDisplayGroup(['imagesArea'], 'imagesGroup');
		
		$this->items = new Zend_Form_Element_Hidden('items');
		$this->mediaCollectionId = new Zend_Form_Element_Hidden('mediaCollectionId');
		$this->relationshipId = new Zend_Form_Element_Hidden('relationshipId');
		
		$this->itemTemplate = new Zend_Form_Element_Hidden('itemTemplate');
		$this->itemTemplate->setValue($this->itemTemplate())
							->setAttribs(['style' => 'hidden']);
	}
	
	public function itemTemplate()
	{
		return $this->_itemTemplate;
	}
	
	public function mediaCollectionComponentName()
	{
		return $this->mediaCollectionComponentName;
	}
}