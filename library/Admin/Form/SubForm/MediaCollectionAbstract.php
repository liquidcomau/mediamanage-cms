<?php
abstract class Admin_Form_SubForm_MediaCollectionAbstract extends Admin_Form_SubForm implements Admin_Form_MediaCollectionInterface
{
	protected $_isArray = false;
	protected $_itemTemplate = "";
	
	public function __construct($options = null)
	{
		parent::__construct($options);
		$this->setElementsBelongTo($this->mediaCollectionComponentName());
	}

	public function setItemTemplatePartial($path, $module = null)
	{
		$this->_itemTemplate = (Zend_Layout::getMVCInstance()->getView()->partial($path, $module));
	}
	
	public function setDefaults($defaults)
	{
		$mediaCollectionName = $this->mediaCollectionComponentName();

		if (isset($defaults[$mediaCollectionName]["items"]) && is_array($defaults[$mediaCollectionName]["items"])) {
			$defaults[$mediaCollectionName]["items"] = json_encode($defaults[$mediaCollectionName]["items"]);
		}
		parent::setDefaults($defaults);
	}

	public function applyFormDecorators()
	{
		$this->setDecorators(array(
				'Form',
				'FormElements',
				array('HtmlTag', array('tag' => 'section', 'class' => 'panel panel-default panel-images-form'))
		));
	}
}
