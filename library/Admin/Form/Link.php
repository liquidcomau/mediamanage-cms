<?php
class Admin_Form_Link extends Admin_Form_MasterAbstract {

	protected $_resourceEndpoint = 'links';
	
	public function init() {
		parent::init();
		
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
					'/theme/js/select2/select2.min.js',
					'/js/admin/ResourceForms/Link.js'
				)
		));
		
		// Content tab
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		$link = new Admin_Form_SubForm();
		
		$link->header = new Zend_Form_Element_Note('header');
		$link->header->setValue('Link');
		
		$link->heading = new Zend_Form_Element_Text('heading');
		$link->heading->setLabel('Label')
						->setAttribs(array(
								'class' => 'form-control'	
						));
		
		$link->linkType = new Zend_Form_Element_Radio('linkType');
		$link->linkType->setAttribs(array(
				'class' => 'link-type',
				'options' => array('internal' => 'Internal Link', 'external' => 'External Link')
		));
		
		$link->newWindow = new Zend_Form_Element_Checkbox('newWindow');
		$link->newWindow->setLabel('Open In New Tab/Window')
						->setAttribs(array(
								'class' => 'form-control'
						));
						
		$link->reference = new Zend_Form_Element_Hidden('reference');
		
		$link->addDisplayGroup(array('heading', 'linkType', 'newWindow'), 'linkDisplayGroup');
		
		$contentF->addSubForm($link, 'link', null, false);
		
		$this->addSubForm($contentF, 'contentF', null, false);
		
		$link->applyDecorators();
		
		$contentF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$settingsF = new Admin_Form_SubForm();
		$settingsF->setAttribs(array(
				'icon' => 'fa-cog',
				'alias' => 'Settings'
		));
		
		$pageSettings = new Admin_Form_SubForm_PageSettings();
		$settingsF->addSubForm($pageSettings, 'pageSettings', null, false);
		
		$settingsF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$pageSettings->applyDecorators();
		
		$this->addSubForm($settingsF, 'settingsF', null, false);
		
		$link->setElementDecorators(array('ViewHelper'), array('reference'));
	}
	
	public function populate($data) 
	{
		if (isset($data['href'])) {
			$data['reference'] = $data['href'];
			unset($data['href']);
		} elseif (isset($data['pageId'])) {
			$data['reference'] = $data['pageId'];
			unset($data['pageId']); // unsetting this here and adding back in js to keep consistent with 'href' condition
		}
		
		parent::populate($data);
	}
}