<?php
class Admin_Form_Redirect extends Admin_Form_Resource {

	protected $_resourceEndpoint = 'redirects';
	
	public function init() {
		parent::init();
		
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
					'/js/admin/ResourceForm.js',
					'/js/admin/ResourceForms/Resource.js'
				),
				'data-resource-alias' => 'redirect',
				'data-resource-type' => 'redirect',
				'data-resource-endpoint' => '/ajax/api/redirects'			
		));
		
		// Content tab
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'main'
		));
		
		$redirectF = new Admin_Form_SubForm();
		
		$redirectF->header = new Zend_Form_Element_Note('header');
		$redirectF->header->setValue('Redirect');
		
		$redirectF->url = new Zend_Form_Element_Text("url");
		$redirectF->url->setLabel("Url")
						->setAttribs(["class" => "form-control"]);
		
		$redirectF->destination = new Zend_Form_Element_Text("destination");
		$redirectF->destination->setLabel("Destination")
								->setAttribs(["class" => "form-control"]);
		
		$redirectF->type = new Zend_Form_Element_Select("type");
		$redirectF->type->setLabel("Type")
						->setAttribs(["class" => "form-control"])
						->setMultiOptions([
							"" => "--- Please Select ---",
							"301" => "301",
							"302" => "302"
						]);
		
		$redirectF->addDisplayGroup(array('url', 'destination', 'type'), 'redirectGroup');
		
		$contentF->addSubForm($redirectF, 'redirectF', null, false);
		
		$this->addSubForm($contentF, 'contentF', null, false);
		
		$redirectF->applyDecorators();
		
		$contentF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
	}
}
