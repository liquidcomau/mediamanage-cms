<?php
class Admin_Form_Collection extends Admin_Form_MasterAbstract 
{
	protected $_resourceEndpoint = "collections";
	protected $_entityName = null;
	
	public function __construct($entityType, $options = null) 
	{
		if (!$entityType) throw new Exception("Collection form  must have entity type");
		$this->_entityName = $entityType;
		
		parent::__construct($options);
	}
	
	public function init() {
		$this->_meta->entityName = new Zend_Form_Element_Hidden("entityName");
		$this->_meta->entityName->setValue($this->_entityName);
		
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
						'/theme/js/tinymce/tinymce.min.js',
						'/js/admin/parsley.js',
						'/js/admin/manageParsley.js',
						'/js/admin/jqSortable.js',
						'/js/admin/ResourceForm.js',
						'/js/admin/ResourceForms/Resource.js'
				),
				'data-resource-alias' => 'collection',
				'data-resource-type' => 'collection',
				'data-resource-endpoint' => '/ajax/api/collections'			
		));
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
				'icon' => 'fa-file-text',
				'alias' => 'content'
		));

		$mainF = new Admin_Form_SubForm_Main();

		$mainF->removeElement("url");

		$mainF->identifier = new Zend_Form_Element_Text("identifier");
		$mainF->identifier->setLabel("Identifier")
							->setAttribs(["class" => "form-control"])
							->setDescription("Used for referencing collection in shortcodes");

		$mainF->removeElement("linkText");
		$mainF->addDisplayGroup(["heading", "identifier"], "mainGroup");
		$mainF->applyDecorators();

		$contentF->addSubForm($mainF, 'mainF', null, false);
		
		$this->addEntitiesForm($contentF);
		
		$this->addSubForm($contentF, 'contentF');
	}
	
	public function addEntitiesForm($to) 
	{
		$entitiesF = new Admin_Form_SubForm();
		
		$entitiesF->header = new Zend_Form_Element_Note("header");
		$entitiesF->header->setValue("Entities");
		
		$collectionClass = Admin_Factory::getClass("Model_Collection");
		$entities = $collectionClass::loadEntities($this->_entityName);

		$options = [];
		foreach ($entities as $entity) {
			if (is_object($entity)) {
				$entity = $entity->toArray();
			}
			$options[$entity["id"]] = $entity["internalLabel"] ?: $entity["heading"];
		}
		
		$entitiesF->entityIds = new Zend_Form_Element_Multiselect("entityIds");
		$entitiesF->entityIds->setMultiOptions($options)
							->setAttribs(["class" => "multiselect-sortable"]);
		
		$entitiesF->addDisplayGroup(["entityIds"], "entitiesGroup");
		$entitiesF->applyDecorators();
		
		$to->addSubForm($entitiesF, 'entitiesF', null, false);
	}

	public function setDefaults($defaults)
	{
		$order = isset($defaults["entityIds"]) ? json_encode($defaults["entityIds"]) : "";
		$this->getSubForm("entitiesF", true)->entityIds->setAttrib("data-initial-order", $order);
		parent::setDefaults($defaults);
	}
}
