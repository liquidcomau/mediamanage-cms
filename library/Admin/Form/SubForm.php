<?php
class Admin_Form_SubForm extends Admin_Form
{
	/**
	 * Whether or not form elements are members of an array
	 * @var bool
	 */
	protected $_isArray = true;
	
	/**
	 * Load the default decorators
	 *
	 * @return Zend_Form_SubForm
	 */
	public function loadDefaultDecorators()
	{
		if ($this->loadDefaultDecoratorsIsDisabled()) {
			return $this;
		}
	
		$decorators = $this->getDecorators();
		if (empty($decorators)) {
			$this->addDecorator('FormElements')
			->addDecorator('HtmlTag', array('tag' => 'dl'))
			->addDecorator('Fieldset')
			->addDecorator('DtDdWrapper');
		}
		return $this;
	}
	
	public function applyFormDecorators()
	{
		// doesn't use form tags
		
		$this->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'section', 'class' => 'panel panel-default'))
		));
	}
}