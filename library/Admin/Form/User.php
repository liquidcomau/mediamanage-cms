<?php
class Admin_Form_User extends Admin_Form_Resource 
{
	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array('/js/admin/ResourceForms/Resource.js'),
				'data-resource-alias' => 'user',
				'data-resource-type' => 'user',
				'data-resource-endpoint' => '/ajax/api/users'			
		));
		
		// Content table
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		$this->addSubForm($contentF, 'contentF', null, false);
		
		$main = new Admin_Form_SubForm();
		$contentF->addSubForm($main, 'main', null, false);

		$main->header = new Zend_Form_Element_Note('header');
		$main->header->setValue('User Details');
		
		$main->userName = new Zend_Form_Element_Text('userName');
		$main->userName->setLabel('Username')
							->setAttribs(array('class' => 'form-control'));
		
		$main->firstName = new Zend_Form_Element_Text('firstName');
		$main->firstName->setLabel("First Name")
						->setAttribs(array('class' => 'form-control'));
		
		$main->lastName = new Zend_Form_Element_Text('lastName');
		$main->lastName->setLabel("Last Name")
						->setAttribs(array("class" => "form-control"));
		
		$main->email = new Zend_Form_Element_Text('email');
		$main->email->setLabel('Email')
					->setAttribs(array('class' => 'form-control'));
		
		$main->role = new Zend_Form_Element_Select('role');
		$main->role->setLabel('Role')
					->setAttribs(array('class' => 'form-control'))
					->setMultiOptions(array(
						'admin' => 'Admin',
						'webmaster' => 'Webmaster',
					));
		
		$main->addDisplayGroup(array('userName', 'firstName', 'lastName', 'email', 'role'), 'mainGroup');
					
		$passwordF = new Admin_Form_SubForm();
		$contentF->addSubForm($passwordF, 'passwordF', null, false);
		
		$passwordF->header = new Zend_Form_Element_Note('header');
		$passwordF->header->setValue('Edit Password');
		
		$passwordF->newPassword = new Zend_Form_Element_Password('newPassword');
		$passwordF->newPassword->setLabel('New Password')
								->setAttribs(array('class' => 'form-control'));
		
		$passwordF->verifyPassword = new Zend_Form_Element_Password('verifyPassword');
		$passwordF->verifyPassword->setLabel('Verify Password')
									->setAttribs(array('class' => 'form-control'));
		
		$passwordF->addDisplayGroup(array('newPassword', 'verifyPassword'), 'pwGroup');
		
		$main->applyDecorators();
		$passwordF->applyDecorators();
	}
}