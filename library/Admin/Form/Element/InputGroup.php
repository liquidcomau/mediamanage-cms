<?php
class Admin_Form_Element_InputGroup extends Zend_Form_Element_Text
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formInputGroup';
    
    protected $addon = null;
    
}
