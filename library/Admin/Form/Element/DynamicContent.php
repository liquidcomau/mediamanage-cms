<?php
class Admin_Form_Element_DynamicContent extends Zend_Form_Element_Xhtml
{
    /**
     * Default form view helper to use for rendering
     *
     * @var string
     */
    public $helper = 'formDynamicContent';
    
    /**
     * Ignore flag (used when retrieving values at form level)
     *
     * @var bool
     */
    protected $_ignore = true;
    
    /**
     * Validate element value (pseudo)
     *
     * There is no need to reset the value
     *
     * @param  mixed $value Is always ignored
     * @param  mixed $context Is always ignored
     * @return boolean Returns always TRUE
     */
    public function isValid($value, $context = null)
    {
        return true;
    }
}
