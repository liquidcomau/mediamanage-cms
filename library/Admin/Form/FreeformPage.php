<?php
class Admin_Form_FreeformPage extends Admin_Form_MasterAbstract {

	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array('/js/admin/jqSortable.js', '/js/admin/ResourceForms/Page.js'),
				'data-resource-alias' => 'freeform page',
				'data-resource-type' => 'freeform-page',
				'data-resource-endpoint' => '/ajax/api/freeform-pages'			
		));
		
		// Content table
		
		$mainF = new Admin_Form_SubForm();
		$mainF->setAttribs(array(
			'icon' => 'fa-home',
			'alias' => 'Main'
		));
		
		$main = Admin_Factory::create("Form_SubForm_Main");
		$mainF->addSubForm($main, 'main', null, false);
		
		$seo = new Admin_Form_SubForm_Seo();
		$mainF->addSubForm($seo, 'seo');
		
		$mainF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		$this->addSubForm($mainF, 'mainF', null, false);
		
		$this->addContentSubForm($this);
		
		$main->applyDecorators();
		$seo->applyDecorators();
		
		$settingsF = new Admin_Form_SubForm();
		$settingsF->setAttribs(array(
				'icon' => 'fa-cog',
				'alias' => 'Settings'				
		));

		$this->addSettingsSubForm($settingsF);
		
		$settingsF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$this->addSubForm($settingsF, 'settingsF', null, false);
	}
	
	protected function addContentSubForm($to)
	{
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'Content'
		));

		$contentSubForm = Admin_Form_SubForm_ContentFactory::createForType('FreeformPage');
		$contentSubForm->applyDecorators();
		
		$contentF->addSubForm($contentSubForm, 'contentSubForm', null, false);
		
		$to->addSubForm($contentF, 'contentF', null, false);
	}
	
	protected function addSettingsSubForm($to)
	{
		$pageSettings = new Admin_Form_SubForm_PageSettings();
		$pageSettings->applyDecorators();

		$to->addSubForm($pageSettings, 'pageSettings', null, false);
	}
}
