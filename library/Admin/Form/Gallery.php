<?php
class Admin_Form_Gallery extends Admin_Form_MasterAbstract 
{
	protected $_resourceEndpoint = "galleries";

	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
						'/theme/js/tinymce/tinymce.min.js',
						'/js/admin/parsley.js',
						'/js/admin/manageParsley.js',
						'/js/admin/jqSortable.js',
						'/js/admin/ResourceForm.js',
						'/js/admin/ResourceForms/Gallery.js'
				),
				'data-resource-alias' => 'gallery',
				'data-resource-type' => 'gallery',
				'data-resource-endpoint' => '/ajax/api/galleries'			
		));
		
		$this->addMainForm($this);
			
		$this->addImagesForm($this);
		
		$settingsF = new Admin_Form_SubForm();
		$settingsF->setAttribs(array(
				'icon' => 'fa-cog',
				'alias' => 'Settings'				
		));

		$this->addSettingsForm($settingsF);
		
		$settingsF->setDecorators(array('FormElements', array('HtmlTag', array('tag' => 'div'))));
		
		$this->addSubForm($settingsF, 'settingsF', null, false);
	}
	
	public function addMainForm($to) 
	{
		$mainF = new Admin_Form_SubForm();
		$mainF->setAttribs(array(
				'icon' => 'fa-home',
				'alias' => 'Main'
		));
		
		$main = Admin_Factory::create("Form_SubForm_Main");

		$main->identifier = new Zend_Form_Element_Text("identifier");
		$main->identifier->setLabel("Identifier")
							->setAttribs(["class" => "form-control"])
							->setDescription("Used for referencing gallery in shortcodes");
		
		$main->publishedDate = new Admin_Form_Element_InputGroup("publishedDate");
		$main->publishedDate->setLabel("Published Date")
							->setAttribs(["class" => "form-control",
											"addon" => ["markup" => '<i class="glyphicon glyphicon-calendar data-time-icon="icon-time" data-date-icon="icon-calendar"></i>'],
											"group" => ["id" => "publishedDateGroup", "class" => "input-append datetimepicker", "data-date-format" => "DD/MM/YYYY hh:mm A"]	
											]);

		$dispEls = $main->getDisplayGroup("mainGroup")->getElements();
		$dispEls[] = "identifier";
		$dispEls[] = "publishedDate";
		$main->addDisplayGroup($dispEls, "mainGroup");

		$main->applyDecorators();
		
		$mainF->addSubForm($main, 'main', null, false);
		
		$contentF = new Admin_Form_SubForm_MainContent(null, ["description"]);
		$contentF->applyDecorators();
		
		$mainF->addSubForm($contentF, "contentF", null, false);
		
		$modelClass = Admin_Factory::getClass("Model_Gallery");
		if ($modelClass::hasCategories()) {
			$categories = $modelClass::categories();
			$this->addCategoriesSubForm($mainF);
		}
		
		$to->addSubForm($mainF, 'mainF', null, false);
	}
	
	public function addImagesForm($to)
	{
		$imagesF = new Admin_Form_SubForm();
		$imagesF->setAttribs(array(
			'icon' => 'fa-picture-o',
			'alias' => 'Images'
		));

		$imagesSF = Admin_Factory::create("Form_SubForm_Images");
		$imagesSF->setImageTemplatePartial($this->_imageItemPartial ?: 'partials/form/imageItem.phtml');

		$imagesSF->applyDecorators();
		
		$imagesF->addSubForm($imagesSF, "imagesSF", null, false);
		
		$to->addSubForm($imagesF, 'imagesF', null, false);
	}
	
	public function addSettingsForm($to) 
	{
		$pageSettings = Admin_Factory::create("Form_SubForm_PageSettings");
		$pageSettings->applyDecorators();

		$to->addSubForm($pageSettings, 'pageSettings', null, false);
	}
	
	/**
	 * Convert published date to appropriate format.
	 * @param type $defaults
	 */
	public function populate($defaults)
	{
		if (isset($defaults["publishedDate"])) {
			$defaults["publishedDate"] = date("d/m/Y h:i A", strtotime($defaults["publishedDate"]));
		}
		parent::populate($defaults);
	}
	
	protected function addCategoriesSubForm($parent)
	{
		$catF = new Admin_Form_SubForm();

		$catF->header = new Zend_Form_Element_Note("header");
		$catF->header->setValue("Categories");

		$catClass = Admin_Factory::getClass("Model_Category");
		$categories = $catClass::getAll();

		$options = [];
		foreach ($categories as $category) {
			$options[$category->getId()] = $category->getLabel();
		}

		$catF->categoryIds = new Zend_Form_Element_Multiselect("categoryIds");
		$catF->categoryIds->setMultiOptions($options)
							->setAttribs(["class" => "form-control"]);

		$catF->addDisplayGroup(["categoryIds"], "categoriesGroup");
		$catF->applyDecorators();

		$parent->addSubForm($catF, 'catF', null, false);
	}
}
