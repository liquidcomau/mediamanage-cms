<?php
class Admin_Form_Menu extends Admin_Form_MasterAbstract {

	protected $_resourceEndpoint = 'menus';
	
	public function init() {
		parent::init();
		
		$this->setAttribs(array(
			'role' => 'form', 
			'class' => 'resourceForm',
			'scripts' => array('/js/admin/ResourceForms/menu.js')
		));
		
		// Content tab
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		$main = new Admin_Form_SubForm();
		
		$main->header = new Zend_Form_Element_Note('header');
		$main->header->setValue('Main');
		
		$main->heading = new Zend_Form_Element_Text('heading');
		$main->heading->setLabel('Name')
						->setAttribs(array(
							'class' => 'form-control'
						));
		
		$main->addDisplayGroup(array('heading'), 'menuDisplayGroup');
						
		$contentF->addSubForm($main, 'main', null, false);

		$main->applyDecorators();
		
		$this->addSubForm($contentF, 'contentF', null, false);
	}
}