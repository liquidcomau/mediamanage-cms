<?php
class Admin_Form_Slideshow extends Admin_Form_MasterAbstract {

	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array('/js/admin/ResourceForms/Slideshow.js'),
				'data-resource-alias' => 'slideshow',
				'data-resource-type' => 'slideshow',
				'data-resource-endpoint' => '/ajax/api/slideshows'			
		));
		
		$contentF = new Admin_Form_SubForm();
		$contentF->setAttribs(array(
				'icon' => 'fa-file-text',
				'alias' => 'content'
		));
			
		$slides = new Zend_Form_SubForm();
		//$slides->setElementsBelongTo($this->_masterName);
		
		$slides->header = new Zend_Form_Element_Note('header');
		$slides->header->setValue('<h4 class="slideshowName">Slides</h4>');
		
		$slides->slides = new Zend_Form_Element_Note('slides');
		$slides->slides->setValue('<div class="slidesContainer">No slides.</div><div class="pull-right"><button type="button" class="btn btn-primary addSlideBtn">Add Slide</button></div><em>Slides can be re-ordered by clicking & dragging</em>');
		
		$slides->addDisplayGroup(array('slides'), 'slidesGroup');
		
		$slides->setElementDecorators(array(
			'ViewHelper',
			'Description',
		));
		
		$contentF->setDecorators(array(
				'FormElements'
		));
		
		$contentF->addSubForm($slides, 'slides');
	
		$contentF->setDecorators(array(
				'FormElements'
		));
		
		$this->addSubForm($contentF, 'contentF');
		
		$this->name = new Zend_Form_Element_Hidden('name');
	}
	
	public function setDefaults($defaults)
	{
		parent::setDefaults($defaults);
		
		$slides = $this->getSubForm('slides', true);
		$slides->header->setValue('<h4 class="slideshowName"> ' . $this->name->getValue() . '</h4>');
	}
}