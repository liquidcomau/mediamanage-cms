<?php
class Admin_Form_Agent extends Admin_Form_Resource {
	protected $_resourceEndpoint = 'agents';
	
	public function init() {
		$this->setAttribs(array(
				'role' => 'form', 
				'class' => 'resourceForm',
				'scripts' => array(
						'/theme/js/tinymce/tinymce.min.js',
						'/js/admin/parsley.js',
						'/js/admin/manageParsley.js',
						'/js/admin/ResourceForm.js',
						'/js/admin/ResourceForms/Page.js'
				),
				'data-resource-alias' => 'agent',
				'data-resource-type' => 'agent',
				'data-resource-endpoint' => '/ajax/api/agents'			
		));
		
		// Content table
		
		$mainF = new Admin_Form_SubForm();
		$mainF->setAttribs(array(
			'icon' => 'fa-file-text',
			'alias' => 'content'
		));
		
		$agentF = new Admin_Form_SubForm();
		
		$agentF->header = new Zend_Form_Element_Note('header');
		$agentF->header->setValue('Agent Details');
		
		$agentF->name = new Zend_Form_Element_Text('name');
		$agentF->name->setLabel("Agent Name")
						->setAttribs(["class" => "form-control"]);

		$agencyClass = Admin_Factory::getClass("Model_Agency");
		$options = array_column($agencyClass::getAll(null, Admin_Constants::TYPE_ARRAY), "name",  "id");

		$agentF->agencyId = new Zend_Form_Element_Select("agencyId");
		$agentF->agencyId->setLabel("Agency")
						->setMultiOptions($options)
						->setAttribs(["class" => "form-control"]);

		$agentF->email = new Zend_Form_Element_Text('email');
		$agentF->email->setLabel("Email")
						->setAttribs(["class" => "form-control"]);

		$agentF->phone = new Zend_Form_Element_Text('phone');
		$agentF->phone->setLabel("Phone")
						->setAttribs(["class" => "form-control"]);

		$browseBtn = '<button class="btn btn-default thumbnailBrowse moxmanBrowse" type="button" data-for=".profilePicInput">Browse</button>';
		
		$agentF->profilePicture = new Admin_Form_Element_InputGroup('profilePicture');
		$agentF->profilePicture->setLabel('Profile Picture')
						->setAttribs(array(
								'class' => 'form-control profilePicInput',
								'addon' => 	array(
										'type' => 'button',
										'markup' => $browseBtn,
										'position' => 'append'
								)
						));

		$agentF->addDisplayGroup(["name", "agencyId", "email", "phone", "profilePicture"], "agentGroup");
		$agentF->applyDecorators();

		$mainF->addSubForm($agentF, "agentF", null, false);
		$this->addSubForm($mainF, "mainF", null, false);
	}
}
