<?php
class Admin_Navigation
{
	protected function subNavArray($subpages) {
		$subNav = array();
		if ($subpages) {
			foreach ($subpages as $subpage) {
				$url = "/admin/" . $subpage['url'];
				$active = (strstr($_SERVER['REQUEST_URI'], $url)) ? true : false;
				$subNav[] = array(
					"label" => $subpage['module'],
	    	    	"uri" => $url,
					"active" => $active,
    				"type" => "uri",
    				"resource" => $subpage['accessLevel']
				);
			}
		}
		return $subNav;
	}
	
    /**
     * Return the navigation array
     *
     * @return array
     */
    public static function jsonNavArray()
    {
        $navArray = array();
        $menuItems = Admin_Model_Menu::getAdminMenu();
        foreach ($menuItems as $menuItem) {
        	$url = "/admin/" . $menuItem['url'];
        	$active = (strstr($_SERVER['REQUEST_URI'], $url) || (($url == "/index") && ($_SERVER['REQUEST_URI'] == "/"))) ? true : false;
        	$navArray[$menuItem['url']] = array(
            	"label" => $menuItem['module'],
            	"uri" => $url,
        		"active" => $active,
        		"type" => "uri",
        		"icon" => $menuItem['icon'],
        		"pages" => self::subNavArray($menuItem['children']),
        		"resource" => $menuItem['accessLevel']
        	);
        }
        return json_encode($navArray);
    }
}