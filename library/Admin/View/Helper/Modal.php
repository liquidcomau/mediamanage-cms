<?php
class Admin_View_Helper_Modal extends Zend_View_Helper_Placeholder_Container
{    
    public function modal()
    {
    	$modal = '<div class="modal fade" id="modal-form">
    			  <div class="modal-dialog">
    			    <div class="modal-content">
    			  		<form id="addMember" data-validate="parsley">
    				    	<div class="modal-header">
    					        <button type="button" class="close" data-dismiss="modal">&times;</button>
    					        <h4 class="modal-title">Add Member</h4>
    				        </div>
    				    	<div class="modal-body">
    				        	<div class="form-group pull-in clearfix">
    			                	<div class="col-sm-6">
    			                    	<label>First name</label>
    			                        <input id="firstName" type="text" class="form-control" placeholder="First Name" data-required="true">
    			                    </div>
    			                    <div class="col-sm-6">
    			                    	<label>Last Name</label>
    			                        <input id="lastName" type="test" class="form-control" placeholder="Last Name" data-required="true">
    			                    </div>
    			                </div>
    			                <div class="form-group">
    			                	<label>Screen Name</label>
    			                    <input id="screenName" type="text" data-type="alphanum"  data-required="true" class="form-control" placeholder="Screen Name">
    			                </div>
    			                <div class="form-group">
    			                	<label>Email Address</label>
    			                    <input id="emailAddress" type="email" data-type="email"  data-required="true" class="form-control" placeholder="Email">
    			                </div>
    			                <div class="form-group pull-in clearfix">
    			                	<div class="col-sm-6">
    			                		<label>Gender</label>
    			                		    
    			                	</div>
    			                	<div class="col-sm-6">
    				                	<label>Phone</label>
    				                    <input id="phone" type="text" data-type="number"  data-required="true" class="form-control" placeholder="Phone">
    				                </div>
    			                </div>
    					    </div>
    					    <div class="modal-footer">
    					        <button type="submit" class="btn btn-success btn-s-xs">Add Member</button>
    					    </div>
    				    </form>
    			    </div><!-- /.modal-content -->
    			</div><!-- /.modal-dialog -->
    	        </div>';
    	
    	return $modal;
    }

    public function __toString()
    {        
        return $this;
    }
    
    public function create() {
    	
    }
}