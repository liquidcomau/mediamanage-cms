<?php

class Admin_View_Helper_Email
{
    /**
     * Email content.
     * 
     * @var string
     */
    protected $_email;
	
    /**
     * Email header.
     * 
     * @var string
     */
    protected $_header;

    /**
     * Email body.
     * 
     * @var string
     */
    protected $_body;
    
    /**
     * Email footer.
     * 
     * @var string
     */
    protected $_footer;

    /**
     * Email footer for password recovery.
     * 
     * @var string
     */
    protected $_cmsfooter;
    
    /**
     * Config link
     * 
     * @var string
     */
    protected $_config;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
    	
    	$this->_config = Zend_Registry::get('config'); 
    	
    	$this->_header = '<div>
							<table width="100%" cellpadding="10" cellspacing="0">
								<tr>
									<td valign="top" align="left">
									<table cellspacing="0" cellpadding="0" width="780" style="margin-top: 0px">
										<tr>
											<td>';	

    	
    	 
          $this->_footer =             	'<tr>
											<td valign="top" colspan="2" align="left" style="background-color: #bfbfbf; border-top: 1px solid #b5ad8e; padding: 14px 20px">
											<div
												style="font-size: 12px; color: #4f4f4f; line-height: 100%; font-family: Lucida Sans, Lucida; line-height: 20px;">
												Email courtesy <a href="http://www.interacthub.com.au">http://www.interacthbub.com.au</a>.<br />
											</div>
											</td>
										</tr>
									</table>
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					<img height="30" width="1"></div>';
    }
    
    /**
     * Calls the navigatio helper constructor.
     *
     * @return Retail_View_Helper_Navigation $this The current object.
     */
    public function email()
    {
        return $this;
    }
        
    /**
     * Return the current navigation for a particular page.
     *
     * @return string $xhtml The xhtml representation of this object.
     */
    public function __toString()
    {        
        return $this->_email;
    }

    /*public function supportRequest($firstname, $surname,$address,$suburb,$state,$postcode,$telephone, $email, $type,$hear,$latest, $message)
    {    	    	
    	
    	$this->_body = '<table width="780" cellpadding="30" cellspacing="0">
						<tr>
							<td valign="top" align="left" style="font-size: 12px; color: #333333; line-height: 150%; font-family: Lucida Sans, Lucida; background-color: #FFFFFF; padding: 10px 20px 30px 0px;">
								<br><b>Name:</b> '. $firstname .' ' . $surname . '<br>
								<br><b>Address:</b> '. $address .'<br>
								<br><b>Suburb:</b> '. $suburb .'<br>
								<br><b>State:</b> '. $state .'<br>
								<br><b>Postcode:</b> '. $postcode .'<br>
								<br><b>Telephone:</b> '. $telephone .'<br>
								<br><b>Email:</b> '. $email .'<br>
								<br><b>I am a...</b> '. $type .'<br>
								<br><b>Where did you hear about MAB Funds Management? </b> '. $hear .'<br>
								<br><b>I would like to receive the latest offer document:</b> '. $latest .'<br>
								<br><b>Message:</b> '. $message .'<br>
								<br>
							</td>';	
		$this->_body .=	'</tr>';

    	$this->_email  = $this->_header;
    	$this->_email .= $this->_body;
    	$this->_email .= $this->_footer;
    	
    	return $this->_email;
    	
    }
    */

    /*public function passwordRecovery($user, $key)
    {    	    	    	
    	$this->_body = '<table width="590" cellpadding="30" cellspacing="0">
						<tr>
							<td valign="top" align="left" style="font-size: 12px; color: #333333; line-height: 150%; font-family: Lucida Sans, Lucida; background-color: #FFFFFF; padding: 10px 20px 30px 20px;">
								Dear '. $user->name .',<br /><br />
								This is a system generated email - please do not reply.<br /><br />
								***********************************************************************<br />
								Account Holder\'s Name: ' . $user->name . '<br />
								Account Email: ' . $user->email . '<br />
								Link to Update Password: <a href="http://go.oliverhue.com.au/admin/login/confirm/email/' . $user->name . '/key/' . $key . '">http://go.oliverhue.com.au/admin/login/confirm/email/' . $user->email . '/key/' . $key . '</a>
								***********************************************************************
							</td>';	
		$this->_body .=	'</tr>';

    	$this->_email  = $this->_header;
    	$this->_email .= $this->_body;
    	$this->_email .= $this->_cmsfooter;
    	
    	return $this->_email;    	
    }  
    */
    
   

	public function verificationEmail($firstName, $email, $verificationKey)
    {    	    
    		    	
    	$this->_body = '<table width="590" cellpadding="30" cellspacing="0">
							<tr>
								<td valign="top" align="left" style="font-size: 12px; color: #333333; line-height: 150%; font-family: Lucida Sans, Lucida; background-color: #FFFFFF; padding: 0px 0px 30px 0px;">
									Dear '. $firstName .',<br /><br />
									Welcome to Interact Hub.<br /><br />
									
									Please confirm your account:  <br />
									<strong><a href="' . $this->_config->settings->websitefullurl  . '/member/confirm/email/' . $email. '/key/' .  $verificationKey . '">Confirm your account now</a></strong><br /><br />
									
									Or click the link below:<br />
									<a href="' . $this->_config->settings->websitefullurl  . '/member/confirm/email/' . $email . '/key/' .  $verificationKey . '">' . $this->_config->settings->websitefullurl  . '/member/confirm/email/' . $email . '/key/' .  $verificationKey . '</a><br /><br />
									
									If you have already confirmed your password, please <a href="' . $this->_config->settings->websitefullurl  . '">CLICK HERE to login</a> or go to <a href="' . $this->_config->settings->websitefullurl  . '">' . $this->_config->settings->websitefullurl  . '</a><br /><br />
									
		                			Your email address: ' . $email . '<br /><br />
		                			
									Should you experience any difficulties please contact Support for assistance.<br />
		                			Email: <a href="mailto:support@interacthub.com.au">support@interacthub.com.au</a> <br /><br />
		                			
									Kind regards,<br /><br />
									
									Interact Hub.<br />
								</td>';	
		

    	$this->_email  = $this->_header;
    	$this->_email .= $this->_body;
    	$this->_email .= $this->_footer;
    	
    	return $this->_email;    	
    }
    
    
    
}