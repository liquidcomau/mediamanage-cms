<?php
class Admin_View_Helper_BodyClass
{
	protected $_classes = array();
	
	public function appendClass($classes)
	{
		if (is_string($classes)) {
			$classes = [$classes];
		}
		
		$this->_classes = array_merge($this->_classes, $classes);
	}
	
	public function bodyClass($wrap = false)
	{
		$str = implode(" ", $this->_classes);
		return $wrap ? 'class="' . $str . '"' : $str;
	}
}
