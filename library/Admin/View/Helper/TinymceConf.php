<?php
class Admin_View_Helper_TinymceConf extends Zend_View_Helper_Abstract
{
	public function tinymceConf() 
	{
		$f = APPLICATION_PATH . "/config/tinymce.json";

		if (!file_exists($f)) {
			return "{}";
		}
		
		return file_get_contents($f);
	}
}