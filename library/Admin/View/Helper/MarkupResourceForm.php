<?php
class Admin_View_Helper_MarkupResourceForm extends Zend_View_Helper_FormElement
{
    public function markupResourceForm($form)
    {
		/*
    	$out = '<section id="formContainer">';
    	
    	$out .= '<header class="header b-b b-light bg-white formContainerHeader">' .
    				'<div class="btn-group pull-right saveBtns">' .
    				'<button class="btn btn-primary">Save</button>' .
    			'</div>';
    	
    	$out .= '<h3 class="formTypeHeading">' . ucwords($form->getAttrib('data-resource-alias')) . '</h3></header>';
    		
    	$out .=	'<section class="scrollable formContainerBody padder">';
    	
    	$attribs = $form->getAttribs();
    	$attribKeys = array_filter(array_keys($form->getAttribs()), function($key) {
    		return preg_match('/^data\-/', $key);
    	});
    			
    	$attribs = array_reduce($attribKeys, function($out, $key) use ($attribs) {
    		return $out . ' ' . $key . '="' . $attribs[$key] . '"';
    	}, '');			
    		
    	$out .= '<form class="panel panel-default resourceForm"' . $attribs . '>';

    	$out .= '<header class="panel-heading bg-light resourceFormHeader">' .
    				'<ul class="nav nav-tabs pull-right">';

    	$out .= array_reduce($form->getSubForms(), function($tab, $subForm) use ($form) {
    		$icon = $subForm->getAttrib('icon') ? '<i class="fa ' . $subForm->getAttrib('icon') . '"></i>' : null;
    		$activeClass = array_keys($form->getSubForms())[0] === $subForm->getAttrib('name') ? 'active' : null;
    		
    		$label = $subForm->getAttrib('alias') ? $subForm->getAttrib('alias') : $subForm->getAttrib('name');
    								
    		return $tab . '<li class="' . $activeClass . '"><a href="#resource-form-' . $subForm->getAttrib('name') . '" data-toggle="tab">' . $icon . ' ' . ucFirst($label) . '</a></li>';
    	}, '');
    					
    	$out .= '</ul>' .
    			'<span class="clearfix"></span>' .
    			'</header>';

    	// end tabs area
    	
    	$out .= '<div class="panel-body">' .
    				'<div class="tab-content">';
    	
    	$out .= array_reduce($form->getSubForms(), function($tab, $subForm) use ($form) {
    		$activeClass = array_keys($form->getSubForms())[0] === $subForm->getAttrib('name') ? ' active' : null;
    		return $tab . '<div class="tab-pane' . $activeClass . '" id="resource-form-' . $subForm->getAttrib('name') . '">' . $subForm->render() . '</div>';
    	}, '');
    					
    	$out .= '</div>';
    	
    	$out .= $form->getMeta();
    				
    	$out .= '</div>' .
    			'</form>' .
    			'</section>' .
    			'</section>';
    	
    	return $out;
		*/
		
		return '<section id="formContainer">' . $this->view->partial("partials/form.phtml", "admin", ["form" => $form]) . '</section>';
    }
}
