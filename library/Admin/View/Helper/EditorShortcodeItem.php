<?php
class Admin_View_Helper_EditorShortcodeItem
{
	public function editorShortcodeItem($shortcode_item)
	{
		$mu = "";
		
		if (!is_array($shortcode_item->getValue())) {
			$mu .= $this->outputListItem($shortcode_item);
		} else {
			$mu .= $this->outputList($shortcode_item);
		}
		
		echo $mu;
	}
	
	public function outputList($shortcode_item)
	{
		$mu  = '<li><a href="#" data-editor-bc="' . $shortcode_item->getLabel() . '">' . $shortcode_item->getLabel() . '</a><ul class="dl-submenu">';
			
		foreach ($shortcode_item->getValue() as $child_item) {
			$mu .= $this->outputListItem($child_item);
		}
	
		$mu .=	'</ul></li>';
		
		return $mu;
	}
	
	public function outputListItem($shortcode_item)
	{
		return '<li><a href="#" data-identifier="' . $shortcode_item->getValue() . '">' . $shortcode_item->getLabel() . '</a></li>';
	}
}
