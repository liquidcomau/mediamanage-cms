<?php

class Admin_View_Helper_Validation extends Zend_View_Helper_Placeholder_Container
{    
    public function validation()
    {
        return $this;
    }

    public function __toString()
    {        
        return $this;
    }

	public function validationErrors($errorMessages, $style = "html") {					
		if ($style == "html") {
			// html response for roar alerts
			$newLine = "<br />";
			$strongOpen = "<strong>";
			$strongClose = "</strong>";
		} else {
			// plain text response for js alerts
			$newLine = "\n";
			$strongOpen = "";
			$strongClose = ": ";
		}
		$errorString = '';
		foreach ($errorMessages as $fKey => $field) {
			foreach ($field as $mKey => $message) {
				$errorString .= $strongOpen . ucwords($fKey) . $strongClose . $message . $newLine;
			}
		}
		return "Validation errors detected::" . $errorString;	
	}
}