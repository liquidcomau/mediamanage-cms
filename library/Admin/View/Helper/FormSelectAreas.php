<?php
/**
 * Same as the zend framework formSelect helper, but can use multiOptions to specify areas attribute
 * so JS knows how to generate WYSIWYG areas
 * 
 * multioptions takes value, label, and then everything else gets appended as an attr
 */
class Admin_View_Helper_FormSelectAreas extends Zend_View_Helper_FormElement
{
    public function formSelectAreas($name, $value = null, $attribs = null,
        $options = null, $listsep = "<br />\n")
    {
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, id, value, attribs, options, listsep, disable

        // force $value to array so we can compare multiple values to multiple
        // options; also ensure it's a string for comparison purposes.
        $value = array_map('strval', (array) $value);

        // check if element may have multiple values
        $multiple = '';

        if (substr($name, -2) == '[]') {
            // multiple implied by the name
            $multiple = ' multiple="multiple"';
        }

        if (isset($attribs['multiple'])) {
            // Attribute set
            if ($attribs['multiple']) {
                // True attribute; set multiple attribute
                $multiple = ' multiple="multiple"';

                // Make sure name indicates multiple values are allowed
                if (!empty($multiple) && (substr($name, -2) != '[]')) {
                    $name .= '[]';
                }
            } else {
                // False attribute; ensure attribute not set
                $multiple = '';
            }
            unset($attribs['multiple']);
        }

        // handle the options classes
        $optionClasses = array();
        if (isset($attribs['optionClasses'])) {
            $optionClasses = $attribs['optionClasses'];
            unset($attribs['optionClasses']);
        }
        
        // now start building the XHTML.
        $disabled = '';
        if (true === $disable) {
            $disabled = ' disabled="disabled"';
        }

        // Build the surrounding select element first.
        $xhtml = '<select'
                . ' name="' . $this->view->escape($name) . '"'
                . ' id="' . $this->view->escape($id) . '"'
                . $multiple
                . $disabled
                . $this->_htmlAttribs($attribs)
                . ">\n    ";

        // build the list of options
        $list       = array();
        $translator = $this->getTranslator();

        foreach ((array) $options as $opt) {
        	
        	// Got rid of the option-group stuff, that wont work now as the original helper
        	
        	$list[] = $this->_build($opt['value'], $opt['label'], $opt['areas'], $value, $disable, $optionClasses);
        }

        // add the options to the xhtml and close the select
        $xhtml .= implode("\n    ", $list) . "\n</select>";

        return $xhtml;
    }

    /**
     * Attrs contains additional information: name of field/json key, label alias
     * 
     * @param unknown $value
     * @param unknown $label
     * @param unknown $attrs
     * @param unknown $selected
     * @param unknown $disable
     * @param unknown $optionClasses
     * @return string
     */
    protected function _build($value, $label, $areas, $selected, $disable, $optionClasses = array())
    {
        if (is_bool($disable)) {
            $disable = array();
        }

        $class = null;
        if (array_key_exists($value, $optionClasses)) {
            $class = $optionClasses[$value];
        }


        $opt = '<option'
             . ' value="' . $this->view->escape($value) . '"';

        $opt .= ' data-areas="' . htmlentities($areas) . '"';
        
             if ($class) {
             $opt .= ' class="' . $class . '"';
         }
        // selected?
        if (in_array((string) $value, $selected)) {
            $opt .= ' selected="selected"';
        }

        // disabled?
        if (in_array($value, $disable)) {
            $opt .= ' disabled="disabled"';
        }

        $opt .= '>' . $this->view->escape($label) . "</option>";

        return $opt;
    }

}
