<?php
class Admin_View_Helper_FormInputGroup extends Zend_View_Helper_FormElement
{
    /**
     * Generates a 'text' element.
     *
     * @access public
     *
     * @param string|array $name If a string, the element name.  If an
     * array, all other parameters are ignored, and the array elements
     * are used in place of added parameters.
     *
     * @param mixed $value The element value.
     *
     * @param array $attribs Attributes for the element tag.
     *
     * @return string The element XHTML.
     */
    public function formInputGroup($name, $value = null, $attribs = null, $addon = null)
    {
    	$info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

        if (isset($attribs['addon'])) {
			$addon = $attribs['addon'];
			$position = $addon["position"] ?: "append";
			unset($attribs['addon']);
			

			$addClass = $addon['type'] === 'button' ? 'input-group-btn' : 'input-group-addon add-on';
			$addon = '<span class="' . $addClass . '">' . $addon['markup'] . '</span>';
        } else {
        	$addon = null;
        }
        
        // build the element
        $disabled = '';
        if ($disable) {
            // disabled
            $disabled = ' disabled="disabled"';
        }

		$groupAttribs = " ";
		if (isset($attribs["group"])) {
			if (isset($attribs["group"]["class"])) {
				$attribs["group"]["class"] = "input-group " . $attribs["group"]["class"];
			} else {
				$attribs["group"]["class"] = "input-group";
			}
			foreach ($attribs["group"] as $key => $val) {
				$groupAttribs .= ' ' . $key  . '="' . $val .'"';
				unset($attribs["group"]);
			}
		} else {
			$groupAttribs = ' class="input-group"';
		}

        $xhtml = '<input type="text"'
                . ' name="' . $this->view->escape($name) . '"'
                . ' id="' . $this->view->escape($id) . '"'
                . ' value="' . $this->view->escape($value) . '"'
                . $disabled
                . $this->_htmlAttribs($attribs)
                . $this->getClosingBracket();

        if (isset($position) && $position === 'prepend') {
        	$xhtml = '<div' . $groupAttribs . '>' . $addon . $xhtml . '</div>';
        } else {
        	$xhtml = '<div' . $groupAttribs . '>' . $xhtml . $addon . '</div>';
        }
        
        return $xhtml;
    }
}
