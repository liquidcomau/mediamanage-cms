<?php
class Admin_View_Helper_FormDynamicContent extends Zend_View_Helper_FormElement
{
    /**
    * Helper to show a "note" based on a hidden value.
     *
     * @access public
     *
     * @param string|array $name If a string, the element name.  If an
     * array, all other parameters are ignored, and the array elements
     * are extracted in place of added parameters.
     *
     * @param array $value The note to display.  HTML is *not* escaped; the
     * note is displayed as-is.
     *
     * @return string The element XHTML.
     */
    public function formDynamicContent($name, $value = null, $attribs = null)
    {
        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

		$view = Zend_Layout::getMVCInstance()->getView();
		return $view->partial('partials/form/dynamicContentArea.phtml', ['id' => $info['id'], 'attribs' => $attribs, 'value' => $value]);
    }
}
