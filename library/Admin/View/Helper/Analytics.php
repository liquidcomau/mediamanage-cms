<?php

class Admin_View_Helper_Analytics
{
	protected $_analytics = '';
	protected $_goalTracking = '';
	protected $_gaResults;
	private $_config;
	private $_logger;
	
    public function __construct()
    {    
    	$this->_config = Zend_Registry::get('config');
    	$this->_logger = Zend_Registry::get('logger');
    	$this->_analytics = '';	
    }
    
    
    /**
     * Return the current analytics for a particular page.
     *
     * @return string $xhtml The xhtml representation of this object.
     */
    public function __toString()
    {        
        return $this;
    } 
    
    /**
     * Calls the analytics helper constructor.
     *
     * @return Retail_View_Helper_Navigation $this The current object.
     */
    public function analytics()
    {   
        return $this;
    }
    
    public function trackingCode() {
    	if ((APPLICATION_ENV == 'production' || APPLICATION_ENV == 'stage') && ($this->_config->settings->analytics == true)) {	    	
    		$analyticsDb = new Admin_DbTable_Analytics();
		    $result = $analyticsDb->fetchRow();

			if (!$result) {
				$this->_analytics = "<!-- TRACKING IS NOT ENABLED -->";
				return $this->_analytics;
			}
			
			$result = $result->toArray();
			
		    $tracking = ($result) ? $result['trackingId'] : false;
    		
		    if ($tracking) {
			    $this->_analytics = "<script type=\"text/javascript\">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-" . $tracking . "']);
					_gaq.push(['_trackPageview']);

					(function() {
					   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>";
		    }
	    }  else $this->_analytics = "<!-- TRACKING IS NOT ENABLED -->";
        return $this->_analytics;	
    }
    
    public function goalTracking($nodeID) {
    	if ((APPLICATION_ENV == 'production') && ($this->_config->settings->analytics == true)) {
    		$seoTable = new Admin_DbTable_Seo();
    		$result = $seoTable->fetchByID($nodeID);
    		if ($result->goalTracking != '') $this->_goalTracking = $result->goalTracking;
    	} else $this->_goalTracking = "<!-- GOAL TRACKING IS NOT ENABLED -->";
    	return $this->_goalTracking;
    }
    
    public function gaResults($period)
    {   
    	$config = Zend_Registry::get(Admin_Constants::CONFIG);
    	
		// gapi analytics
		switch ($period) {
			case "year"  : $startDate = date("Y-m-d",strtotime("-1 year"));
						   $endDate = date("Y-m-d",strtotime("-1 day"));
						   break;
			case "month" : $startDate = date("Y-m-d",strtotime("-1 month"));
						   $endDate = date("Y-m-d",strtotime("-1 day"));
						   break;
			case "week"  : $startDate = date("Y-m-d",strtotime("-1 week"));
						   $endDate = date("Y-m-d",strtotime("-1 day"));
						   break;
		    	 default : return false;
		}

		// cache analytics lookup
		$frontendOptions = array('lifetime' => 43200, 'automatic_serialization' => true);
		$backendOptions = array('cache_dir' => $config->resources->cachemanager->default->backend->options->cache_dir);
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);	
		$cacheID = "analytics_" . $period;
		try {		
			if (!($this->_gaResults = $cache->load($cacheID))) { //!($this->_gaResults = $cache->load($cacheID))						
				require MEDIAMANAGE_ENV . '/library/Admin/gapi.class.php';

				$username = $password = $view = '';
			    $result = Admin_Model_Analytics::getAnalytics(Admin_Constants::TYPE_ARRAY);
				
			    if ($result) {
			        $username = $result['username'];
			        $password = $result['password']; 
			        $view = $result['viewId']; 
			    }
			    
			    // test test test development
			    // ----------------------
			    
// 			    $username = 'mark@liquid.com.au';
// 			    $password = 'LiQ_547_';
// 			    $view = '65554128';
			    
			    // ----------------------
			    // test end
				
				// Set Google Analytics credentials 
				define('ga_account' , $username);
				define('ga_password', $password);
				define('ga_view_id' , $view);
						
				$ga = new gapi(ga_account,ga_password); 

				// site visits
				$dimensions = array('date');
				$metrics = array('pageviews', 'visits');
				$ga->requestReportData(ga_view_id, $dimensions, $metrics, 'date', '', $startDate, $endDate, '1', '500'); 			
				$reportData = $ga->getResults();				
				
				$gaResults['pageViews'] = $ga->getPageViews();
				$gaResults['visitTotal'] = $ga->getVisits();
				
				foreach ($reportData as $result) {
					$visits[] = $result->getVisits();
				}
				$visits = implode(',', $visits);
				
				// site page hits
				$dimensions = array('pagePath');
				$metrics    = array('pageViews');
				$ga->requestReportData(ga_view_id, $dimensions, $metrics, '-pageViews', '', $startDate, $endDate, 1, 40); 				
				$pageResults = $ga->getResults();
				$count = 0;
				$pages = array();
				foreach($pageResults as $result)
		        {
		           $pages[$count]['path'] = $result->getPagePath();
		           $pages[$count]['views'] = $result->getPageViews();
		           $count++;
		        }
		        
				// traffic sources
				$dimensions = array('source');
				$metrics    = array('visits');
				$ga->requestReportData(ga_view_id, $dimensions, $metrics, '-visits', '', $startDate, $endDate, 1, 8); 
				$gaSources = $ga->getResults();
				$total = 0;
				foreach($gaSources as $result) {
					$total = $total + $result->getVisits();
				}
				foreach($gaSources as $result) {
					$sourceData[] = array($result->getSource(), round(($result->getVisits() / $total) * 100, 2));
				}
				$sources = json_encode($sourceData);
				
				// external search keywords
				$dimensions = array('keyword');
				$metrics    = array('visits');
				$ga->requestReportData(ga_view_id, $dimensions, $metrics, '-visits', '', $startDate, $endDate, 1, 20); 
				$keywordResults = $ga->getResults();
				
				$count = 0;
				$keywords = array();
				foreach($keywordResults as $result) {
		            if($result->getKeyword() != '(not set)') {	
			            $keywords[$count]['keyword'] = $result->getKeyword();
				        $keywords[$count]['visits'] = $result->getVisits();
				        $count++;
		            }
		        }
	
				$gaResults['visits'] = $visits;
				$gaResults['pages'] = $pages;
				$gaResults['sources'] = $sources;
				$gaResults['keywords'] = $keywords;								
							
				$cache->save($gaResults, $cacheID);	
				$this->_gaResults = $gaResults;
			}		
		} catch (Exception $e) {
			$this->_logger->info(print_r($e, true));
			$this->_gaResults = false;
		}
		//Zend_Debug::dump($this->_gaResults); die;
		return $this->_gaResults;
    }
}
