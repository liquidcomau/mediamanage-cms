<?php
class Admin_View_Helper_AutoVer
{		
	private $_content = '';
	
    public function __construct()
    {        	
    }
    
    public function autoVer($url)
    {	
        /*$blockTable = new Admin_DbTable_Block();
    	$this->_content = $blockTable->getContent($block)->content;
    	if ($this->_content == "") $this->_content = "<!-- \"" . $block . "\" block/tab not found -->";
    	return $this;*/
    	$path = pathinfo($url);
    	$ver = '.'.filemtime($_SERVER['DOCUMENT_ROOT'].$url).'.';
    	$this->_content =  $path['dirname'].'/'.str_replace('.', $ver, $path['basename']);
    	return $this->_content;   	    
    }

    public function __toString()
    {        
    	return $this->_content;
    }
}