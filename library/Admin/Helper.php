<?php
class Admin_Helper
{
	/**
	 * Converts a MYSQL timestamp (yyyy-mm-dd hh:mm:ss)
	 * to (hh:mm:ss dd/mm/yyyy) format.
	 *
	 * Totally forgot I could just use the date function.
	 *
	 * @param (string) $thisProp. A MYSQL timestamp.
	 * @param (bool) $includeSeconds
	 */
	public static function modDate($timestamp, $includeSeconds = false) {
		$rawDate = array_shift(explode(' ', $timestamp));
		$rawTime = array_pop(explode(' ', $timestamp));
	
		$date = implode('/', array_reverse(explode('-', $rawDate)));
	
		if (!$includeSeconds) {
			$time = explode(':', $rawTime);
			array_pop($time);
			$time = implode(':', $time);
		} else {
			$time = $rawTime;
		}
	
		return $time . ' ' . $date;
	}
	
}