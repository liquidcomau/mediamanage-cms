<?php
class Admin_DbTable_Collection extends Admin_DbTable
{
	protected $_name = "collection";
	
	public function fetchById($id)
	{
		$select = $this->select();
    	$select->setIntegrityCheck(false)
    			->from("collection as c")
				->join("object_type as ot", "ot.id = c.object_type", ["objectName"])
				->where("c.id = ?", $id);
    	
    	return $this->fetchRow($select);
	}
	
	public function fetchByIdentifier($identifier)
	{
		$select = $this->select();
    	$select->setIntegrityCheck(false)
    			->from("collection as c")
				->join("object_type as ot", "ot.id = c.object_type", ["objectName"])
				->where("c.identifier = ?", $identifier);
    	
    	return $this->fetchRow($select);
	}

	public function fetch($clauses)
	{
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from("collection as c")
				->join("object_type as ot", "ot.id = c.object_type", ["objectName"])
				->order("heading ASC");
		
		$select = $this->appendDbClauses($select, $clauses);
    	
    	return $this->fetchAll($select);
	}
	
	public function deleteById($id)
	{
		return $this->delete($this->getAdapter()->quoteInto("id = ?", $id));
	}
}
