<?php
class Admin_DbTable_MediaCollection extends Admin_DbTable
{
	protected $_name = "media_collection";
	protected $_mediaCollectionType = null;
	
	public function __construct($mediaCollectionType, $config, $definition)
	{
		if (!$mediaCollectionType) {
			throw new Exception("Must provide media collection type as first param in constructor");
		}
		
		parent::__construct($config, $definition);
		$this->setMediaCollectionType($mediaCollectionType);
	}
	
	public function setMediaCollectionType($type)
	{
		$this->_mediaCollectionType = $type;
		return $this;
	}
	
	public function fetchById($mediaCollectionId) {
    	$select = $this->joinSelect()
						->where("mediaCollectionId = ?", $mediaCollectionId);

    	return $this->fetchRow($select);
    }
	
	public function fetchByRelationshipId($relationshipId) {
    	$select = $this->joinSelect()
						->join("media_collection_relationship as mcr", "mcr.relationshipId = mc.relationshipId")
						->where("mcr.relationshipId = ?", $relationshipId);

		return $this->fetchRow($select);
    }
	
    public function fetch($clauses) {
    	$select = $this->joinSelect();
    	
    	$select = $this->appendDbClauses($select, $clauses);

		return $this->fetchAll($select);
    }
	
	public function deleteById($id)
	{
		if (!$id) {
			throw new Exception("Must provide item Id to delete media collection");
		}
		$where = $this->getAdapter()->quoteInto('mediaCollectionId = ?', $id);
		
		return $this->delete($where);
	}
	
	protected function joinSelect()
	{
		if (!$this->_mediaCollectionType) {
			throw new Exception("Must set media collection type before query");
		}
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from("media_collection as mc")
						->join("media_collection_type as mct", "mct.typeId = mc.typeId")
						->where("mediaCollectionType = ?", $this->_mediaCollectionType);
		return $select;
	}
}
