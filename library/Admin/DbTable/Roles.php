<?php

class Admin_DbTable_Roles extends Zend_Db_Table
{
    protected $_name = 'roles';
    
    public function getRoles() 
    {  
    	$query = $this->select()->order('roleID DESC');
    	return $this->fetchAll($query);
    } 
}
