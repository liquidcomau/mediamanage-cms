<?php
class Admin_DbTable_Category extends Admin_DbTable
{
    protected $_name = 'category';
    
    public function fetch($clauses)
    {
    	$select = $this->select()
                        ->setIntegrityCheck(false)
                        ->from('category as c',
								["c.*", "(" . $this->childSubSelect() . ") as childCategoryIds"]);
						

    	$select = $this->appendDbClauses($select, $clauses);
    	return $this->fetchAll($select);
    }
    
    public function fetchById($id) {
    	$select = $this->select()
                        ->setIntegrityCheck(false)
                        ->from('category as c',
								["c.*", "(" . $this->childSubSelect() . ") as childCategoryIds"])
                        ->where('c.id = ?', $id);
    	
    	return $this->fetchRow($select);
    }

	public function deleteById($id)
	{
		if (!$id) throw new Exception("No ID provided to delete");

		$where = $this->getAdapter()->quoteInto("id = ?", $id);
		return $this->delete($where);
	}
	
	public function childSubSelect()
	{
		return $this->select()
					->from("category as cc",
							["GROUP_CONCAT(cc.id) as childCategoryIds"])
					->where("cc.parentCategoryId = c.id");
	}
}
