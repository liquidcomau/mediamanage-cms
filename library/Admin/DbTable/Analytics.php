<?php
class Admin_DbTable_Analytics extends Admin_DbTable {
	
	protected $_name = 'analytics';
	
	public function fetch($clauses) {
		return $this->getAnalytics();
	}
	
	public function getAnalytics() {
		return $this->fetchRow();
	}
	
	public function updateAnalytics($data) {
		$record = $this->getAnalytics();
		// Only ever one row

		$id = $record ? $record->id : $this->createRow()->save();
		
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		return $this->update($data, $where);
	}
	
}