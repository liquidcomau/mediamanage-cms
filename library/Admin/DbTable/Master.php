<?php
class Admin_DbTable_Master extends Admin_DbTable
{
    protected $_name = 'master';
    protected $_primary = 'id';
    
	private static $_nodeTypes = null;

	/**
	 * Pre-built select object with added join tables.
	 * So we don't have to repeat the select setup for
	 * each fetch query.
	 *
	 * @return Zend_Db_Table_Select
	 */
	public function masterSelect()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from("master as m")
						->joinLeft("object_type as ot", "ot.id = m.objectTypeId", "objectName");

		return $select;
	}
	
    public function create($data)
    {
    	// because we're still putting news posts etc in ie stuff that doesnt appear in the
    	// tree menu need to check for object type before incrementing position. goddammit.
    	
    	
    	// would love to do incrementing position in a trigger....

		$where = array('parentId = ?' => $data['parentId'] ?: 0);
		$pos = $this->fetchRow($where, 'position DESC')->position;

		$objName = array_flip(self::getObjectTypeIndex())[$data['objectTypeId']];
    	if (preg_match('/^(.+_page|link|menu|folder)$/', $objName) && !isset($data['position'])) {
    		$data['position'] = isset($pos) ? $pos + 1 : 0;
    	}
		
    	return $this->insert($data);
    }
    
	    public function fetch($clauses) {
    	$select = $this->select()
    					->setIntegrityCheck(false)
    					->from('master as m')
    					->joinLeft('object_type', 'object_type.id = m.objectTypeId', 'objectName');
    	
    	$select = $this->appendDbClauses($select, $clauses);
    	
    	return $this->fetchAll($select);
    }

    public function fetchById($id)
    {
    	$query = $this->select()
					->setIntegrityCheck(false)
					->from("master as m")
					->joinLeft("object_type as ot", "ot.id = m.objectTypeId", "objectName")
					->where("m.id = ?", "$id");
    	$result = $this->fetchRow($query);
    	return $result;
    }
	
	public function fetchByUrl($url)
	{
		$query = $this->select();
        $query->setIntegrityCheck(false)
	        ->from('object_type as o')
	        ->joinLeft('master as m', 'o.id = m.objectTypeId')
        	->where("m.url = ?", "$url");
        
        return $this->fetchRow($query);
	}
    
	public function touchDate($nodeID) {
    	$result = $this->update(array('lastUpdated' => date("Y-m-d H:i:s")), 'nodeID = "'. $nodeID .'"');
    	return $result;
    }
    
	/**
	 * Returns tree node types
	 * 
	 * @return type
	 * @throws Exception
	 */
	private static function getNodeTypes()
	{
		if (self::$_nodeTypes === null) {
			$appFile = APPLICATION_PATH . '/config/treeObjects.json'; 
			$mmFile = MEDIAMANAGE_ENV . '/application/config/treeObjects.json';

			$file = file_exists($appFile) ? $appFile : $mmFile;
			$file = file_get_contents($file);

			if (!$file) throw new Exception("Tree objects missing");
			$types = array_keys(json_decode($file, true));
			
			//need to convert back to underscores... should prob move away from underscores in the future for object names...
			self::$_nodeTypes = array_map(function($type) {
				return str_replace('-', '_', $type);
			}, $types);
		}
		
		return self::$_nodeTypes;
	}
	
	/**
	 * Fetch master objects that are tree nodes
	 * 
	 * @param type $clauses
	 * @return type
	 */
    public function fetchNodes($clauses)
    {
    	$select = $this->select()
    				->setIntegrityCheck(false)
    				->from('master as m')
    				->join('object_type', 'object_type.id = m.objectTypeId', 'objectName')
    				->where('objectName IN(?)', self::getNodeTypes());
    	 
    	$select = $this->appendDbClauses($select, $clauses);
    	
    	return $this->fetchAll($select);
    }
	
	/**
	 * Similar to fetchByUrl but considers parentId and must be tree node
	 * 
	 * @param type $clauses
	 * @return type
	 */ 
	public function fetchNode($url, $parentID = null) 
    {       
    	$query = $this->select();
        $query->setIntegrityCheck(false)
	        ->from('object_type as o')
	        ->joinLeft('master as m', 'o.id = m.objectTypeId')
	        ->where('objectName IN(?)', self::getNodeTypes())
        	->where("m.url = ?", "$url");
        
        	if ($parentID !== null) {
	        	$query->where("m.parentID = ?", "$parentID");
        	}
        	
        $result = $this->fetchRow($query);
        return $result;
    }
    
    public function fetchByHeading($heading)
    {
    	$query = $this->select();
    	$query->setIntegrityCheck(false)
    	->from('object_type as o')
    	->joinLeft('master as m', 'o.id = m.objectTypeId')
    	->where("m.heading = ?", $heading);
    	$result = $this->fetchRow($query);
    	return $result;
    }

    public function fetchByParentID($parentID) {
    	$query = $this->select();
    	$query->setIntegrityCheck(false)
	    	->from('object_type as o')
	    	->joinLeft('master as m', 'o.id = m.objectTypeId')
    		->where("m.parentID = ?", "$parentID")
    		->order('position ASC');
    	$nodes = $this->fetchAll($query);
    	return $nodes;
    }
    
    public function fetchFolders()
    {
    	$select = $this->select()
    			->setIntegrityCheck(false)
    			->from('master as m')
    			->joinLeft('object_type', 'object_type.id = m.objectTypeId', 'objectName')
    			->where('objectName = ?', 'folder');
    		 
    	$select = $this->appendDbClauses($select, $clauses);
    	 
    	return $this->fetchAll($select);
    }
    
    public function fetchFolderById($id)
    {
    	$select = $this->select()
    					->setIntegrityCheck(false)
    					->from('master as m')
    					->joinLeft('object_type', 'object_type.id = m.objectTypeId', 'objectName')
    					->where('objectName = ?', 'folder')
    					->where('m.id = ?', $id);
    	 
    	return $this->fetchRow($select);
    }
    
    public function fetchMenus()
    {
    	$select = $this->select()
				    	->setIntegrityCheck(false)
				    	->from('master as m')
				    	->joinLeft('object_type', 'object_type.id = m.objectTypeId', 'objectName')
				    	->where('objectName = ?', 'menu');
    	 
    	$select = $this->appendDbClauses($select, $clauses);
    
    	return $this->fetchAll($select);
    }
    
    public function fetchMenuById($id)
    {
    	$select = $this->select()
				    	->setIntegrityCheck(false)
				    	->from('master as m')
				    	->joinLeft('object_type', 'object_type.id = m.objectTypeId', 'objectName')
				    	->where('objectName = ?', 'menu')
				    	->where('m.id = ?', $id);
    
    	return $this->fetchRow($select);
    }
    
    public function deleteById($id) {
    	$where = $this->getAdapter()->quoteInto('id = ?', $id);
    	$rec = $this->fetchRow($where);
    	
    	if (!$rec) throw new Exception("Nothing to delete");
    	
    	if ($this->delete($where)) {
    		
    		$objName = array_flip(self::getObjectTypeIndex())[$data['objectTypeId']];
    		if ($objName !== 'news') {
	    		// yeah this mapper logic below really should be in the datatable layer...
    			(new Admin_Model_Mapper_Tree())->decrementChildPositions($rec->parentId, $rec->position + 1);
    		}
    	};
    	
    	return 1;
    }
    
    public function getAll() {
	    $query = $this->select();
	    $query->setIntegrityCheck(false)
	    	->from('object_type as o')
	    	->joinRight('master as m', 'o.id = m.objectTypeId')
	    	->order('m.position ASC');
	    $nodes = $this->fetchAll($query);
	    return $nodes;
    }
    
    public function fetchSiblings($parentID) {
    	$query = $this->select();
    	/* @todo: grab object exceptions from array located elsewhere */
    	$query->setIntegrityCheck(false)
	    	->from('object_type as o')
	    	->joinRight('master as m', 'o.id = m.objectTypeId')
	    	->where("m.parentID = ?", "$parentID")
    		->where("o.objectName != ?", "cta")
    		->where("o.objectName != ?", "firstClass")
    		->where("o.objectName != ?", "footer")
    		->where("o.objectName != ?", "comment")
    		->where("o.objectName != ?", "news")
	    	->order('m.position ASC');
    	$nodes = $this->fetchAll($query);
    	return $nodes;
    }
    
    /**
     * Returns an array of active elements for use in main navigation
     * 
     * @param int $parentID
     * @return array
     */
    public function fetchActiveNavByParentID($parentID) {
    	$query = $this->select();
    	$query->setIntegrityCheck(false)
	    	->from('object_type as o')
	    	->joinRight('master as m', 'o.id = m.objectTypeId')
	    	->where("m.parentId = ?", "$parentID")
	    	//->where("m.active = ?", true)
	    	//->where("navigation = ?", true)
	    	//->where("heading != ?", "Home")
	    	->where("o.objectName != ?", "cta")
	    	->where("o.objectName != ?", "firstClass")
	    	->where("o.objectName != ?", "footer")
	    	->where("o.objectName != ?", "comment")
	    	->where("o.objectName != ?", "news")
	    	->order("position ASC");
    	$nodes = $this->fetchAll($query);
    	return $nodes;
    }
    
    /**
     * Returns the first child of a given node
     * 
     * @param integer $nodeID
     * @return array
     */
    public function firstActiveChild($nodeId) {
    	$query = $this->select();
    	$query->where("parentId = ?", "$nodeId")
	    	->where("published = ?", true)
    		->where("hidden != ?", true)
	    	->order('position ASC');
    	$node = $this->fetchRow($query);
    	return $node;
    }
    
	/**
     * Returns an array of published elements for use in main navigation
     * 
     * @param int $parentID
     * @return array
     */
    public function fetchPublishedNavByParentID($parentID) {
    	$query = $this->select();
    	$query->setIntegrityCheck(false)
	    	->from('object_type as o')
	    	->join('master as m', 'o.id = m.objectTypeId')
	    	->joinLeft('link as l', 'm.id = l.id', array('href', 'pageId', 'newWindow'))
	    	->where("m.parentId = ?", "$parentID")
	    	->where("m.published = ?", true)
	    	->where("m.hidden != ?", true)
			->where('o.objectName REGEXP ?', '^(.+_page|link|menu)$')
	    	->order("position ASC");
    	$nodes = $this->fetchAll($query);
    	return $nodes;
    }

	/**
	 * Counts resources in master table by object type.
	 * If a string is provided, filters by object name.
	 * If an int is provided, filters by objectTypeId.
	 * 
	 * If no $type is provided, counts all published resources.
	 * 
	 * @param (int|string) $type
	 */
	public function countPublished($type = null, $clauses = null, $andExecute = true)
	{
		$select = $this->select();
		$select->setIntegrityCheck('false')
				->from('master as m')
				->where("m.published = ?", true);

		if (is_string($type)) {
			$select->joinLeft("object_type as ot", "m.objectTypeId = ot.id", [])
					->where("objectName = ?", $type);
		} elseif (is_int($type)) {
			$select->where("objectTypeId = ?", $type);
		}

		$select = $this->appendDbClauses($select, $clauses);
		$select = $this->appendMasterClauses($select, $clauses);

		if (!$rec) return $select;
		
		$res = $this->fetchAll($select);
		return count($res);
	}
}
