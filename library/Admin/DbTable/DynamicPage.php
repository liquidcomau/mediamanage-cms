<?php
class Admin_DbTable_DynamicPage extends Admin_DbTable 
{
	protected $_name = 'dynamic_page';	
	
	public function fetch($clauses) {
		// @todo: add clauses to select stmt
		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from('dynamic_page as dp')
						->join('master as m', 'dp.id = m.id')
						->join('dynamic_page_layout as l', 'dp.layoutId = l.id', array('layoutName'));
						//->join('dynamic_page_componen as dpc', 'dpc.typeId = dpct.id');
		
		foreach ($clauses as $clauseKey => $clauseVal) {
			switch ($clauseKey) {
				case 'in':
					if (!is_array($clauseVal)) $clauseVal = explode(',', $clauseVal);
					$select->where('dp.id IN(?)', $clauseVal);
					break;
			}
		}
		
		return $this->fetchAll($select);
	}
	
};