<?php

class Admin_DbTable_Slideshow extends Admin_DbTable
{
    protected $_name = 'slideshow';
    
    public function fetchById($pageId) {
    	$query = $this->select();
    	$query->where("id = ?", $pageId);

    	$result = $this->fetchRow($query);
        
        return $result;
    }
    
    public function fetch($clauses) {
    	$select = $this->select();
    	$select = $this->appendDbClauses($select, $clauses);

    	return $this->fetchAll($select);
    }
}