<?php
class Admin_DbTable_Gallery extends Admin_DbTable
{
    protected $_name = 'gallery';
	protected $_categoryTable = "category";
	protected $_categoryJoinTable = "gallery_category";
    
    public function fetchById($id) {
    	$query = $this->joinSelect();
    	$query->where("m.id = ?", $id);

    	$result = $this->fetchRow($query);

		return $result;
    }
    
    public function fetch($clauses) {
    	$select = $this->joinSelect()
						->order("publishedDate DESC");
    	$select = $this->appendDbClauses($select, $clauses);
		$select = $this->appendMasterClauses($select, $clauses);
		$select = $this->appendGalleryClauses($select, $clauses);

		$select = $this->checkPublished($select);
		
    	return $this->fetchAll($select);
    }

    public function fetchByIdentifier($id) {
    	$query = $this->joinSelect();
    	$query->where("identifier = ?", $id);

    	$result = $this->fetchRow($query);
        
        return $result;
    }
	
	public function joinSelect() 
	{
		$select = $this->select()->setIntegrityCheck(false);
		
		$modelClass = Admin_Factory::getClass("Model_Gallery");
		if ($modelClass::hasCategories()) {
			$select->from('gallery as g',
							["g.*", "GROUP_CONCAT(gc.categoryId) as categoryIds"])
					->joinLeft($this->_categoryJoinTable . " as gc", "gc.galleryId = g.id", []);
		} else {
			$select->from("gallery as g");
		}
		
		$select->join('page as p', 'p.id = g.id')
				->join('master as m', 'p.id = m.id')
				->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName')
				->group("g.id");
		
		return $select;
	}
	
	public function appendGalleryClauses($select, $clauses)
	{
		$modelClass = Admin_Factory::getClass("Model_Gallery");
		if (!$modelClass::hasCategories()) {
			unset($clauses["category"]);
		}

		foreach ($clauses as $key => $val) {
    		switch ($key) {
				case "publishedDateRange":
					$select->where("publishedDate >= ?", $val[0]);
					$select->where("publishedDate <= ?", $val[1]);
					break;
    			case 'category':
					
					// creates a set of category names that are linked to
					// the news.id field we're grouping each record by.
					$catSelect = $this->select()
									->setIntegrityCheck(false)
									->from($this->_categoryJoinTable, ["cat.name"])
									->join($this->_categoryTable . " as cat", $this->_categoryJoinTable . ".categoryId = cat.id", [])
									->where("g.id = " . $this->_categoryJoinTable . ".galleryId");
					
					$select->where("? IN(" . $catSelect . ")", $val);
					
					break;
    		}
    	}
		
		return $select;
	}
	
	/**
	 * Returns a set consisting of columns (category) id, (category) name and totalGalleries.
	 * 
	 * $category can either be a string or int. If a string, will add a where clause on the (category)
	 * name column, else if an int, on the (category) id column.
	 * 
	 * If $category is null, will do a fetchAll and return a set or row objects, otherwise a single row object.
	 * 
	 * @param (int|string) $category
	 * @param bool $available. Ie for display on the front end - avaible = true meaning published must be true.
	 */
	public function fetchTotalsByCategory($category = null, $available = true)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from($this->_categoryJoinTable . " as gc",
								["COUNT(DISTINCT gc.galleryId) as totalGalleries",
								"GROUP_CONCAT(DISTINCT gc.galleryId) as galleryIds"])
						->join($this->_categoryTable . " as c", "c.id = gc.categoryId", ["c.id", "c.name", "c.label"])
						->order("c.name ASC")
						->group("c.id");
		
		if ($available) {
			$select->join("master as m", "m.id = gc.galleryId", [])
					->where("m.published = ?", true)
					->where("m.hidden != ?", true);
		}
		
		if ($category) {
			if (is_int($category)) {
				$select->where("c.id = ?", $category);
			} elseif (is_string($category)) {
				$select->where("c.name = ?", $category);
			} else {
				throw new Exception("Category type must either be an id or name");
			}
			
			return $this->fetchRow($select);
		} else {
			return $this->fetchAll($select);
		}
	}
}
