<?php
class Admin_DbTable_MediaItem extends Admin_DbTable
{
	protected $_name = "media_item";
	
	public function fetchById($itemId) {
    	$select = $this->joinSelect()
						->where("mi.itemId = ?", $itemId);

    	return $this->fetchRow($select);
    }
	
	public function fetchAllByCollectionId($mediaCollectionId)
	{
		return $this->fetch(["mediaCollectionId" => $mediaCollectionId]);
	}
    
    public function fetch($clauses) {
    	$select = $this->joinSelect()
						->order("position ASC");
    	
    	$select = $this->appendDbClauses($select, $clauses);

		foreach ($clauses as $key => $val) {
    		switch ($key) {
				case "mediaCollectionId":
					$select->where("mediaCollectionId = ?", $val);
					break;
    		}
    	}

		return $this->fetchAll($select);    	
    }
	
	public function deleteById($id)
	{
		if (!$id) {
			throw new Exception("Must provide item Id to delete media item");
		}
		$where = $this->getAdapter()->quoteInto('itemId = ?', $id);
		
		return $this->delete($where);
	}
	
	protected function joinSelect()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from("media_item as mi")
						->join("media_item_view as miv", "mi.itemId = miv.itemId");
		
		return $select;
	}
}
