<?php
class Admin_DbTable_DynamicPageComponentChild extends Admin_DbTable 
{
	protected $_name = 'dynamic_page_component_child';	
	
	public function fetch($clauses) {
		// @todo: add clauses to select stmt

		$select = $this->select()
						->setIntegrityCheck(false)
						->from('dynamic_page_component_child as child')
						->joinLeft('dynamic_page_component as dpc', 'child.subComponentId = dpc.id', array('dpc.typeId', 'dpc.componentTitle'))
						->joinLeft('dynamic_page_component_type as ct', 'ct.id = dpc.typeId', array('ct.componentName'))
						->joinLeft('fragment as f', 'child.fragmentId = f.id')
						->order('childPosition ASC');
		
		foreach ($clauses as $clauseKey => $clauseVal) {
			switch ($clauseKey) {
				case 'pageId':
					$pageIds = is_array($clauseVal) ? $clauseVal : array($clauseVal);
					$select->where('pageId IN(?)', $pageIds);
					break;
			};
		}
		
		return $this->fetchAll($select);
	}
	
};