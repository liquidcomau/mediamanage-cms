<?php
class Admin_DbTable_LoginLog extends Admin_DbTable
{
	protected $_name = 'login_log';
	
	public function fetch($clauses) 
	{
		$select = $this->select();
		return $this->fetchAll();		
	}
	
	public function logLogin($id)
	{
		return $this->insert(array(
				'memberId' => $id,
				'dateTime' => (date("Y-m-d H:i:s"))
		));
	}
}