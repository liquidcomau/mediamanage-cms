<?php

class Admin_DbTable_FreeformPage extends Admin_DbTable
{
    protected $_name = 'freeform_page';
    
    public function fetchById($id) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
    			->from('freeform_page as fp')
    			->join('page as p', 'p.id = fp.id')
    			->join('master as m', 'p.id = m.id')
    			->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName');
    	$select = $this->appendDbClauses($select, $clauses);
    	
    	$select->where('m.id = ?', $id);
    	
    	return $this->fetchRow($select);
    }
    
    public function fetchByUrl($url) {
    	return $this->fetch(array('url' => $url))[0];
    }
    
    public function fetch($clauses) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
    			->from('freeform_page as fp')
    			->join('page as p', 'p.id = fp.id')
    			->join('master as m', 'p.id = m.id')
    			->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName');
    	
		if (isset($clauses["in"])) {
			$clauses["in"] = ["column" => "m.id", "values" => $clauses["in"]];
		}
		
    	$select = $this->appendDbClauses($select, $clauses);
    	$select = $this->appendMasterClauses($select, $clauses);
    	
		foreach ($clauses as $key => $val) {
    		switch ($key) {
				case "template":
					$select->where("template = ?", $val);
					break;
    		}
    	}
		
    	$select = $this->checkPublished($select);
		// add hidden check
		
    	return $this->fetchAll($select);
    }
}