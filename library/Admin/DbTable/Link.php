<?php

class Admin_DbTable_Link extends Admin_DbTable
{
    protected $_name = 'link';
    
    public function fetchById($pageId) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
    			->from('link as l')
    			->join('master as m', 'm.id = l.id')
    			->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName');

    	$select->where("m.id = ?", $pageId);

    	$result = $this->fetchRow($select);
        
        return $result;
    }
    
    public function fetch($clauses) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
				->from('link as l')
    			->join('master as m', 'm.id = l.id')
    			->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName');
    	
    	$select = $this->appendDbClauses($select, $clauses);
    	$select = $this->appendMasterClauses($select, $clauses);
    	
    	$select = $this->checkPublished($select);
    	
    	return $this->fetchAll($select);    	
    }
}