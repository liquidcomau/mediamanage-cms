<?php
class Admin_DbTable_Layout extends Admin_DbTable
{
	public function fetch($clauses) {
		return $this->fetchAll();
	}
	
    /**
     * Returns an array of main menu items
     * 
     * Replaces 'firstClass' object name with name of 
     * object it's referring to. Makes no sense? Email me.
     *
     * @return Zend_Db_Rowset
     */
    public function getAll() {
	    $expr = 'SELECT m.*, l.*, o.*' . PHP_EOL .
	    	'FROM master as m' . PHP_EOL .
		    'JOIN ' . $this->_name . ' as l' . PHP_EOL .
		    'ON m.id = l.masterId' . PHP_EOL .
		    'JOIN object_type as o' . PHP_EOL .
		    'ON CASE' . PHP_EOL .
		    'WHEN m.objectTypeId = 12 THEN o.id = m.parentId' . PHP_EOL .
		    'ELSE o.id = m.objectTypeId END' . PHP_EOL .
		    'ORDER BY l.order';
	     
	    $stmt = new Zend_Db_Statement_Pdo($this->getAdapter(), $expr);
		$stmt->execute();
		$results = $stmt->fetchAll(Zend_Db::FETCH_OBJ);
		
		$data  = array(
            'table'    => $this,
            'data'     => $results,
            'readOnly' => false,
            'rowClass' => $this->getRowClass(),
            'stored'   => true
        );

        $rowsetClass = $this->getRowsetClass();
        if (!class_exists($rowsetClass)) {
            require_once 'Zend/Loader.php';
            Zend_Loader::loadClass($rowsetClass);
        }
        
        return new $rowsetClass($data);
    }
}