<?php
class Admin_DbTable_LayoutAdminMenu extends Admin_DbTable_Layout
{
    protected $_name = 'admin_menu';
    
    /**
     * Returns an array of elements for use in admin navigation
     *
     * @param int $parentID
     * @return array
     */
    public function fetchByParentID($parentID) {
    	$query = $this->select()
	    	->where("parentId = ?", "$parentID")
	    	->order("position ASC");
    	return $this->fetchAll($query);
    } 
}
