<?php
class Admin_DbTable_Agent extends Admin_DbTable
{
    protected $_name = 'agent';
    
    public function fetchById($id) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
				->from('agent')
				->join('agency', 'agent.agencyId = agency.id', ['agencyName', 'agencyEmail', 'address', 'logo', 'website'])
				->where("agent.id = ?", $id)
				->order('name ASC');
    	
    	return $this->fetchRow($select);
    }
    
    public function fetch($clauses) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
				->from('agent')
				->join('agency', 'agent.agencyId = agency.id', ['agencyName', 'agencyEmail', 'address', 'logo', 'website'])
				->order('name ASC');

		if (isset($clauses["in"])) {
			$clauses["in"] = ["column" => "agent.id", "values" => $clauses["in"]];
		}

		$select = $this->appendDbClauses($select, $clauses, "agent");

		return $this->fetchAll($select);
    }

	public function deleteById($id)
	{
		if (!$id) throw new Exception("Must provide ID to delete by ID");

		$where = $this->getAdapter()->quoteInto("id = ?", $id);
		return $this->delete($where);
	}
}
