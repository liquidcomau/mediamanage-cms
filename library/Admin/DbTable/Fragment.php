<?php

class Admin_DbTable_Fragment extends Admin_DbTable
{
    protected $_name = 'fragment';
    
    public function fetchById($pageId) {
    	$query = $this->select();
    	$query->where("id = ?", $pageId);

    	$result = $this->fetchRow($query);
        
        return $result;
    }
    
    public function fetch($clauses) {
    	$select = $this->select()
						->order("internalLabel ASC");
    	$select = $this->appendDbClauses($select, $clauses);
    	
    	return $this->fetchAll($select);    	
    }
	
	public function deleteById($id)
	{
		if (!id) throw new Exception("Must provide ID to delete by Id");
		
		$where = $this->getAdapter()->quoteInto("id = ?", $id);
		return $this->delete($where);
	}
}