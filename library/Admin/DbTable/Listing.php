<?php
class Admin_DbTable_Listing extends Admin_DbTable
{
    protected $_name = 'listing';
    
    public function fetchById($id) {
    	$select = $this->getSelectObj()
                        ->where('m.id = ?', $id);

		return $this->fetchRow($select);
    }
	
    public function fetchByUrl($url) {
    	return $this->fetch(array('url' => $url))[0];
    }
    
    public function fetch($clauses) {
    	$select = $this->getSelectObj();

    	$select = $this->appendDbClauses($select, $clauses);
    	$select = $this->appendMasterClauses($select, $clauses);
		$select = $this->appendListingClauses($select, $clauses);
    	
		if (!count($select->getPart("order"))) {
			$select->order("publishedDate DESC");
		}

    	$select = $this->checkPublished($select);
    	$select = $this->checkNotHidden($select);

		return parent::fetchAll($select);
    }
	
	protected function appendListingClauses($select, $clauses)
	{
		foreach ($clauses as $key => $val) {
			switch ($key) {
				case "featured":
					$select->where("featured = ?", $val);
					break;
				case "publishedDateLessThan":
					$select->where("publishedDate < ?", $val);	
					break;
				case "publishedDateGreaterThan":
					$select->where("publishedDate > ?", $val);	
					break;
				case "publishedDateRange":
					$select->where("publishedDate >= ?", $val[0]);
					$select->where("publishedDate <= ?", $val[1]);
					break;
    			case 'category':
					
					// creates a set of category names that are linked to
					// the news.id field we're grouping each record by.
					$catSelect = $this->select()
									->setIntegrityCheck(false)
									->from("news_category", ["cat.name"])
									->join("category as cat", "news_category.categoryId = cat.id", [])
									->where("n.id = news_category.newsId");
					
					$select->where("? IN(" . $catSelect . ")", $val);
					
					break;
    			case 'featured':
    				if ($val == true) $select->where('featured = ?', true);
    				break;
    		}
		}

		return $select;
	}
	
	public function getSelectObj()
	{
		$select = $this->select()
                        ->setIntegrityCheck(false)
                        ->from('listing as l', [
								"l.*",
								"GROUP_CONCAT(la.agentId) as agentIds"
						])
						->join('master as m', 'l.id = m.id')
						->joinLeft("listing_agent as la", "la.listingId = l.id", [])
						->joinLeft("media_collection_relationship as mcr", "mcr.listingId = l.id", ["relationshipId"]);
		
		$select->join('object_type as ot', 'ot.id = m.objectTypeId', array('objectName'))
				->group("l.id");
		
		return $select;
	}
}
