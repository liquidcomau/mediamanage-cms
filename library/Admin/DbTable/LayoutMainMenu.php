<?php
class Admin_DbTable_LayoutMainMenu extends Admin_DbTable_Layout
{
    protected $_name = 'layout_main_menu';
    
    /**
     * Returns an array of main menu items
     *
     * @return Zend_Db_Rowset
     */
    public function getAll() {
	    $select = $this->select();
	    $select->setIntegrityCheck(false)
	    	->from('master as m')
	    	->join('layout_main_menu as l', 'm.id = l.masterId')
	    	->joinLeft('object_type as ot', 'ot.id = m.objectTypeId')
	    	->order('l.order');
	    $results = $this->fetchAll($select);
	    return $results;
    }
}
