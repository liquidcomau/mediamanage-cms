<?php
class Admin_DbTable_SiteSettings extends Admin_DbTable
{
    protected $_name = 'site';
    
    public function fetch($clauses)
    {
    	$query = $this->select()->where("id = ?", $clauses['id']);
    	$result = $this->fetchRow($query);
    	return $result;
    }
}