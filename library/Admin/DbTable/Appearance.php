<?php
class Admin_DbTable_Appearance extends Zend_Db_Table
{
    protected $_name = 'appearance';
    
    public function fetchById($id)
    {
    	$query = $this->select()->where("id = ?", "$id");
    	$result = $this->fetchRow($query);
    	return $result;
    }
}