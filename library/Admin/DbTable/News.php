<?php
class Admin_DbTable_News extends Admin_DbTable
{
    protected $_name = 'news';
    
    public function fetch($clauses)
    {
    	$select = $this->getSelectObj();

    	$select = $this->appendDbClauses($select, $clauses);
    	$select = $this->appendMasterClauses($select, $clauses);
    	
    	foreach ($clauses as $key => $val) {
    		switch ($key) {
				case "featured":
					$select->where("featured = ?", $val);
					break;
				case "publishedDateLessThan":
					$select->where("publishedDate < ?", $val);	
					break;
				case "publishedDateGreaterThan":
					$select->where("publishedDate > ?", $val);	
					break;
				case "publishedDateRange":
					$select->where("publishedDate >= ?", $val[0]);
					$select->where("publishedDate <= ?", $val[1]);
					break;
    			case 'category':
					
					// creates a set of category names that are linked to
					// the news.id field we're grouping each record by.
					$catSelect = $this->select()
									->setIntegrityCheck(false)
									->from("news_category", ["cat.name"])
									->join("category as cat", "news_category.categoryId = cat.id", [])
									->where("n.id = news_category.newsId");
					
					$select->where("? IN(" . $catSelect . ")", $val);
					
					break;
    			case 'featured':
    				if ($val == true) $select->where('featured = ?', true);
    				break;
    		}
    	}
		
		if (!count($select->getPart("order"))) {
			$select->order("publishedDate DESC");
		}

    	$select = $this->checkPublished($select);
    	$select = $this->checkNotHidden($select);

		return parent::fetchAll($select);
    }
    
    public function fetchById($id) {
    	$select = $this->getSelectObj()
                        ->where('m.id = ?', $id);

		return $this->fetchRow($select);
    }
	
	/**
	 * Returns a set consisting of columns (category) id, (category) name and totalPosts.
	 * 
	 * $category can either be a string or int. If a string, will add a where clause on the (category)
	 * name column, else if an int, on the (category) id column.
	 * 
	 * If $category is null, will do a fetchAll and return a set or row objects, otherwise a single row object.
	 * 
	 * @param (int|string) $category
	 * @param bool $available. Ie for display on the front end - avaible = true meaning published must be true.
	 */
	public function fetchTotalPostsByCategory($category = null, $available = true)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from("news_category as nc",
								["COUNT(DISTINCT nc.newsId) as totalPosts",
								"GROUP_CONCAT(DISTINCT nc.newsId) as postIds"])
						->join("category as c", "c.id = nc.categoryId", ["c.id", "c.name", "c.label"])
						->join("master as m", "m.id = nc.newsId", [])
						->join("object_type as ot", "m.objectTypeId = ot.id")
						->where("ot.objectName = ?", "news")
						->order("c.name ASC")
						->group("c.id");
		
		if ($available) {
				$select->where("m.published = ?", true);
		}
		
		if ($category) {
			if (is_int($category)) {
				$select->where("c.id = ?", $category);
			} elseif (is_string($category)) {
				$select->where("c.name = ?", $category);
			} else {
				throw new Exception("Category type must either be an id or name");
			}
			
			return $this->fetchRow($select);
		} else {
			return $this->fetchAll($select);
		}
	}
	
	public function getSelectObj()
	{
		$recommendations = Zend_Registry::get("config")->cms->modules->news->recommendations;
		
		$subfrom = ["n.*", "GROUP_CONCAT(nc.categoryId) as categoryIds"];
		
		// attach related IDs if in config
		if ($recommendations) {
			$subfrom["newsRecommendationIds"] = new Zend_Db_Expr("GROUP_CONCAT(nr.childId ORDER BY nr.position ASC)");
		}
		
		$select = $this->select()
                        ->setIntegrityCheck(false)
                        ->from('news as n',
								$subfrom)
						->join('master as m', 'n.id = m.id')
						->joinLeft("news_category as nc", "nc.newsId = n.id", []);
		
		// attach table for related IDs if in config
		if ($recommendations) {
			$select->joinLeft("news_recommendations as nr", "nr.parentId = n.id", []);
		}
		
		$select->join('object_type as ot', 'ot.id = m.objectTypeId', array('objectName'))
				->group("n.id");
		
		return $select;
	}
}
