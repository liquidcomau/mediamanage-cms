<?php
class Admin_DbTable_Seo extends Admin_DbTable
{
    protected $_name = 'seo';
    
    public function fetch($clauses) {
    	$select = $this->select();
		
    	$select = $this->appendDbClauses($select, $clauses);
    	
    	return $this->fetchAll($select); 
    }
    
    public function fetchById($id)
    {
    	$query = $this->select()->where("id = ?", "$id");
    	return $this->fetchRow($query);
    }
    
    public function create($fields) {
    	return $this->insert($fields);
    }
}
