<?php

class Admin_DbTable_StaticPage extends Admin_DbTable
{
    protected $_name = 'master';
    
    public function fetchById($pageId) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
    			->from('page as p')
    			->join('master as m', 'm.id = p.id')
    			->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName')
    			->where('objectName = ?', 'static_page');

    	$select->where("m.id = ?", $pageId);

    	$result = $this->fetchRow($select);
        
        return $result;
    }
    
    public function fetchByUrl($url) {
    	return $this->fetch(array('url' => $url))[0];
    }
    
    public function fetch($clauses) {
    	$select = $this->select();
    	$select->setIntegrityCheck(false)
				->from('page as p')
    			->join('master as m', 'm.id = p.id')
    			->join('object_type as ot', 'ot.id = m.objectTypeId', 'objectName')
    			->where('objectName = ?', 'static_page');
    	
		if (isset($clauses["in"])) {
			$clauses["in"] = ["column" => "m.id", "values" => $clauses["in"]];
		}
		
    	$select = $this->appendDbClauses($select, $clauses);
    	$select = $this->appendMasterClauses($select, $clauses);
    	
    	$select = $this->checkPublished($select);
    	
    	return $this->fetchAll($select);    	
    }
}