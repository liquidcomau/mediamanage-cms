<?php
class Admin_DbTable_DynamicPageComponent extends Admin_DbTable 
{
	protected $_name = 'dynamic_page_component';	
	
	public function fetch($clauses) {
		// @todo: add clauses to select stmt

		$select = $this->select()
						->setIntegrityCheck(false)
						->from('dynamic_page_component as dpc')
						->join('dynamic_page_component_type as dpct', 'dpc.typeId = dpct.id', array('componentTypeId' => 'dpct.id', 'componentName' => 'dpct.componentName'));
		
		foreach ($clauses as $clauseKey => $clauseVal) {
			switch ($clauseKey) {
				case 'pageId':
					$pageIds = is_array($clauseVal) ? $clauseVal : array($clauseVal);
					$select->where('dynamicPageId IN(?)', $pageIds);
					break;
			};
		}
		
		return $this->fetchAll($select);
	}
	
};