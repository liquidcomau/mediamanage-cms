<?php
class Admin_DbTable_Agency extends Admin_DbTable
{
    protected $_name = 'agency';
    
    public function fetchById($id) {
    	$select = $this->select();
    	$select->where("id = ?", $id)
				->order('agencyName ASC');
    	
    	return $this->fetchRow($select);
    }
    
    public function fetch($clauses) {
    	$select = $this->select();
		$select->order('agencyName ASC');
    	
    	$select = $this->appendDbClauses($select, $clauses);
    	
    	return $this->fetchAll($select);
    }
	
	public function deleteById($id)
	{
		if (!$id) throw new Exception("Must provide ID to delete by ID");

		$where = $this->getAdapter()->quoteInto("id = ?", $id);
		return $this->delete($where);
	}
}
