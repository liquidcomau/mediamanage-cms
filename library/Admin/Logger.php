<?php
class Admin_Logger
{
	public static function dump($data, $esc = true) {
		$data = strip_tags(Zend_Debug::dump($data, null, false));
		$data = $esc ? html_entity_decode($data) : $data;
		
		Zend_Registry::get('logger')->info($data);
	}
}