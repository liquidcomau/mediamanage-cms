<?php
/**
 * Microtime logging convenience class
 * 
 * To start specify a key with Admin_Microtime::start('time');
 * To end Admin_Microtime::end('time') will return the time difference in microseconds.
 * 
 * Admin_Microtime::log($key) is a shortcut using Admin_Logger to immediately log the time difference.
 * 
 * @author AdrianNorman
 */

class Admin_Microtime 
{
	public static $microtimeInstance = null;
	
	public static function getInstance() {
		if (!self::$microtimeInstance) {
			self::$microtimeInstance = new Admin_Microtime();
		}
		
		return self::$microtimeInstance;
	}
	
	public static function start($key, $float = true) {
		self::getInstance()->$key = microtime($float); 
	}
	
	/**
	 * Returns the time difference between the start function
	 * and the end function's recorded microtime by key.
	 * 
	 * @param string $key
	 * @param bool $float
	 * @return void|number
	 */
	public static function end($key, $float = true) {
		if (!self::getInstance()->$key) {
			return;
		} else {
			return microtime($float) - self::getInstance()->$key;
		}
	}
	
	/**
	 * Calls ::end and logs the result
	 * 
	 * @param string $key
	 * @param string $msg
	 * @param bool $float
	 */
	public static function log($key, $msg, $float = true) {
		Admin_Logger::dump(($msg ? $msg . ': ' : null) . self::end($key));
	}
}