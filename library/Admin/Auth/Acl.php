<?php
class Admin_Auth_Acl extends Zend_Acl
{
	/**
	 * Roles.
	 */	
	const ROLE_MEMBER = 'member';
	const ROLE_MODERATOR = 'moderator';
	const ROLE_ADMIN = 'admin';
	const ROLE_WEBMASTER = 'webmaster';

	/**
	 * Class constructor.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// add resources
		$this->add(new Zend_Acl_Resource('moderator'));
		$this->add(new Zend_Acl_Resource('admin'));
		$this->add(new Zend_Acl_Resource('webmaster'));

		// add roles
		$this->addRole(new Zend_Acl_Role(self::ROLE_MODERATOR));
		$this->addRole(new Zend_Acl_Role(self::ROLE_ADMIN), self::ROLE_MODERATOR);
		$this->addRole(new Zend_Acl_Role(self::ROLE_WEBMASTER), self::ROLE_ADMIN);
		
		// add rules
		$this->allow(self::ROLE_MODERATOR, array('moderator'));
		$this->allow(self::ROLE_ADMIN, array('admin'));
		$this->allow(self::ROLE_WEBMASTER, array('webmaster'));
	}
}