<?php
class Admin_EditorShortcodeItem
{
	protected $_fns = array();
	protected $_viewHelper = "Admin_View_Helper_EditorShortcodeItem";
	protected $_value = null;
	
	public function callFn($key)
	{
		if (isset($this->_fns[$key])) {
			return $this->_fns[$key]();
		} else {
			throw new Exception("Missing shortcode function");
		}
	}
	
	public function setFn($key, $fn)
	{
		$this->_fns[$key] = $fn;
	}
	
	public function getLabel()
	{
		return $this->_label;
	}
	
	public function getValue()
	{
		return $this->callFn("get_value");
	}
	
	public function setLabel($label)
	{
		$this->_label = $label;
	}
	
	public function setGetValue($fn)
	{
		$this->_fns["get_value"] = $fn;
	}
	
	public function setViewHelper($helper)
	{
		$this->_viewHelper = $helper;
	}
	
	public function output()
	{
		$view = Zend_Layout::getMvcInstance()->getView();
		return $view->{lcfirst(array_pop(explode("_", $this->_viewHelper)))}($this);
	}
}
