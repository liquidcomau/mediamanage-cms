<?php
class Admin_Rest_Server_CacheService
{
    /* 
     * Clears contents of static cache and static tag directories 
     * WARNING: Ensure correct paths are set in the config.ini to avoid recursively deleting incorrect files
     * A check is in place to ensure path contains "cache" however this could still end badly if used incorrectly  
     * 
     * Usage: <hostname>/rest/cache-service/clear-static
     */ 
    public function clearStatic() 
    {
    	$cacheHelper = new Admin_View_Helper_Cache();
    	return $cacheHelper->clearStatic();
    }
}