<?php
abstract class Admin_Rest_Controller extends Zend_Rest_Controller
{
	protected $_params = null;
	protected $_clauses = null;
	protected $_response = null;
	
	/**
	 * Fatal error: Class ... contains 1 abstract method and must therefore be declared abstract or implement the remaining methods (Zend_Rest_Controller::headAction) in /Users/marka/Documents/htdocs/mediamanage_v3/application/modules/rest/controllers/MemberController.php on line 3
	 */
	public function headAction()
	{
	}
	
	public function init()
	{
		$this->_helper->layout->disableLayout();
		Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
		$this->_response->setHeader("content-type", "application/json");

		/* start basic auth */
		$auth = Zend_Auth::getInstance();
		$config = array(
	        'accept_schemes' => 'basic',
	        'realm'          => 'interact',
	        'nonce_timeout'  => 3600,
		);
		$adapter = new Zend_Auth_Adapter_Http($config);
		$basicResolver = new Zend_Auth_Adapter_Http_Resolver_File();
		$basicResolver->setFile(realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'basicPasswd.txt'));
		$adapter->setBasicResolver($basicResolver);
		$adapter->setRequest($this->getRequest());
		$adapter->setResponse(new Zend_Controller_Response_Http());
		
		$result = $adapter->authenticate();
		if (!$result->isValid()) {
			/// set 401 response
			$response = Admin_Api::errorMsg("Unauthorised", 401);
			$this->getResponse()->setHttpResponseCode(401);
			$this->_helper->json($response);
		}
		/* end basic auth */
				
		// assign params (json data is contained within rawBody)
		$request = $this->getRequest();
		$params = $request->getParams();

		/*
		if (isset($params['filter'])) {
			$this->_filter = $params['filter'];
			unset($params['filter']);
		}
		*/
		$action = $params['action'];
		unset($params['module'], $params['controller'], $params['action'], $params['masterModel']);
		$contentType = $request->getHeader('Content-Type');
		$rawBody = $request->getRawBody();
		
		// gather database-related clauses
		if ($action != "post") { // disable clauses for post action
			$this->_clauses = array();
			foreach ($params as $key => $value) {
				$clauses = ['limit', 'order', 'in'];
				if (in_array($key, $clauses)) {
					$this->_clauses[$key] = $value;
					unset($params[$key]);
				}
			}
		}
		
		switch (true) {
			case (strstr($contentType, 'application/json')):
				$this->_params = ($rawBody) ? array_merge($params, Zend_Json::decode($rawBody)) : $params;
				break;
			default:
				$this->_params = $params;
		}
	}
	
	/**
	 * Sends the response as JSON
	 * 
	 * @param int $code. HTTP Status code
	 * @param (string|array) $response. The data to send as json
	 */
	public function send($response, $code = null) {
		if ($code) $this->getResponse()->setHttpResponseCode($code);
		$this->_helper->json($response);
	}
	
}