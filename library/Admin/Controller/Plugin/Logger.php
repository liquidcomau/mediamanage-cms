<?php
/**
 * System Logger plugin.
 * 
 * @author     Jon Trumbull <jon@jtwebspace.com>
 * @link       http://www.jtwebspace.com/
 */

class Admin_Controller_Plugin_Logger extends Zend_Controller_Plugin_Abstract
{
    /**
     * The secret key (cookie name) for production screen logging.
     * 
     * @var string
     */
    private static $_secretKey = 'debug';
        
    /**
     * The secret value (cookie value) for production screen logging.
     * 
     * @var string
     */
    private static $_secretValue = 'froth';    
    
    /**
     * In production, if this is true output will be dumped to screen.
     * 
     * @var boolean
     */
    private $_debugMode;
        
    /**
     * The logger.
     * 
     * @var Zend_Log
     */
    private $_logger;

    /**
     * The Mock logger object.
     *
     * @var Zend_Log_Writer_Mock
     */
    private $_writerMock;

    /**
     * The file logger object.
     *
     * @var Zend_Log_Writer_Stream
     */
    private $_writerStream;
        
    /**
     * The Syslog logger object.
     *
     * @var Admin_Log_Writer_Syslog
     */
    private $_writerSyslog;

    /**
     * The location for the log file for the stream.
     * 
     * @var string
     */
    private $_writerStreamLocation; 

    /**
     * Default logging path.
     * 
     * Default container for log files.
     * 
     * @var string
     */
    private $_logPath;
    
    /**
     * Called before Zend_Controller_Front begins evaluating the request against its routes.
     *
     * @param Zend_Controller_Request_Abstract $request Request object.
     * @return void
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {   
    	$config = Admin_Util::getGlobalConfig();

        // Create Logger
        $this->_logger = new Zend_Log();

        // Syslog writer
        $this->_writerSyslog = new Admin_Log_Writer_Syslog();
        $this->_logger->addWriter($this->_writerSyslog);
        // Log file location
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();    
        $path = (isset($config->log) && isset($config->log->path)) ? $config->log->path : 'C:/wamp/logs';    
        $this->_writerStreamLocation = $path . '/' . preg_replace("/^www\\./", "", $request->getServer('HTTP_HOST')) . '-' . date('o-m-d') . '.log';                
        
        // File writer
        $this->_writerStream = new Zend_Log_Writer_Stream($this->_writerStreamLocation, 'a+');
        $this->_logger->addWriter($this->_writerStream);        
        
        // cross project log
        //$writerStreamLocation = MEDIAMANAGE_ENV . '/logs/mediamanage-' . date('o-m-d') . '.log';
        //$writerStreamShared = new Zend_Log_Writer_Stream($writerStreamLocation, 'a+');
        //$this->_logger->addWriter($writerStreamShared);

        // Check debug mode in cookie               
        $this->_debugMode = self::isDebugModeEnabled();
        
        // Turn on error reporting if in debug mode
        if ($this->_debugMode) {
            // Turn off caching in this mode else we end up caching debugging
            // messages for our customers :(
        	Admin_Util::cancelOutputCaching();
        	Admin_Util::cancelStaticCaching();
            
	        error_reporting(E_ALL | E_STRICT);
            ini_set('display_startup_errors', 1);
            ini_set('display_errors', 1);
            Zend_Controller_Front::getInstance()->throwExceptions(true);                        
            $this->_logger->debug("Debugging enabled");                       
        }        
        
        
        // Only log INFO events and above in production
        // Do NOT log to screen unless in debug mode
        if (APPLICATION_ENV == "production" && !$this->_debugMode) {
            $filter = new Zend_Log_Filter_Priority(Zend_Log::INFO);
            $this->_logger->addFilter($filter);
        } else if (strstr(APPLICATION_ENV, "development") || APPLICATION_ENV == "stage" || $this->_debugMode) {  
        	// For the screen in development or debug mode
            $this->_writerMock = new Zend_Log_Writer_Mock();
            $this->_logger->addWriter($this->_writerMock);
            
            // Log to the Firebug console too
            $this->_logger->addWriter(new Zend_Log_Writer_Firebug());
        }
        
        Zend_Registry::set('logger', $this->_logger);
    }

    /**
     * Get the current logger object.
     * 
     * @return Zend_Log $logger The current logger.
     */
    public function getLogger()
    {
        return $this->_logger;
    }
    
    /**
     * Check if debug mode has been enabled in the current environment.
     *
     * @return boolean $debugEnabled Returns true if the cookie/secret key combination match.
     */
    public static function isDebugModeEnabled()
    {       
        // Get the request object
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $secretKey = $request->getCookie(self::$_secretKey);
        
        if (self::$_secretValue == $secretKey) {
            return true;
        }
                 
        return false;
    }
}