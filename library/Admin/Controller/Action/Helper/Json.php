<?php
class Admin_Controller_Action_Helper_Json extends Zend_Controller_Action_Helper_Json
{
    /**
     * Custon override of default Zend behaviour. Sensitive data is now obscured before adding to response.
     *
     * Allows encoding JSON. If $sendNow is true, immediately sends JSON
     * response.
     *
     * @param  mixed   $data
     * @param  boolean $sendNow
     * @param  boolean $keepLayouts
     * @return string|void
     */
    public function direct($data, $sendNow = true, $keepLayouts = false)
    {
    	if ($sendNow) {
    		if (!is_array($data)) return $this->sendJson($data);
    		return $this->sendJson(Admin_Util::obscureArrayData($data), $keepLayouts);
        }
        
        if (!is_array($data)) return $this->encodeJson($data);
        return $this->encodeJson(Admin_Util::obscureArrayData($data), $keepLayouts);
    }
}
