<?php
class Admin_Controller_Action_Helper_Seo extends Zend_Controller_Action_Helper_Abstract
{
	public function direct(Admin_Model_Seo $seo, array $fallbacks = [])
	{
		$view = $this->getActionController()->view;
		
		if ($seo->getTitle()) {
			$view->headTitle($seo->getTitle(), "PREPEND");
		} elseif (isset($fallbacks["title"])) {
			$view->headTitle($fallbacks["title"], "PREPEND");
		}
		
		if ($seo->getMetaDescription()) {
			$view->headMeta()->setName('description', $seo->getMetaDescription());
		} elseif (isset($fallbacks["description"])) {
			$view->headMeta()->setName("description", $fallbacks["description"]);
		}
		
		if ($seo->getRobots()) {
			$view->headMeta()->setName("robots", $seo->getRobots());
		} elseif (isset($fallbacks["robots"])) {
			$view->headMeta()->setName("robots", $fallbacks["robots"]);
		}
		
		if ($seo->getGoalTracking()) {
			$view->goalTracking = $seo->getGoalTracking();
		}
	}
}