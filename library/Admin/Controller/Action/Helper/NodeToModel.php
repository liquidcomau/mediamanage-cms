<?php
class Admin_Controller_Action_Helper_NodeToModel extends Zend_Controller_Action_Helper_Abstract
{
    public function direct($node)
    {
		if (!is_array($node)) {
			$node = $node->toArray();
		}
		
		if (!$node['objectName']) throw new Exception("Node has no object name");
		
    	switch ($node['objectName']) {
			default:
				$class = 'Admin_Model_' . ucfirst(Zend_Filter::filterStatic($node['objectName'], 'Word_UnderscoreToCamelCase'));
		}
		return $class::get($node['id']);
	}
}
