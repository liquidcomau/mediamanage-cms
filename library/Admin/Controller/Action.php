<?php
/**
 * Admin Top-Level Controller.
 * 
 * PHP Version 5 
 * 
 * @version    CVS: $Id
 * @package    Project
 * @subpackage Project_Controller
 * @author     Jon Trumbull <jon@jtwebspace.com>
 * @link       http://www.jtwebspace.com/
 */
class Admin_Controller_Action extends Zend_Controller_Action
{
	/**
     * Global logger object.
     *
     * @var Zend_Log
     */
    protected $_logger;
	
    /**
     * Initialize object.
     *
     * @return void
     */
    public function init()
    {
    	if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/admin/login/index');
        } else if (Zend_Auth::getInstance()->getIdentity()->role == Admin_Auth_Acl::ROLE_MEMBER) {
        	$this->_redirect('/');
        } else {
        	$this->view->identity = Zend_Auth::getInstance()->getIdentity();
        }

    	try {
            $this->_logger = Zend_Registry::get(Admin_Constants::REGISTRY_LOGGER);
        } catch (Zend_Exception $e) {
            $this->_logger = new Zend_Log_Writer_Mock();
        }
        $this->view->layout()->setLayout('admin/admin');     
    }
    
    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * @return void
     */
    public function preDispatch()
    { 	
    	$this->view->headScript()->appendFile('/js/admin/jquery.js');
    	$this->view->headScript()->appendFile('/theme/bootstrap/dist/js/bootstrap.min.js');

    	/* browser js */
    	$this->view->headScript()->appendFile('/theme/js/ie/html5shiv.js', 'text/javascript', array('conditional' => 'lt IE 9'));
    	$this->view->headScript()->appendFile('/theme/js/ie/respond.min.js', 'text/javascript', array('conditional' => 'lt IE 9'));
    	$this->view->headScript()->appendFile('/theme/js/ie/excanvas.js', 'text/javascript', array('conditional' => 'lt IE 9'));
    	$this->view->headScript()->appendFile('/admin/polyfills.js', 'text/javascript', array('conditional' => 'lt IE 9'));    	

    	$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
    	
    	/* theme css */
    	$this->view->headLink()->appendStylesheet('/css/admin/style.css');
    	$this->view->headLink()->appendStylesheet('/theme/css/animate.css');  
    	$this->view->headLink()->appendStylesheet('/theme/css/font-awesome.min.css');  
    	$this->view->headLink()->appendStylesheet('/theme/css/font.css');  
    	
    	$this->view->headScript()->appendFile('/js/admin/extends.js');
    	$this->view->headScript()->appendFile('/js/admin/admin.js');
    	
    	// Notifications
    	
    	$this->view->headLink()->appendStylesheet('/js/admin/mmNotify/mmNotify.css');
    	$this->view->headScript()->appendFile('/js/admin/mmNotify/mmNotify.js');
    	
    	//$this->view->headScript()->appendFile('/js/moxiemanager/js/moxman.loader.min.js');
    	
    	/* initialise navigation */
    	$this->_initNavigation();
    }
    
    /**
	 * Initialize navigation.
	 * 
     * @return void
	 */
	protected function _initNavigation()
	{   
		// populate nav container from json array 
		$jsonArray = Admin_Navigation::jsonNavArray();
		$config = new Zend_Config_Json($jsonArray);
		$navigation = new Zend_Navigation($config);

		// add navigation to the view
		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$view->navigation($navigation);
		
		//set acl and role
		$acl = new Admin_Auth_Acl();
		$role = (Zend_Auth::getInstance()->hasIdentity()) ? Zend_Auth::getInstance()->getIdentity()->role : Admin_Auth_Acl::ROLE_PUBLIC;

		//$role = Admin_Auth_Acl::ROLE_MODERATOR;		
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($role);
	}
}
