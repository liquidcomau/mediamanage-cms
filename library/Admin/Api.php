<?php
class Admin_Api
{
	/**
	 * Zend Config.
	 *
	 * @var Zend_Config
	 */
	protected $_config = null;
	
	/**
	 * Zend Log.
	 *
	 * @var Zend_Log
	 */
	protected $_logger = null;
		
	/**
	 * HTTP Client.
	 *
	 * @var Zend_Http_Client
	 */
	protected $_client = null;
	
	/**
	 * Access rule override. For use when the CMS wishes to use the class. 
	 * 
	 * @var bool
	 */
	protected $_accessOverride = false;
	
	

	/**
	 * Constructor.
	 *
	 * @return void
	 */
	public function __construct()
	{
		try {
			$this->_config = Zend_Registry::get(Admin_Constants::CONFIG);
			$this->_logger = Zend_Registry::get(Admin_Constants::REGISTRY_LOGGER);
			
			$this->_client = new Zend_Http_Client(null, array(
				'adapter' => 'Zend_Http_Client_Adapter_Curl',
				'keepalive' => true,
				'timeout' => 30,
				'storeresponse' => false
			));
			$this->_client->setAuth($this->_config->api->username, $this->_config->api->password, Zend_Http_Client::AUTH_BASIC);
		} catch (Zend_Exception $e) {
			$this->_logger->err(print_r($e, true)); // log exception
		}
	}
	
	public function getAccessOverride()
	{
		return $this->_accessOverride;
	}
	
	public function setAccessOverride($b)
	{
		$this->_accessOverride = $b;
	}
	
	/**
	 * Checks to see if user is allowed to access resource 
	 * 
	 * @param string $routeName
	 * @param array $values
	 * @param string $method
	 * @param stdClass|false $user
	 * @return boolean
	 */
	private function accessAllowed($routeName, $values, $method, $user, $filter = null) {
		if ($this->_accessOverride || in_array($user->role, array(Admin_Util::ROLE_MODERATOR, Admin_Util::ROLE_ADMIN, Admin_Util::ROLE_WEBMASTER))) { // moderators have unrestricted access to all resources
			$response = true;		
		} else {
			
			if ($filter === 'all') return false;
			
			switch($routeName) {
				case 'api_tree' :
					$response = false;
					break;
				case 'api_members' :
					switch ($method) {
						case Zend_Http_Client::GET :
						case Zend_Http_Client::PUT : // a member can only fetch and update their own details 
							if ($user) { // member must be logged in
								if (isset($values['memberid'])) { // uri must contain a member id
									if ($values['memberid'] === $user->id) {
										$response = true;
									}
								} 
							}
							break;
						case Zend_Http_Client::POST : // authentication not required to create a member
							$response = true;
							break;
						default :
							$response = false;
					}
					break;
				case 'api_memberAdd' :
					$response = ($user && $user->role !== 'member') ? true : false;
					break;
				case 'api_forum' :
				case 'api_freeform' :
				case 'api_comments' :
				case 'api_forumComments' :
				case 'api_tags' :
				case 'api_categories' :
				case 'api_commentTags' :
				case 'api_votes' :
				case 'api_commentReports' :
				case 'api_layout' :
				case 'api_poll' :
				case 'api_pollVote' :
				case 'api_event' :
				case 'api_collection' :
				case 'api_cta' :
				case 'api_faq' :
				case 'api_news' :
					$response = true;
					break;
				case 'api_siteSettings':
					$response = ($user && $user->role !== 'member') ? true : false;
					break;
				case 'api_survey' :
				case 'api_gallery' :
				default : 
					$response = true;
			}
		}
		return $response;
	}
	
	/**
	 * Returns true|false to indicate if a an object can be cached
	 * @todo: add custom rules for objects that require special handling
	 * 
	 * @param string $routeName
	 * @param string $uri
	 * @return boolean
	 */
	private function isCachable($routeName, $path) {
		if ($this->_config->api->cache) {
			
			/* @todo: move blacklist to config.ini */
			$routeBlacklist = array(); // blacklist routes						
			$pathBlacklist = array('layout?type=tree&id=0'); // blacklist paths						
			
			if (in_array($routeName, $routeBlacklist) || in_array($path, $pathBlacklist)) {
				return false;
			} else {
				return true;
			}
		} 
		return false;
	}
	
	/**
	 * Returns an object name matching the route (route tags)
	 * 
	 * @param string $routeName
	 * @return string
	 */
	private function getRouteObject($routeName) {
		switch($routeName) {
			case 'api_members' :
				$object = "member";
				break;
			case 'api_freeform' :
				$object = "freeform_page";
				break;
			case 'api_tags' :
				$object = "tags";
				break;
			case 'api_votes' :
				$object = "vote";
				break;
			case 'api_comments' :
			case 'api_forumComments' :
			case 'api_commentTags' :
			case 'api_commentReports' :
				$object = "comment";
				break;
			case 'api_pollVote' :
				$object = "poll";
				break;
			case 'api_project' :
				$object = "project_page";
				break;
			default :
				$object = str_replace("api_", "", $routeName); // strip "api_" from the routename
		}
		return $object;
	}
	
	
	/**
	 * Returns an array of tags for caching
	 *
	 * @param string $objectName
	 * @return array
	 */
	private function cacheTags($objectName) {
		
		$tags = array($objectName); // tag by object name and master as default
		
		switch($objectName) {
			case 'cta' :
			case 'freeform_page' :
				$tags[] = "layout"; // clear menu cache
				break;
			case 'comment' :
				$tags[] = "forum";
				$tags[] = 'tags';
				break;
			case 'commentTags':
				$tags[] = 'tags';
				break;
			case "tree":
				$tags[] = "menu";
				break;
		}
		
		return $tags;
	}
	
	/**
	 * Send Request.
	 *
	 * @param string $path Request path.
	 * @param string $method Request method.
	 * @param mixed $data Request data.
     * @return Zend_Http_Response
	 */
	public function request($path, $method, $data = null)
	{
		if ($method !== Zend_Http_Client::GET && Zend_Registry::get("config")->caching->enabled) {
			// clearing page cache always for now
			Admin_Cache::clearDir("page");
		}
		
		$response = null;
		$cache = Zend_Registry::get("cache");
		try {
			// set request path
			$uri = $this->_config->api->host.$path;
			
			$user = (Zend_Auth::getInstance()->hasIdentity()) ? Zend_Auth::getInstance()->getStorage()->read() : false;
			
			// match uri to route name
			$request = new Zend_Controller_Request_Http($uri);
			$router = Zend_Controller_Front::getInstance()->getRouter();
			$router->route($request);
			$routeName = $router->getCurrentRouteName();
			
			if (get_class($router->getCurrentRoute()) === "Admin_Rest_Controller_Route") { // only continue for valid api routes				
				
				// fetch values associated with API route
				$values = $router->getCurrentRoute()->getValues();
				
				$filter = Zend_Controller_Front::getInstance()->getRequest()->getParam('filter');
				
				if ($this->accessAllowed($routeName, $values, $method, $user, $filter)) { // check if user has access to API resource	
					$this->_client->setUri($uri);
					$this->_client->resetParameters(); // clear any existing request parameters

					// set post data
					if (in_array($method, array(Zend_Http_Client::POST, Zend_Http_Client::PUT)) && $data) {
						if ($data instanceof Admin_Api_Abstract) $data = $data->getJsonData($data); // json encode object
						if (!json_decode($data)) throw new Zend_Exception('Admin_Api->request() failed: Request data not JSON format.');
						$this->_client->setRawData($data, 'application/json');
						$dataArray = Zend_Json::decode($data);
					}
					
					// catche retrieval
					if (($method == Zend_Http_Client::GET) && $this->isCachable($routeName, $path)) { 
						$cacheId = $method . preg_replace("/[^a-zA-Z0-9]+/", "", $path);
						
						// response set here for GET
						
						if ($response = $cache->load($cacheId)) {
							$this->_logger->info('API | Cache Retrieval | URI: [' . $uri . '] | User IP: [' . $_SERVER['REMOTE_ADDR'] . '] | Request URI: [' . $_SERVER['REQUEST_URI'] . ']');
						}
					} 
					
					if (!$response) {
						// send API request
						$this->_logger->info('API | Sending Request | Method: [' . $method . '] | URI: [' . $uri . '] | User IP: [' . $_SERVER['REMOTE_ADDR'] . '] | Request URI: [' . $_SERVER['REQUEST_URI'] . '] | Parameters: [' . print_r(json_decode($data), true) . ']');
						$start = microtime(true);
						$response = $this->_client->request($method); 
						$finish = microtime(true);
						$this->_logger->info('API | Received Response | Method: [' . $method . '] | URI: [' . $uri . '] | User IP: [' . $_SERVER['REMOTE_ADDR'] . '] | Time: [' . number_format($finish - $start, 4) . ' seconds] | Response: [' . print_r(json_decode($response->getBody()), true) . ']');
						
						if ($this->isCachable($routeName, $path)) {
							// fetch tags for cach settings & cache clearing
							$routeObject = $this->getRouteObject($routeName);
							$tags = $this->cacheTags($routeObject);
							if ($method == Zend_Http_Client::GET) { // cache response for get requests
								$this->_logger->info('API | Cache Create | TAGS: [' . implode(",", $tags) . ']');
								$cache->save($response, $cacheId, $tags, null);
							} else { // flush cache for create, update and delete requests
								
								if ($routeObject != "collection") {
									
									// collection delete handled within collection class
									
									//clear main menu cache for all updates when page manager is in use
									// if ($this->_config->admin->settings->navigation == "tree") array_push($tags, 'layout');

									if (preg_match('/(?:_page|folder|link|menu)$/', $routeObject)) $tags = array_merge($tags, ['menu', 'tree']);
									
									$this->_logger->info('API | Cache Clean | TAGS: [' . implode(",", $tags) . ']');
									foreach ($tags as $tag) { // clearing each tag individually as bulk clear not working as intended
										$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array($tag));
									}
								}
							}
						}
					}
										
				} else {
					// set unauthorised HTTP response
					$response = new Zend_Http_Response(401, array(), json_encode(self::errorMsg("Unauthorised", 401)));
				}
				
			} else {
				// set 404 HTTP response
				$response = new Zend_Http_Response(401, array(), json_encode(self::errorMsg("Resource not found", 404)));
			}

		} catch (Zend_Exception $e) {
			$this->_logger->err(print_r($e, true)); // log exception
		}
		
		return $response;
	}
	
	public static function cleanRoute($routeName) {
		$cache = Zend_Registry::get("cache");
		$api = new self();
		
		$tags = $api->cacheTags($api->getRouteObject($routeName));
		
		foreach ($tags as $tag) {
			$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array($tag)); // clearing each tag individually as bulk clear not working as intended
		}
	}
	
	public static function get($path, $override = false) {
		$api = new Admin_Api();
		$api->setAccessOverride($override);
		return $api->request($path, Zend_Http_Client::GET);
	}
	
	public static function post($path, $data = null, $override = false) {
		$api = new Admin_Api();
		$api->setAccessOverride($override);
		return $api->request($path, Zend_Http_Client::POST, $data);
	}
	
	public static function put($path, $data = null, $override = false) {
		$api = new Admin_Api();
		$api->setAccessOverride($override);
		return $api->request($path, Zend_Http_Client::PUT, $data);
	}
	
	public static function delete($path, $override = false) {
		$api = new Admin_Api();
		$api->setAccessOverride($override);
		return $api->request($path, Zend_Http_Client::DELETE);
	}
	
	/**
	 * Returns a user presentable error message representing a system generated error
	 * 
	 * @param string $error
	 * @return string
	 */
	protected function mapUserError($e) {
		Admin_Logger::dump($e);

		switch ($e->getCode()) {
			/*
			case '23000' : // SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '<value>' for key '<fieldName>'
				preg_match('/SQLSTATE\[23000\]\: Integrity constraint violation\: 1062 Duplicate entry \'(.*)\' for key \'(.*)\'$/', $e->getMessage(), $matches);
				$userMessage = ucFirst($matches[2]) . " matching \"" . $matches[1] . "\" is already in use"; 
				break;
			 * */
			default: 
//				$userMessage = "";
				$userMessage = $e->getMessage();
		}
		return $userMessage;
	} 
	
	/**
	 * Returns an array of error data obtained from exception
	 * 
	 * @param Exception $e
	 * @return array
	 */
	public static function errorResponse($e) { /* @var $e Exception */
		$response = array(
			'userMessage' => self::mapUserError($e),
			'developerMessage' => $e->getMessage(),
			'errorCode' => $e->getCode()
		);
		return $response;
	}
	
	
	/**
	 * Returns an array of error data from params
	 *
	 * @param string $msg
	 * @param int $code
	 * @return array
	 */
	public static function errorMsg($msg, $code) {
		$response = array(
			'userMessage' => $msg,
			'developerMessage' => $msg,
			'errorCode' => $code
		);
		return $response;
	}
}