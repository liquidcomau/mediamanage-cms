<?php
abstract class Admin_Api_ResourceController extends Admin_Rest_Controller
{
	/**
	 * Resource model class
	 * @var string 
	 */
	protected $_model = null;
	
	public function init()
	{
		parent::init();
		try {
			$class = end(explode('_', get_class($this)));
			$class = Admin_Factory::getClass('Model_' . preg_replace('/Controller$/', '', $class));

			$reflection = new ReflectionClass($class);

			if ($reflection->isSubclassOf('Admin_Model_Resource')) {
				$this->_model = $class;
			} else {
				throw new Exception("Could not find corresponding model of type Admin_Model_Resource");
			}
			
		} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
			$this->send($response);
    	}
	}
	
	public function indexAction()
	{
		$model = $this->_model;
    	try {
    		$response = $model::getAll($this->_params, Admin_Constants::TYPE_ARRAY);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }

	public function getAction()
    {
		$model = $this->_model;
    	try {
    		$response = $model::get($this->_params["id"], Admin_Constants::TYPE_ARRAY);
    		if (!$response) $this->getResponse()->setHttpResponseCode(404);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
	    $this->send($response);
	}
	
	public function postAction()
	{
		$model = $this->_model;
		try {
			$instance = new $model($this->_params);
			$id = $instance->create();
			
			$response = $model::get($id, Admin_Constants::TYPE_ARRAY);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}
	
	public function putAction()
	{
		$model = $this->_model;
		try {
			$instance = new $model($this->_params);
			$instance->update();
			$response = $model::get($this->_params['id']);
		} catch (Exception $e) {
			$response = Admin_Api::errorResponse($e);
			$this->getResponse()->setHttpResponseCode(500);
		}
		$this->send($response);
	}

	public function deleteAction() {
    	$model = $this->_model;
		try {
    		$reponse = $model::deleteById($this->_params['id']);
    	} catch (Exception $e) {
    		$response = Admin_Api::errorResponse($e);
    		$this->getResponse()->setHttpResponseCode(500);
    	}
    	$this->send($response);
    }
}