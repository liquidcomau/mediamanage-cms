<?php
class Admin_Api_Abstract
{
	/**
	 * Class constructor.
	 *
	 * @param array $data Object data.
	 */
	public function __construct(array $data = array())
	{
		if (!empty($data)) {
			$this->setValues($data);
		}
	}
		
	/**
	 * Generic function to serialise nested php objects to JSON. 
	 * 
	 * @param object $obj Object to serialise
	 * @param int $depth Depth of recursion
	 * @return array
	 */
	public function getJsonData($obj = false, $depth = 0) {
		$obj = (!$obj) ? $this : $obj;
		$objArray = array();
		foreach ($obj as $key => $data) {
			if ($data !== null) {
				if (is_array($data)) {
					foreach ($data as $val) {
						if (is_object($val) && method_exists($val,'getJsonData')){
							$objArray[str_replace("_", "", $key)][] = $val->getJsonData($val, $depth + 1);
						}
					} 
				} else {
					$objArray[str_replace("_", "", $key)] = $data;
				}
			}
		}
		if ($depth == 0) {
			return json_encode($objArray);
		} else {
			return $objArray;
		}		
	}
	
	/**
	 * Dynamically set object fields from array
	 *
	 * @param array $fields
	 */
	public function setValues($fields) {
		foreach ($fields as $key => $value) {
			$setter = "set" . ucfirst($key);
			if (method_exists($this,$setter)) {
				$this->$setter($value);
			}
		}
	}	
}