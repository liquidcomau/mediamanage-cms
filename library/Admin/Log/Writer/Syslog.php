<?php
/**
 * System Log Writer.
 * 
 * Logs operations using the native system logger. All websites
 * should use this logger as other systems can hook into this easily, enabling
 * a central logging system and subsequently a central notification
 * mechanism. 
 */
class Admin_Log_Writer_Syslog extends Zend_Log_Writer_Abstract
{
    /**
     * This is the log facility to send logs to.
     *
     * @var string
     */
    const LOG_FACILITY = LOG_LOCAL2;
    
    /**
     * Construct the log writer.
     * 
     * Opens the syslog connection.
     */
    public function __construct()
    {
        openlog('', LOG_NDELAY, self::LOG_FACILITY);        
    }
    
    /**
     * Log a message to this writer.
     *
     * @param array|mixed $event Log data event.
     * @return void
     */    
    protected function _write($event)
    {
        $priority = $event['priority'];
        if (is_array($event['message'])) {
            $message = print_r($event['message'], true);
        } else if (!is_string($event['message'])) {
            $message = (string) $event['message'];
        } else {
            $message = $event['message'];
        }

        syslog($priority, $message);               
    }
    
    /**
     * Create a new instance of MIT_Log_Writer_Syslog.
     *
     * @param array|Zend_Config $config Config file.
     * @return Zend_Log_Writer_Syslog
     * @throws Zend_Log_Exception When config is invalid, Zend_Exception is thrown.
     */
    static public function factory($config)
    {
        return new self(self::_parseConfig($config));
    }
    
    
    /**
     * Destructor.
     *
     * Closes connections etc.
     */
    public function __destruct()
    {
        closelog();       
    }
}