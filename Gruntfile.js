module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		less: {
			mediamanage: {
				options: {
					paths: ['public/less/admin']
				},
				files: {
					'public/css/admin/style.css': 'public/less/admin/style.less'
				}
			},
		},
		
		notify: {
			watch: {
				options: {
					title: 'Task Complete',
					message: 'Compiled LESS'
				}
			},
		},

		watch: {
			less: {
				files: ['public/less/admin/**/*.less'],
				tasks: ['less', 'notify'],
				options: {
					livereload: true,
				}
			}
		},

		concat: {
		
			theme_bootstrap: {
				src: [
				  'public/theme/bootstrap/js/transition.js',
				  'public/theme/bootstrap/js/alert.js',
				  'public/theme/bootstrap/js/button.js',
				  'public/theme/bootstrap/js/carousel.js',
				  'public/theme/bootstrap/js/collapse.js',
				  'public/theme/bootstrap/js/dropdown.js',
				  'public/theme/bootstrap/js/modal.js',
				  'public/theme/bootstrap/js/tooltip.js',
				  'public/theme/bootstrap/js/popover.js',
				  'public/theme/bootstrap/js/scrollspy.js',
				  'public/theme/bootstrap/js/tab.js',
				  'public/theme/bootstrap/js/affix.js'
				],
				dest: 'public/theme/bootstrap/dist/js/bootstrap.js'
			}
		},

		uglify: {
			theme_bootstrap: {
		        src: [
		          'public/theme/bootstrap/dist/js/bootstrap.js'
		        ],
		        dest: 'public/theme/bootstrap/dist/js/bootstrap.min.js'			
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-notify');

	grunt.registerTask('default', ['watch']);
	grunt.registerTask('theme_bootstrap_js', ['concat:theme_bootstrap', 'uglify:theme_bootstrap']);
};
